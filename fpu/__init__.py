"""
=========
fpu
=========

fpu model template The System Development Kit
Used as a template for all TheSyDeKick Entities.

Current docstring documentation style is Numpy
https://numpydoc.readthedocs.io/en/latest/format.html

This text here is to remind you that documentation is important.
However, youu may find it out the even the documentation of this 
entity may be outdated and incomplete. Regardless of that, every day 
and in every way we are getting better and better :).

Initially written by Marko Kosunen, marko.kosunen@aalto.fi, 2017.

"""

import os
import sys
if not (os.path.abspath('../../thesdk') in sys.path):
    sys.path.append(os.path.abspath('../../thesdk'))

from thesdk import IO, thesdk
from rtl import rtl, rtl_iofile

import numpy as np

class fpu(rtl, thesdk):

    def __init__(self,*arg): 
        self.print_log(type='I', msg='Inititalizing %s' %(__name__)) 
        self.proplist = [ 'Rs' ];    # Properties that can be propagated from parent
        self.Rs =  100e6;            # Sampling frequency
        self.model='py';             # Can be set externally, but is not propagated
        self.par= False              # By default, no parallel processing
        self.queue= []               # By default, no parallel processing
        self.op_dict = { # Dictionary to convert operation string to int
            "FADD_S": 0,
            "FSUB_S": 1,
            "FMUL_S": 2,
            "FDIV_S": 3,
            "FSQRT_S": 4,
            "FMIN_S": 5,
            "FMAX_S": 6,
            "FMADD_S": 7,
            "FMSUB_S": 8,
            "FNMADD_S": 9,
            "FNMSUB_S": 10,
            "FCMP_FEQ_S": 11,
            "FCMP_FLT_S": 12,
            "FCMP_FLE_S": 13,
            "FCVT_W_S": 14,
            "FCVT_S_W": 15,
            "FCVT_WU_S": 16,
            "FCVT_S_WU": 17,
            "FSGNJ_S": 18,
            "FSGNJN_S": 19,
            "FSGNJX_S": 20,
            "FCLASS_S": 21,
            "NULL": 22
                    }
        self.vlogext = ".v"

        # IO definitions

        # Inputs
        self.IOS.Members['io_in_valid'] = IO()
        self.IOS.Members['io_in_bits_rs1'] = IO()
        self.IOS.Members['io_in_bits_rs2'] = IO()
        self.IOS.Members['io_in_bits_rs3'] = IO()
        self.IOS.Members['io_in_bits_op'] = IO()
        self.IOS.Members['io_in_bits_fcsr'] = IO()
        self.IOS.Members['io_out_ready'] = IO()

        # Outputs
        self.IOS.Members['io_in_ready'] = IO()
        self.IOS.Members['io_out_valid'] = IO()
        self.IOS.Members['io_out_bits_rd'] = IO()
        self.IOS.Members['io_out_bits_fcsr'] = IO()
        
        if len(arg) >= 1:
            parent = arg[0]
            self.copy_propval(parent,self.proplist)
            self.parent = parent

        self.init()

    def init(self):
        pass #Currently nohing to add

    def define_io_conditions(self):
        # Set io condition to initdone for all signals by default
        # i.e. it starts writing/reading values to/from files after reset
        for key in self.iofile_bundle.Members.keys():
            self.iofile_bundle.Members[key].rtl_io_condition = 'initdone'
        # Feed new data only if FPU is ready
        self.iofile_bundle.Members["io_in_bits_rs1"].rtl_io_condition = 'initdone && io_in_ready && io_in_valid'
        self.iofile_bundle.Members["io_in_bits_rs2"].rtl_io_condition = 'initdone && io_in_ready && io_in_valid'
        self.iofile_bundle.Members["io_in_bits_rs3"].rtl_io_condition = 'initdone && io_in_ready && io_in_valid'
        self.iofile_bundle.Members["io_in_bits_op"].rtl_io_condition = 'initdone && io_in_ready && io_in_valid'
        self.iofile_bundle.Members["io_in_bits_fcsr"].rtl_io_condition = 'initdone && io_in_ready && io_in_valid'
        self.iofile_bundle.Members["io_in_valid"].rtl_io_condition = 'initdone && io_in_ready'
        #self.iofile_bundle.Members["io_out_ready"].rtl_io_condition = 'initdone && io_in_valid'  # added now
        # Log outputs only with valid data
        self.iofile_bundle.Members["io_out_bits_rd"].rtl_io_condition = 'io_out_valid && io_out_ready'
        self.iofile_bundle.Members["io_out_bits_fcsr"].rtl_io_condition = 'io_out_valid && io_out_ready'

    def main(self):
        '''No Python model.
        '''
        self.print_log(type='E', msg='No python model!')

    def convert_ops(self):
        new_arr = np.empty(self.IOS.Members["io_in_bits_op"].Data.size)
        for i, sample in enumerate(self.IOS.Members["io_in_bits_op"].Data):
            new_arr[i] = self.op_dict[sample[0]]
        self.IOS.Members["io_in_bits_op"].Data = new_arr.reshape(-1,1)

    def run(self,*arg):
        '''Guideline: Define model depencies of executions in `run` method.

        '''

        if self.model != 'sv':
            self.print_log(type="F", msg="Only sv model supported!")

        self.convert_ops()

        _=rtl_iofile(self, name='io_in_valid', dir='in', iotype='sample', datatype='int', ionames=['io_in_valid'])
        _=rtl_iofile(self, name='io_in_bits_rs1', dir='in', iotype='sample', datatype='int', ionames=['io_in_bits_rs1'])
        _=rtl_iofile(self, name='io_in_bits_rs2', dir='in', iotype='sample', datatype='int', ionames=['io_in_bits_rs2'])
        _=rtl_iofile(self, name='io_in_bits_rs3', dir='in', iotype='sample', datatype='int', ionames=['io_in_bits_rs3'])
        _=rtl_iofile(self, name='io_in_bits_op', dir='in', iotype='sample', datatype='int', ionames=['io_in_bits_op'])
        _=rtl_iofile(self, name='io_in_bits_fcsr', dir='in', iotype='sample', datatype='int', ionames=['io_in_bits_fcsr'])
        _=rtl_iofile(self, name='io_out_ready', dir='in', iotype='sample', datatype='int', ionames=['io_out_ready'])

        _=rtl_iofile(self, name='io_out_bits_rd', dir='out', iotype='sample', datatype='int', ionames=['io_out_bits_rd'])
        _=rtl_iofile(self, name='io_out_bits_fcsr', dir='out', iotype='sample', datatype='int', ionames=['io_out_bits_fcsr'])

        self.rtlparameters=dict([ ('g_Rs', (float, self.Rs)), ])

        if len(arg)>0:
            self.par=True      #flag for parallel processing
            self.queue=arg[0]  #multiprocessing.queue as the first argument
        if self.model=='sv':
            self.run_rtl()

if __name__=="__main__":
    import argparse
    import matplotlib.pyplot as plt
    from  fpu import *
    from  fpu.controller import controller as fpu_controller
    import pdb
    import math
    # Implement argument parser
    parser = argparse.ArgumentParser(description='Parse selectors')
    parser.add_argument('--show', dest='show', type=bool, nargs='?', const = True, 
            default=False,help='Show figures on screen')
    args=parser.parse_args()

    length=1024
    rs=100e6
    indata=000000

    models=[ 'sv']
    duts=[]
    plotters=[]
    for model in models:
        d=fpu()
        duts.append(d) 
        d.model=model
        d.Rs=rs
        d.IOS.Members['A'].Data=indata
        d.init()
        d.run()

    for k in range(len(duts)):
        hfont = {'fontname':'Sans'}
        figure,axes=plt.subplots(2,1,sharex=True)
        x = np.arange(length).reshape(-1,1)
        axes[0].plot(x,indata)
        axes[0].set_ylim(-1.1, 1.1);
        axes[0].set_xlim((np.amin(x), np.amax(x)));
        axes[0].set_ylabel('Input', **hfont,fontsize=18);
        axes[0].grid(True)
        axes[1].plot(x, duts[k].IOS.Members['Z'].Data)
        axes[1].set_ylim(-1.1, 1.1);
        axes[1].set_xlim((np.amin(x), np.amax(x)));
        axes[1].set_ylabel('Output', **hfont,fontsize=18);
        axes[1].set_xlabel('Sample (n)', **hfont,fontsize=18);
        axes[1].grid(True)
        titlestr = "Myentity model %s" %(duts[k].model) 
        plt.suptitle(titlestr,fontsize=20);
        plt.grid(True);
        printstr="./inv_%s.eps" %(duts[k].model)
        plt.show(block=False);
        figure.savefig(printstr, format='eps', dpi=300);
    #This is here to keep the images visible
    #For batch execution, you should comment the following line 
    if args.show:
       input()
    #This is to have exit status for succesfuulexecution
    sys.exit(0)

