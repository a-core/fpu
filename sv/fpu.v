module FPUPreprocessor(
  output        io_in_ready,
  input         io_in_valid,
  input  [31:0] io_in_bits_rs1,
  input  [31:0] io_in_bits_rs2,
  input  [31:0] io_in_bits_rs3,
  input  [4:0]  io_in_bits_op,
  input         io_in_bits_fcsr_0,
  input         io_in_bits_fcsr_1,
  input         io_in_bits_fcsr_2,
  input         io_in_bits_fcsr_3,
  input         io_in_bits_fcsr_4,
  input         io_in_bits_fcsr_5,
  input         io_in_bits_fcsr_6,
  input         io_in_bits_fcsr_7,
  input         io_out_ready,
  output        io_out_valid,
  output [25:0] io_out_bits_a_mantissa,
  output [7:0]  io_out_bits_a_exponent,
  output        io_out_bits_a_sign,
  output [25:0] io_out_bits_b_mantissa,
  output [7:0]  io_out_bits_b_exponent,
  output        io_out_bits_b_sign,
  output [25:0] io_out_bits_c_mantissa,
  output [7:0]  io_out_bits_c_exponent,
  output        io_out_bits_c_sign,
  output [4:0]  io_out_bits_op,
  output        io_out_bits_fcsr_0,
  output        io_out_bits_fcsr_1,
  output        io_out_bits_fcsr_2,
  output        io_out_bits_fcsr_3,
  output        io_out_bits_fcsr_4,
  output        io_out_bits_fcsr_5,
  output        io_out_bits_fcsr_6,
  output        io_out_bits_fcsr_7,
  output [11:0] io_out_bits_fflags,
  output        io_out_bits_control_adder,
  output        io_out_bits_control_multiplier,
  output        io_out_bits_control_fused,
  output        io_out_bits_control_divider,
  output        io_out_bits_control_comparator,
  output        io_out_bits_control_squareRoot,
  output        io_out_bits_control_bitInject,
  output        io_out_bits_control_classify,
  output        io_out_bits_stall_stall
);
  wire  _control_adder_T_1 = io_in_bits_op == 5'h1; // @[FPUPreprocessor.scala 76:40]
  wire  control_multiplier = io_in_bits_op == 5'h2; // @[FPUPreprocessor.scala 78:40]
  wire  _control_fused_T_1 = io_in_bits_op == 5'h8; // @[FPUPreprocessor.scala 81:40]
  wire  _control_fused_T_2 = io_in_bits_op == 5'h7 | _control_fused_T_1; // @[FPUPreprocessor.scala 80:63]
  wire  _control_fused_T_3 = io_in_bits_op == 5'h9; // @[FPUPreprocessor.scala 82:40]
  wire  _control_fused_T_4 = _control_fused_T_2 | _control_fused_T_3; // @[FPUPreprocessor.scala 81:63]
  wire  _control_fused_T_5 = io_in_bits_op == 5'ha; // @[FPUPreprocessor.scala 83:40]
  wire  control_fused = _control_fused_T_4 | _control_fused_T_5; // @[FPUPreprocessor.scala 82:64]
  wire  control_divider = io_in_bits_op == 5'h3; // @[FPUPreprocessor.scala 85:40]
  wire  _control_comparator_T_1 = io_in_bits_op == 5'hc; // @[FPUPreprocessor.scala 88:40]
  wire  _control_comparator_T_2 = io_in_bits_op == 5'hb | _control_comparator_T_1; // @[FPUPreprocessor.scala 87:66]
  wire  _control_comparator_T_3 = io_in_bits_op == 5'hd; // @[FPUPreprocessor.scala 89:40]
  wire  _control_comparator_T_4 = _control_comparator_T_2 | _control_comparator_T_3; // @[FPUPreprocessor.scala 88:66]
  wire  _control_comparator_T_5 = io_in_bits_op == 5'h5; // @[FPUPreprocessor.scala 90:40]
  wire  _control_comparator_T_6 = _control_comparator_T_4 | _control_comparator_T_5; // @[FPUPreprocessor.scala 89:66]
  wire  _control_comparator_T_7 = io_in_bits_op == 5'h6; // @[FPUPreprocessor.scala 91:40]
  wire  control_squareRoot = io_in_bits_op == 5'h4; // @[FPUPreprocessor.scala 93:40]
  wire  _control_bitInject_T_1 = io_in_bits_op == 5'h13; // @[FPUPreprocessor.scala 96:40]
  wire  _control_bitInject_T_2 = io_in_bits_op == 5'h12 | _control_bitInject_T_1; // @[FPUPreprocessor.scala 95:63]
  wire  _control_bitInject_T_3 = io_in_bits_op == 5'h14; // @[FPUPreprocessor.scala 97:40]
  wire  _convert_T = io_in_bits_op == 5'hf; // @[FPUPreprocessor.scala 102:32]
  wire  _convert_T_1 = io_in_bits_op == 5'h11; // @[FPUPreprocessor.scala 103:32]
  wire  convert = io_in_bits_op == 5'hf | _convert_T_1; // @[FPUPreprocessor.scala 102:56]
  wire  _T_2 = io_in_bits_rs1 == 32'h80000000; // @[FPUPreprocessor.scala 124:26]
  wire  _T_3 = _convert_T & _T_2; // @[FPUPreprocessor.scala 123:52]
  wire [30:0] _intA_T_2 = ~io_in_bits_rs1[30:0]; // @[FPUPreprocessor.scala 131:36]
  wire [30:0] _intA_T_4 = _intA_T_2 + 31'h1; // @[FPUPreprocessor.scala 131:53]
  wire [30:0] _intA_T_6 = io_in_bits_rs1[31] ? _intA_T_4 : io_in_bits_rs1[30:0]; // @[FPUPreprocessor.scala 131:20]
  wire [31:0] intA = _convert_T ? {{1'd0}, _intA_T_6} : io_in_bits_rs1; // @[FPUPreprocessor.scala 130:46 131:14 134:14]
  wire [15:0] _GEN_22 = {{8'd0}, intA[15:8]}; // @[Bitwise.scala 108:31]
  wire [15:0] _biasCount_T_5 = _GEN_22 & 16'hff; // @[Bitwise.scala 108:31]
  wire [15:0] _biasCount_T_7 = {intA[7:0], 8'h0}; // @[Bitwise.scala 108:70]
  wire [15:0] _biasCount_T_9 = _biasCount_T_7 & 16'hff00; // @[Bitwise.scala 108:80]
  wire [15:0] _biasCount_T_10 = _biasCount_T_5 | _biasCount_T_9; // @[Bitwise.scala 108:39]
  wire [15:0] _GEN_23 = {{4'd0}, _biasCount_T_10[15:4]}; // @[Bitwise.scala 108:31]
  wire [15:0] _biasCount_T_15 = _GEN_23 & 16'hf0f; // @[Bitwise.scala 108:31]
  wire [15:0] _biasCount_T_17 = {_biasCount_T_10[11:0], 4'h0}; // @[Bitwise.scala 108:70]
  wire [15:0] _biasCount_T_19 = _biasCount_T_17 & 16'hf0f0; // @[Bitwise.scala 108:80]
  wire [15:0] _biasCount_T_20 = _biasCount_T_15 | _biasCount_T_19; // @[Bitwise.scala 108:39]
  wire [15:0] _GEN_24 = {{2'd0}, _biasCount_T_20[15:2]}; // @[Bitwise.scala 108:31]
  wire [15:0] _biasCount_T_25 = _GEN_24 & 16'h3333; // @[Bitwise.scala 108:31]
  wire [15:0] _biasCount_T_27 = {_biasCount_T_20[13:0], 2'h0}; // @[Bitwise.scala 108:70]
  wire [15:0] _biasCount_T_29 = _biasCount_T_27 & 16'hcccc; // @[Bitwise.scala 108:80]
  wire [15:0] _biasCount_T_30 = _biasCount_T_25 | _biasCount_T_29; // @[Bitwise.scala 108:39]
  wire [15:0] _GEN_25 = {{1'd0}, _biasCount_T_30[15:1]}; // @[Bitwise.scala 108:31]
  wire [15:0] _biasCount_T_35 = _GEN_25 & 16'h5555; // @[Bitwise.scala 108:31]
  wire [15:0] _biasCount_T_37 = {_biasCount_T_30[14:0], 1'h0}; // @[Bitwise.scala 108:70]
  wire [15:0] _biasCount_T_39 = _biasCount_T_37 & 16'haaaa; // @[Bitwise.scala 108:80]
  wire [15:0] _biasCount_T_40 = _biasCount_T_35 | _biasCount_T_39; // @[Bitwise.scala 108:39]
  wire [7:0] _GEN_26 = {{4'd0}, intA[23:20]}; // @[Bitwise.scala 108:31]
  wire [7:0] _biasCount_T_46 = _GEN_26 & 8'hf; // @[Bitwise.scala 108:31]
  wire [7:0] _biasCount_T_48 = {intA[19:16], 4'h0}; // @[Bitwise.scala 108:70]
  wire [7:0] _biasCount_T_50 = _biasCount_T_48 & 8'hf0; // @[Bitwise.scala 108:80]
  wire [7:0] _biasCount_T_51 = _biasCount_T_46 | _biasCount_T_50; // @[Bitwise.scala 108:39]
  wire [7:0] _GEN_27 = {{2'd0}, _biasCount_T_51[7:2]}; // @[Bitwise.scala 108:31]
  wire [7:0] _biasCount_T_56 = _GEN_27 & 8'h33; // @[Bitwise.scala 108:31]
  wire [7:0] _biasCount_T_58 = {_biasCount_T_51[5:0], 2'h0}; // @[Bitwise.scala 108:70]
  wire [7:0] _biasCount_T_60 = _biasCount_T_58 & 8'hcc; // @[Bitwise.scala 108:80]
  wire [7:0] _biasCount_T_61 = _biasCount_T_56 | _biasCount_T_60; // @[Bitwise.scala 108:39]
  wire [7:0] _GEN_28 = {{1'd0}, _biasCount_T_61[7:1]}; // @[Bitwise.scala 108:31]
  wire [7:0] _biasCount_T_66 = _GEN_28 & 8'h55; // @[Bitwise.scala 108:31]
  wire [7:0] _biasCount_T_68 = {_biasCount_T_61[6:0], 1'h0}; // @[Bitwise.scala 108:70]
  wire [7:0] _biasCount_T_70 = _biasCount_T_68 & 8'haa; // @[Bitwise.scala 108:80]
  wire [7:0] _biasCount_T_71 = _biasCount_T_66 | _biasCount_T_70; // @[Bitwise.scala 108:39]
  wire [30:0] _biasCount_T_92 = {_biasCount_T_40,_biasCount_T_71,intA[24],intA[25],intA[26],intA[27],intA[28],intA[29],
    intA[30]}; // @[Cat.scala 33:92]
  wire [4:0] _biasCount_T_124 = _biasCount_T_92[29] ? 5'h1d : 5'h1e; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_125 = _biasCount_T_92[28] ? 5'h1c : _biasCount_T_124; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_126 = _biasCount_T_92[27] ? 5'h1b : _biasCount_T_125; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_127 = _biasCount_T_92[26] ? 5'h1a : _biasCount_T_126; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_128 = _biasCount_T_92[25] ? 5'h19 : _biasCount_T_127; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_129 = _biasCount_T_92[24] ? 5'h18 : _biasCount_T_128; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_130 = _biasCount_T_92[23] ? 5'h17 : _biasCount_T_129; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_131 = _biasCount_T_92[22] ? 5'h16 : _biasCount_T_130; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_132 = _biasCount_T_92[21] ? 5'h15 : _biasCount_T_131; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_133 = _biasCount_T_92[20] ? 5'h14 : _biasCount_T_132; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_134 = _biasCount_T_92[19] ? 5'h13 : _biasCount_T_133; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_135 = _biasCount_T_92[18] ? 5'h12 : _biasCount_T_134; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_136 = _biasCount_T_92[17] ? 5'h11 : _biasCount_T_135; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_137 = _biasCount_T_92[16] ? 5'h10 : _biasCount_T_136; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_138 = _biasCount_T_92[15] ? 5'hf : _biasCount_T_137; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_139 = _biasCount_T_92[14] ? 5'he : _biasCount_T_138; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_140 = _biasCount_T_92[13] ? 5'hd : _biasCount_T_139; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_141 = _biasCount_T_92[12] ? 5'hc : _biasCount_T_140; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_142 = _biasCount_T_92[11] ? 5'hb : _biasCount_T_141; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_143 = _biasCount_T_92[10] ? 5'ha : _biasCount_T_142; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_144 = _biasCount_T_92[9] ? 5'h9 : _biasCount_T_143; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_145 = _biasCount_T_92[8] ? 5'h8 : _biasCount_T_144; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_146 = _biasCount_T_92[7] ? 5'h7 : _biasCount_T_145; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_147 = _biasCount_T_92[6] ? 5'h6 : _biasCount_T_146; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_148 = _biasCount_T_92[5] ? 5'h5 : _biasCount_T_147; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_149 = _biasCount_T_92[4] ? 5'h4 : _biasCount_T_148; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_150 = _biasCount_T_92[3] ? 5'h3 : _biasCount_T_149; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_151 = _biasCount_T_92[2] ? 5'h2 : _biasCount_T_150; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_152 = _biasCount_T_92[1] ? 5'h1 : _biasCount_T_151; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_153 = _biasCount_T_92[0] ? 5'h0 : _biasCount_T_152; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_155 = 5'h1e - _biasCount_T_153; // @[FPUPreprocessor.scala 132:42]
  wire [5:0] _biasCount_T_157 = {1'h0,_biasCount_T_155}; // @[FPUPreprocessor.scala 132:84]
  wire [31:0] _GEN_29 = {{16'd0}, intA[31:16]}; // @[Bitwise.scala 108:31]
  wire [31:0] _biasCount_T_161 = _GEN_29 & 32'hffff; // @[Bitwise.scala 108:31]
  wire [31:0] _biasCount_T_163 = {intA[15:0], 16'h0}; // @[Bitwise.scala 108:70]
  wire [31:0] _biasCount_T_165 = _biasCount_T_163 & 32'hffff0000; // @[Bitwise.scala 108:80]
  wire [31:0] _biasCount_T_166 = _biasCount_T_161 | _biasCount_T_165; // @[Bitwise.scala 108:39]
  wire [31:0] _GEN_30 = {{8'd0}, _biasCount_T_166[31:8]}; // @[Bitwise.scala 108:31]
  wire [31:0] _biasCount_T_171 = _GEN_30 & 32'hff00ff; // @[Bitwise.scala 108:31]
  wire [31:0] _biasCount_T_173 = {_biasCount_T_166[23:0], 8'h0}; // @[Bitwise.scala 108:70]
  wire [31:0] _biasCount_T_175 = _biasCount_T_173 & 32'hff00ff00; // @[Bitwise.scala 108:80]
  wire [31:0] _biasCount_T_176 = _biasCount_T_171 | _biasCount_T_175; // @[Bitwise.scala 108:39]
  wire [31:0] _GEN_31 = {{4'd0}, _biasCount_T_176[31:4]}; // @[Bitwise.scala 108:31]
  wire [31:0] _biasCount_T_181 = _GEN_31 & 32'hf0f0f0f; // @[Bitwise.scala 108:31]
  wire [31:0] _biasCount_T_183 = {_biasCount_T_176[27:0], 4'h0}; // @[Bitwise.scala 108:70]
  wire [31:0] _biasCount_T_185 = _biasCount_T_183 & 32'hf0f0f0f0; // @[Bitwise.scala 108:80]
  wire [31:0] _biasCount_T_186 = _biasCount_T_181 | _biasCount_T_185; // @[Bitwise.scala 108:39]
  wire [31:0] _GEN_32 = {{2'd0}, _biasCount_T_186[31:2]}; // @[Bitwise.scala 108:31]
  wire [31:0] _biasCount_T_191 = _GEN_32 & 32'h33333333; // @[Bitwise.scala 108:31]
  wire [31:0] _biasCount_T_193 = {_biasCount_T_186[29:0], 2'h0}; // @[Bitwise.scala 108:70]
  wire [31:0] _biasCount_T_195 = _biasCount_T_193 & 32'hcccccccc; // @[Bitwise.scala 108:80]
  wire [31:0] _biasCount_T_196 = _biasCount_T_191 | _biasCount_T_195; // @[Bitwise.scala 108:39]
  wire [31:0] _GEN_33 = {{1'd0}, _biasCount_T_196[31:1]}; // @[Bitwise.scala 108:31]
  wire [31:0] _biasCount_T_201 = _GEN_33 & 32'h55555555; // @[Bitwise.scala 108:31]
  wire [31:0] _biasCount_T_203 = {_biasCount_T_196[30:0], 1'h0}; // @[Bitwise.scala 108:70]
  wire [31:0] _biasCount_T_205 = _biasCount_T_203 & 32'haaaaaaaa; // @[Bitwise.scala 108:80]
  wire [31:0] _biasCount_T_206 = _biasCount_T_201 | _biasCount_T_205; // @[Bitwise.scala 108:39]
  wire [4:0] _biasCount_T_239 = _biasCount_T_206[30] ? 5'h1e : 5'h1f; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_240 = _biasCount_T_206[29] ? 5'h1d : _biasCount_T_239; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_241 = _biasCount_T_206[28] ? 5'h1c : _biasCount_T_240; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_242 = _biasCount_T_206[27] ? 5'h1b : _biasCount_T_241; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_243 = _biasCount_T_206[26] ? 5'h1a : _biasCount_T_242; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_244 = _biasCount_T_206[25] ? 5'h19 : _biasCount_T_243; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_245 = _biasCount_T_206[24] ? 5'h18 : _biasCount_T_244; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_246 = _biasCount_T_206[23] ? 5'h17 : _biasCount_T_245; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_247 = _biasCount_T_206[22] ? 5'h16 : _biasCount_T_246; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_248 = _biasCount_T_206[21] ? 5'h15 : _biasCount_T_247; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_249 = _biasCount_T_206[20] ? 5'h14 : _biasCount_T_248; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_250 = _biasCount_T_206[19] ? 5'h13 : _biasCount_T_249; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_251 = _biasCount_T_206[18] ? 5'h12 : _biasCount_T_250; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_252 = _biasCount_T_206[17] ? 5'h11 : _biasCount_T_251; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_253 = _biasCount_T_206[16] ? 5'h10 : _biasCount_T_252; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_254 = _biasCount_T_206[15] ? 5'hf : _biasCount_T_253; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_255 = _biasCount_T_206[14] ? 5'he : _biasCount_T_254; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_256 = _biasCount_T_206[13] ? 5'hd : _biasCount_T_255; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_257 = _biasCount_T_206[12] ? 5'hc : _biasCount_T_256; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_258 = _biasCount_T_206[11] ? 5'hb : _biasCount_T_257; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_259 = _biasCount_T_206[10] ? 5'ha : _biasCount_T_258; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_260 = _biasCount_T_206[9] ? 5'h9 : _biasCount_T_259; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_261 = _biasCount_T_206[8] ? 5'h8 : _biasCount_T_260; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_262 = _biasCount_T_206[7] ? 5'h7 : _biasCount_T_261; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_263 = _biasCount_T_206[6] ? 5'h6 : _biasCount_T_262; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_264 = _biasCount_T_206[5] ? 5'h5 : _biasCount_T_263; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_265 = _biasCount_T_206[4] ? 5'h4 : _biasCount_T_264; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_266 = _biasCount_T_206[3] ? 5'h3 : _biasCount_T_265; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_267 = _biasCount_T_206[2] ? 5'h2 : _biasCount_T_266; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_268 = _biasCount_T_206[1] ? 5'h1 : _biasCount_T_267; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_269 = _biasCount_T_206[0] ? 5'h0 : _biasCount_T_268; // @[Mux.scala 47:70]
  wire [4:0] _biasCount_T_271 = 5'h1f - _biasCount_T_269; // @[FPUPreprocessor.scala 135:42]
  wire [5:0] _biasCount_T_273 = {1'h0,_biasCount_T_271}; // @[FPUPreprocessor.scala 135:77]
  wire [5:0] _GEN_1 = _convert_T ? $signed(_biasCount_T_157) : $signed(_biasCount_T_273); // @[FPUPreprocessor.scala 130:46 132:19 135:19]
  wire [7:0] _GEN_6 = _T_3 ? $signed(8'sh0) : $signed({{2{_GEN_1[5]}},_GEN_1}); // @[FPUPreprocessor.scala 117:29 124:46]
  wire [7:0] biasCount = io_in_bits_rs1 == 32'h0 ? $signed(8'sh0) : $signed(_GEN_6); // @[FPUPreprocessor.scala 119:28 117:29]
  wire [7:0] _shiftedBits_T = io_in_bits_rs1 == 32'h0 ? $signed(8'sh0) : $signed(_GEN_6); // @[FPUPreprocessor.scala 138:42]
  wire [7:0] _shiftedBits_T_2 = _shiftedBits_T - 8'h17; // @[FPUPreprocessor.scala 138:49]
  wire [31:0] _shiftedBits_T_3 = intA >> _shiftedBits_T_2; // @[FPUPreprocessor.scala 138:29]
  wire [7:0] _shiftedBits_T_6 = 8'h17 - _shiftedBits_T; // @[FPUPreprocessor.scala 140:37]
  wire [286:0] _GEN_0 = {{255'd0}, intA}; // @[FPUPreprocessor.scala 140:29]
  wire [286:0] _shiftedBits_T_7 = _GEN_0 << _shiftedBits_T_6; // @[FPUPreprocessor.scala 140:29]
  wire [286:0] _GEN_2 = $signed(biasCount) >= 8'sh17 ? {{255'd0}, _shiftedBits_T_3} : _shiftedBits_T_7; // @[FPUPreprocessor.scala 137:31 138:21 140:21]
  wire [286:0] _GEN_7 = _T_3 ? {{255'd0}, io_in_bits_rs1} : _GEN_2; // @[FPUPreprocessor.scala 118:34 124:46]
  wire [286:0] shiftedBits = io_in_bits_rs1 == 32'h0 ? {{255'd0}, io_in_bits_rs1} : _GEN_7; // @[FPUPreprocessor.scala 119:28 118:34]
  wire [25:0] _r1_mantissa_T_1 = {3'h1,shiftedBits[22:0]}; // @[Cat.scala 33:92]
  wire [25:0] _GEN_3 = _T_3 ? 26'h800000 : _r1_mantissa_T_1; // @[FPUPreprocessor.scala 124:46 125:19 142:19]
  wire [7:0] _GEN_4 = _T_3 ? $signed(-8'sh61) : $signed(biasCount); // @[FPUPreprocessor.scala 124:46 126:19 143:19]
  wire  _GEN_5 = _T_3 ? 1'h0 : _convert_T & io_in_bits_rs1[31]; // @[FPUPreprocessor.scala 124:46 127:15 144:15]
  wire [25:0] _GEN_8 = io_in_bits_rs1 == 32'h0 ? 26'h0 : _GEN_3; // @[FPUPreprocessor.scala 119:28 120:19]
  wire [7:0] _GEN_9 = io_in_bits_rs1 == 32'h0 ? $signed(-8'sh7e) : $signed(_GEN_4); // @[FPUPreprocessor.scala 119:28 121:19]
  wire  _GEN_10 = io_in_bits_rs1 == 32'h0 ? 1'h0 : _GEN_5; // @[FPUPreprocessor.scala 119:28 122:15]
  wire  r1_isSubnormal = io_in_bits_rs1[30:23] == 8'h0; // @[FPUPreprocessor.scala 41:33]
  wire  r1_hiddenBit = r1_isSubnormal ? 1'h0 : 1'h1; // @[FPUPreprocessor.scala 45:24]
  wire [25:0] r1_mantissa = {2'h0,r1_hiddenBit,io_in_bits_rs1[22:0]}; // @[Cat.scala 33:92]
  wire [7:0] _r1_bundle_exponent_T_1 = io_in_bits_rs1[30:23]; // @[FPUPreprocessor.scala 60:64]
  wire [7:0] _r1_bundle_exponent_T_4 = $signed(_r1_bundle_exponent_T_1) - 8'sh7f; // @[FPUPreprocessor.scala 60:71]
  wire [7:0] _r1_bundle_exponent_T_5 = r1_isSubnormal ? $signed(-8'sh7e) : $signed(_r1_bundle_exponent_T_4); // @[FPUPreprocessor.scala 60:31]
  wire [7:0] _GEN_13 = io_in_bits_rs1[30:23] != 8'hff ? $signed(_r1_bundle_exponent_T_5) : $signed(8'sh7f); // @[FPUPreprocessor.scala 59:34 60:25 62:25]
  wire [7:0] r1_bundle_exponent = ~control_squareRoot ? $signed(_GEN_13) : $signed(_r1_bundle_exponent_T_1); // @[FPUPreprocessor.scala 58:17 65:23]
  wire [25:0] r1__mantissa = convert ? _GEN_8 : r1_mantissa; // @[FPUPreprocessor.scala 114:17 147:8]
  wire [7:0] r1__exponent = convert ? $signed(_GEN_9) : $signed(r1_bundle_exponent); // @[FPUPreprocessor.scala 114:17 147:8]
  wire  r2_isSubnormal = io_in_bits_rs2[30:23] == 8'h0; // @[FPUPreprocessor.scala 41:33]
  wire  r2_hiddenBit = r2_isSubnormal ? 1'h0 : 1'h1; // @[FPUPreprocessor.scala 45:24]
  wire [23:0] r2_mantissa_lo = {r2_hiddenBit,io_in_bits_rs2[22:0]}; // @[Cat.scala 33:92]
  wire [25:0] r2_mantissa = {2'h0,r2_hiddenBit,io_in_bits_rs2[22:0]}; // @[Cat.scala 33:92]
  wire [7:0] _r2_bundle_exponent_T_1 = io_in_bits_rs2[30:23]; // @[FPUPreprocessor.scala 60:64]
  wire [7:0] _r2_bundle_exponent_T_4 = $signed(_r2_bundle_exponent_T_1) - 8'sh7f; // @[FPUPreprocessor.scala 60:71]
  wire [7:0] _r2_bundle_exponent_T_5 = r2_isSubnormal ? $signed(-8'sh7e) : $signed(_r2_bundle_exponent_T_4); // @[FPUPreprocessor.scala 60:31]
  wire [7:0] _GEN_18 = io_in_bits_rs2[30:23] != 8'hff ? $signed(_r2_bundle_exponent_T_5) : $signed(8'sh7f); // @[FPUPreprocessor.scala 59:34 60:25 62:25]
  wire [7:0] r2_bundle_exponent = ~control_squareRoot ? $signed(_GEN_18) : $signed(_r2_bundle_exponent_T_1); // @[FPUPreprocessor.scala 58:17 65:23]
  wire  r3_isSubnormal = io_in_bits_rs3[30:23] == 8'h0; // @[FPUPreprocessor.scala 41:33]
  wire  r3_hiddenBit = r3_isSubnormal ? 1'h0 : 1'h1; // @[FPUPreprocessor.scala 45:24]
  wire [23:0] r3_mantissa_lo = {r3_hiddenBit,io_in_bits_rs3[22:0]}; // @[Cat.scala 33:92]
  wire [25:0] r3_mantissa = {2'h0,r3_hiddenBit,io_in_bits_rs3[22:0]}; // @[Cat.scala 33:92]
  wire [7:0] _r3_bundle_exponent_T_1 = io_in_bits_rs3[30:23]; // @[FPUPreprocessor.scala 60:64]
  wire [7:0] _r3_bundle_exponent_T_4 = $signed(_r3_bundle_exponent_T_1) - 8'sh7f; // @[FPUPreprocessor.scala 60:71]
  wire [7:0] _r3_bundle_exponent_T_5 = r3_isSubnormal ? $signed(-8'sh7e) : $signed(_r3_bundle_exponent_T_4); // @[FPUPreprocessor.scala 60:31]
  wire [7:0] _GEN_20 = io_in_bits_rs3[30:23] != 8'hff ? $signed(_r3_bundle_exponent_T_5) : $signed(8'sh7f); // @[FPUPreprocessor.scala 59:34 60:25 62:25]
  wire [7:0] r3_bundle_exponent = ~control_squareRoot ? $signed(_GEN_20) : $signed(_r3_bundle_exponent_T_1); // @[FPUPreprocessor.scala 58:17 65:23]
  wire  aNaN = r1__mantissa[22:0] > 23'h0; // @[FPUPreprocessor.scala 210:35]
  wire  bNaN = r2_mantissa[22:0] > 23'h0; // @[FPUPreprocessor.scala 212:35]
  wire  cNaN = r3_mantissa[22:0] > 23'h0; // @[FPUPreprocessor.scala 214:35]
  wire  maxExpA = $signed(r1__exponent) == 8'sh7f; // @[FPUPreprocessor.scala 216:30]
  wire  maxExpB = $signed(r2_bundle_exponent) == 8'sh7f; // @[FPUPreprocessor.scala 218:30]
  wire  maxExpC = $signed(r3_bundle_exponent) == 8'sh7f; // @[FPUPreprocessor.scala 220:30]
  wire  zeroExpA = $signed(r1__exponent) == -8'sh7e; // @[FPUPreprocessor.scala 222:31]
  wire  zeroExpB = $signed(r2_bundle_exponent) == -8'sh7e; // @[FPUPreprocessor.scala 224:31]
  wire  zeroExpC = $signed(r3_bundle_exponent) == -8'sh7e; // @[FPUPreprocessor.scala 226:31]
  wire  zeroMantA = r1__mantissa[22:0] == 23'h0; // @[FPUPreprocessor.scala 228:38]
  wire  zeroMantB = r2_mantissa[22:0] == 23'h0; // @[FPUPreprocessor.scala 230:38]
  wire  zeroMantC = r3_mantissa[22:0] == 23'h0; // @[FPUPreprocessor.scala 232:38]
  wire  _stall_T_4 = control_multiplier | control_fused | control_divider; // @[FPUPreprocessor.scala 245:69]
  wire [5:0] io_out_bits_fflags_lo = {maxExpC,maxExpB,maxExpA,cNaN,bNaN,aNaN}; // @[Cat.scala 33:92]
  wire [5:0] io_out_bits_fflags_hi = {zeroMantC,zeroMantB,zeroMantA,zeroExpC,zeroExpB,zeroExpA}; // @[Cat.scala 33:92]
  assign io_in_ready = io_out_ready; // @[FPUPreprocessor.scala 254:21]
  assign io_out_valid = io_in_valid; // @[FPUPreprocessor.scala 255:21]
  assign io_out_bits_a_mantissa = convert ? _GEN_8 : r1_mantissa; // @[FPUPreprocessor.scala 114:17 147:8]
  assign io_out_bits_a_exponent = convert ? $signed(_GEN_9) : $signed(r1_bundle_exponent); // @[FPUPreprocessor.scala 114:17 147:8]
  assign io_out_bits_a_sign = convert ? _GEN_10 : io_in_bits_rs1[31]; // @[FPUPreprocessor.scala 114:17 147:8]
  assign io_out_bits_b_mantissa = {2'h0,r2_mantissa_lo}; // @[Cat.scala 33:92]
  assign io_out_bits_b_exponent = ~control_squareRoot ? $signed(_GEN_18) : $signed(_r2_bundle_exponent_T_1); // @[FPUPreprocessor.scala 58:17 65:23]
  assign io_out_bits_b_sign = io_in_bits_rs2[31]; // @[FPUPreprocessor.scala 67:22]
  assign io_out_bits_c_mantissa = {2'h0,r3_mantissa_lo}; // @[Cat.scala 33:92]
  assign io_out_bits_c_exponent = ~control_squareRoot ? $signed(_GEN_20) : $signed(_r3_bundle_exponent_T_1); // @[FPUPreprocessor.scala 58:17 65:23]
  assign io_out_bits_c_sign = io_in_bits_rs3[31]; // @[FPUPreprocessor.scala 67:22]
  assign io_out_bits_op = io_in_bits_op; // @[FPUPreprocessor.scala 252:21]
  assign io_out_bits_fcsr_0 = io_in_bits_fcsr_0; // @[FPUPreprocessor.scala 253:21]
  assign io_out_bits_fcsr_1 = io_in_bits_fcsr_1; // @[FPUPreprocessor.scala 253:21]
  assign io_out_bits_fcsr_2 = io_in_bits_fcsr_2; // @[FPUPreprocessor.scala 253:21]
  assign io_out_bits_fcsr_3 = io_in_bits_fcsr_3; // @[FPUPreprocessor.scala 253:21]
  assign io_out_bits_fcsr_4 = io_in_bits_fcsr_4; // @[FPUPreprocessor.scala 253:21]
  assign io_out_bits_fcsr_5 = io_in_bits_fcsr_5; // @[FPUPreprocessor.scala 253:21]
  assign io_out_bits_fcsr_6 = io_in_bits_fcsr_6; // @[FPUPreprocessor.scala 253:21]
  assign io_out_bits_fcsr_7 = io_in_bits_fcsr_7; // @[FPUPreprocessor.scala 253:21]
  assign io_out_bits_fflags = {io_out_bits_fflags_hi,io_out_bits_fflags_lo}; // @[Cat.scala 33:92]
  assign io_out_bits_control_adder = io_in_bits_op == 5'h0 | _control_adder_T_1; // @[FPUPreprocessor.scala 75:62]
  assign io_out_bits_control_multiplier = io_in_bits_op == 5'h2; // @[FPUPreprocessor.scala 78:40]
  assign io_out_bits_control_fused = _control_fused_T_4 | _control_fused_T_5; // @[FPUPreprocessor.scala 82:64]
  assign io_out_bits_control_divider = io_in_bits_op == 5'h3; // @[FPUPreprocessor.scala 85:40]
  assign io_out_bits_control_comparator = _control_comparator_T_6 | _control_comparator_T_7; // @[FPUPreprocessor.scala 90:62]
  assign io_out_bits_control_squareRoot = io_in_bits_op == 5'h4; // @[FPUPreprocessor.scala 93:40]
  assign io_out_bits_control_bitInject = _control_bitInject_T_2 | _control_bitInject_T_3; // @[FPUPreprocessor.scala 96:64]
  assign io_out_bits_control_classify = io_in_bits_op == 5'h15; // @[FPUPreprocessor.scala 99:40]
  assign io_out_bits_stall_stall = _stall_T_4 | control_squareRoot; // @[FPUPreprocessor.scala 246:40]
endmodule
module SPFPAdd(
  input         io_in_valid,
  input  [25:0] io_in_bits_a_mantissa,
  input  [7:0]  io_in_bits_a_exponent,
  input         io_in_bits_a_sign,
  input  [25:0] io_in_bits_b_mantissa,
  input  [7:0]  io_in_bits_b_exponent,
  input         io_in_bits_b_sign,
  input  [4:0]  io_in_bits_op,
  input         io_in_bits_fcsr_0,
  input         io_in_bits_fcsr_1,
  input         io_in_bits_fcsr_2,
  input         io_in_bits_fcsr_3,
  input         io_in_bits_fcsr_4,
  input         io_in_bits_fcsr_5,
  input         io_in_bits_fcsr_6,
  input         io_in_bits_fcsr_7,
  input  [11:0] io_in_bits_fflags,
  output        io_out_valid,
  output [25:0] io_out_bits_result_mantissa,
  output [7:0]  io_out_bits_result_exponent,
  output        io_out_bits_result_sign,
  output        io_out_bits_fcsr_0,
  output        io_out_bits_fcsr_1,
  output        io_out_bits_fcsr_2,
  output        io_out_bits_fcsr_3,
  output        io_out_bits_fcsr_4,
  output        io_out_bits_fcsr_5,
  output        io_out_bits_fcsr_6,
  output        io_out_bits_fcsr_7,
  output [4:0]  io_out_bits_op
);
  wire  aNaN = io_in_bits_fflags[3] & io_in_bits_fflags[0]; // @[SPFPAdd.scala 39:36]
  wire  bNaN = io_in_bits_fflags[4] & io_in_bits_fflags[1]; // @[SPFPAdd.scala 40:36]
  wire  cNaN = io_in_bits_fflags[5] & io_in_bits_fflags[2]; // @[SPFPAdd.scala 41:36]
  wire  _dNaN_T_1 = io_in_bits_a_exponent == 8'h80; // @[SPFPAdd.scala 42:29]
  wire  dNaN = io_in_bits_a_exponent == 8'h80 & io_in_bits_a_mantissa > 26'h0; // @[SPFPAdd.scala 42:40]
  wire  aInf = io_in_bits_fflags[3] & io_in_bits_fflags[9]; // @[SPFPAdd.scala 43:36]
  wire  bInf = io_in_bits_fflags[4] & io_in_bits_fflags[10]; // @[SPFPAdd.scala 44:36]
  wire  cInf = io_in_bits_fflags[5] & io_in_bits_fflags[11]; // @[SPFPAdd.scala 45:36]
  wire  _dInf_T_2 = io_in_bits_a_mantissa == 26'h0; // @[SPFPAdd.scala 46:50]
  wire  dInf = _dNaN_T_1 & io_in_bits_a_mantissa == 26'h0; // @[SPFPAdd.scala 46:40]
  wire  aZero = io_in_bits_fflags[6] & io_in_bits_fflags[9]; // @[SPFPAdd.scala 47:36]
  wire  bZero = io_in_bits_fflags[7] & io_in_bits_fflags[10]; // @[SPFPAdd.scala 48:36]
  wire  cZero = io_in_bits_fflags[8] & io_in_bits_fflags[11]; // @[SPFPAdd.scala 49:36]
  wire  dZero = $signed(io_in_bits_a_exponent) == -8'sh7e & _dInf_T_2; // @[SPFPAdd.scala 50:34]
  wire  _isFused_T = io_in_bits_op == 5'h7; // @[SPFPAdd.scala 53:32]
  wire  _isFused_T_1 = io_in_bits_op == 5'h8; // @[SPFPAdd.scala 54:32]
  wire  _isFused_T_2 = io_in_bits_op == 5'h7 | _isFused_T_1; // @[SPFPAdd.scala 53:55]
  wire  _isFused_T_3 = io_in_bits_op == 5'h9; // @[SPFPAdd.scala 55:32]
  wire  _isFused_T_4 = _isFused_T_2 | _isFused_T_3; // @[SPFPAdd.scala 54:55]
  wire  _isFused_T_5 = io_in_bits_op == 5'ha; // @[SPFPAdd.scala 56:32]
  wire  isFused = _isFused_T_4 | _isFused_T_5; // @[SPFPAdd.scala 55:56]
  wire  _isSub_T_2 = io_in_bits_op == 5'h1 | _isFused_T_1; // @[SPFPAdd.scala 59:52]
  wire  isSub = _isSub_T_2 | _isFused_T_5; // @[SPFPAdd.scala 60:53]
  wire  _isAdd_T_2 = io_in_bits_op == 5'h0 | _isFused_T; // @[SPFPAdd.scala 64:52]
  wire  isAdd = _isAdd_T_2 | _isFused_T_3; // @[SPFPAdd.scala 65:53]
  wire  nanOne = isFused ? cNaN & dNaN : aNaN & bNaN; // @[SPFPAdd.scala 69:20]
  wire  nanTwo = isFused ? ~cNaN & dNaN : aNaN & ~bNaN; // @[SPFPAdd.scala 71:20]
  wire  nanThr = isFused ? cNaN & ~dNaN : ~aNaN & bNaN; // @[SPFPAdd.scala 73:20]
  wire  infOne = isFused ? cInf | dInf : aInf | bInf; // @[SPFPAdd.scala 75:20]
  wire  infTwo = isFused ? ~dInf : ~aInf; // @[SPFPAdd.scala 77:20]
  wire  infThr = isFused ? ~cInf : ~bInf; // @[SPFPAdd.scala 79:20]
  wire  zeroOne = isFused ? cZero | dZero : aZero | bZero; // @[SPFPAdd.scala 81:20]
  wire  zeroTwo = isFused ? ~dZero : ~aZero; // @[SPFPAdd.scala 83:20]
  wire  zeroThr = isFused ? ~cZero : ~bZero; // @[SPFPAdd.scala 85:20]
  wire  _T_1 = ~io_in_bits_a_mantissa[22]; // @[SPFPAdd.scala 93:10]
  wire  _T_3 = ~io_in_bits_b_mantissa[22]; // @[SPFPAdd.scala 93:24]
  wire  _GEN_12 = ~io_in_bits_a_mantissa[22] | ~io_in_bits_b_mantissa[22] | io_in_bits_fcsr_4; // @[SPFPAdd.scala 93:36 88:6]
  wire [22:0] _io_out_bits_result_mantissa_T_1 = {1'h1,io_in_bits_a_mantissa[21:0]}; // @[Cat.scala 33:92]
  wire  _GEN_28 = _T_1 | io_in_bits_fcsr_4; // @[SPFPAdd.scala 101:23 88:6]
  wire  _GEN_44 = _T_3 | io_in_bits_fcsr_4; // @[SPFPAdd.scala 109:22 88:6]
  wire [22:0] _io_out_bits_result_mantissa_T_5 = {1'h1,io_in_bits_b_mantissa[21:0]}; // @[Cat.scala 33:92]
  wire [22:0] _GEN_72 = infThr ? 23'h0 : 23'h400000; // @[SPFPAdd.scala 124:25 125:35 130:35]
  wire  _GEN_74 = infThr ? io_in_bits_a_sign : io_in_bits_a_sign ^ io_in_bits_b_sign; // @[SPFPAdd.scala 124:25 127:12 132:12]
  wire  _GEN_79 = infThr ? 1'h0 : 1'h1; // @[SPFPAdd.scala 124:25]
  wire [22:0] _GEN_83 = infTwo ? 23'h0 : _GEN_72; // @[SPFPAdd.scala 119:18 120:35]
  wire  _GEN_85 = infTwo ? io_in_bits_b_sign : _GEN_74; // @[SPFPAdd.scala 119:18 122:12]
  wire  _GEN_90 = infTwo ? 1'h0 : _GEN_79; // @[SPFPAdd.scala 119:18]
  wire  _GEN_127 = infTwo ? io_in_bits_b_sign ^ 1'h1 : _GEN_74; // @[SPFPAdd.scala 137:18 140:12]
  wire [25:0] _GEN_136 = zeroThr ? io_in_bits_b_mantissa : 26'h0; // @[SPFPAdd.scala 158:26 159:35 163:35]
  wire [7:0] _GEN_137 = zeroThr ? $signed(io_in_bits_b_exponent) : $signed(-8'sh7e); // @[SPFPAdd.scala 158:26 160:35 164:35]
  wire  _GEN_138 = zeroThr & io_in_bits_b_sign; // @[SPFPAdd.scala 158:26 161:12 165:12]
  wire [25:0] _GEN_139 = zeroTwo ? io_in_bits_a_mantissa : _GEN_136; // @[SPFPAdd.scala 154:19 155:35]
  wire [7:0] _GEN_140 = zeroTwo ? $signed(io_in_bits_a_exponent) : $signed(_GEN_137); // @[SPFPAdd.scala 154:19 156:35]
  wire  _GEN_141 = zeroTwo ? io_in_bits_a_sign : _GEN_138; // @[SPFPAdd.scala 154:19 157:12]
  wire  _sign_T_3 = ~io_in_bits_b_sign; // @[SPFPAdd.scala 176:15]
  wire  _GEN_142 = zeroThr & ~io_in_bits_b_sign; // @[SPFPAdd.scala 173:25 176:12 180:12]
  wire  _GEN_143 = zeroTwo ? io_in_bits_a_sign : _GEN_142; // @[SPFPAdd.scala 169:19 172:12]
  wire  compareCond = $signed(io_in_bits_a_exponent) > $signed(io_in_bits_b_exponent); // @[SPFPAdd.scala 189:32]
  wire [7:0] larger_exponent = compareCond ? $signed(io_in_bits_a_exponent) : $signed(io_in_bits_b_exponent); // @[SPFPAdd.scala 190:30]
  wire [7:0] _shiftAmount_T = compareCond ? $signed(io_in_bits_a_exponent) : $signed(io_in_bits_b_exponent); // @[SPFPAdd.scala 192:43]
  wire [7:0] _shiftAmount_T_1 = compareCond ? $signed(io_in_bits_b_exponent) : $signed(io_in_bits_a_exponent); // @[SPFPAdd.scala 192:69]
  wire [7:0] shiftAmount = _shiftAmount_T - _shiftAmount_T_1; // @[SPFPAdd.scala 192:50]
  wire [25:0] _mantAadj_T = io_in_bits_a_mantissa >> shiftAmount; // @[SPFPAdd.scala 193:57]
  wire [25:0] mantAadj = compareCond ? io_in_bits_a_mantissa : _mantAadj_T; // @[SPFPAdd.scala 193:30]
  wire [25:0] _mantBadj_T = io_in_bits_b_mantissa >> shiftAmount; // @[SPFPAdd.scala 194:50]
  wire [25:0] mantBadj = compareCond ? _mantBadj_T : io_in_bits_b_mantissa; // @[SPFPAdd.scala 194:30]
  wire  _T_22 = $signed(io_in_bits_a_exponent) == $signed(io_in_bits_b_exponent); // @[SPFPAdd.scala 206:23]
  wire  _T_24 = $signed(io_in_bits_a_exponent) == $signed(io_in_bits_b_exponent) & io_in_bits_a_mantissa ==
    io_in_bits_b_mantissa; // @[SPFPAdd.scala 206:33]
  wire  _T_27 = _T_22 & io_in_bits_a_mantissa > io_in_bits_b_mantissa; // @[SPFPAdd.scala 208:33]
  wire  _GEN_144 = _T_22 & io_in_bits_a_mantissa > io_in_bits_b_mantissa ? io_in_bits_a_sign : _sign_T_3; // @[SPFPAdd.scala 208:53 209:13 211:13]
  wire  _GEN_145 = $signed(io_in_bits_a_exponent) == $signed(io_in_bits_b_exponent) & io_in_bits_a_mantissa ==
    io_in_bits_b_mantissa ? io_in_bits_a_sign & _sign_T_3 : _GEN_144; // @[SPFPAdd.scala 206:55 207:13]
  wire  _GEN_146 = compareCond ? io_in_bits_a_sign : _GEN_145; // @[SPFPAdd.scala 204:24 205:13]
  wire  _GEN_147 = _T_27 ? io_in_bits_a_sign : io_in_bits_b_sign; // @[SPFPAdd.scala 218:53 219:13 221:13]
  wire  _GEN_148 = _T_24 ? io_in_bits_a_sign & io_in_bits_b_sign : _GEN_147; // @[SPFPAdd.scala 216:55 217:13]
  wire  _GEN_149 = compareCond ? io_in_bits_a_sign : _GEN_148; // @[SPFPAdd.scala 214:24 215:13]
  wire  _GEN_150 = isSub ? _GEN_146 : _GEN_149; // @[SPFPAdd.scala 203:16]
  wire [25:0] _operA_T_1 = ~mantAadj; // @[SPFPAdd.scala 226:34]
  wire [25:0] _operA_T_3 = _operA_T_1 + 26'h1; // @[SPFPAdd.scala 226:44]
  wire [25:0] operA = io_in_bits_a_sign ? _operA_T_3 : mantAadj; // @[SPFPAdd.scala 226:16]
  wire [25:0] _operB_T_1 = ~mantBadj; // @[SPFPAdd.scala 227:34]
  wire [25:0] _operB_T_3 = _operB_T_1 + 26'h1; // @[SPFPAdd.scala 227:44]
  wire [25:0] operB = io_in_bits_b_sign ? _operB_T_3 : mantBadj; // @[SPFPAdd.scala 227:16]
  wire [25:0] _mantissa_T = ~operB; // @[SPFPAdd.scala 228:37]
  wire [25:0] _mantissa_T_2 = _mantissa_T + 26'h1; // @[SPFPAdd.scala 228:44]
  wire [26:0] _mantissa_T_3 = operA + _mantissa_T_2; // @[SPFPAdd.scala 228:33]
  wire [26:0] _mantissa_T_4 = operA + operB; // @[SPFPAdd.scala 228:58]
  wire [26:0] _mantissa_T_5 = isSub ? _mantissa_T_3 : _mantissa_T_4; // @[SPFPAdd.scala 228:19]
  wire  _GEN_153 = zeroOne & isSub ? _GEN_143 : _GEN_150; // @[SPFPAdd.scala 167:33]
  wire  _GEN_156 = zeroOne & isAdd ? _GEN_141 : _GEN_153; // @[SPFPAdd.scala 152:33]
  wire  _GEN_167 = infOne & isSub ? _GEN_127 : _GEN_156; // @[SPFPAdd.scala 134:32]
  wire  _GEN_178 = infOne & isAdd ? _GEN_85 : _GEN_167; // @[SPFPAdd.scala 116:34]
  wire  _GEN_189 = nanThr ? io_in_bits_b_sign : _GEN_178; // @[SPFPAdd.scala 108:23 115:10]
  wire  _GEN_200 = nanTwo ? io_in_bits_a_sign : _GEN_189; // @[SPFPAdd.scala 100:23 107:10]
  wire  sign = nanOne ? io_in_bits_a_sign : _GEN_200; // @[SPFPAdd.scala 92:17 99:10]
  wire [25:0] mantissa = _mantissa_T_5[25:0]; // @[SPFPAdd.scala 195:38 228:13]
  wire [25:0] _finalMantissa_T_1 = ~mantissa; // @[SPFPAdd.scala 229:39]
  wire [25:0] _finalMantissa_T_3 = _finalMantissa_T_1 + 26'h1; // @[SPFPAdd.scala 229:49]
  wire [25:0] finalMantissa = sign ? _finalMantissa_T_3 : mantissa; // @[SPFPAdd.scala 229:24]
  wire [25:0] _GEN_151 = zeroOne & isSub ? _GEN_139 : finalMantissa; // @[SPFPAdd.scala 167:33 232:32]
  wire [7:0] _GEN_152 = zeroOne & isSub ? $signed(_GEN_140) : $signed(larger_exponent); // @[SPFPAdd.scala 167:33 233:32]
  wire [25:0] _GEN_154 = zeroOne & isAdd ? _GEN_139 : _GEN_151; // @[SPFPAdd.scala 152:33]
  wire [7:0] _GEN_155 = zeroOne & isAdd ? $signed(_GEN_140) : $signed(_GEN_152); // @[SPFPAdd.scala 152:33]
  wire  _GEN_161 = infOne & isSub ? _GEN_90 : io_in_bits_fcsr_4; // @[SPFPAdd.scala 134:32 88:6]
  wire [25:0] _GEN_165 = infOne & isSub ? {{3'd0}, _GEN_83} : _GEN_154; // @[SPFPAdd.scala 134:32]
  wire [8:0] _GEN_166 = infOne & isSub ? $signed(9'sh80) : $signed({{1{_GEN_155[7]}},_GEN_155}); // @[SPFPAdd.scala 134:32]
  wire  _GEN_172 = infOne & isAdd ? _GEN_90 : _GEN_161; // @[SPFPAdd.scala 116:34]
  wire [25:0] _GEN_176 = infOne & isAdd ? {{3'd0}, _GEN_83} : _GEN_165; // @[SPFPAdd.scala 116:34]
  wire [8:0] _GEN_177 = infOne & isAdd ? $signed(9'sh80) : $signed(_GEN_166); // @[SPFPAdd.scala 116:34]
  wire  _GEN_183 = nanThr ? _GEN_44 : _GEN_172; // @[SPFPAdd.scala 108:23]
  wire [25:0] _GEN_187 = nanThr ? {{3'd0}, _io_out_bits_result_mantissa_T_5} : _GEN_176; // @[SPFPAdd.scala 108:23 113:33]
  wire [8:0] _GEN_188 = nanThr ? $signed(9'sh80) : $signed(_GEN_177); // @[SPFPAdd.scala 108:23 114:33]
  wire  _GEN_194 = nanTwo ? _GEN_28 : _GEN_183; // @[SPFPAdd.scala 100:23]
  wire [25:0] _GEN_198 = nanTwo ? {{3'd0}, _io_out_bits_result_mantissa_T_1} : _GEN_187; // @[SPFPAdd.scala 100:23 105:33]
  wire [8:0] _GEN_199 = nanTwo ? $signed(9'sh80) : $signed(_GEN_188); // @[SPFPAdd.scala 100:23 106:33]
  wire [8:0] _GEN_210 = nanOne ? $signed(9'sh80) : $signed(_GEN_199); // @[SPFPAdd.scala 92:17 98:33]
  wire  inv = _isFused_T_3 | _isFused_T_5; // @[SPFPAdd.scala 237:52]
  assign io_out_valid = io_in_valid; // @[SPFPAdd.scala 245:22]
  assign io_out_bits_result_mantissa = nanOne ? {{3'd0}, _io_out_bits_result_mantissa_T_1} : _GEN_198; // @[SPFPAdd.scala 92:17 97:33]
  assign io_out_bits_result_exponent = _GEN_210[7:0];
  assign io_out_bits_result_sign = ~inv ? sign : sign ^ 1'h1; // @[SPFPAdd.scala 240:33]
  assign io_out_bits_fcsr_0 = io_in_bits_fcsr_0; // @[SPFPAdd.scala 92:17]
  assign io_out_bits_fcsr_1 = io_in_bits_fcsr_1; // @[SPFPAdd.scala 92:17]
  assign io_out_bits_fcsr_2 = io_in_bits_fcsr_2; // @[SPFPAdd.scala 92:17]
  assign io_out_bits_fcsr_3 = io_in_bits_fcsr_3; // @[SPFPAdd.scala 92:17]
  assign io_out_bits_fcsr_4 = nanOne ? _GEN_12 : _GEN_194; // @[SPFPAdd.scala 92:17]
  assign io_out_bits_fcsr_5 = io_in_bits_fcsr_5; // @[SPFPAdd.scala 92:17]
  assign io_out_bits_fcsr_6 = io_in_bits_fcsr_6; // @[SPFPAdd.scala 92:17]
  assign io_out_bits_fcsr_7 = io_in_bits_fcsr_7; // @[SPFPAdd.scala 92:17]
  assign io_out_bits_op = io_in_bits_op; // @[SPFPAdd.scala 243:22]
endmodule
module SPFPMul(
  input         clock,
  input         reset,
  input         io_in_valid,
  input  [25:0] io_in_bits_a_mantissa,
  input  [7:0]  io_in_bits_a_exponent,
  input         io_in_bits_a_sign,
  input  [25:0] io_in_bits_b_mantissa,
  input  [7:0]  io_in_bits_b_exponent,
  input         io_in_bits_b_sign,
  input  [25:0] io_in_bits_c_mantissa,
  input  [7:0]  io_in_bits_c_exponent,
  input         io_in_bits_c_sign,
  input  [4:0]  io_in_bits_op,
  input         io_in_bits_fcsr_0,
  input         io_in_bits_fcsr_1,
  input         io_in_bits_fcsr_2,
  input         io_in_bits_fcsr_3,
  input         io_in_bits_fcsr_4,
  input         io_in_bits_fcsr_5,
  input         io_in_bits_fcsr_6,
  input         io_in_bits_fcsr_7,
  input  [11:0] io_in_bits_fflags,
  output        io_out_valid,
  output [25:0] io_out_bits_result_mantissa,
  output [7:0]  io_out_bits_result_exponent,
  output        io_out_bits_result_sign,
  output        io_out_bits_fcsr_0,
  output        io_out_bits_fcsr_1,
  output        io_out_bits_fcsr_2,
  output        io_out_bits_fcsr_3,
  output        io_out_bits_fcsr_4,
  output        io_out_bits_fcsr_5,
  output        io_out_bits_fcsr_6,
  output        io_out_bits_fcsr_7,
  output [4:0]  io_out_bits_op,
  output        io_out_bits_mulInProc
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
`endif // RANDOMIZE_REG_INIT
  wire  executorAdd_io_in_valid; // @[SPFPMul.scala 136:28]
  wire [25:0] executorAdd_io_in_bits_a_mantissa; // @[SPFPMul.scala 136:28]
  wire [7:0] executorAdd_io_in_bits_a_exponent; // @[SPFPMul.scala 136:28]
  wire  executorAdd_io_in_bits_a_sign; // @[SPFPMul.scala 136:28]
  wire [25:0] executorAdd_io_in_bits_b_mantissa; // @[SPFPMul.scala 136:28]
  wire [7:0] executorAdd_io_in_bits_b_exponent; // @[SPFPMul.scala 136:28]
  wire  executorAdd_io_in_bits_b_sign; // @[SPFPMul.scala 136:28]
  wire [4:0] executorAdd_io_in_bits_op; // @[SPFPMul.scala 136:28]
  wire  executorAdd_io_in_bits_fcsr_0; // @[SPFPMul.scala 136:28]
  wire  executorAdd_io_in_bits_fcsr_1; // @[SPFPMul.scala 136:28]
  wire  executorAdd_io_in_bits_fcsr_2; // @[SPFPMul.scala 136:28]
  wire  executorAdd_io_in_bits_fcsr_3; // @[SPFPMul.scala 136:28]
  wire  executorAdd_io_in_bits_fcsr_4; // @[SPFPMul.scala 136:28]
  wire  executorAdd_io_in_bits_fcsr_5; // @[SPFPMul.scala 136:28]
  wire  executorAdd_io_in_bits_fcsr_6; // @[SPFPMul.scala 136:28]
  wire  executorAdd_io_in_bits_fcsr_7; // @[SPFPMul.scala 136:28]
  wire [11:0] executorAdd_io_in_bits_fflags; // @[SPFPMul.scala 136:28]
  wire  executorAdd_io_out_valid; // @[SPFPMul.scala 136:28]
  wire [25:0] executorAdd_io_out_bits_result_mantissa; // @[SPFPMul.scala 136:28]
  wire [7:0] executorAdd_io_out_bits_result_exponent; // @[SPFPMul.scala 136:28]
  wire  executorAdd_io_out_bits_result_sign; // @[SPFPMul.scala 136:28]
  wire  executorAdd_io_out_bits_fcsr_0; // @[SPFPMul.scala 136:28]
  wire  executorAdd_io_out_bits_fcsr_1; // @[SPFPMul.scala 136:28]
  wire  executorAdd_io_out_bits_fcsr_2; // @[SPFPMul.scala 136:28]
  wire  executorAdd_io_out_bits_fcsr_3; // @[SPFPMul.scala 136:28]
  wire  executorAdd_io_out_bits_fcsr_4; // @[SPFPMul.scala 136:28]
  wire  executorAdd_io_out_bits_fcsr_5; // @[SPFPMul.scala 136:28]
  wire  executorAdd_io_out_bits_fcsr_6; // @[SPFPMul.scala 136:28]
  wire  executorAdd_io_out_bits_fcsr_7; // @[SPFPMul.scala 136:28]
  wire [4:0] executorAdd_io_out_bits_op; // @[SPFPMul.scala 136:28]
  reg [25:0] mulReg_mantissaA; // @[SPFPMul.scala 40:23]
  reg [25:0] mulReg_mantissaB; // @[SPFPMul.scala 40:23]
  reg [25:0] mulReg_mantissaC; // @[SPFPMul.scala 40:23]
  reg [7:0] mulReg_exponentA; // @[SPFPMul.scala 40:23]
  reg [7:0] mulReg_exponentB; // @[SPFPMul.scala 40:23]
  reg [7:0] mulReg_exponentC; // @[SPFPMul.scala 40:23]
  reg  mulReg_signBitA; // @[SPFPMul.scala 40:23]
  reg  mulReg_signBitB; // @[SPFPMul.scala 40:23]
  reg  mulReg_signBitC; // @[SPFPMul.scala 40:23]
  reg [11:0] mulReg_faultFlags; // @[SPFPMul.scala 40:23]
  reg  mulReg_status_0; // @[SPFPMul.scala 40:23]
  reg  mulReg_status_1; // @[SPFPMul.scala 40:23]
  reg  mulReg_status_2; // @[SPFPMul.scala 40:23]
  reg  mulReg_status_3; // @[SPFPMul.scala 40:23]
  reg  mulReg_status_4; // @[SPFPMul.scala 40:23]
  reg  mulReg_status_5; // @[SPFPMul.scala 40:23]
  reg  mulReg_status_6; // @[SPFPMul.scala 40:23]
  reg  mulReg_status_7; // @[SPFPMul.scala 40:23]
  reg [4:0] mulReg_operation; // @[SPFPMul.scala 40:23]
  reg  mulReg_valid; // @[SPFPMul.scala 40:23]
  wire  aNaN = mulReg_faultFlags[3] & mulReg_faultFlags[0]; // @[SPFPMul.scala 64:25]
  wire  bNaN = mulReg_faultFlags[4] & mulReg_faultFlags[1]; // @[SPFPMul.scala 65:25]
  wire  aInf = mulReg_faultFlags[3] & mulReg_faultFlags[9]; // @[SPFPMul.scala 66:25]
  wire  bInf = mulReg_faultFlags[4] & mulReg_faultFlags[10]; // @[SPFPMul.scala 67:25]
  wire  aZero = mulReg_faultFlags[6] & mulReg_faultFlags[9]; // @[SPFPMul.scala 68:25]
  wire  bZero = mulReg_faultFlags[7] & mulReg_faultFlags[10]; // @[SPFPMul.scala 69:25]
  wire  _T_2 = ~mulReg_mantissaA[22]; // @[SPFPMul.scala 81:10]
  wire  _T_4 = ~mulReg_mantissaB[22]; // @[SPFPMul.scala 81:24]
  wire  _GEN_12 = ~mulReg_mantissaA[22] | ~mulReg_mantissaB[22] | mulReg_status_4; // @[SPFPMul.scala 81:36 78:6]
  wire [22:0] _resultMant_T_1 = {1'h1,mulReg_mantissaA[21:0]}; // @[Cat.scala 33:92]
  wire  _GEN_28 = _T_2 | mulReg_status_4; // @[SPFPMul.scala 89:23 78:6]
  wire  _GEN_44 = _T_4 | mulReg_status_4; // @[SPFPMul.scala 97:23 78:6]
  wire [22:0] _resultMant_T_5 = {1'h1,mulReg_mantissaB[21:0]}; // @[Cat.scala 33:92]
  wire  _T_14 = aInf | bInf; // @[SPFPMul.scala 106:15]
  wire [22:0] _GEN_56 = aInf | bInf ? 23'h400000 : 23'h0; // @[SPFPMul.scala 106:24 108:18 114:18]
  wire [8:0] _GEN_57 = aInf | bInf ? $signed(9'sh80) : $signed(-9'sh7e); // @[SPFPMul.scala 106:24 109:18 115:18]
  wire  _GEN_58 = aInf | bInf ? 1'h0 : mulReg_signBitA ^ mulReg_signBitB; // @[SPFPMul.scala 106:24 110:18 74:14]
  wire  _GEN_63 = aInf | bInf | mulReg_status_4; // @[SPFPMul.scala 106:24 78:6]
  wire [7:0] exponent = $signed(mulReg_exponentA) + $signed(mulReg_exponentB); // @[SPFPMul.scala 130:25]
  wire [51:0] mantissa = mulReg_mantissaA * mulReg_mantissaB; // @[SPFPMul.scala 131:26]
  wire [28:0] _GEN_67 = mulReg_faultFlags[7] & mulReg_mantissaB == 26'h800000 ? {{3'd0}, mulReg_mantissaA} : mantissa[51
    :23]; // @[SPFPMul.scala 125:49 127:16 132:16]
  wire [7:0] _GEN_68 = mulReg_faultFlags[7] & mulReg_mantissaB == 26'h800000 ? $signed(mulReg_exponentA) : $signed(
    exponent); // @[SPFPMul.scala 125:49 128:16 133:16]
  wire [28:0] _GEN_69 = mulReg_faultFlags[6] & mulReg_mantissaA == 26'h800000 ? {{3'd0}, mulReg_mantissaB} : _GEN_67; // @[SPFPMul.scala 121:49 123:16]
  wire [7:0] _GEN_70 = mulReg_faultFlags[6] & mulReg_mantissaA == 26'h800000 ? $signed(mulReg_exponentB) : $signed(
    _GEN_68); // @[SPFPMul.scala 121:49 124:16]
  wire [28:0] _GEN_71 = _T_14 ? 29'h0 : _GEN_69; // @[SPFPMul.scala 117:29 119:16]
  wire [8:0] _GEN_72 = _T_14 ? $signed(9'sh80) : $signed({{1{_GEN_70[7]}},_GEN_70}); // @[SPFPMul.scala 117:29 120:16]
  wire [28:0] _GEN_73 = aZero | bZero ? {{6'd0}, _GEN_56} : _GEN_71; // @[SPFPMul.scala 104:31]
  wire [8:0] _GEN_74 = aZero | bZero ? $signed(_GEN_57) : $signed(_GEN_72); // @[SPFPMul.scala 104:31]
  wire  _GEN_75 = aZero | bZero ? _GEN_58 : mulReg_signBitA ^ mulReg_signBitB; // @[SPFPMul.scala 104:31 74:14]
  wire  _GEN_80 = aZero | bZero ? _GEN_63 : mulReg_status_4; // @[SPFPMul.scala 104:31 78:6]
  wire [28:0] _GEN_92 = bNaN ? {{6'd0}, _resultMant_T_5} : _GEN_73; // @[SPFPMul.scala 101:16 96:21]
  wire [8:0] _GEN_93 = bNaN ? $signed(9'sh80) : $signed(_GEN_74); // @[SPFPMul.scala 102:16 96:21]
  wire [28:0] _GEN_103 = aNaN ? {{6'd0}, _resultMant_T_1} : _GEN_92; // @[SPFPMul.scala 88:21 93:16]
  wire [8:0] _GEN_104 = aNaN ? $signed(9'sh80) : $signed(_GEN_93); // @[SPFPMul.scala 88:21 94:16]
  wire [28:0] _GEN_114 = aNaN & bNaN ? {{6'd0}, _resultMant_T_1} : _GEN_103; // @[SPFPMul.scala 80:22 85:16]
  wire [8:0] _GEN_115 = aNaN & bNaN ? $signed(9'sh80) : $signed(_GEN_104); // @[SPFPMul.scala 80:22 86:16]
  reg [25:0] fusedReg_mantissaA; // @[SPFPMul.scala 152:25]
  reg [25:0] fusedReg_mantissaB; // @[SPFPMul.scala 152:25]
  reg [7:0] fusedReg_exponentA; // @[SPFPMul.scala 152:25]
  reg [7:0] fusedReg_exponentB; // @[SPFPMul.scala 152:25]
  reg  fusedReg_signBitA; // @[SPFPMul.scala 152:25]
  reg  fusedReg_signBitB; // @[SPFPMul.scala 152:25]
  reg [11:0] fusedReg_faultFlags; // @[SPFPMul.scala 152:25]
  reg  fusedReg_status_0; // @[SPFPMul.scala 152:25]
  reg  fusedReg_status_1; // @[SPFPMul.scala 152:25]
  reg  fusedReg_status_2; // @[SPFPMul.scala 152:25]
  reg  fusedReg_status_3; // @[SPFPMul.scala 152:25]
  reg  fusedReg_status_4; // @[SPFPMul.scala 152:25]
  reg  fusedReg_status_5; // @[SPFPMul.scala 152:25]
  reg  fusedReg_status_6; // @[SPFPMul.scala 152:25]
  reg  fusedReg_status_7; // @[SPFPMul.scala 152:25]
  reg [4:0] fusedReg_operation; // @[SPFPMul.scala 152:25]
  reg  fusedReg_valid; // @[SPFPMul.scala 152:25]
  wire  _fused_T_1 = fusedReg_operation == 5'h8; // @[SPFPMul.scala 179:35]
  wire  _fused_T_2 = fusedReg_operation == 5'h7 | _fused_T_1; // @[SPFPMul.scala 178:58]
  wire  _fused_T_3 = fusedReg_operation == 5'h9; // @[SPFPMul.scala 180:35]
  wire  _fused_T_4 = _fused_T_2 | _fused_T_3; // @[SPFPMul.scala 179:58]
  wire  _fused_T_5 = fusedReg_operation == 5'ha; // @[SPFPMul.scala 181:35]
  wire  fused = _fused_T_4 | _fused_T_5; // @[SPFPMul.scala 180:59]
  wire [25:0] resultMant = _GEN_114[25:0]; // @[SPFPMul.scala 71:31]
  wire [7:0] resultExp = _GEN_115[7:0]; // @[SPFPMul.scala 72:31]
  SPFPAdd executorAdd ( // @[SPFPMul.scala 136:28]
    .io_in_valid(executorAdd_io_in_valid),
    .io_in_bits_a_mantissa(executorAdd_io_in_bits_a_mantissa),
    .io_in_bits_a_exponent(executorAdd_io_in_bits_a_exponent),
    .io_in_bits_a_sign(executorAdd_io_in_bits_a_sign),
    .io_in_bits_b_mantissa(executorAdd_io_in_bits_b_mantissa),
    .io_in_bits_b_exponent(executorAdd_io_in_bits_b_exponent),
    .io_in_bits_b_sign(executorAdd_io_in_bits_b_sign),
    .io_in_bits_op(executorAdd_io_in_bits_op),
    .io_in_bits_fcsr_0(executorAdd_io_in_bits_fcsr_0),
    .io_in_bits_fcsr_1(executorAdd_io_in_bits_fcsr_1),
    .io_in_bits_fcsr_2(executorAdd_io_in_bits_fcsr_2),
    .io_in_bits_fcsr_3(executorAdd_io_in_bits_fcsr_3),
    .io_in_bits_fcsr_4(executorAdd_io_in_bits_fcsr_4),
    .io_in_bits_fcsr_5(executorAdd_io_in_bits_fcsr_5),
    .io_in_bits_fcsr_6(executorAdd_io_in_bits_fcsr_6),
    .io_in_bits_fcsr_7(executorAdd_io_in_bits_fcsr_7),
    .io_in_bits_fflags(executorAdd_io_in_bits_fflags),
    .io_out_valid(executorAdd_io_out_valid),
    .io_out_bits_result_mantissa(executorAdd_io_out_bits_result_mantissa),
    .io_out_bits_result_exponent(executorAdd_io_out_bits_result_exponent),
    .io_out_bits_result_sign(executorAdd_io_out_bits_result_sign),
    .io_out_bits_fcsr_0(executorAdd_io_out_bits_fcsr_0),
    .io_out_bits_fcsr_1(executorAdd_io_out_bits_fcsr_1),
    .io_out_bits_fcsr_2(executorAdd_io_out_bits_fcsr_2),
    .io_out_bits_fcsr_3(executorAdd_io_out_bits_fcsr_3),
    .io_out_bits_fcsr_4(executorAdd_io_out_bits_fcsr_4),
    .io_out_bits_fcsr_5(executorAdd_io_out_bits_fcsr_5),
    .io_out_bits_fcsr_6(executorAdd_io_out_bits_fcsr_6),
    .io_out_bits_fcsr_7(executorAdd_io_out_bits_fcsr_7),
    .io_out_bits_op(executorAdd_io_out_bits_op)
  );
  assign io_out_valid = ~fused ? fusedReg_valid : executorAdd_io_out_valid; // @[SPFPMul.scala 184:16 191:33 198:33]
  assign io_out_bits_result_mantissa = ~fused ? fusedReg_mantissaA : executorAdd_io_out_bits_result_mantissa; // @[SPFPMul.scala 184:16 185:33 194:33]
  assign io_out_bits_result_exponent = ~fused ? $signed(fusedReg_exponentA) : $signed(
    executorAdd_io_out_bits_result_exponent); // @[SPFPMul.scala 184:16 186:33 194:33]
  assign io_out_bits_result_sign = ~fused ? fusedReg_signBitA : executorAdd_io_out_bits_result_sign; // @[SPFPMul.scala 184:16 187:33 194:33]
  assign io_out_bits_fcsr_0 = ~fused ? fusedReg_status_0 : executorAdd_io_out_bits_fcsr_0; // @[SPFPMul.scala 184:16 188:33 195:33]
  assign io_out_bits_fcsr_1 = ~fused ? fusedReg_status_1 : executorAdd_io_out_bits_fcsr_1; // @[SPFPMul.scala 184:16 188:33 195:33]
  assign io_out_bits_fcsr_2 = ~fused ? fusedReg_status_2 : executorAdd_io_out_bits_fcsr_2; // @[SPFPMul.scala 184:16 188:33 195:33]
  assign io_out_bits_fcsr_3 = ~fused ? fusedReg_status_3 : executorAdd_io_out_bits_fcsr_3; // @[SPFPMul.scala 184:16 188:33 195:33]
  assign io_out_bits_fcsr_4 = ~fused ? fusedReg_status_4 : executorAdd_io_out_bits_fcsr_4; // @[SPFPMul.scala 184:16 188:33 195:33]
  assign io_out_bits_fcsr_5 = ~fused ? fusedReg_status_5 : executorAdd_io_out_bits_fcsr_5; // @[SPFPMul.scala 184:16 188:33 195:33]
  assign io_out_bits_fcsr_6 = ~fused ? fusedReg_status_6 : executorAdd_io_out_bits_fcsr_6; // @[SPFPMul.scala 184:16 188:33 195:33]
  assign io_out_bits_fcsr_7 = ~fused ? fusedReg_status_7 : executorAdd_io_out_bits_fcsr_7; // @[SPFPMul.scala 184:16 188:33 195:33]
  assign io_out_bits_op = ~fused ? fusedReg_operation : executorAdd_io_out_bits_op; // @[SPFPMul.scala 184:16 189:33 196:33]
  assign io_out_bits_mulInProc = mulReg_valid | fusedReg_valid; // @[SPFPMul.scala 202:42]
  assign executorAdd_io_in_valid = fusedReg_valid; // @[SPFPMul.scala 174:37]
  assign executorAdd_io_in_bits_a_mantissa = fusedReg_mantissaA; // @[SPFPMul.scala 166:37]
  assign executorAdd_io_in_bits_a_exponent = fusedReg_exponentA; // @[SPFPMul.scala 167:37]
  assign executorAdd_io_in_bits_a_sign = fusedReg_signBitA; // @[SPFPMul.scala 168:37]
  assign executorAdd_io_in_bits_b_mantissa = fusedReg_mantissaB; // @[SPFPMul.scala 169:37]
  assign executorAdd_io_in_bits_b_exponent = fusedReg_exponentB; // @[SPFPMul.scala 170:37]
  assign executorAdd_io_in_bits_b_sign = fusedReg_signBitB; // @[SPFPMul.scala 171:37]
  assign executorAdd_io_in_bits_op = fusedReg_operation; // @[SPFPMul.scala 173:37]
  assign executorAdd_io_in_bits_fcsr_0 = fusedReg_status_0; // @[SPFPMul.scala 172:37]
  assign executorAdd_io_in_bits_fcsr_1 = fusedReg_status_1; // @[SPFPMul.scala 172:37]
  assign executorAdd_io_in_bits_fcsr_2 = fusedReg_status_2; // @[SPFPMul.scala 172:37]
  assign executorAdd_io_in_bits_fcsr_3 = fusedReg_status_3; // @[SPFPMul.scala 172:37]
  assign executorAdd_io_in_bits_fcsr_4 = fusedReg_status_4; // @[SPFPMul.scala 172:37]
  assign executorAdd_io_in_bits_fcsr_5 = fusedReg_status_5; // @[SPFPMul.scala 172:37]
  assign executorAdd_io_in_bits_fcsr_6 = fusedReg_status_6; // @[SPFPMul.scala 172:37]
  assign executorAdd_io_in_bits_fcsr_7 = fusedReg_status_7; // @[SPFPMul.scala 172:37]
  assign executorAdd_io_in_bits_fflags = fusedReg_faultFlags; // @[SPFPMul.scala 175:37]
  always @(posedge clock) begin
    if (reset) begin // @[SPFPMul.scala 40:23]
      mulReg_mantissaA <= 26'h0; // @[SPFPMul.scala 40:23]
    end else begin
      mulReg_mantissaA <= io_in_bits_a_mantissa; // @[SPFPMul.scala 42:21]
    end
    if (reset) begin // @[SPFPMul.scala 40:23]
      mulReg_mantissaB <= 26'h0; // @[SPFPMul.scala 40:23]
    end else begin
      mulReg_mantissaB <= io_in_bits_b_mantissa; // @[SPFPMul.scala 43:21]
    end
    if (reset) begin // @[SPFPMul.scala 40:23]
      mulReg_mantissaC <= 26'h0; // @[SPFPMul.scala 40:23]
    end else begin
      mulReg_mantissaC <= io_in_bits_c_mantissa; // @[SPFPMul.scala 44:21]
    end
    if (reset) begin // @[SPFPMul.scala 40:23]
      mulReg_exponentA <= 8'sh0; // @[SPFPMul.scala 40:23]
    end else begin
      mulReg_exponentA <= io_in_bits_a_exponent; // @[SPFPMul.scala 45:21]
    end
    if (reset) begin // @[SPFPMul.scala 40:23]
      mulReg_exponentB <= 8'sh0; // @[SPFPMul.scala 40:23]
    end else begin
      mulReg_exponentB <= io_in_bits_b_exponent; // @[SPFPMul.scala 46:21]
    end
    if (reset) begin // @[SPFPMul.scala 40:23]
      mulReg_exponentC <= 8'sh0; // @[SPFPMul.scala 40:23]
    end else begin
      mulReg_exponentC <= io_in_bits_c_exponent; // @[SPFPMul.scala 47:21]
    end
    if (reset) begin // @[SPFPMul.scala 40:23]
      mulReg_signBitA <= 1'h0; // @[SPFPMul.scala 40:23]
    end else begin
      mulReg_signBitA <= io_in_bits_a_sign; // @[SPFPMul.scala 48:21]
    end
    if (reset) begin // @[SPFPMul.scala 40:23]
      mulReg_signBitB <= 1'h0; // @[SPFPMul.scala 40:23]
    end else begin
      mulReg_signBitB <= io_in_bits_b_sign; // @[SPFPMul.scala 49:21]
    end
    if (reset) begin // @[SPFPMul.scala 40:23]
      mulReg_signBitC <= 1'h0; // @[SPFPMul.scala 40:23]
    end else begin
      mulReg_signBitC <= io_in_bits_c_sign; // @[SPFPMul.scala 50:21]
    end
    if (reset) begin // @[SPFPMul.scala 40:23]
      mulReg_faultFlags <= 12'h0; // @[SPFPMul.scala 40:23]
    end else begin
      mulReg_faultFlags <= io_in_bits_fflags; // @[SPFPMul.scala 51:21]
    end
    if (reset) begin // @[SPFPMul.scala 40:23]
      mulReg_status_0 <= 1'h0; // @[SPFPMul.scala 40:23]
    end else begin
      mulReg_status_0 <= io_in_bits_fcsr_0; // @[SPFPMul.scala 52:21]
    end
    if (reset) begin // @[SPFPMul.scala 40:23]
      mulReg_status_1 <= 1'h0; // @[SPFPMul.scala 40:23]
    end else begin
      mulReg_status_1 <= io_in_bits_fcsr_1; // @[SPFPMul.scala 52:21]
    end
    if (reset) begin // @[SPFPMul.scala 40:23]
      mulReg_status_2 <= 1'h0; // @[SPFPMul.scala 40:23]
    end else begin
      mulReg_status_2 <= io_in_bits_fcsr_2; // @[SPFPMul.scala 52:21]
    end
    if (reset) begin // @[SPFPMul.scala 40:23]
      mulReg_status_3 <= 1'h0; // @[SPFPMul.scala 40:23]
    end else begin
      mulReg_status_3 <= io_in_bits_fcsr_3; // @[SPFPMul.scala 52:21]
    end
    if (reset) begin // @[SPFPMul.scala 40:23]
      mulReg_status_4 <= 1'h0; // @[SPFPMul.scala 40:23]
    end else begin
      mulReg_status_4 <= io_in_bits_fcsr_4; // @[SPFPMul.scala 52:21]
    end
    if (reset) begin // @[SPFPMul.scala 40:23]
      mulReg_status_5 <= 1'h0; // @[SPFPMul.scala 40:23]
    end else begin
      mulReg_status_5 <= io_in_bits_fcsr_5; // @[SPFPMul.scala 52:21]
    end
    if (reset) begin // @[SPFPMul.scala 40:23]
      mulReg_status_6 <= 1'h0; // @[SPFPMul.scala 40:23]
    end else begin
      mulReg_status_6 <= io_in_bits_fcsr_6; // @[SPFPMul.scala 52:21]
    end
    if (reset) begin // @[SPFPMul.scala 40:23]
      mulReg_status_7 <= 1'h0; // @[SPFPMul.scala 40:23]
    end else begin
      mulReg_status_7 <= io_in_bits_fcsr_7; // @[SPFPMul.scala 52:21]
    end
    if (reset) begin // @[SPFPMul.scala 40:23]
      mulReg_operation <= 5'h0; // @[SPFPMul.scala 40:23]
    end else begin
      mulReg_operation <= io_in_bits_op; // @[SPFPMul.scala 53:21]
    end
    if (reset) begin // @[SPFPMul.scala 40:23]
      mulReg_valid <= 1'h0; // @[SPFPMul.scala 40:23]
    end else begin
      mulReg_valid <= io_in_valid; // @[SPFPMul.scala 54:21]
    end
    if (reset) begin // @[SPFPMul.scala 152:25]
      fusedReg_mantissaA <= 26'h0; // @[SPFPMul.scala 152:25]
    end else begin
      fusedReg_mantissaA <= resultMant; // @[SPFPMul.scala 153:23]
    end
    if (reset) begin // @[SPFPMul.scala 152:25]
      fusedReg_mantissaB <= 26'h0; // @[SPFPMul.scala 152:25]
    end else begin
      fusedReg_mantissaB <= mulReg_mantissaC; // @[SPFPMul.scala 154:23]
    end
    if (reset) begin // @[SPFPMul.scala 152:25]
      fusedReg_exponentA <= 8'sh0; // @[SPFPMul.scala 152:25]
    end else begin
      fusedReg_exponentA <= resultExp; // @[SPFPMul.scala 155:23]
    end
    if (reset) begin // @[SPFPMul.scala 152:25]
      fusedReg_exponentB <= 8'sh0; // @[SPFPMul.scala 152:25]
    end else begin
      fusedReg_exponentB <= mulReg_exponentC; // @[SPFPMul.scala 156:23]
    end
    if (reset) begin // @[SPFPMul.scala 152:25]
      fusedReg_signBitA <= 1'h0; // @[SPFPMul.scala 152:25]
    end else if (aNaN & bNaN) begin // @[SPFPMul.scala 80:22]
      fusedReg_signBitA <= mulReg_signBitA; // @[SPFPMul.scala 87:16]
    end else if (aNaN) begin // @[SPFPMul.scala 88:21]
      fusedReg_signBitA <= mulReg_signBitA; // @[SPFPMul.scala 95:16]
    end else if (bNaN) begin // @[SPFPMul.scala 96:21]
      fusedReg_signBitA <= mulReg_signBitB; // @[SPFPMul.scala 103:16]
    end else begin
      fusedReg_signBitA <= _GEN_75;
    end
    if (reset) begin // @[SPFPMul.scala 152:25]
      fusedReg_signBitB <= 1'h0; // @[SPFPMul.scala 152:25]
    end else begin
      fusedReg_signBitB <= mulReg_signBitC; // @[SPFPMul.scala 158:23]
    end
    if (reset) begin // @[SPFPMul.scala 152:25]
      fusedReg_faultFlags <= 12'h0; // @[SPFPMul.scala 152:25]
    end else begin
      fusedReg_faultFlags <= mulReg_faultFlags; // @[SPFPMul.scala 159:23]
    end
    if (reset) begin // @[SPFPMul.scala 152:25]
      fusedReg_status_0 <= 1'h0; // @[SPFPMul.scala 152:25]
    end else begin
      fusedReg_status_0 <= mulReg_status_0; // @[SPFPMul.scala 160:23]
    end
    if (reset) begin // @[SPFPMul.scala 152:25]
      fusedReg_status_1 <= 1'h0; // @[SPFPMul.scala 152:25]
    end else begin
      fusedReg_status_1 <= mulReg_status_1; // @[SPFPMul.scala 160:23]
    end
    if (reset) begin // @[SPFPMul.scala 152:25]
      fusedReg_status_2 <= 1'h0; // @[SPFPMul.scala 152:25]
    end else begin
      fusedReg_status_2 <= mulReg_status_2; // @[SPFPMul.scala 160:23]
    end
    if (reset) begin // @[SPFPMul.scala 152:25]
      fusedReg_status_3 <= 1'h0; // @[SPFPMul.scala 152:25]
    end else begin
      fusedReg_status_3 <= mulReg_status_3; // @[SPFPMul.scala 160:23]
    end
    if (reset) begin // @[SPFPMul.scala 152:25]
      fusedReg_status_4 <= 1'h0; // @[SPFPMul.scala 152:25]
    end else if (aNaN & bNaN) begin // @[SPFPMul.scala 80:22]
      fusedReg_status_4 <= _GEN_12;
    end else if (aNaN) begin // @[SPFPMul.scala 88:21]
      fusedReg_status_4 <= _GEN_28;
    end else if (bNaN) begin // @[SPFPMul.scala 96:21]
      fusedReg_status_4 <= _GEN_44;
    end else begin
      fusedReg_status_4 <= _GEN_80;
    end
    if (reset) begin // @[SPFPMul.scala 152:25]
      fusedReg_status_5 <= 1'h0; // @[SPFPMul.scala 152:25]
    end else begin
      fusedReg_status_5 <= mulReg_status_5; // @[SPFPMul.scala 160:23]
    end
    if (reset) begin // @[SPFPMul.scala 152:25]
      fusedReg_status_6 <= 1'h0; // @[SPFPMul.scala 152:25]
    end else begin
      fusedReg_status_6 <= mulReg_status_6; // @[SPFPMul.scala 160:23]
    end
    if (reset) begin // @[SPFPMul.scala 152:25]
      fusedReg_status_7 <= 1'h0; // @[SPFPMul.scala 152:25]
    end else begin
      fusedReg_status_7 <= mulReg_status_7; // @[SPFPMul.scala 160:23]
    end
    if (reset) begin // @[SPFPMul.scala 152:25]
      fusedReg_operation <= 5'h0; // @[SPFPMul.scala 152:25]
    end else begin
      fusedReg_operation <= mulReg_operation; // @[SPFPMul.scala 161:23]
    end
    if (reset) begin // @[SPFPMul.scala 152:25]
      fusedReg_valid <= 1'h0; // @[SPFPMul.scala 152:25]
    end else begin
      fusedReg_valid <= mulReg_valid; // @[SPFPMul.scala 162:23]
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  mulReg_mantissaA = _RAND_0[25:0];
  _RAND_1 = {1{`RANDOM}};
  mulReg_mantissaB = _RAND_1[25:0];
  _RAND_2 = {1{`RANDOM}};
  mulReg_mantissaC = _RAND_2[25:0];
  _RAND_3 = {1{`RANDOM}};
  mulReg_exponentA = _RAND_3[7:0];
  _RAND_4 = {1{`RANDOM}};
  mulReg_exponentB = _RAND_4[7:0];
  _RAND_5 = {1{`RANDOM}};
  mulReg_exponentC = _RAND_5[7:0];
  _RAND_6 = {1{`RANDOM}};
  mulReg_signBitA = _RAND_6[0:0];
  _RAND_7 = {1{`RANDOM}};
  mulReg_signBitB = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  mulReg_signBitC = _RAND_8[0:0];
  _RAND_9 = {1{`RANDOM}};
  mulReg_faultFlags = _RAND_9[11:0];
  _RAND_10 = {1{`RANDOM}};
  mulReg_status_0 = _RAND_10[0:0];
  _RAND_11 = {1{`RANDOM}};
  mulReg_status_1 = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  mulReg_status_2 = _RAND_12[0:0];
  _RAND_13 = {1{`RANDOM}};
  mulReg_status_3 = _RAND_13[0:0];
  _RAND_14 = {1{`RANDOM}};
  mulReg_status_4 = _RAND_14[0:0];
  _RAND_15 = {1{`RANDOM}};
  mulReg_status_5 = _RAND_15[0:0];
  _RAND_16 = {1{`RANDOM}};
  mulReg_status_6 = _RAND_16[0:0];
  _RAND_17 = {1{`RANDOM}};
  mulReg_status_7 = _RAND_17[0:0];
  _RAND_18 = {1{`RANDOM}};
  mulReg_operation = _RAND_18[4:0];
  _RAND_19 = {1{`RANDOM}};
  mulReg_valid = _RAND_19[0:0];
  _RAND_20 = {1{`RANDOM}};
  fusedReg_mantissaA = _RAND_20[25:0];
  _RAND_21 = {1{`RANDOM}};
  fusedReg_mantissaB = _RAND_21[25:0];
  _RAND_22 = {1{`RANDOM}};
  fusedReg_exponentA = _RAND_22[7:0];
  _RAND_23 = {1{`RANDOM}};
  fusedReg_exponentB = _RAND_23[7:0];
  _RAND_24 = {1{`RANDOM}};
  fusedReg_signBitA = _RAND_24[0:0];
  _RAND_25 = {1{`RANDOM}};
  fusedReg_signBitB = _RAND_25[0:0];
  _RAND_26 = {1{`RANDOM}};
  fusedReg_faultFlags = _RAND_26[11:0];
  _RAND_27 = {1{`RANDOM}};
  fusedReg_status_0 = _RAND_27[0:0];
  _RAND_28 = {1{`RANDOM}};
  fusedReg_status_1 = _RAND_28[0:0];
  _RAND_29 = {1{`RANDOM}};
  fusedReg_status_2 = _RAND_29[0:0];
  _RAND_30 = {1{`RANDOM}};
  fusedReg_status_3 = _RAND_30[0:0];
  _RAND_31 = {1{`RANDOM}};
  fusedReg_status_4 = _RAND_31[0:0];
  _RAND_32 = {1{`RANDOM}};
  fusedReg_status_5 = _RAND_32[0:0];
  _RAND_33 = {1{`RANDOM}};
  fusedReg_status_6 = _RAND_33[0:0];
  _RAND_34 = {1{`RANDOM}};
  fusedReg_status_7 = _RAND_34[0:0];
  _RAND_35 = {1{`RANDOM}};
  fusedReg_operation = _RAND_35[4:0];
  _RAND_36 = {1{`RANDOM}};
  fusedReg_valid = _RAND_36[0:0];
`endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
module SPFPDiv(
  input         clock,
  input         reset,
  input         io_in_valid,
  input  [25:0] io_in_bits_a_mantissa,
  input  [7:0]  io_in_bits_a_exponent,
  input         io_in_bits_a_sign,
  input  [25:0] io_in_bits_b_mantissa,
  input  [7:0]  io_in_bits_b_exponent,
  input         io_in_bits_b_sign,
  input  [4:0]  io_in_bits_op,
  input         io_in_bits_fcsr_0,
  input         io_in_bits_fcsr_1,
  input         io_in_bits_fcsr_2,
  input         io_in_bits_fcsr_3,
  input         io_in_bits_fcsr_4,
  input         io_in_bits_fcsr_5,
  input         io_in_bits_fcsr_6,
  input         io_in_bits_fcsr_7,
  input  [11:0] io_in_bits_fflags,
  output        io_out_valid,
  output [25:0] io_out_bits_result_mantissa,
  output [7:0]  io_out_bits_result_exponent,
  output        io_out_bits_result_sign,
  output        io_out_bits_fcsr_0,
  output        io_out_bits_fcsr_1,
  output        io_out_bits_fcsr_2,
  output        io_out_bits_fcsr_3,
  output        io_out_bits_fcsr_4,
  output        io_out_bits_fcsr_5,
  output        io_out_bits_fcsr_6,
  output        io_out_bits_fcsr_7,
  output [4:0]  io_out_bits_op,
  output        io_out_bits_divInProc
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [31:0] _RAND_39;
  reg [31:0] _RAND_40;
  reg [31:0] _RAND_41;
  reg [31:0] _RAND_42;
  reg [31:0] _RAND_43;
  reg [31:0] _RAND_44;
  reg [31:0] _RAND_45;
  reg [31:0] _RAND_46;
  reg [31:0] _RAND_47;
  reg [31:0] _RAND_48;
  reg [31:0] _RAND_49;
  reg [31:0] _RAND_50;
  reg [31:0] _RAND_51;
  reg [31:0] _RAND_52;
  reg [31:0] _RAND_53;
  reg [31:0] _RAND_54;
  reg [31:0] _RAND_55;
  reg [31:0] _RAND_56;
  reg [31:0] _RAND_57;
  reg [31:0] _RAND_58;
  reg [31:0] _RAND_59;
  reg [31:0] _RAND_60;
  reg [31:0] _RAND_61;
  reg [31:0] _RAND_62;
  reg [31:0] _RAND_63;
  reg [31:0] _RAND_64;
  reg [31:0] _RAND_65;
  reg [31:0] _RAND_66;
  reg [31:0] _RAND_67;
  reg [31:0] _RAND_68;
  reg [31:0] _RAND_69;
  reg [31:0] _RAND_70;
  reg [31:0] _RAND_71;
  reg [31:0] _RAND_72;
  reg [31:0] _RAND_73;
  reg [31:0] _RAND_74;
  reg [31:0] _RAND_75;
  reg [31:0] _RAND_76;
  reg [31:0] _RAND_77;
  reg [31:0] _RAND_78;
  reg [31:0] _RAND_79;
  reg [31:0] _RAND_80;
  reg [31:0] _RAND_81;
  reg [31:0] _RAND_82;
  reg [31:0] _RAND_83;
  reg [31:0] _RAND_84;
  reg [31:0] _RAND_85;
  reg [31:0] _RAND_86;
  reg [31:0] _RAND_87;
  reg [31:0] _RAND_88;
  reg [31:0] _RAND_89;
  reg [31:0] _RAND_90;
  reg [31:0] _RAND_91;
  reg [31:0] _RAND_92;
  reg [31:0] _RAND_93;
  reg [31:0] _RAND_94;
  reg [31:0] _RAND_95;
  reg [31:0] _RAND_96;
  reg [31:0] _RAND_97;
  reg [31:0] _RAND_98;
  reg [31:0] _RAND_99;
  reg [31:0] _RAND_100;
  reg [31:0] _RAND_101;
  reg [31:0] _RAND_102;
  reg [31:0] _RAND_103;
  reg [31:0] _RAND_104;
  reg [31:0] _RAND_105;
  reg [31:0] _RAND_106;
  reg [31:0] _RAND_107;
  reg [31:0] _RAND_108;
  reg [31:0] _RAND_109;
  reg [31:0] _RAND_110;
  reg [31:0] _RAND_111;
  reg [31:0] _RAND_112;
  reg [31:0] _RAND_113;
  reg [31:0] _RAND_114;
  reg [31:0] _RAND_115;
  reg [31:0] _RAND_116;
  reg [31:0] _RAND_117;
  reg [31:0] _RAND_118;
  reg [31:0] _RAND_119;
  reg [31:0] _RAND_120;
  reg [31:0] _RAND_121;
  reg [31:0] _RAND_122;
  reg [31:0] _RAND_123;
  reg [31:0] _RAND_124;
  reg [31:0] _RAND_125;
  reg [31:0] _RAND_126;
  reg [31:0] _RAND_127;
  reg [31:0] _RAND_128;
  reg [31:0] _RAND_129;
  reg [31:0] _RAND_130;
  reg [31:0] _RAND_131;
  reg [31:0] _RAND_132;
  reg [31:0] _RAND_133;
  reg [31:0] _RAND_134;
  reg [31:0] _RAND_135;
  reg [31:0] _RAND_136;
  reg [31:0] _RAND_137;
  reg [31:0] _RAND_138;
  reg [31:0] _RAND_139;
  reg [31:0] _RAND_140;
  reg [31:0] _RAND_141;
  reg [31:0] _RAND_142;
  reg [31:0] _RAND_143;
  reg [31:0] _RAND_144;
  reg [31:0] _RAND_145;
  reg [31:0] _RAND_146;
  reg [31:0] _RAND_147;
  reg [31:0] _RAND_148;
  reg [31:0] _RAND_149;
  reg [31:0] _RAND_150;
  reg [31:0] _RAND_151;
  reg [31:0] _RAND_152;
  reg [31:0] _RAND_153;
  reg [31:0] _RAND_154;
  reg [31:0] _RAND_155;
  reg [31:0] _RAND_156;
  reg [31:0] _RAND_157;
  reg [31:0] _RAND_158;
  reg [31:0] _RAND_159;
  reg [31:0] _RAND_160;
  reg [31:0] _RAND_161;
  reg [31:0] _RAND_162;
  reg [31:0] _RAND_163;
  reg [31:0] _RAND_164;
  reg [31:0] _RAND_165;
  reg [31:0] _RAND_166;
  reg [31:0] _RAND_167;
  reg [31:0] _RAND_168;
  reg [31:0] _RAND_169;
  reg [31:0] _RAND_170;
  reg [31:0] _RAND_171;
  reg [31:0] _RAND_172;
  reg [31:0] _RAND_173;
  reg [31:0] _RAND_174;
  reg [31:0] _RAND_175;
  reg [31:0] _RAND_176;
  reg [31:0] _RAND_177;
  reg [31:0] _RAND_178;
  reg [31:0] _RAND_179;
  reg [31:0] _RAND_180;
  reg [31:0] _RAND_181;
  reg [31:0] _RAND_182;
  reg [31:0] _RAND_183;
  reg [31:0] _RAND_184;
  reg [31:0] _RAND_185;
  reg [31:0] _RAND_186;
  reg [31:0] _RAND_187;
  reg [31:0] _RAND_188;
  reg [31:0] _RAND_189;
  reg [31:0] _RAND_190;
  reg [31:0] _RAND_191;
  reg [31:0] _RAND_192;
  reg [31:0] _RAND_193;
  reg [31:0] _RAND_194;
  reg [31:0] _RAND_195;
  reg [31:0] _RAND_196;
  reg [31:0] _RAND_197;
  reg [31:0] _RAND_198;
  reg [31:0] _RAND_199;
  reg [31:0] _RAND_200;
  reg [31:0] _RAND_201;
  reg [31:0] _RAND_202;
  reg [31:0] _RAND_203;
  reg [31:0] _RAND_204;
  reg [31:0] _RAND_205;
  reg [31:0] _RAND_206;
  reg [31:0] _RAND_207;
  reg [31:0] _RAND_208;
  reg [31:0] _RAND_209;
  reg [31:0] _RAND_210;
  reg [31:0] _RAND_211;
  reg [31:0] _RAND_212;
  reg [31:0] _RAND_213;
  reg [31:0] _RAND_214;
  reg [31:0] _RAND_215;
  reg [31:0] _RAND_216;
  reg [31:0] _RAND_217;
  reg [31:0] _RAND_218;
  reg [31:0] _RAND_219;
  reg [31:0] _RAND_220;
  reg [31:0] _RAND_221;
  reg [31:0] _RAND_222;
  reg [31:0] _RAND_223;
  reg [31:0] _RAND_224;
  reg [31:0] _RAND_225;
  reg [31:0] _RAND_226;
  reg [31:0] _RAND_227;
  reg [31:0] _RAND_228;
  reg [31:0] _RAND_229;
  reg [31:0] _RAND_230;
  reg [31:0] _RAND_231;
  reg [31:0] _RAND_232;
  reg [31:0] _RAND_233;
  reg [31:0] _RAND_234;
  reg [31:0] _RAND_235;
  reg [31:0] _RAND_236;
  reg [31:0] _RAND_237;
  reg [31:0] _RAND_238;
  reg [31:0] _RAND_239;
  reg [31:0] _RAND_240;
  reg [31:0] _RAND_241;
  reg [31:0] _RAND_242;
  reg [31:0] _RAND_243;
  reg [31:0] _RAND_244;
  reg [31:0] _RAND_245;
  reg [31:0] _RAND_246;
  reg [31:0] _RAND_247;
  reg [31:0] _RAND_248;
  reg [31:0] _RAND_249;
  reg [31:0] _RAND_250;
  reg [31:0] _RAND_251;
  reg [31:0] _RAND_252;
  reg [31:0] _RAND_253;
  reg [31:0] _RAND_254;
  reg [31:0] _RAND_255;
  reg [31:0] _RAND_256;
  reg [31:0] _RAND_257;
  reg [31:0] _RAND_258;
  reg [31:0] _RAND_259;
  reg [31:0] _RAND_260;
  reg [31:0] _RAND_261;
  reg [31:0] _RAND_262;
  reg [31:0] _RAND_263;
  reg [31:0] _RAND_264;
  reg [31:0] _RAND_265;
  reg [31:0] _RAND_266;
  reg [31:0] _RAND_267;
  reg [31:0] _RAND_268;
  reg [31:0] _RAND_269;
  reg [31:0] _RAND_270;
  reg [31:0] _RAND_271;
  reg [31:0] _RAND_272;
  reg [31:0] _RAND_273;
  reg [31:0] _RAND_274;
  reg [31:0] _RAND_275;
  reg [31:0] _RAND_276;
  reg [31:0] _RAND_277;
  reg [31:0] _RAND_278;
  reg [31:0] _RAND_279;
  reg [31:0] _RAND_280;
  reg [31:0] _RAND_281;
  reg [31:0] _RAND_282;
  reg [31:0] _RAND_283;
  reg [31:0] _RAND_284;
  reg [31:0] _RAND_285;
  reg [31:0] _RAND_286;
  reg [31:0] _RAND_287;
  reg [31:0] _RAND_288;
  reg [31:0] _RAND_289;
  reg [31:0] _RAND_290;
  reg [31:0] _RAND_291;
  reg [31:0] _RAND_292;
  reg [31:0] _RAND_293;
  reg [31:0] _RAND_294;
  reg [31:0] _RAND_295;
  reg [31:0] _RAND_296;
  reg [31:0] _RAND_297;
  reg [31:0] _RAND_298;
  reg [31:0] _RAND_299;
  reg [31:0] _RAND_300;
  reg [31:0] _RAND_301;
  reg [31:0] _RAND_302;
  reg [31:0] _RAND_303;
  reg [31:0] _RAND_304;
  reg [31:0] _RAND_305;
  reg [31:0] _RAND_306;
  reg [31:0] _RAND_307;
  reg [31:0] _RAND_308;
  reg [31:0] _RAND_309;
  reg [31:0] _RAND_310;
  reg [31:0] _RAND_311;
  reg [31:0] _RAND_312;
  reg [31:0] _RAND_313;
  reg [31:0] _RAND_314;
  reg [31:0] _RAND_315;
  reg [31:0] _RAND_316;
  reg [31:0] _RAND_317;
  reg [31:0] _RAND_318;
  reg [31:0] _RAND_319;
  reg [31:0] _RAND_320;
  reg [31:0] _RAND_321;
  reg [31:0] _RAND_322;
  reg [31:0] _RAND_323;
  reg [31:0] _RAND_324;
  reg [31:0] _RAND_325;
  reg [31:0] _RAND_326;
  reg [31:0] _RAND_327;
  reg [31:0] _RAND_328;
  reg [31:0] _RAND_329;
  reg [31:0] _RAND_330;
  reg [31:0] _RAND_331;
  reg [31:0] _RAND_332;
  reg [31:0] _RAND_333;
  reg [31:0] _RAND_334;
  reg [31:0] _RAND_335;
  reg [31:0] _RAND_336;
  reg [31:0] _RAND_337;
  reg [31:0] _RAND_338;
  reg [31:0] _RAND_339;
  reg [31:0] _RAND_340;
  reg [31:0] _RAND_341;
  reg [31:0] _RAND_342;
  reg [31:0] _RAND_343;
  reg [31:0] _RAND_344;
  reg [31:0] _RAND_345;
  reg [31:0] _RAND_346;
  reg [31:0] _RAND_347;
  reg [31:0] _RAND_348;
  reg [31:0] _RAND_349;
  reg [31:0] _RAND_350;
  reg [31:0] _RAND_351;
  reg [31:0] _RAND_352;
  reg [31:0] _RAND_353;
  reg [31:0] _RAND_354;
  reg [31:0] _RAND_355;
  reg [31:0] _RAND_356;
  reg [31:0] _RAND_357;
  reg [31:0] _RAND_358;
  reg [31:0] _RAND_359;
  reg [31:0] _RAND_360;
  reg [31:0] _RAND_361;
  reg [31:0] _RAND_362;
  reg [31:0] _RAND_363;
  reg [31:0] _RAND_364;
  reg [31:0] _RAND_365;
  reg [31:0] _RAND_366;
  reg [31:0] _RAND_367;
  reg [31:0] _RAND_368;
  reg [31:0] _RAND_369;
  reg [31:0] _RAND_370;
  reg [31:0] _RAND_371;
  reg [31:0] _RAND_372;
  reg [31:0] _RAND_373;
  reg [31:0] _RAND_374;
  reg [31:0] _RAND_375;
  reg [31:0] _RAND_376;
  reg [31:0] _RAND_377;
  reg [31:0] _RAND_378;
  reg [31:0] _RAND_379;
  reg [31:0] _RAND_380;
  reg [31:0] _RAND_381;
  reg [31:0] _RAND_382;
  reg [31:0] _RAND_383;
  reg [31:0] _RAND_384;
  reg [31:0] _RAND_385;
  reg [31:0] _RAND_386;
  reg [31:0] _RAND_387;
  reg [31:0] _RAND_388;
  reg [31:0] _RAND_389;
  reg [31:0] _RAND_390;
  reg [31:0] _RAND_391;
  reg [31:0] _RAND_392;
  reg [31:0] _RAND_393;
  reg [31:0] _RAND_394;
  reg [31:0] _RAND_395;
  reg [31:0] _RAND_396;
  reg [31:0] _RAND_397;
  reg [31:0] _RAND_398;
  reg [31:0] _RAND_399;
  reg [31:0] _RAND_400;
  reg [31:0] _RAND_401;
  reg [31:0] _RAND_402;
  reg [31:0] _RAND_403;
  reg [31:0] _RAND_404;
  reg [31:0] _RAND_405;
  reg [31:0] _RAND_406;
  reg [31:0] _RAND_407;
  reg [31:0] _RAND_408;
  reg [31:0] _RAND_409;
  reg [31:0] _RAND_410;
  reg [31:0] _RAND_411;
  reg [31:0] _RAND_412;
  reg [31:0] _RAND_413;
  reg [31:0] _RAND_414;
  reg [31:0] _RAND_415;
  reg [31:0] _RAND_416;
  reg [31:0] _RAND_417;
  reg [31:0] _RAND_418;
  reg [31:0] _RAND_419;
  reg [31:0] _RAND_420;
  reg [31:0] _RAND_421;
  reg [31:0] _RAND_422;
  reg [31:0] _RAND_423;
  reg [31:0] _RAND_424;
  reg [31:0] _RAND_425;
  reg [31:0] _RAND_426;
  reg [31:0] _RAND_427;
  reg [31:0] _RAND_428;
  reg [31:0] _RAND_429;
  reg [31:0] _RAND_430;
  reg [31:0] _RAND_431;
  reg [31:0] _RAND_432;
  reg [31:0] _RAND_433;
  reg [31:0] _RAND_434;
  reg [31:0] _RAND_435;
  reg [31:0] _RAND_436;
  reg [31:0] _RAND_437;
  reg [31:0] _RAND_438;
  reg [31:0] _RAND_439;
  reg [31:0] _RAND_440;
  reg [31:0] _RAND_441;
  reg [31:0] _RAND_442;
  reg [31:0] _RAND_443;
  reg [31:0] _RAND_444;
  reg [31:0] _RAND_445;
  reg [31:0] _RAND_446;
  reg [31:0] _RAND_447;
  reg [31:0] _RAND_448;
  reg [31:0] _RAND_449;
  reg [31:0] _RAND_450;
  reg [31:0] _RAND_451;
  reg [31:0] _RAND_452;
  reg [31:0] _RAND_453;
  reg [31:0] _RAND_454;
  reg [31:0] _RAND_455;
  reg [31:0] _RAND_456;
  reg [31:0] _RAND_457;
  reg [31:0] _RAND_458;
  reg [31:0] _RAND_459;
  reg [31:0] _RAND_460;
  reg [31:0] _RAND_461;
  reg [31:0] _RAND_462;
  reg [31:0] _RAND_463;
  reg [31:0] _RAND_464;
  reg [31:0] _RAND_465;
  reg [31:0] _RAND_466;
  reg [31:0] _RAND_467;
  reg [31:0] _RAND_468;
  reg [31:0] _RAND_469;
  reg [31:0] _RAND_470;
  reg [31:0] _RAND_471;
  reg [31:0] _RAND_472;
  reg [31:0] _RAND_473;
  reg [31:0] _RAND_474;
  reg [31:0] _RAND_475;
  reg [31:0] _RAND_476;
  reg [31:0] _RAND_477;
  reg [31:0] _RAND_478;
  reg [31:0] _RAND_479;
  reg [31:0] _RAND_480;
  reg [31:0] _RAND_481;
  reg [31:0] _RAND_482;
  reg [31:0] _RAND_483;
  reg [31:0] _RAND_484;
  reg [31:0] _RAND_485;
  reg [31:0] _RAND_486;
  reg [31:0] _RAND_487;
  reg [31:0] _RAND_488;
  reg [31:0] _RAND_489;
  reg [31:0] _RAND_490;
  reg [31:0] _RAND_491;
  reg [31:0] _RAND_492;
`endif // RANDOMIZE_REG_INIT
  reg [25:0] divReg_0_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_0_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_0_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_0_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_0_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_0_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_0_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_0_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_0_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_0_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_0_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_0_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_0_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_0_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_0_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_0_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_0_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_0_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_1_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_1_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_1_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_1_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_1_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_1_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_1_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_1_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_1_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_1_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_1_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_1_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_1_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_1_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_1_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_1_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_1_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_1_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_1_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_2_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_2_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_2_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_2_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_2_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_2_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_2_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_2_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_2_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_2_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_2_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_2_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_2_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_2_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_2_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_2_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_2_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_2_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_2_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_3_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_3_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_3_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_3_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_3_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_3_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_3_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_3_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_3_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_3_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_3_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_3_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_3_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_3_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_3_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_3_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_3_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_3_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_3_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_4_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_4_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_4_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_4_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_4_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_4_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_4_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_4_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_4_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_4_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_4_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_4_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_4_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_4_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_4_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_4_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_4_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_4_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_4_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_5_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_5_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_5_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_5_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_5_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_5_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_5_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_5_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_5_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_5_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_5_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_5_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_5_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_5_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_5_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_5_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_5_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_5_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_5_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_6_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_6_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_6_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_6_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_6_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_6_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_6_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_6_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_6_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_6_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_6_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_6_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_6_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_6_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_6_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_6_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_6_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_6_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_6_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_7_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_7_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_7_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_7_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_7_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_7_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_7_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_7_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_7_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_7_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_7_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_7_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_7_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_7_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_7_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_7_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_7_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_7_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_7_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_8_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_8_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_8_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_8_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_8_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_8_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_8_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_8_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_8_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_8_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_8_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_8_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_8_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_8_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_8_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_8_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_8_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_8_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_8_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_9_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_9_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_9_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_9_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_9_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_9_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_9_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_9_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_9_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_9_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_9_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_9_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_9_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_9_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_9_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_9_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_9_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_9_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_9_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_10_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_10_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_10_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_10_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_10_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_10_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_10_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_10_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_10_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_10_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_10_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_10_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_10_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_10_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_10_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_10_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_10_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_10_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_10_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_11_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_11_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_11_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_11_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_11_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_11_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_11_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_11_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_11_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_11_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_11_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_11_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_11_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_11_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_11_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_11_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_11_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_11_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_11_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_12_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_12_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_12_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_12_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_12_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_12_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_12_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_12_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_12_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_12_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_12_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_12_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_12_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_12_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_12_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_12_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_12_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_12_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_12_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_13_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_13_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_13_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_13_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_13_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_13_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_13_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_13_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_13_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_13_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_13_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_13_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_13_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_13_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_13_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_13_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_13_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_13_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_13_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_14_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_14_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_14_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_14_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_14_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_14_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_14_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_14_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_14_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_14_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_14_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_14_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_14_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_14_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_14_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_14_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_14_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_14_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_14_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_15_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_15_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_15_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_15_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_15_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_15_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_15_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_15_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_15_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_15_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_15_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_15_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_15_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_15_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_15_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_15_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_15_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_15_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_15_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_16_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_16_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_16_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_16_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_16_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_16_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_16_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_16_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_16_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_16_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_16_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_16_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_16_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_16_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_16_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_16_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_16_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_16_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_16_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_17_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_17_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_17_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_17_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_17_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_17_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_17_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_17_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_17_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_17_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_17_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_17_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_17_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_17_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_17_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_17_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_17_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_17_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_17_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_18_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_18_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_18_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_18_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_18_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_18_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_18_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_18_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_18_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_18_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_18_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_18_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_18_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_18_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_18_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_18_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_18_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_18_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_18_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_19_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_19_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_19_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_19_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_19_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_19_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_19_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_19_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_19_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_19_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_19_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_19_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_19_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_19_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_19_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_19_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_19_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_19_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_19_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_20_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_20_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_20_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_20_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_20_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_20_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_20_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_20_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_20_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_20_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_20_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_20_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_20_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_20_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_20_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_20_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_20_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_20_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_20_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_21_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_21_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_21_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_21_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_21_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_21_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_21_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_21_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_21_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_21_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_21_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_21_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_21_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_21_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_21_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_21_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_21_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_21_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_21_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_22_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_22_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_22_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_22_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_22_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_22_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_22_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_22_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_22_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_22_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_22_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_22_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_22_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_22_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_22_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_22_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_22_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_22_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_22_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_23_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_23_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_23_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_23_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_23_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_23_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_23_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_23_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_23_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_23_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_23_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_23_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_23_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_23_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_23_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_23_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_23_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_23_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_23_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_24_mantissaA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_24_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_24_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_24_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_24_exponentB; // @[SPFPDiv.scala 44:23]
  reg  divReg_24_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_24_signBitB; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_24_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_24_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_24_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_24_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_24_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_24_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_24_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_24_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_24_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_24_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_24_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_24_exMantA; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_25_mantissaB; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_25_mantissaC; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_25_exponentA; // @[SPFPDiv.scala 44:23]
  reg [7:0] divReg_25_exponentC; // @[SPFPDiv.scala 44:23]
  reg  divReg_25_signBitA; // @[SPFPDiv.scala 44:23]
  reg  divReg_25_signBitB; // @[SPFPDiv.scala 44:23]
  reg  divReg_25_signBitC; // @[SPFPDiv.scala 44:23]
  reg [11:0] divReg_25_faultFlags; // @[SPFPDiv.scala 44:23]
  reg  divReg_25_status_0; // @[SPFPDiv.scala 44:23]
  reg  divReg_25_status_1; // @[SPFPDiv.scala 44:23]
  reg  divReg_25_status_2; // @[SPFPDiv.scala 44:23]
  reg  divReg_25_status_3; // @[SPFPDiv.scala 44:23]
  reg  divReg_25_status_4; // @[SPFPDiv.scala 44:23]
  reg  divReg_25_status_5; // @[SPFPDiv.scala 44:23]
  reg  divReg_25_status_6; // @[SPFPDiv.scala 44:23]
  reg  divReg_25_status_7; // @[SPFPDiv.scala 44:23]
  reg [4:0] divReg_25_op; // @[SPFPDiv.scala 44:23]
  reg  divReg_25_valid; // @[SPFPDiv.scala 44:23]
  reg [25:0] divReg_25_exMantA; // @[SPFPDiv.scala 44:23]
  wire [26:0] _T_1 = {1'h0,divReg_0_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_3 = {1'h0,divReg_0_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_6 = $signed(_T_1) - $signed(_T_3); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_1_mantissaA_T = {divReg_0_mantissaA, 1'h0}; // @[SPFPDiv.scala 66:52]
  wire [25:0] _divReg_1_mantissaA_T_1 = ~divReg_0_mantissaB; // @[SPFPDiv.scala 69:57]
  wire [25:0] _divReg_1_mantissaA_T_3 = _divReg_1_mantissaA_T_1 + 26'h1; // @[SPFPDiv.scala 69:78]
  wire [26:0] _divReg_1_mantissaA_T_4 = divReg_0_mantissaA + _divReg_1_mantissaA_T_3; // @[SPFPDiv.scala 69:53]
  wire [27:0] _divReg_1_mantissaA_T_5 = {_divReg_1_mantissaA_T_4, 1'h0}; // @[SPFPDiv.scala 69:86]
  wire [27:0] _GEN_0 = $signed(_T_6) < 27'sh0 ? {{1'd0}, _divReg_1_mantissaA_T} : _divReg_1_mantissaA_T_5; // @[SPFPDiv.scala 65:105 66:29 69:29]
  wire [26:0] _GEN_1 = $signed(_T_6) < 27'sh0 ? 27'h0 : 27'h1; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [26:0] _T_9 = {1'h0,divReg_1_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_11 = {1'h0,divReg_1_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_14 = $signed(_T_9) - $signed(_T_11); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_2_mantissaA_T = {divReg_1_mantissaA, 1'h0}; // @[SPFPDiv.scala 66:52]
  wire [26:0] _divReg_2_mantissaC_T = {divReg_1_mantissaC, 1'h0}; // @[SPFPDiv.scala 67:52]
  wire [25:0] _divReg_2_mantissaA_T_1 = ~divReg_1_mantissaB; // @[SPFPDiv.scala 69:57]
  wire [25:0] _divReg_2_mantissaA_T_3 = _divReg_2_mantissaA_T_1 + 26'h1; // @[SPFPDiv.scala 69:78]
  wire [26:0] _divReg_2_mantissaA_T_4 = divReg_1_mantissaA + _divReg_2_mantissaA_T_3; // @[SPFPDiv.scala 69:53]
  wire [27:0] _divReg_2_mantissaA_T_5 = {_divReg_2_mantissaA_T_4, 1'h0}; // @[SPFPDiv.scala 69:86]
  wire [25:0] _divReg_2_mantissaC_T_2 = {divReg_1_mantissaC[24:0],1'h1}; // @[Cat.scala 33:92]
  wire [27:0] _GEN_2 = $signed(_T_14) < 27'sh0 ? {{1'd0}, _divReg_2_mantissaA_T} : _divReg_2_mantissaA_T_5; // @[SPFPDiv.scala 65:105 66:29 69:29]
  wire [26:0] _GEN_3 = $signed(_T_14) < 27'sh0 ? _divReg_2_mantissaC_T : {{1'd0}, _divReg_2_mantissaC_T_2}; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [26:0] _T_17 = {1'h0,divReg_2_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_19 = {1'h0,divReg_2_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_22 = $signed(_T_17) - $signed(_T_19); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_3_mantissaA_T = {divReg_2_mantissaA, 1'h0}; // @[SPFPDiv.scala 66:52]
  wire [26:0] _divReg_3_mantissaC_T = {divReg_2_mantissaC, 1'h0}; // @[SPFPDiv.scala 67:52]
  wire [25:0] _divReg_3_mantissaA_T_1 = ~divReg_2_mantissaB; // @[SPFPDiv.scala 69:57]
  wire [25:0] _divReg_3_mantissaA_T_3 = _divReg_3_mantissaA_T_1 + 26'h1; // @[SPFPDiv.scala 69:78]
  wire [26:0] _divReg_3_mantissaA_T_4 = divReg_2_mantissaA + _divReg_3_mantissaA_T_3; // @[SPFPDiv.scala 69:53]
  wire [27:0] _divReg_3_mantissaA_T_5 = {_divReg_3_mantissaA_T_4, 1'h0}; // @[SPFPDiv.scala 69:86]
  wire [25:0] _divReg_3_mantissaC_T_2 = {divReg_2_mantissaC[24:0],1'h1}; // @[Cat.scala 33:92]
  wire [27:0] _GEN_4 = $signed(_T_22) < 27'sh0 ? {{1'd0}, _divReg_3_mantissaA_T} : _divReg_3_mantissaA_T_5; // @[SPFPDiv.scala 65:105 66:29 69:29]
  wire [26:0] _GEN_5 = $signed(_T_22) < 27'sh0 ? _divReg_3_mantissaC_T : {{1'd0}, _divReg_3_mantissaC_T_2}; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [26:0] _T_25 = {1'h0,divReg_3_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_27 = {1'h0,divReg_3_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_30 = $signed(_T_25) - $signed(_T_27); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_4_mantissaA_T = {divReg_3_mantissaA, 1'h0}; // @[SPFPDiv.scala 66:52]
  wire [26:0] _divReg_4_mantissaC_T = {divReg_3_mantissaC, 1'h0}; // @[SPFPDiv.scala 67:52]
  wire [25:0] _divReg_4_mantissaA_T_1 = ~divReg_3_mantissaB; // @[SPFPDiv.scala 69:57]
  wire [25:0] _divReg_4_mantissaA_T_3 = _divReg_4_mantissaA_T_1 + 26'h1; // @[SPFPDiv.scala 69:78]
  wire [26:0] _divReg_4_mantissaA_T_4 = divReg_3_mantissaA + _divReg_4_mantissaA_T_3; // @[SPFPDiv.scala 69:53]
  wire [27:0] _divReg_4_mantissaA_T_5 = {_divReg_4_mantissaA_T_4, 1'h0}; // @[SPFPDiv.scala 69:86]
  wire [25:0] _divReg_4_mantissaC_T_2 = {divReg_3_mantissaC[24:0],1'h1}; // @[Cat.scala 33:92]
  wire [27:0] _GEN_6 = $signed(_T_30) < 27'sh0 ? {{1'd0}, _divReg_4_mantissaA_T} : _divReg_4_mantissaA_T_5; // @[SPFPDiv.scala 65:105 66:29 69:29]
  wire [26:0] _GEN_7 = $signed(_T_30) < 27'sh0 ? _divReg_4_mantissaC_T : {{1'd0}, _divReg_4_mantissaC_T_2}; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [26:0] _T_33 = {1'h0,divReg_4_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_35 = {1'h0,divReg_4_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_38 = $signed(_T_33) - $signed(_T_35); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_5_mantissaA_T = {divReg_4_mantissaA, 1'h0}; // @[SPFPDiv.scala 66:52]
  wire [26:0] _divReg_5_mantissaC_T = {divReg_4_mantissaC, 1'h0}; // @[SPFPDiv.scala 67:52]
  wire [25:0] _divReg_5_mantissaA_T_1 = ~divReg_4_mantissaB; // @[SPFPDiv.scala 69:57]
  wire [25:0] _divReg_5_mantissaA_T_3 = _divReg_5_mantissaA_T_1 + 26'h1; // @[SPFPDiv.scala 69:78]
  wire [26:0] _divReg_5_mantissaA_T_4 = divReg_4_mantissaA + _divReg_5_mantissaA_T_3; // @[SPFPDiv.scala 69:53]
  wire [27:0] _divReg_5_mantissaA_T_5 = {_divReg_5_mantissaA_T_4, 1'h0}; // @[SPFPDiv.scala 69:86]
  wire [25:0] _divReg_5_mantissaC_T_2 = {divReg_4_mantissaC[24:0],1'h1}; // @[Cat.scala 33:92]
  wire [27:0] _GEN_8 = $signed(_T_38) < 27'sh0 ? {{1'd0}, _divReg_5_mantissaA_T} : _divReg_5_mantissaA_T_5; // @[SPFPDiv.scala 65:105 66:29 69:29]
  wire [26:0] _GEN_9 = $signed(_T_38) < 27'sh0 ? _divReg_5_mantissaC_T : {{1'd0}, _divReg_5_mantissaC_T_2}; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [26:0] _T_41 = {1'h0,divReg_5_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_43 = {1'h0,divReg_5_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_46 = $signed(_T_41) - $signed(_T_43); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_6_mantissaA_T = {divReg_5_mantissaA, 1'h0}; // @[SPFPDiv.scala 66:52]
  wire [26:0] _divReg_6_mantissaC_T = {divReg_5_mantissaC, 1'h0}; // @[SPFPDiv.scala 67:52]
  wire [25:0] _divReg_6_mantissaA_T_1 = ~divReg_5_mantissaB; // @[SPFPDiv.scala 69:57]
  wire [25:0] _divReg_6_mantissaA_T_3 = _divReg_6_mantissaA_T_1 + 26'h1; // @[SPFPDiv.scala 69:78]
  wire [26:0] _divReg_6_mantissaA_T_4 = divReg_5_mantissaA + _divReg_6_mantissaA_T_3; // @[SPFPDiv.scala 69:53]
  wire [27:0] _divReg_6_mantissaA_T_5 = {_divReg_6_mantissaA_T_4, 1'h0}; // @[SPFPDiv.scala 69:86]
  wire [25:0] _divReg_6_mantissaC_T_2 = {divReg_5_mantissaC[24:0],1'h1}; // @[Cat.scala 33:92]
  wire [27:0] _GEN_10 = $signed(_T_46) < 27'sh0 ? {{1'd0}, _divReg_6_mantissaA_T} : _divReg_6_mantissaA_T_5; // @[SPFPDiv.scala 65:105 66:29 69:29]
  wire [26:0] _GEN_11 = $signed(_T_46) < 27'sh0 ? _divReg_6_mantissaC_T : {{1'd0}, _divReg_6_mantissaC_T_2}; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [26:0] _T_49 = {1'h0,divReg_6_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_51 = {1'h0,divReg_6_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_54 = $signed(_T_49) - $signed(_T_51); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_7_mantissaA_T = {divReg_6_mantissaA, 1'h0}; // @[SPFPDiv.scala 66:52]
  wire [26:0] _divReg_7_mantissaC_T = {divReg_6_mantissaC, 1'h0}; // @[SPFPDiv.scala 67:52]
  wire [25:0] _divReg_7_mantissaA_T_1 = ~divReg_6_mantissaB; // @[SPFPDiv.scala 69:57]
  wire [25:0] _divReg_7_mantissaA_T_3 = _divReg_7_mantissaA_T_1 + 26'h1; // @[SPFPDiv.scala 69:78]
  wire [26:0] _divReg_7_mantissaA_T_4 = divReg_6_mantissaA + _divReg_7_mantissaA_T_3; // @[SPFPDiv.scala 69:53]
  wire [27:0] _divReg_7_mantissaA_T_5 = {_divReg_7_mantissaA_T_4, 1'h0}; // @[SPFPDiv.scala 69:86]
  wire [25:0] _divReg_7_mantissaC_T_2 = {divReg_6_mantissaC[24:0],1'h1}; // @[Cat.scala 33:92]
  wire [27:0] _GEN_12 = $signed(_T_54) < 27'sh0 ? {{1'd0}, _divReg_7_mantissaA_T} : _divReg_7_mantissaA_T_5; // @[SPFPDiv.scala 65:105 66:29 69:29]
  wire [26:0] _GEN_13 = $signed(_T_54) < 27'sh0 ? _divReg_7_mantissaC_T : {{1'd0}, _divReg_7_mantissaC_T_2}; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [26:0] _T_57 = {1'h0,divReg_7_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_59 = {1'h0,divReg_7_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_62 = $signed(_T_57) - $signed(_T_59); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_8_mantissaA_T = {divReg_7_mantissaA, 1'h0}; // @[SPFPDiv.scala 66:52]
  wire [26:0] _divReg_8_mantissaC_T = {divReg_7_mantissaC, 1'h0}; // @[SPFPDiv.scala 67:52]
  wire [25:0] _divReg_8_mantissaA_T_1 = ~divReg_7_mantissaB; // @[SPFPDiv.scala 69:57]
  wire [25:0] _divReg_8_mantissaA_T_3 = _divReg_8_mantissaA_T_1 + 26'h1; // @[SPFPDiv.scala 69:78]
  wire [26:0] _divReg_8_mantissaA_T_4 = divReg_7_mantissaA + _divReg_8_mantissaA_T_3; // @[SPFPDiv.scala 69:53]
  wire [27:0] _divReg_8_mantissaA_T_5 = {_divReg_8_mantissaA_T_4, 1'h0}; // @[SPFPDiv.scala 69:86]
  wire [25:0] _divReg_8_mantissaC_T_2 = {divReg_7_mantissaC[24:0],1'h1}; // @[Cat.scala 33:92]
  wire [27:0] _GEN_14 = $signed(_T_62) < 27'sh0 ? {{1'd0}, _divReg_8_mantissaA_T} : _divReg_8_mantissaA_T_5; // @[SPFPDiv.scala 65:105 66:29 69:29]
  wire [26:0] _GEN_15 = $signed(_T_62) < 27'sh0 ? _divReg_8_mantissaC_T : {{1'd0}, _divReg_8_mantissaC_T_2}; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [26:0] _T_65 = {1'h0,divReg_8_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_67 = {1'h0,divReg_8_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_70 = $signed(_T_65) - $signed(_T_67); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_9_mantissaA_T = {divReg_8_mantissaA, 1'h0}; // @[SPFPDiv.scala 66:52]
  wire [26:0] _divReg_9_mantissaC_T = {divReg_8_mantissaC, 1'h0}; // @[SPFPDiv.scala 67:52]
  wire [25:0] _divReg_9_mantissaA_T_1 = ~divReg_8_mantissaB; // @[SPFPDiv.scala 69:57]
  wire [25:0] _divReg_9_mantissaA_T_3 = _divReg_9_mantissaA_T_1 + 26'h1; // @[SPFPDiv.scala 69:78]
  wire [26:0] _divReg_9_mantissaA_T_4 = divReg_8_mantissaA + _divReg_9_mantissaA_T_3; // @[SPFPDiv.scala 69:53]
  wire [27:0] _divReg_9_mantissaA_T_5 = {_divReg_9_mantissaA_T_4, 1'h0}; // @[SPFPDiv.scala 69:86]
  wire [25:0] _divReg_9_mantissaC_T_2 = {divReg_8_mantissaC[24:0],1'h1}; // @[Cat.scala 33:92]
  wire [27:0] _GEN_16 = $signed(_T_70) < 27'sh0 ? {{1'd0}, _divReg_9_mantissaA_T} : _divReg_9_mantissaA_T_5; // @[SPFPDiv.scala 65:105 66:29 69:29]
  wire [26:0] _GEN_17 = $signed(_T_70) < 27'sh0 ? _divReg_9_mantissaC_T : {{1'd0}, _divReg_9_mantissaC_T_2}; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [26:0] _T_73 = {1'h0,divReg_9_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_75 = {1'h0,divReg_9_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_78 = $signed(_T_73) - $signed(_T_75); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_10_mantissaA_T = {divReg_9_mantissaA, 1'h0}; // @[SPFPDiv.scala 66:52]
  wire [26:0] _divReg_10_mantissaC_T = {divReg_9_mantissaC, 1'h0}; // @[SPFPDiv.scala 67:52]
  wire [25:0] _divReg_10_mantissaA_T_1 = ~divReg_9_mantissaB; // @[SPFPDiv.scala 69:57]
  wire [25:0] _divReg_10_mantissaA_T_3 = _divReg_10_mantissaA_T_1 + 26'h1; // @[SPFPDiv.scala 69:78]
  wire [26:0] _divReg_10_mantissaA_T_4 = divReg_9_mantissaA + _divReg_10_mantissaA_T_3; // @[SPFPDiv.scala 69:53]
  wire [27:0] _divReg_10_mantissaA_T_5 = {_divReg_10_mantissaA_T_4, 1'h0}; // @[SPFPDiv.scala 69:86]
  wire [25:0] _divReg_10_mantissaC_T_2 = {divReg_9_mantissaC[24:0],1'h1}; // @[Cat.scala 33:92]
  wire [27:0] _GEN_18 = $signed(_T_78) < 27'sh0 ? {{1'd0}, _divReg_10_mantissaA_T} : _divReg_10_mantissaA_T_5; // @[SPFPDiv.scala 65:105 66:29 69:29]
  wire [26:0] _GEN_19 = $signed(_T_78) < 27'sh0 ? _divReg_10_mantissaC_T : {{1'd0}, _divReg_10_mantissaC_T_2}; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [26:0] _T_81 = {1'h0,divReg_10_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_83 = {1'h0,divReg_10_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_86 = $signed(_T_81) - $signed(_T_83); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_11_mantissaA_T = {divReg_10_mantissaA, 1'h0}; // @[SPFPDiv.scala 66:52]
  wire [26:0] _divReg_11_mantissaC_T = {divReg_10_mantissaC, 1'h0}; // @[SPFPDiv.scala 67:52]
  wire [25:0] _divReg_11_mantissaA_T_1 = ~divReg_10_mantissaB; // @[SPFPDiv.scala 69:57]
  wire [25:0] _divReg_11_mantissaA_T_3 = _divReg_11_mantissaA_T_1 + 26'h1; // @[SPFPDiv.scala 69:78]
  wire [26:0] _divReg_11_mantissaA_T_4 = divReg_10_mantissaA + _divReg_11_mantissaA_T_3; // @[SPFPDiv.scala 69:53]
  wire [27:0] _divReg_11_mantissaA_T_5 = {_divReg_11_mantissaA_T_4, 1'h0}; // @[SPFPDiv.scala 69:86]
  wire [25:0] _divReg_11_mantissaC_T_2 = {divReg_10_mantissaC[24:0],1'h1}; // @[Cat.scala 33:92]
  wire [27:0] _GEN_20 = $signed(_T_86) < 27'sh0 ? {{1'd0}, _divReg_11_mantissaA_T} : _divReg_11_mantissaA_T_5; // @[SPFPDiv.scala 65:105 66:29 69:29]
  wire [26:0] _GEN_21 = $signed(_T_86) < 27'sh0 ? _divReg_11_mantissaC_T : {{1'd0}, _divReg_11_mantissaC_T_2}; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [26:0] _T_89 = {1'h0,divReg_11_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_91 = {1'h0,divReg_11_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_94 = $signed(_T_89) - $signed(_T_91); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_12_mantissaA_T = {divReg_11_mantissaA, 1'h0}; // @[SPFPDiv.scala 66:52]
  wire [26:0] _divReg_12_mantissaC_T = {divReg_11_mantissaC, 1'h0}; // @[SPFPDiv.scala 67:52]
  wire [25:0] _divReg_12_mantissaA_T_1 = ~divReg_11_mantissaB; // @[SPFPDiv.scala 69:57]
  wire [25:0] _divReg_12_mantissaA_T_3 = _divReg_12_mantissaA_T_1 + 26'h1; // @[SPFPDiv.scala 69:78]
  wire [26:0] _divReg_12_mantissaA_T_4 = divReg_11_mantissaA + _divReg_12_mantissaA_T_3; // @[SPFPDiv.scala 69:53]
  wire [27:0] _divReg_12_mantissaA_T_5 = {_divReg_12_mantissaA_T_4, 1'h0}; // @[SPFPDiv.scala 69:86]
  wire [25:0] _divReg_12_mantissaC_T_2 = {divReg_11_mantissaC[24:0],1'h1}; // @[Cat.scala 33:92]
  wire [27:0] _GEN_22 = $signed(_T_94) < 27'sh0 ? {{1'd0}, _divReg_12_mantissaA_T} : _divReg_12_mantissaA_T_5; // @[SPFPDiv.scala 65:105 66:29 69:29]
  wire [26:0] _GEN_23 = $signed(_T_94) < 27'sh0 ? _divReg_12_mantissaC_T : {{1'd0}, _divReg_12_mantissaC_T_2}; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [26:0] _T_97 = {1'h0,divReg_12_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_99 = {1'h0,divReg_12_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_102 = $signed(_T_97) - $signed(_T_99); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_13_mantissaA_T = {divReg_12_mantissaA, 1'h0}; // @[SPFPDiv.scala 66:52]
  wire [26:0] _divReg_13_mantissaC_T = {divReg_12_mantissaC, 1'h0}; // @[SPFPDiv.scala 67:52]
  wire [25:0] _divReg_13_mantissaA_T_1 = ~divReg_12_mantissaB; // @[SPFPDiv.scala 69:57]
  wire [25:0] _divReg_13_mantissaA_T_3 = _divReg_13_mantissaA_T_1 + 26'h1; // @[SPFPDiv.scala 69:78]
  wire [26:0] _divReg_13_mantissaA_T_4 = divReg_12_mantissaA + _divReg_13_mantissaA_T_3; // @[SPFPDiv.scala 69:53]
  wire [27:0] _divReg_13_mantissaA_T_5 = {_divReg_13_mantissaA_T_4, 1'h0}; // @[SPFPDiv.scala 69:86]
  wire [25:0] _divReg_13_mantissaC_T_2 = {divReg_12_mantissaC[24:0],1'h1}; // @[Cat.scala 33:92]
  wire [27:0] _GEN_24 = $signed(_T_102) < 27'sh0 ? {{1'd0}, _divReg_13_mantissaA_T} : _divReg_13_mantissaA_T_5; // @[SPFPDiv.scala 65:105 66:29 69:29]
  wire [26:0] _GEN_25 = $signed(_T_102) < 27'sh0 ? _divReg_13_mantissaC_T : {{1'd0}, _divReg_13_mantissaC_T_2}; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [26:0] _T_105 = {1'h0,divReg_13_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_107 = {1'h0,divReg_13_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_110 = $signed(_T_105) - $signed(_T_107); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_14_mantissaA_T = {divReg_13_mantissaA, 1'h0}; // @[SPFPDiv.scala 66:52]
  wire [26:0] _divReg_14_mantissaC_T = {divReg_13_mantissaC, 1'h0}; // @[SPFPDiv.scala 67:52]
  wire [25:0] _divReg_14_mantissaA_T_1 = ~divReg_13_mantissaB; // @[SPFPDiv.scala 69:57]
  wire [25:0] _divReg_14_mantissaA_T_3 = _divReg_14_mantissaA_T_1 + 26'h1; // @[SPFPDiv.scala 69:78]
  wire [26:0] _divReg_14_mantissaA_T_4 = divReg_13_mantissaA + _divReg_14_mantissaA_T_3; // @[SPFPDiv.scala 69:53]
  wire [27:0] _divReg_14_mantissaA_T_5 = {_divReg_14_mantissaA_T_4, 1'h0}; // @[SPFPDiv.scala 69:86]
  wire [25:0] _divReg_14_mantissaC_T_2 = {divReg_13_mantissaC[24:0],1'h1}; // @[Cat.scala 33:92]
  wire [27:0] _GEN_26 = $signed(_T_110) < 27'sh0 ? {{1'd0}, _divReg_14_mantissaA_T} : _divReg_14_mantissaA_T_5; // @[SPFPDiv.scala 65:105 66:29 69:29]
  wire [26:0] _GEN_27 = $signed(_T_110) < 27'sh0 ? _divReg_14_mantissaC_T : {{1'd0}, _divReg_14_mantissaC_T_2}; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [26:0] _T_113 = {1'h0,divReg_14_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_115 = {1'h0,divReg_14_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_118 = $signed(_T_113) - $signed(_T_115); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_15_mantissaA_T = {divReg_14_mantissaA, 1'h0}; // @[SPFPDiv.scala 66:52]
  wire [26:0] _divReg_15_mantissaC_T = {divReg_14_mantissaC, 1'h0}; // @[SPFPDiv.scala 67:52]
  wire [25:0] _divReg_15_mantissaA_T_1 = ~divReg_14_mantissaB; // @[SPFPDiv.scala 69:57]
  wire [25:0] _divReg_15_mantissaA_T_3 = _divReg_15_mantissaA_T_1 + 26'h1; // @[SPFPDiv.scala 69:78]
  wire [26:0] _divReg_15_mantissaA_T_4 = divReg_14_mantissaA + _divReg_15_mantissaA_T_3; // @[SPFPDiv.scala 69:53]
  wire [27:0] _divReg_15_mantissaA_T_5 = {_divReg_15_mantissaA_T_4, 1'h0}; // @[SPFPDiv.scala 69:86]
  wire [25:0] _divReg_15_mantissaC_T_2 = {divReg_14_mantissaC[24:0],1'h1}; // @[Cat.scala 33:92]
  wire [27:0] _GEN_28 = $signed(_T_118) < 27'sh0 ? {{1'd0}, _divReg_15_mantissaA_T} : _divReg_15_mantissaA_T_5; // @[SPFPDiv.scala 65:105 66:29 69:29]
  wire [26:0] _GEN_29 = $signed(_T_118) < 27'sh0 ? _divReg_15_mantissaC_T : {{1'd0}, _divReg_15_mantissaC_T_2}; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [26:0] _T_121 = {1'h0,divReg_15_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_123 = {1'h0,divReg_15_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_126 = $signed(_T_121) - $signed(_T_123); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_16_mantissaA_T = {divReg_15_mantissaA, 1'h0}; // @[SPFPDiv.scala 66:52]
  wire [26:0] _divReg_16_mantissaC_T = {divReg_15_mantissaC, 1'h0}; // @[SPFPDiv.scala 67:52]
  wire [25:0] _divReg_16_mantissaA_T_1 = ~divReg_15_mantissaB; // @[SPFPDiv.scala 69:57]
  wire [25:0] _divReg_16_mantissaA_T_3 = _divReg_16_mantissaA_T_1 + 26'h1; // @[SPFPDiv.scala 69:78]
  wire [26:0] _divReg_16_mantissaA_T_4 = divReg_15_mantissaA + _divReg_16_mantissaA_T_3; // @[SPFPDiv.scala 69:53]
  wire [27:0] _divReg_16_mantissaA_T_5 = {_divReg_16_mantissaA_T_4, 1'h0}; // @[SPFPDiv.scala 69:86]
  wire [25:0] _divReg_16_mantissaC_T_2 = {divReg_15_mantissaC[24:0],1'h1}; // @[Cat.scala 33:92]
  wire [27:0] _GEN_30 = $signed(_T_126) < 27'sh0 ? {{1'd0}, _divReg_16_mantissaA_T} : _divReg_16_mantissaA_T_5; // @[SPFPDiv.scala 65:105 66:29 69:29]
  wire [26:0] _GEN_31 = $signed(_T_126) < 27'sh0 ? _divReg_16_mantissaC_T : {{1'd0}, _divReg_16_mantissaC_T_2}; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [26:0] _T_129 = {1'h0,divReg_16_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_131 = {1'h0,divReg_16_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_134 = $signed(_T_129) - $signed(_T_131); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_17_mantissaA_T = {divReg_16_mantissaA, 1'h0}; // @[SPFPDiv.scala 66:52]
  wire [26:0] _divReg_17_mantissaC_T = {divReg_16_mantissaC, 1'h0}; // @[SPFPDiv.scala 67:52]
  wire [25:0] _divReg_17_mantissaA_T_1 = ~divReg_16_mantissaB; // @[SPFPDiv.scala 69:57]
  wire [25:0] _divReg_17_mantissaA_T_3 = _divReg_17_mantissaA_T_1 + 26'h1; // @[SPFPDiv.scala 69:78]
  wire [26:0] _divReg_17_mantissaA_T_4 = divReg_16_mantissaA + _divReg_17_mantissaA_T_3; // @[SPFPDiv.scala 69:53]
  wire [27:0] _divReg_17_mantissaA_T_5 = {_divReg_17_mantissaA_T_4, 1'h0}; // @[SPFPDiv.scala 69:86]
  wire [25:0] _divReg_17_mantissaC_T_2 = {divReg_16_mantissaC[24:0],1'h1}; // @[Cat.scala 33:92]
  wire [27:0] _GEN_32 = $signed(_T_134) < 27'sh0 ? {{1'd0}, _divReg_17_mantissaA_T} : _divReg_17_mantissaA_T_5; // @[SPFPDiv.scala 65:105 66:29 69:29]
  wire [26:0] _GEN_33 = $signed(_T_134) < 27'sh0 ? _divReg_17_mantissaC_T : {{1'd0}, _divReg_17_mantissaC_T_2}; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [26:0] _T_137 = {1'h0,divReg_17_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_139 = {1'h0,divReg_17_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_142 = $signed(_T_137) - $signed(_T_139); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_18_mantissaA_T = {divReg_17_mantissaA, 1'h0}; // @[SPFPDiv.scala 66:52]
  wire [26:0] _divReg_18_mantissaC_T = {divReg_17_mantissaC, 1'h0}; // @[SPFPDiv.scala 67:52]
  wire [25:0] _divReg_18_mantissaA_T_1 = ~divReg_17_mantissaB; // @[SPFPDiv.scala 69:57]
  wire [25:0] _divReg_18_mantissaA_T_3 = _divReg_18_mantissaA_T_1 + 26'h1; // @[SPFPDiv.scala 69:78]
  wire [26:0] _divReg_18_mantissaA_T_4 = divReg_17_mantissaA + _divReg_18_mantissaA_T_3; // @[SPFPDiv.scala 69:53]
  wire [27:0] _divReg_18_mantissaA_T_5 = {_divReg_18_mantissaA_T_4, 1'h0}; // @[SPFPDiv.scala 69:86]
  wire [25:0] _divReg_18_mantissaC_T_2 = {divReg_17_mantissaC[24:0],1'h1}; // @[Cat.scala 33:92]
  wire [27:0] _GEN_34 = $signed(_T_142) < 27'sh0 ? {{1'd0}, _divReg_18_mantissaA_T} : _divReg_18_mantissaA_T_5; // @[SPFPDiv.scala 65:105 66:29 69:29]
  wire [26:0] _GEN_35 = $signed(_T_142) < 27'sh0 ? _divReg_18_mantissaC_T : {{1'd0}, _divReg_18_mantissaC_T_2}; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [26:0] _T_145 = {1'h0,divReg_18_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_147 = {1'h0,divReg_18_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_150 = $signed(_T_145) - $signed(_T_147); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_19_mantissaA_T = {divReg_18_mantissaA, 1'h0}; // @[SPFPDiv.scala 66:52]
  wire [26:0] _divReg_19_mantissaC_T = {divReg_18_mantissaC, 1'h0}; // @[SPFPDiv.scala 67:52]
  wire [25:0] _divReg_19_mantissaA_T_1 = ~divReg_18_mantissaB; // @[SPFPDiv.scala 69:57]
  wire [25:0] _divReg_19_mantissaA_T_3 = _divReg_19_mantissaA_T_1 + 26'h1; // @[SPFPDiv.scala 69:78]
  wire [26:0] _divReg_19_mantissaA_T_4 = divReg_18_mantissaA + _divReg_19_mantissaA_T_3; // @[SPFPDiv.scala 69:53]
  wire [27:0] _divReg_19_mantissaA_T_5 = {_divReg_19_mantissaA_T_4, 1'h0}; // @[SPFPDiv.scala 69:86]
  wire [25:0] _divReg_19_mantissaC_T_2 = {divReg_18_mantissaC[24:0],1'h1}; // @[Cat.scala 33:92]
  wire [27:0] _GEN_36 = $signed(_T_150) < 27'sh0 ? {{1'd0}, _divReg_19_mantissaA_T} : _divReg_19_mantissaA_T_5; // @[SPFPDiv.scala 65:105 66:29 69:29]
  wire [26:0] _GEN_37 = $signed(_T_150) < 27'sh0 ? _divReg_19_mantissaC_T : {{1'd0}, _divReg_19_mantissaC_T_2}; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [26:0] _T_153 = {1'h0,divReg_19_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_155 = {1'h0,divReg_19_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_158 = $signed(_T_153) - $signed(_T_155); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_20_mantissaA_T = {divReg_19_mantissaA, 1'h0}; // @[SPFPDiv.scala 66:52]
  wire [26:0] _divReg_20_mantissaC_T = {divReg_19_mantissaC, 1'h0}; // @[SPFPDiv.scala 67:52]
  wire [25:0] _divReg_20_mantissaA_T_1 = ~divReg_19_mantissaB; // @[SPFPDiv.scala 69:57]
  wire [25:0] _divReg_20_mantissaA_T_3 = _divReg_20_mantissaA_T_1 + 26'h1; // @[SPFPDiv.scala 69:78]
  wire [26:0] _divReg_20_mantissaA_T_4 = divReg_19_mantissaA + _divReg_20_mantissaA_T_3; // @[SPFPDiv.scala 69:53]
  wire [27:0] _divReg_20_mantissaA_T_5 = {_divReg_20_mantissaA_T_4, 1'h0}; // @[SPFPDiv.scala 69:86]
  wire [25:0] _divReg_20_mantissaC_T_2 = {divReg_19_mantissaC[24:0],1'h1}; // @[Cat.scala 33:92]
  wire [27:0] _GEN_38 = $signed(_T_158) < 27'sh0 ? {{1'd0}, _divReg_20_mantissaA_T} : _divReg_20_mantissaA_T_5; // @[SPFPDiv.scala 65:105 66:29 69:29]
  wire [26:0] _GEN_39 = $signed(_T_158) < 27'sh0 ? _divReg_20_mantissaC_T : {{1'd0}, _divReg_20_mantissaC_T_2}; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [26:0] _T_161 = {1'h0,divReg_20_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_163 = {1'h0,divReg_20_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_166 = $signed(_T_161) - $signed(_T_163); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_21_mantissaA_T = {divReg_20_mantissaA, 1'h0}; // @[SPFPDiv.scala 66:52]
  wire [26:0] _divReg_21_mantissaC_T = {divReg_20_mantissaC, 1'h0}; // @[SPFPDiv.scala 67:52]
  wire [25:0] _divReg_21_mantissaA_T_1 = ~divReg_20_mantissaB; // @[SPFPDiv.scala 69:57]
  wire [25:0] _divReg_21_mantissaA_T_3 = _divReg_21_mantissaA_T_1 + 26'h1; // @[SPFPDiv.scala 69:78]
  wire [26:0] _divReg_21_mantissaA_T_4 = divReg_20_mantissaA + _divReg_21_mantissaA_T_3; // @[SPFPDiv.scala 69:53]
  wire [27:0] _divReg_21_mantissaA_T_5 = {_divReg_21_mantissaA_T_4, 1'h0}; // @[SPFPDiv.scala 69:86]
  wire [25:0] _divReg_21_mantissaC_T_2 = {divReg_20_mantissaC[24:0],1'h1}; // @[Cat.scala 33:92]
  wire [27:0] _GEN_40 = $signed(_T_166) < 27'sh0 ? {{1'd0}, _divReg_21_mantissaA_T} : _divReg_21_mantissaA_T_5; // @[SPFPDiv.scala 65:105 66:29 69:29]
  wire [26:0] _GEN_41 = $signed(_T_166) < 27'sh0 ? _divReg_21_mantissaC_T : {{1'd0}, _divReg_21_mantissaC_T_2}; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [26:0] _T_169 = {1'h0,divReg_21_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_171 = {1'h0,divReg_21_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_174 = $signed(_T_169) - $signed(_T_171); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_22_mantissaA_T = {divReg_21_mantissaA, 1'h0}; // @[SPFPDiv.scala 66:52]
  wire [26:0] _divReg_22_mantissaC_T = {divReg_21_mantissaC, 1'h0}; // @[SPFPDiv.scala 67:52]
  wire [25:0] _divReg_22_mantissaA_T_1 = ~divReg_21_mantissaB; // @[SPFPDiv.scala 69:57]
  wire [25:0] _divReg_22_mantissaA_T_3 = _divReg_22_mantissaA_T_1 + 26'h1; // @[SPFPDiv.scala 69:78]
  wire [26:0] _divReg_22_mantissaA_T_4 = divReg_21_mantissaA + _divReg_22_mantissaA_T_3; // @[SPFPDiv.scala 69:53]
  wire [27:0] _divReg_22_mantissaA_T_5 = {_divReg_22_mantissaA_T_4, 1'h0}; // @[SPFPDiv.scala 69:86]
  wire [25:0] _divReg_22_mantissaC_T_2 = {divReg_21_mantissaC[24:0],1'h1}; // @[Cat.scala 33:92]
  wire [27:0] _GEN_42 = $signed(_T_174) < 27'sh0 ? {{1'd0}, _divReg_22_mantissaA_T} : _divReg_22_mantissaA_T_5; // @[SPFPDiv.scala 65:105 66:29 69:29]
  wire [26:0] _GEN_43 = $signed(_T_174) < 27'sh0 ? _divReg_22_mantissaC_T : {{1'd0}, _divReg_22_mantissaC_T_2}; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [26:0] _T_177 = {1'h0,divReg_22_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_179 = {1'h0,divReg_22_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_182 = $signed(_T_177) - $signed(_T_179); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_23_mantissaA_T = {divReg_22_mantissaA, 1'h0}; // @[SPFPDiv.scala 66:52]
  wire [26:0] _divReg_23_mantissaC_T = {divReg_22_mantissaC, 1'h0}; // @[SPFPDiv.scala 67:52]
  wire [25:0] _divReg_23_mantissaA_T_1 = ~divReg_22_mantissaB; // @[SPFPDiv.scala 69:57]
  wire [25:0] _divReg_23_mantissaA_T_3 = _divReg_23_mantissaA_T_1 + 26'h1; // @[SPFPDiv.scala 69:78]
  wire [26:0] _divReg_23_mantissaA_T_4 = divReg_22_mantissaA + _divReg_23_mantissaA_T_3; // @[SPFPDiv.scala 69:53]
  wire [27:0] _divReg_23_mantissaA_T_5 = {_divReg_23_mantissaA_T_4, 1'h0}; // @[SPFPDiv.scala 69:86]
  wire [25:0] _divReg_23_mantissaC_T_2 = {divReg_22_mantissaC[24:0],1'h1}; // @[Cat.scala 33:92]
  wire [27:0] _GEN_44 = $signed(_T_182) < 27'sh0 ? {{1'd0}, _divReg_23_mantissaA_T} : _divReg_23_mantissaA_T_5; // @[SPFPDiv.scala 65:105 66:29 69:29]
  wire [26:0] _GEN_45 = $signed(_T_182) < 27'sh0 ? _divReg_23_mantissaC_T : {{1'd0}, _divReg_23_mantissaC_T_2}; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [26:0] _T_185 = {1'h0,divReg_23_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_187 = {1'h0,divReg_23_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_190 = $signed(_T_185) - $signed(_T_187); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_24_mantissaA_T = {divReg_23_mantissaA, 1'h0}; // @[SPFPDiv.scala 66:52]
  wire [26:0] _divReg_24_mantissaC_T = {divReg_23_mantissaC, 1'h0}; // @[SPFPDiv.scala 67:52]
  wire [25:0] _divReg_24_mantissaA_T_1 = ~divReg_23_mantissaB; // @[SPFPDiv.scala 69:57]
  wire [25:0] _divReg_24_mantissaA_T_3 = _divReg_24_mantissaA_T_1 + 26'h1; // @[SPFPDiv.scala 69:78]
  wire [26:0] _divReg_24_mantissaA_T_4 = divReg_23_mantissaA + _divReg_24_mantissaA_T_3; // @[SPFPDiv.scala 69:53]
  wire [27:0] _divReg_24_mantissaA_T_5 = {_divReg_24_mantissaA_T_4, 1'h0}; // @[SPFPDiv.scala 69:86]
  wire [25:0] _divReg_24_mantissaC_T_2 = {divReg_23_mantissaC[24:0],1'h1}; // @[Cat.scala 33:92]
  wire [27:0] _GEN_46 = $signed(_T_190) < 27'sh0 ? {{1'd0}, _divReg_24_mantissaA_T} : _divReg_24_mantissaA_T_5; // @[SPFPDiv.scala 65:105 66:29 69:29]
  wire [26:0] _GEN_47 = $signed(_T_190) < 27'sh0 ? _divReg_24_mantissaC_T : {{1'd0}, _divReg_24_mantissaC_T_2}; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [26:0] _T_193 = {1'h0,divReg_24_mantissaA}; // @[SPFPDiv.scala 65:46]
  wire [26:0] _T_195 = {1'h0,divReg_24_mantissaB}; // @[SPFPDiv.scala 65:90]
  wire [26:0] _T_198 = $signed(_T_193) - $signed(_T_195); // @[SPFPDiv.scala 65:53]
  wire [26:0] _divReg_25_mantissaC_T = {divReg_24_mantissaC, 1'h0}; // @[SPFPDiv.scala 67:52]
  wire [25:0] _divReg_25_mantissaC_T_2 = {divReg_24_mantissaC[24:0],1'h1}; // @[Cat.scala 33:92]
  wire [26:0] _GEN_49 = $signed(_T_198) < 27'sh0 ? _divReg_25_mantissaC_T : {{1'd0}, _divReg_25_mantissaC_T_2}; // @[SPFPDiv.scala 65:105 67:29 70:29]
  wire [7:0] _divReg_25_exponentC_T_2 = $signed(divReg_24_exponentA) - $signed(divReg_24_exponentB); // @[SPFPDiv.scala 74:48]
  wire [7:0] _divReg_25_exponentC_T_5 = $signed(_divReg_25_exponentC_T_2) - 8'sh1; // @[SPFPDiv.scala 74:71]
  wire  aNaN = divReg_25_faultFlags[3] & divReg_25_faultFlags[0]; // @[SPFPDiv.scala 87:25]
  wire  bNaN = divReg_25_faultFlags[4] & divReg_25_faultFlags[1]; // @[SPFPDiv.scala 88:25]
  wire  zeroA = divReg_25_faultFlags[6] & divReg_25_faultFlags[9]; // @[SPFPDiv.scala 89:25]
  wire  zeroB = divReg_25_faultFlags[7] & divReg_25_faultFlags[10]; // @[SPFPDiv.scala 90:25]
  wire  infA = divReg_25_faultFlags[3] & divReg_25_faultFlags[9]; // @[SPFPDiv.scala 91:25]
  wire  infB = divReg_25_faultFlags[4] & divReg_25_faultFlags[10]; // @[SPFPDiv.scala 92:25]
  wire [25:0] _io_out_bits_result_mantissa_T_1 = {4'h1,divReg_25_exMantA[21:0]}; // @[Cat.scala 33:92]
  wire [25:0] _io_out_bits_result_mantissa_T_5 = {4'h1,divReg_25_mantissaB[21:0]}; // @[Cat.scala 33:92]
  wire  _io_out_bits_result_sign_T = divReg_25_signBitA ^ divReg_25_signBitB; // @[SPFPDiv.scala 132:44]
  wire  _T_218 = ~infA; // @[SPFPDiv.scala 138:12]
  wire [25:0] _GEN_122 = zeroA & ~zeroB ? divReg_25_exMantA : 26'h0; // @[SPFPDiv.scala 128:33 130:35 135:35]
  wire [8:0] _GEN_123 = zeroA & ~zeroB ? $signed({{1{divReg_25_exponentA[7]}},divReg_25_exponentA}) : $signed(9'sh80); // @[SPFPDiv.scala 128:33 131:35 136:35]
  wire  _GEN_124 = zeroA & ~zeroB ? divReg_25_signBitA ^ divReg_25_signBitB : divReg_25_signBitA ^ divReg_25_signBitB; // @[SPFPDiv.scala 128:33 132:35 137:35]
  wire [25:0] _GEN_133 = zeroA & zeroB ? 26'h400000 : _GEN_122; // @[SPFPDiv.scala 122:25 124:35]
  wire [8:0] _GEN_134 = zeroA & zeroB ? $signed(9'sh80) : $signed(_GEN_123); // @[SPFPDiv.scala 122:25 125:35]
  wire  _GEN_135 = zeroA & zeroB | _GEN_124; // @[SPFPDiv.scala 122:25 126:35]
  wire [25:0] _GEN_152 = infA & ~infB ? divReg_25_exMantA : 26'h400000; // @[SPFPDiv.scala 150:32 152:35 157:35]
  wire [8:0] _GEN_153 = infA & ~infB ? $signed({{1{divReg_25_exponentA[7]}},divReg_25_exponentA}) : $signed(9'sh80); // @[SPFPDiv.scala 150:32 153:35 158:35]
  wire  _GEN_154 = infA & ~infB ? _io_out_bits_result_sign_T : 1'h1; // @[SPFPDiv.scala 150:32 154:35 159:35]
  wire [25:0] _GEN_163 = _T_218 & infB ? 26'h0 : _GEN_152; // @[SPFPDiv.scala 145:25 147:35]
  wire [8:0] _GEN_164 = _T_218 & infB ? $signed(-9'sh7e) : $signed(_GEN_153); // @[SPFPDiv.scala 145:25 148:35]
  wire  _GEN_165 = _T_218 & infB ? _io_out_bits_result_sign_T : _GEN_154; // @[SPFPDiv.scala 145:25 149:35]
  wire [25:0] _GEN_174 = zeroB ? divReg_25_exMantA : divReg_25_mantissaC; // @[SPFPDiv.scala 162:22 164:33 168:33]
  wire [7:0] _GEN_175 = zeroB ? $signed(divReg_25_exponentA) : $signed(divReg_25_exponentC); // @[SPFPDiv.scala 162:22 165:33 169:33]
  wire  _GEN_176 = zeroB ? _io_out_bits_result_sign_T : divReg_25_signBitC; // @[SPFPDiv.scala 162:22 166:33 170:33]
  wire [25:0] _GEN_177 = infA | infB ? _GEN_163 : _GEN_174; // @[SPFPDiv.scala 143:28]
  wire [8:0] _GEN_178 = infA | infB ? $signed(_GEN_164) : $signed({{1{_GEN_175[7]}},_GEN_175}); // @[SPFPDiv.scala 143:28]
  wire  _GEN_179 = infA | infB ? _GEN_165 : _GEN_176; // @[SPFPDiv.scala 143:28]
  wire [25:0] _GEN_188 = zeroA | zeroB ? _GEN_133 : _GEN_177; // @[SPFPDiv.scala 121:31]
  wire [8:0] _GEN_189 = zeroA | zeroB ? $signed(_GEN_134) : $signed(_GEN_178); // @[SPFPDiv.scala 121:31]
  wire  _GEN_190 = zeroA | zeroB ? _GEN_135 : _GEN_179; // @[SPFPDiv.scala 121:31]
  wire [25:0] _GEN_207 = bNaN ? _io_out_bits_result_mantissa_T_5 : _GEN_188; // @[SPFPDiv.scala 112:21 118:33]
  wire [8:0] _GEN_208 = bNaN ? $signed(9'sh80) : $signed(_GEN_189); // @[SPFPDiv.scala 112:21 119:33]
  wire  _GEN_209 = bNaN ? divReg_25_signBitB : _GEN_190; // @[SPFPDiv.scala 112:21 120:33]
  wire [25:0] _GEN_218 = aNaN ? _io_out_bits_result_mantissa_T_1 : _GEN_207; // @[SPFPDiv.scala 103:21 109:33]
  wire [8:0] _GEN_219 = aNaN ? $signed(9'sh80) : $signed(_GEN_208); // @[SPFPDiv.scala 103:21 110:33]
  wire  _GEN_220 = aNaN ? divReg_25_signBitA : _GEN_209; // @[SPFPDiv.scala 103:21 111:33]
  wire [8:0] _GEN_230 = aNaN & bNaN ? $signed(9'sh80) : $signed(_GEN_219); // @[SPFPDiv.scala 94:23 101:33]
  wire [27:0] _GEN_233 = reset ? 28'h0 : _GEN_0; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_234 = reset ? 27'h0 : _GEN_1; // @[SPFPDiv.scala 44:{23,23}]
  wire [27:0] _GEN_235 = reset ? 28'h0 : _GEN_2; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_236 = reset ? 27'h0 : _GEN_3; // @[SPFPDiv.scala 44:{23,23}]
  wire [27:0] _GEN_237 = reset ? 28'h0 : _GEN_4; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_238 = reset ? 27'h0 : _GEN_5; // @[SPFPDiv.scala 44:{23,23}]
  wire [27:0] _GEN_239 = reset ? 28'h0 : _GEN_6; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_240 = reset ? 27'h0 : _GEN_7; // @[SPFPDiv.scala 44:{23,23}]
  wire [27:0] _GEN_241 = reset ? 28'h0 : _GEN_8; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_242 = reset ? 27'h0 : _GEN_9; // @[SPFPDiv.scala 44:{23,23}]
  wire [27:0] _GEN_243 = reset ? 28'h0 : _GEN_10; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_244 = reset ? 27'h0 : _GEN_11; // @[SPFPDiv.scala 44:{23,23}]
  wire [27:0] _GEN_245 = reset ? 28'h0 : _GEN_12; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_246 = reset ? 27'h0 : _GEN_13; // @[SPFPDiv.scala 44:{23,23}]
  wire [27:0] _GEN_247 = reset ? 28'h0 : _GEN_14; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_248 = reset ? 27'h0 : _GEN_15; // @[SPFPDiv.scala 44:{23,23}]
  wire [27:0] _GEN_249 = reset ? 28'h0 : _GEN_16; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_250 = reset ? 27'h0 : _GEN_17; // @[SPFPDiv.scala 44:{23,23}]
  wire [27:0] _GEN_251 = reset ? 28'h0 : _GEN_18; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_252 = reset ? 27'h0 : _GEN_19; // @[SPFPDiv.scala 44:{23,23}]
  wire [27:0] _GEN_253 = reset ? 28'h0 : _GEN_20; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_254 = reset ? 27'h0 : _GEN_21; // @[SPFPDiv.scala 44:{23,23}]
  wire [27:0] _GEN_255 = reset ? 28'h0 : _GEN_22; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_256 = reset ? 27'h0 : _GEN_23; // @[SPFPDiv.scala 44:{23,23}]
  wire [27:0] _GEN_257 = reset ? 28'h0 : _GEN_24; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_258 = reset ? 27'h0 : _GEN_25; // @[SPFPDiv.scala 44:{23,23}]
  wire [27:0] _GEN_259 = reset ? 28'h0 : _GEN_26; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_260 = reset ? 27'h0 : _GEN_27; // @[SPFPDiv.scala 44:{23,23}]
  wire [27:0] _GEN_261 = reset ? 28'h0 : _GEN_28; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_262 = reset ? 27'h0 : _GEN_29; // @[SPFPDiv.scala 44:{23,23}]
  wire [27:0] _GEN_263 = reset ? 28'h0 : _GEN_30; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_264 = reset ? 27'h0 : _GEN_31; // @[SPFPDiv.scala 44:{23,23}]
  wire [27:0] _GEN_265 = reset ? 28'h0 : _GEN_32; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_266 = reset ? 27'h0 : _GEN_33; // @[SPFPDiv.scala 44:{23,23}]
  wire [27:0] _GEN_267 = reset ? 28'h0 : _GEN_34; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_268 = reset ? 27'h0 : _GEN_35; // @[SPFPDiv.scala 44:{23,23}]
  wire [27:0] _GEN_269 = reset ? 28'h0 : _GEN_36; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_270 = reset ? 27'h0 : _GEN_37; // @[SPFPDiv.scala 44:{23,23}]
  wire [27:0] _GEN_271 = reset ? 28'h0 : _GEN_38; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_272 = reset ? 27'h0 : _GEN_39; // @[SPFPDiv.scala 44:{23,23}]
  wire [27:0] _GEN_273 = reset ? 28'h0 : _GEN_40; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_274 = reset ? 27'h0 : _GEN_41; // @[SPFPDiv.scala 44:{23,23}]
  wire [27:0] _GEN_275 = reset ? 28'h0 : _GEN_42; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_276 = reset ? 27'h0 : _GEN_43; // @[SPFPDiv.scala 44:{23,23}]
  wire [27:0] _GEN_277 = reset ? 28'h0 : _GEN_44; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_278 = reset ? 27'h0 : _GEN_45; // @[SPFPDiv.scala 44:{23,23}]
  wire [27:0] _GEN_279 = reset ? 28'h0 : _GEN_46; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_280 = reset ? 27'h0 : _GEN_47; // @[SPFPDiv.scala 44:{23,23}]
  wire [26:0] _GEN_282 = reset ? 27'h0 : _GEN_49; // @[SPFPDiv.scala 44:{23,23}]
  assign io_out_valid = divReg_25_valid; // @[SPFPDiv.scala 176:31]
  assign io_out_bits_result_mantissa = aNaN & bNaN ? _io_out_bits_result_mantissa_T_1 : _GEN_218; // @[SPFPDiv.scala 94:23 100:33]
  assign io_out_bits_result_exponent = _GEN_230[7:0];
  assign io_out_bits_result_sign = aNaN & bNaN ? divReg_25_signBitA : _GEN_220; // @[SPFPDiv.scala 94:23 102:33]
  assign io_out_bits_fcsr_0 = divReg_25_status_0; // @[SPFPDiv.scala 173:31]
  assign io_out_bits_fcsr_1 = divReg_25_status_1; // @[SPFPDiv.scala 173:31]
  assign io_out_bits_fcsr_2 = divReg_25_status_2; // @[SPFPDiv.scala 173:31]
  assign io_out_bits_fcsr_3 = divReg_25_status_3; // @[SPFPDiv.scala 173:31]
  assign io_out_bits_fcsr_4 = divReg_25_status_4; // @[SPFPDiv.scala 173:31]
  assign io_out_bits_fcsr_5 = divReg_25_status_5; // @[SPFPDiv.scala 173:31]
  assign io_out_bits_fcsr_6 = divReg_25_status_6; // @[SPFPDiv.scala 173:31]
  assign io_out_bits_fcsr_7 = divReg_25_status_7; // @[SPFPDiv.scala 173:31]
  assign io_out_bits_op = divReg_25_op; // @[SPFPDiv.scala 174:31]
  assign io_out_bits_divInProc = divReg_0_valid | divReg_1_valid | divReg_2_valid | divReg_3_valid | divReg_4_valid |
    divReg_5_valid | divReg_6_valid | divReg_7_valid | divReg_8_valid | divReg_9_valid | divReg_10_valid |
    divReg_11_valid | divReg_12_valid | divReg_13_valid | divReg_14_valid | divReg_15_valid | divReg_16_valid |
    divReg_17_valid | divReg_18_valid | divReg_19_valid | divReg_20_valid | divReg_21_valid | divReg_22_valid |
    divReg_23_valid | divReg_24_valid | divReg_25_valid; // @[SPFPDiv.scala 179:62]
  always @(posedge clock) begin
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_0_mantissaA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_0_mantissaA <= io_in_bits_a_mantissa; // @[SPFPDiv.scala 46:24]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_0_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_0_mantissaB <= io_in_bits_b_mantissa; // @[SPFPDiv.scala 47:24]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_0_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_0_exponentA <= io_in_bits_a_exponent; // @[SPFPDiv.scala 49:24]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_0_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_0_exponentB <= io_in_bits_b_exponent; // @[SPFPDiv.scala 50:24]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_0_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_0_signBitA <= io_in_bits_a_sign; // @[SPFPDiv.scala 52:24]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_0_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_0_signBitB <= io_in_bits_b_sign; // @[SPFPDiv.scala 53:24]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_0_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_0_faultFlags <= io_in_bits_fflags; // @[SPFPDiv.scala 55:24]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_0_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_0_status_0 <= io_in_bits_fcsr_0; // @[SPFPDiv.scala 56:24]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_0_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_0_status_1 <= io_in_bits_fcsr_1; // @[SPFPDiv.scala 56:24]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_0_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_0_status_2 <= io_in_bits_fcsr_2; // @[SPFPDiv.scala 56:24]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_0_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_0_status_3 <= io_in_bits_fcsr_3; // @[SPFPDiv.scala 56:24]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_0_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_0_status_4 <= io_in_bits_fcsr_4; // @[SPFPDiv.scala 56:24]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_0_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_0_status_5 <= io_in_bits_fcsr_5; // @[SPFPDiv.scala 56:24]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_0_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_0_status_6 <= io_in_bits_fcsr_6; // @[SPFPDiv.scala 56:24]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_0_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_0_status_7 <= io_in_bits_fcsr_7; // @[SPFPDiv.scala 56:24]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_0_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_0_op <= io_in_bits_op; // @[SPFPDiv.scala 57:24]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_0_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_0_valid <= io_in_valid; // @[SPFPDiv.scala 58:24]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_0_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_0_exMantA <= io_in_bits_a_mantissa; // @[SPFPDiv.scala 59:24]
    end
    divReg_1_mantissaA <= _GEN_233[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_1_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_1_mantissaB <= divReg_0_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_1_mantissaC <= _GEN_234[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_1_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_1_exponentA <= divReg_0_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_1_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_1_exponentB <= divReg_0_exponentB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_1_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_1_signBitA <= divReg_0_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_1_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_1_signBitB <= divReg_0_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_1_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_1_faultFlags <= divReg_0_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_1_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_1_status_0 <= divReg_0_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_1_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_1_status_1 <= divReg_0_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_1_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_1_status_2 <= divReg_0_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_1_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_1_status_3 <= divReg_0_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_1_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_1_status_4 <= divReg_0_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_1_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_1_status_5 <= divReg_0_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_1_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_1_status_6 <= divReg_0_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_1_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_1_status_7 <= divReg_0_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_1_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_1_op <= divReg_0_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_1_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_1_valid <= divReg_0_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_1_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_1_exMantA <= divReg_0_exMantA; // @[SPFPDiv.scala 64:17]
    end
    divReg_2_mantissaA <= _GEN_235[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_2_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_2_mantissaB <= divReg_1_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_2_mantissaC <= _GEN_236[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_2_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_2_exponentA <= divReg_1_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_2_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_2_exponentB <= divReg_1_exponentB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_2_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_2_signBitA <= divReg_1_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_2_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_2_signBitB <= divReg_1_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_2_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_2_faultFlags <= divReg_1_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_2_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_2_status_0 <= divReg_1_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_2_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_2_status_1 <= divReg_1_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_2_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_2_status_2 <= divReg_1_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_2_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_2_status_3 <= divReg_1_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_2_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_2_status_4 <= divReg_1_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_2_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_2_status_5 <= divReg_1_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_2_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_2_status_6 <= divReg_1_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_2_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_2_status_7 <= divReg_1_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_2_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_2_op <= divReg_1_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_2_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_2_valid <= divReg_1_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_2_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_2_exMantA <= divReg_1_exMantA; // @[SPFPDiv.scala 64:17]
    end
    divReg_3_mantissaA <= _GEN_237[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_3_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_3_mantissaB <= divReg_2_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_3_mantissaC <= _GEN_238[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_3_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_3_exponentA <= divReg_2_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_3_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_3_exponentB <= divReg_2_exponentB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_3_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_3_signBitA <= divReg_2_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_3_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_3_signBitB <= divReg_2_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_3_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_3_faultFlags <= divReg_2_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_3_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_3_status_0 <= divReg_2_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_3_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_3_status_1 <= divReg_2_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_3_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_3_status_2 <= divReg_2_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_3_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_3_status_3 <= divReg_2_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_3_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_3_status_4 <= divReg_2_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_3_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_3_status_5 <= divReg_2_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_3_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_3_status_6 <= divReg_2_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_3_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_3_status_7 <= divReg_2_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_3_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_3_op <= divReg_2_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_3_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_3_valid <= divReg_2_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_3_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_3_exMantA <= divReg_2_exMantA; // @[SPFPDiv.scala 64:17]
    end
    divReg_4_mantissaA <= _GEN_239[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_4_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_4_mantissaB <= divReg_3_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_4_mantissaC <= _GEN_240[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_4_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_4_exponentA <= divReg_3_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_4_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_4_exponentB <= divReg_3_exponentB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_4_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_4_signBitA <= divReg_3_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_4_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_4_signBitB <= divReg_3_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_4_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_4_faultFlags <= divReg_3_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_4_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_4_status_0 <= divReg_3_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_4_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_4_status_1 <= divReg_3_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_4_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_4_status_2 <= divReg_3_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_4_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_4_status_3 <= divReg_3_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_4_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_4_status_4 <= divReg_3_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_4_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_4_status_5 <= divReg_3_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_4_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_4_status_6 <= divReg_3_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_4_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_4_status_7 <= divReg_3_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_4_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_4_op <= divReg_3_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_4_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_4_valid <= divReg_3_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_4_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_4_exMantA <= divReg_3_exMantA; // @[SPFPDiv.scala 64:17]
    end
    divReg_5_mantissaA <= _GEN_241[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_5_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_5_mantissaB <= divReg_4_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_5_mantissaC <= _GEN_242[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_5_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_5_exponentA <= divReg_4_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_5_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_5_exponentB <= divReg_4_exponentB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_5_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_5_signBitA <= divReg_4_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_5_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_5_signBitB <= divReg_4_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_5_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_5_faultFlags <= divReg_4_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_5_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_5_status_0 <= divReg_4_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_5_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_5_status_1 <= divReg_4_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_5_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_5_status_2 <= divReg_4_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_5_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_5_status_3 <= divReg_4_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_5_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_5_status_4 <= divReg_4_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_5_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_5_status_5 <= divReg_4_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_5_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_5_status_6 <= divReg_4_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_5_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_5_status_7 <= divReg_4_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_5_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_5_op <= divReg_4_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_5_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_5_valid <= divReg_4_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_5_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_5_exMantA <= divReg_4_exMantA; // @[SPFPDiv.scala 64:17]
    end
    divReg_6_mantissaA <= _GEN_243[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_6_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_6_mantissaB <= divReg_5_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_6_mantissaC <= _GEN_244[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_6_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_6_exponentA <= divReg_5_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_6_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_6_exponentB <= divReg_5_exponentB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_6_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_6_signBitA <= divReg_5_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_6_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_6_signBitB <= divReg_5_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_6_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_6_faultFlags <= divReg_5_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_6_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_6_status_0 <= divReg_5_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_6_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_6_status_1 <= divReg_5_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_6_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_6_status_2 <= divReg_5_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_6_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_6_status_3 <= divReg_5_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_6_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_6_status_4 <= divReg_5_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_6_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_6_status_5 <= divReg_5_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_6_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_6_status_6 <= divReg_5_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_6_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_6_status_7 <= divReg_5_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_6_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_6_op <= divReg_5_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_6_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_6_valid <= divReg_5_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_6_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_6_exMantA <= divReg_5_exMantA; // @[SPFPDiv.scala 64:17]
    end
    divReg_7_mantissaA <= _GEN_245[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_7_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_7_mantissaB <= divReg_6_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_7_mantissaC <= _GEN_246[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_7_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_7_exponentA <= divReg_6_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_7_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_7_exponentB <= divReg_6_exponentB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_7_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_7_signBitA <= divReg_6_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_7_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_7_signBitB <= divReg_6_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_7_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_7_faultFlags <= divReg_6_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_7_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_7_status_0 <= divReg_6_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_7_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_7_status_1 <= divReg_6_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_7_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_7_status_2 <= divReg_6_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_7_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_7_status_3 <= divReg_6_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_7_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_7_status_4 <= divReg_6_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_7_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_7_status_5 <= divReg_6_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_7_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_7_status_6 <= divReg_6_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_7_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_7_status_7 <= divReg_6_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_7_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_7_op <= divReg_6_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_7_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_7_valid <= divReg_6_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_7_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_7_exMantA <= divReg_6_exMantA; // @[SPFPDiv.scala 64:17]
    end
    divReg_8_mantissaA <= _GEN_247[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_8_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_8_mantissaB <= divReg_7_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_8_mantissaC <= _GEN_248[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_8_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_8_exponentA <= divReg_7_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_8_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_8_exponentB <= divReg_7_exponentB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_8_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_8_signBitA <= divReg_7_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_8_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_8_signBitB <= divReg_7_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_8_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_8_faultFlags <= divReg_7_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_8_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_8_status_0 <= divReg_7_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_8_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_8_status_1 <= divReg_7_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_8_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_8_status_2 <= divReg_7_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_8_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_8_status_3 <= divReg_7_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_8_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_8_status_4 <= divReg_7_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_8_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_8_status_5 <= divReg_7_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_8_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_8_status_6 <= divReg_7_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_8_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_8_status_7 <= divReg_7_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_8_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_8_op <= divReg_7_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_8_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_8_valid <= divReg_7_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_8_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_8_exMantA <= divReg_7_exMantA; // @[SPFPDiv.scala 64:17]
    end
    divReg_9_mantissaA <= _GEN_249[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_9_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_9_mantissaB <= divReg_8_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_9_mantissaC <= _GEN_250[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_9_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_9_exponentA <= divReg_8_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_9_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_9_exponentB <= divReg_8_exponentB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_9_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_9_signBitA <= divReg_8_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_9_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_9_signBitB <= divReg_8_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_9_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_9_faultFlags <= divReg_8_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_9_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_9_status_0 <= divReg_8_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_9_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_9_status_1 <= divReg_8_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_9_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_9_status_2 <= divReg_8_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_9_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_9_status_3 <= divReg_8_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_9_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_9_status_4 <= divReg_8_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_9_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_9_status_5 <= divReg_8_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_9_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_9_status_6 <= divReg_8_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_9_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_9_status_7 <= divReg_8_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_9_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_9_op <= divReg_8_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_9_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_9_valid <= divReg_8_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_9_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_9_exMantA <= divReg_8_exMantA; // @[SPFPDiv.scala 64:17]
    end
    divReg_10_mantissaA <= _GEN_251[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_10_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_10_mantissaB <= divReg_9_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_10_mantissaC <= _GEN_252[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_10_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_10_exponentA <= divReg_9_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_10_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_10_exponentB <= divReg_9_exponentB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_10_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_10_signBitA <= divReg_9_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_10_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_10_signBitB <= divReg_9_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_10_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_10_faultFlags <= divReg_9_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_10_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_10_status_0 <= divReg_9_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_10_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_10_status_1 <= divReg_9_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_10_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_10_status_2 <= divReg_9_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_10_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_10_status_3 <= divReg_9_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_10_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_10_status_4 <= divReg_9_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_10_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_10_status_5 <= divReg_9_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_10_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_10_status_6 <= divReg_9_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_10_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_10_status_7 <= divReg_9_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_10_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_10_op <= divReg_9_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_10_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_10_valid <= divReg_9_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_10_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_10_exMantA <= divReg_9_exMantA; // @[SPFPDiv.scala 64:17]
    end
    divReg_11_mantissaA <= _GEN_253[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_11_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_11_mantissaB <= divReg_10_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_11_mantissaC <= _GEN_254[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_11_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_11_exponentA <= divReg_10_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_11_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_11_exponentB <= divReg_10_exponentB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_11_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_11_signBitA <= divReg_10_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_11_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_11_signBitB <= divReg_10_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_11_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_11_faultFlags <= divReg_10_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_11_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_11_status_0 <= divReg_10_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_11_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_11_status_1 <= divReg_10_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_11_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_11_status_2 <= divReg_10_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_11_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_11_status_3 <= divReg_10_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_11_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_11_status_4 <= divReg_10_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_11_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_11_status_5 <= divReg_10_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_11_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_11_status_6 <= divReg_10_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_11_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_11_status_7 <= divReg_10_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_11_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_11_op <= divReg_10_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_11_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_11_valid <= divReg_10_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_11_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_11_exMantA <= divReg_10_exMantA; // @[SPFPDiv.scala 64:17]
    end
    divReg_12_mantissaA <= _GEN_255[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_12_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_12_mantissaB <= divReg_11_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_12_mantissaC <= _GEN_256[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_12_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_12_exponentA <= divReg_11_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_12_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_12_exponentB <= divReg_11_exponentB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_12_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_12_signBitA <= divReg_11_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_12_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_12_signBitB <= divReg_11_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_12_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_12_faultFlags <= divReg_11_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_12_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_12_status_0 <= divReg_11_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_12_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_12_status_1 <= divReg_11_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_12_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_12_status_2 <= divReg_11_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_12_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_12_status_3 <= divReg_11_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_12_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_12_status_4 <= divReg_11_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_12_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_12_status_5 <= divReg_11_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_12_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_12_status_6 <= divReg_11_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_12_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_12_status_7 <= divReg_11_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_12_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_12_op <= divReg_11_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_12_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_12_valid <= divReg_11_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_12_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_12_exMantA <= divReg_11_exMantA; // @[SPFPDiv.scala 64:17]
    end
    divReg_13_mantissaA <= _GEN_257[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_13_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_13_mantissaB <= divReg_12_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_13_mantissaC <= _GEN_258[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_13_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_13_exponentA <= divReg_12_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_13_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_13_exponentB <= divReg_12_exponentB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_13_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_13_signBitA <= divReg_12_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_13_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_13_signBitB <= divReg_12_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_13_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_13_faultFlags <= divReg_12_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_13_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_13_status_0 <= divReg_12_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_13_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_13_status_1 <= divReg_12_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_13_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_13_status_2 <= divReg_12_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_13_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_13_status_3 <= divReg_12_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_13_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_13_status_4 <= divReg_12_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_13_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_13_status_5 <= divReg_12_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_13_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_13_status_6 <= divReg_12_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_13_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_13_status_7 <= divReg_12_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_13_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_13_op <= divReg_12_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_13_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_13_valid <= divReg_12_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_13_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_13_exMantA <= divReg_12_exMantA; // @[SPFPDiv.scala 64:17]
    end
    divReg_14_mantissaA <= _GEN_259[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_14_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_14_mantissaB <= divReg_13_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_14_mantissaC <= _GEN_260[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_14_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_14_exponentA <= divReg_13_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_14_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_14_exponentB <= divReg_13_exponentB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_14_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_14_signBitA <= divReg_13_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_14_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_14_signBitB <= divReg_13_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_14_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_14_faultFlags <= divReg_13_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_14_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_14_status_0 <= divReg_13_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_14_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_14_status_1 <= divReg_13_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_14_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_14_status_2 <= divReg_13_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_14_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_14_status_3 <= divReg_13_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_14_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_14_status_4 <= divReg_13_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_14_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_14_status_5 <= divReg_13_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_14_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_14_status_6 <= divReg_13_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_14_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_14_status_7 <= divReg_13_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_14_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_14_op <= divReg_13_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_14_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_14_valid <= divReg_13_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_14_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_14_exMantA <= divReg_13_exMantA; // @[SPFPDiv.scala 64:17]
    end
    divReg_15_mantissaA <= _GEN_261[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_15_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_15_mantissaB <= divReg_14_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_15_mantissaC <= _GEN_262[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_15_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_15_exponentA <= divReg_14_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_15_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_15_exponentB <= divReg_14_exponentB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_15_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_15_signBitA <= divReg_14_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_15_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_15_signBitB <= divReg_14_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_15_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_15_faultFlags <= divReg_14_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_15_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_15_status_0 <= divReg_14_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_15_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_15_status_1 <= divReg_14_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_15_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_15_status_2 <= divReg_14_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_15_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_15_status_3 <= divReg_14_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_15_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_15_status_4 <= divReg_14_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_15_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_15_status_5 <= divReg_14_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_15_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_15_status_6 <= divReg_14_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_15_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_15_status_7 <= divReg_14_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_15_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_15_op <= divReg_14_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_15_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_15_valid <= divReg_14_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_15_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_15_exMantA <= divReg_14_exMantA; // @[SPFPDiv.scala 64:17]
    end
    divReg_16_mantissaA <= _GEN_263[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_16_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_16_mantissaB <= divReg_15_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_16_mantissaC <= _GEN_264[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_16_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_16_exponentA <= divReg_15_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_16_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_16_exponentB <= divReg_15_exponentB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_16_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_16_signBitA <= divReg_15_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_16_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_16_signBitB <= divReg_15_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_16_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_16_faultFlags <= divReg_15_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_16_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_16_status_0 <= divReg_15_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_16_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_16_status_1 <= divReg_15_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_16_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_16_status_2 <= divReg_15_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_16_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_16_status_3 <= divReg_15_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_16_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_16_status_4 <= divReg_15_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_16_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_16_status_5 <= divReg_15_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_16_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_16_status_6 <= divReg_15_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_16_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_16_status_7 <= divReg_15_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_16_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_16_op <= divReg_15_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_16_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_16_valid <= divReg_15_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_16_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_16_exMantA <= divReg_15_exMantA; // @[SPFPDiv.scala 64:17]
    end
    divReg_17_mantissaA <= _GEN_265[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_17_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_17_mantissaB <= divReg_16_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_17_mantissaC <= _GEN_266[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_17_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_17_exponentA <= divReg_16_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_17_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_17_exponentB <= divReg_16_exponentB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_17_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_17_signBitA <= divReg_16_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_17_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_17_signBitB <= divReg_16_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_17_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_17_faultFlags <= divReg_16_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_17_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_17_status_0 <= divReg_16_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_17_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_17_status_1 <= divReg_16_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_17_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_17_status_2 <= divReg_16_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_17_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_17_status_3 <= divReg_16_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_17_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_17_status_4 <= divReg_16_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_17_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_17_status_5 <= divReg_16_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_17_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_17_status_6 <= divReg_16_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_17_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_17_status_7 <= divReg_16_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_17_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_17_op <= divReg_16_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_17_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_17_valid <= divReg_16_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_17_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_17_exMantA <= divReg_16_exMantA; // @[SPFPDiv.scala 64:17]
    end
    divReg_18_mantissaA <= _GEN_267[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_18_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_18_mantissaB <= divReg_17_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_18_mantissaC <= _GEN_268[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_18_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_18_exponentA <= divReg_17_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_18_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_18_exponentB <= divReg_17_exponentB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_18_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_18_signBitA <= divReg_17_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_18_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_18_signBitB <= divReg_17_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_18_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_18_faultFlags <= divReg_17_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_18_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_18_status_0 <= divReg_17_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_18_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_18_status_1 <= divReg_17_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_18_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_18_status_2 <= divReg_17_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_18_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_18_status_3 <= divReg_17_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_18_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_18_status_4 <= divReg_17_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_18_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_18_status_5 <= divReg_17_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_18_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_18_status_6 <= divReg_17_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_18_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_18_status_7 <= divReg_17_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_18_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_18_op <= divReg_17_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_18_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_18_valid <= divReg_17_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_18_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_18_exMantA <= divReg_17_exMantA; // @[SPFPDiv.scala 64:17]
    end
    divReg_19_mantissaA <= _GEN_269[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_19_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_19_mantissaB <= divReg_18_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_19_mantissaC <= _GEN_270[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_19_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_19_exponentA <= divReg_18_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_19_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_19_exponentB <= divReg_18_exponentB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_19_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_19_signBitA <= divReg_18_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_19_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_19_signBitB <= divReg_18_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_19_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_19_faultFlags <= divReg_18_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_19_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_19_status_0 <= divReg_18_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_19_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_19_status_1 <= divReg_18_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_19_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_19_status_2 <= divReg_18_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_19_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_19_status_3 <= divReg_18_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_19_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_19_status_4 <= divReg_18_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_19_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_19_status_5 <= divReg_18_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_19_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_19_status_6 <= divReg_18_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_19_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_19_status_7 <= divReg_18_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_19_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_19_op <= divReg_18_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_19_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_19_valid <= divReg_18_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_19_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_19_exMantA <= divReg_18_exMantA; // @[SPFPDiv.scala 64:17]
    end
    divReg_20_mantissaA <= _GEN_271[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_20_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_20_mantissaB <= divReg_19_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_20_mantissaC <= _GEN_272[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_20_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_20_exponentA <= divReg_19_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_20_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_20_exponentB <= divReg_19_exponentB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_20_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_20_signBitA <= divReg_19_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_20_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_20_signBitB <= divReg_19_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_20_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_20_faultFlags <= divReg_19_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_20_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_20_status_0 <= divReg_19_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_20_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_20_status_1 <= divReg_19_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_20_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_20_status_2 <= divReg_19_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_20_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_20_status_3 <= divReg_19_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_20_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_20_status_4 <= divReg_19_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_20_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_20_status_5 <= divReg_19_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_20_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_20_status_6 <= divReg_19_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_20_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_20_status_7 <= divReg_19_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_20_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_20_op <= divReg_19_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_20_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_20_valid <= divReg_19_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_20_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_20_exMantA <= divReg_19_exMantA; // @[SPFPDiv.scala 64:17]
    end
    divReg_21_mantissaA <= _GEN_273[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_21_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_21_mantissaB <= divReg_20_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_21_mantissaC <= _GEN_274[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_21_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_21_exponentA <= divReg_20_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_21_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_21_exponentB <= divReg_20_exponentB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_21_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_21_signBitA <= divReg_20_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_21_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_21_signBitB <= divReg_20_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_21_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_21_faultFlags <= divReg_20_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_21_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_21_status_0 <= divReg_20_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_21_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_21_status_1 <= divReg_20_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_21_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_21_status_2 <= divReg_20_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_21_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_21_status_3 <= divReg_20_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_21_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_21_status_4 <= divReg_20_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_21_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_21_status_5 <= divReg_20_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_21_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_21_status_6 <= divReg_20_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_21_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_21_status_7 <= divReg_20_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_21_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_21_op <= divReg_20_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_21_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_21_valid <= divReg_20_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_21_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_21_exMantA <= divReg_20_exMantA; // @[SPFPDiv.scala 64:17]
    end
    divReg_22_mantissaA <= _GEN_275[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_22_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_22_mantissaB <= divReg_21_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_22_mantissaC <= _GEN_276[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_22_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_22_exponentA <= divReg_21_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_22_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_22_exponentB <= divReg_21_exponentB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_22_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_22_signBitA <= divReg_21_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_22_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_22_signBitB <= divReg_21_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_22_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_22_faultFlags <= divReg_21_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_22_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_22_status_0 <= divReg_21_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_22_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_22_status_1 <= divReg_21_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_22_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_22_status_2 <= divReg_21_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_22_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_22_status_3 <= divReg_21_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_22_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_22_status_4 <= divReg_21_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_22_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_22_status_5 <= divReg_21_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_22_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_22_status_6 <= divReg_21_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_22_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_22_status_7 <= divReg_21_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_22_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_22_op <= divReg_21_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_22_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_22_valid <= divReg_21_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_22_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_22_exMantA <= divReg_21_exMantA; // @[SPFPDiv.scala 64:17]
    end
    divReg_23_mantissaA <= _GEN_277[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_23_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_23_mantissaB <= divReg_22_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_23_mantissaC <= _GEN_278[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_23_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_23_exponentA <= divReg_22_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_23_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_23_exponentB <= divReg_22_exponentB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_23_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_23_signBitA <= divReg_22_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_23_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_23_signBitB <= divReg_22_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_23_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_23_faultFlags <= divReg_22_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_23_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_23_status_0 <= divReg_22_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_23_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_23_status_1 <= divReg_22_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_23_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_23_status_2 <= divReg_22_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_23_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_23_status_3 <= divReg_22_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_23_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_23_status_4 <= divReg_22_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_23_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_23_status_5 <= divReg_22_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_23_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_23_status_6 <= divReg_22_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_23_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_23_status_7 <= divReg_22_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_23_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_23_op <= divReg_22_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_23_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_23_valid <= divReg_22_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_23_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_23_exMantA <= divReg_22_exMantA; // @[SPFPDiv.scala 64:17]
    end
    divReg_24_mantissaA <= _GEN_279[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_24_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_24_mantissaB <= divReg_23_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_24_mantissaC <= _GEN_280[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_24_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_24_exponentA <= divReg_23_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_24_exponentB <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_24_exponentB <= divReg_23_exponentB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_24_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_24_signBitA <= divReg_23_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_24_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_24_signBitB <= divReg_23_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_24_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_24_faultFlags <= divReg_23_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_24_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_24_status_0 <= divReg_23_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_24_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_24_status_1 <= divReg_23_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_24_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_24_status_2 <= divReg_23_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_24_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_24_status_3 <= divReg_23_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_24_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_24_status_4 <= divReg_23_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_24_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_24_status_5 <= divReg_23_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_24_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_24_status_6 <= divReg_23_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_24_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_24_status_7 <= divReg_23_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_24_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_24_op <= divReg_23_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_24_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_24_valid <= divReg_23_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_24_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_24_exMantA <= divReg_23_exMantA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_25_mantissaB <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_25_mantissaB <= divReg_24_mantissaB; // @[SPFPDiv.scala 64:17]
    end
    divReg_25_mantissaC <= _GEN_282[25:0]; // @[SPFPDiv.scala 44:{23,23}]
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_25_exponentA <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_25_exponentA <= divReg_24_exponentA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_25_exponentC <= 8'sh0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_25_exponentC <= _divReg_25_exponentC_T_5; // @[SPFPDiv.scala 74:24]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_25_signBitA <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_25_signBitA <= divReg_24_signBitA; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_25_signBitB <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_25_signBitB <= divReg_24_signBitB; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_25_signBitC <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_25_signBitC <= divReg_24_signBitA ^ divReg_24_signBitB; // @[SPFPDiv.scala 75:24]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_25_faultFlags <= 12'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_25_faultFlags <= divReg_24_faultFlags; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_25_status_0 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_25_status_0 <= divReg_24_status_0; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_25_status_1 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_25_status_1 <= divReg_24_status_1; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_25_status_2 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_25_status_2 <= divReg_24_status_2; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_25_status_3 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_25_status_3 <= divReg_24_status_3; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_25_status_4 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_25_status_4 <= divReg_24_status_4; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_25_status_5 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_25_status_5 <= divReg_24_status_5; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_25_status_6 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_25_status_6 <= divReg_24_status_6; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_25_status_7 <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_25_status_7 <= divReg_24_status_7; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_25_op <= 5'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_25_op <= divReg_24_op; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_25_valid <= 1'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_25_valid <= divReg_24_valid; // @[SPFPDiv.scala 64:17]
    end
    if (reset) begin // @[SPFPDiv.scala 44:23]
      divReg_25_exMantA <= 26'h0; // @[SPFPDiv.scala 44:23]
    end else begin
      divReg_25_exMantA <= divReg_24_exMantA; // @[SPFPDiv.scala 64:17]
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  divReg_0_mantissaA = _RAND_0[25:0];
  _RAND_1 = {1{`RANDOM}};
  divReg_0_mantissaB = _RAND_1[25:0];
  _RAND_2 = {1{`RANDOM}};
  divReg_0_exponentA = _RAND_2[7:0];
  _RAND_3 = {1{`RANDOM}};
  divReg_0_exponentB = _RAND_3[7:0];
  _RAND_4 = {1{`RANDOM}};
  divReg_0_signBitA = _RAND_4[0:0];
  _RAND_5 = {1{`RANDOM}};
  divReg_0_signBitB = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  divReg_0_faultFlags = _RAND_6[11:0];
  _RAND_7 = {1{`RANDOM}};
  divReg_0_status_0 = _RAND_7[0:0];
  _RAND_8 = {1{`RANDOM}};
  divReg_0_status_1 = _RAND_8[0:0];
  _RAND_9 = {1{`RANDOM}};
  divReg_0_status_2 = _RAND_9[0:0];
  _RAND_10 = {1{`RANDOM}};
  divReg_0_status_3 = _RAND_10[0:0];
  _RAND_11 = {1{`RANDOM}};
  divReg_0_status_4 = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  divReg_0_status_5 = _RAND_12[0:0];
  _RAND_13 = {1{`RANDOM}};
  divReg_0_status_6 = _RAND_13[0:0];
  _RAND_14 = {1{`RANDOM}};
  divReg_0_status_7 = _RAND_14[0:0];
  _RAND_15 = {1{`RANDOM}};
  divReg_0_op = _RAND_15[4:0];
  _RAND_16 = {1{`RANDOM}};
  divReg_0_valid = _RAND_16[0:0];
  _RAND_17 = {1{`RANDOM}};
  divReg_0_exMantA = _RAND_17[25:0];
  _RAND_18 = {1{`RANDOM}};
  divReg_1_mantissaA = _RAND_18[25:0];
  _RAND_19 = {1{`RANDOM}};
  divReg_1_mantissaB = _RAND_19[25:0];
  _RAND_20 = {1{`RANDOM}};
  divReg_1_mantissaC = _RAND_20[25:0];
  _RAND_21 = {1{`RANDOM}};
  divReg_1_exponentA = _RAND_21[7:0];
  _RAND_22 = {1{`RANDOM}};
  divReg_1_exponentB = _RAND_22[7:0];
  _RAND_23 = {1{`RANDOM}};
  divReg_1_signBitA = _RAND_23[0:0];
  _RAND_24 = {1{`RANDOM}};
  divReg_1_signBitB = _RAND_24[0:0];
  _RAND_25 = {1{`RANDOM}};
  divReg_1_faultFlags = _RAND_25[11:0];
  _RAND_26 = {1{`RANDOM}};
  divReg_1_status_0 = _RAND_26[0:0];
  _RAND_27 = {1{`RANDOM}};
  divReg_1_status_1 = _RAND_27[0:0];
  _RAND_28 = {1{`RANDOM}};
  divReg_1_status_2 = _RAND_28[0:0];
  _RAND_29 = {1{`RANDOM}};
  divReg_1_status_3 = _RAND_29[0:0];
  _RAND_30 = {1{`RANDOM}};
  divReg_1_status_4 = _RAND_30[0:0];
  _RAND_31 = {1{`RANDOM}};
  divReg_1_status_5 = _RAND_31[0:0];
  _RAND_32 = {1{`RANDOM}};
  divReg_1_status_6 = _RAND_32[0:0];
  _RAND_33 = {1{`RANDOM}};
  divReg_1_status_7 = _RAND_33[0:0];
  _RAND_34 = {1{`RANDOM}};
  divReg_1_op = _RAND_34[4:0];
  _RAND_35 = {1{`RANDOM}};
  divReg_1_valid = _RAND_35[0:0];
  _RAND_36 = {1{`RANDOM}};
  divReg_1_exMantA = _RAND_36[25:0];
  _RAND_37 = {1{`RANDOM}};
  divReg_2_mantissaA = _RAND_37[25:0];
  _RAND_38 = {1{`RANDOM}};
  divReg_2_mantissaB = _RAND_38[25:0];
  _RAND_39 = {1{`RANDOM}};
  divReg_2_mantissaC = _RAND_39[25:0];
  _RAND_40 = {1{`RANDOM}};
  divReg_2_exponentA = _RAND_40[7:0];
  _RAND_41 = {1{`RANDOM}};
  divReg_2_exponentB = _RAND_41[7:0];
  _RAND_42 = {1{`RANDOM}};
  divReg_2_signBitA = _RAND_42[0:0];
  _RAND_43 = {1{`RANDOM}};
  divReg_2_signBitB = _RAND_43[0:0];
  _RAND_44 = {1{`RANDOM}};
  divReg_2_faultFlags = _RAND_44[11:0];
  _RAND_45 = {1{`RANDOM}};
  divReg_2_status_0 = _RAND_45[0:0];
  _RAND_46 = {1{`RANDOM}};
  divReg_2_status_1 = _RAND_46[0:0];
  _RAND_47 = {1{`RANDOM}};
  divReg_2_status_2 = _RAND_47[0:0];
  _RAND_48 = {1{`RANDOM}};
  divReg_2_status_3 = _RAND_48[0:0];
  _RAND_49 = {1{`RANDOM}};
  divReg_2_status_4 = _RAND_49[0:0];
  _RAND_50 = {1{`RANDOM}};
  divReg_2_status_5 = _RAND_50[0:0];
  _RAND_51 = {1{`RANDOM}};
  divReg_2_status_6 = _RAND_51[0:0];
  _RAND_52 = {1{`RANDOM}};
  divReg_2_status_7 = _RAND_52[0:0];
  _RAND_53 = {1{`RANDOM}};
  divReg_2_op = _RAND_53[4:0];
  _RAND_54 = {1{`RANDOM}};
  divReg_2_valid = _RAND_54[0:0];
  _RAND_55 = {1{`RANDOM}};
  divReg_2_exMantA = _RAND_55[25:0];
  _RAND_56 = {1{`RANDOM}};
  divReg_3_mantissaA = _RAND_56[25:0];
  _RAND_57 = {1{`RANDOM}};
  divReg_3_mantissaB = _RAND_57[25:0];
  _RAND_58 = {1{`RANDOM}};
  divReg_3_mantissaC = _RAND_58[25:0];
  _RAND_59 = {1{`RANDOM}};
  divReg_3_exponentA = _RAND_59[7:0];
  _RAND_60 = {1{`RANDOM}};
  divReg_3_exponentB = _RAND_60[7:0];
  _RAND_61 = {1{`RANDOM}};
  divReg_3_signBitA = _RAND_61[0:0];
  _RAND_62 = {1{`RANDOM}};
  divReg_3_signBitB = _RAND_62[0:0];
  _RAND_63 = {1{`RANDOM}};
  divReg_3_faultFlags = _RAND_63[11:0];
  _RAND_64 = {1{`RANDOM}};
  divReg_3_status_0 = _RAND_64[0:0];
  _RAND_65 = {1{`RANDOM}};
  divReg_3_status_1 = _RAND_65[0:0];
  _RAND_66 = {1{`RANDOM}};
  divReg_3_status_2 = _RAND_66[0:0];
  _RAND_67 = {1{`RANDOM}};
  divReg_3_status_3 = _RAND_67[0:0];
  _RAND_68 = {1{`RANDOM}};
  divReg_3_status_4 = _RAND_68[0:0];
  _RAND_69 = {1{`RANDOM}};
  divReg_3_status_5 = _RAND_69[0:0];
  _RAND_70 = {1{`RANDOM}};
  divReg_3_status_6 = _RAND_70[0:0];
  _RAND_71 = {1{`RANDOM}};
  divReg_3_status_7 = _RAND_71[0:0];
  _RAND_72 = {1{`RANDOM}};
  divReg_3_op = _RAND_72[4:0];
  _RAND_73 = {1{`RANDOM}};
  divReg_3_valid = _RAND_73[0:0];
  _RAND_74 = {1{`RANDOM}};
  divReg_3_exMantA = _RAND_74[25:0];
  _RAND_75 = {1{`RANDOM}};
  divReg_4_mantissaA = _RAND_75[25:0];
  _RAND_76 = {1{`RANDOM}};
  divReg_4_mantissaB = _RAND_76[25:0];
  _RAND_77 = {1{`RANDOM}};
  divReg_4_mantissaC = _RAND_77[25:0];
  _RAND_78 = {1{`RANDOM}};
  divReg_4_exponentA = _RAND_78[7:0];
  _RAND_79 = {1{`RANDOM}};
  divReg_4_exponentB = _RAND_79[7:0];
  _RAND_80 = {1{`RANDOM}};
  divReg_4_signBitA = _RAND_80[0:0];
  _RAND_81 = {1{`RANDOM}};
  divReg_4_signBitB = _RAND_81[0:0];
  _RAND_82 = {1{`RANDOM}};
  divReg_4_faultFlags = _RAND_82[11:0];
  _RAND_83 = {1{`RANDOM}};
  divReg_4_status_0 = _RAND_83[0:0];
  _RAND_84 = {1{`RANDOM}};
  divReg_4_status_1 = _RAND_84[0:0];
  _RAND_85 = {1{`RANDOM}};
  divReg_4_status_2 = _RAND_85[0:0];
  _RAND_86 = {1{`RANDOM}};
  divReg_4_status_3 = _RAND_86[0:0];
  _RAND_87 = {1{`RANDOM}};
  divReg_4_status_4 = _RAND_87[0:0];
  _RAND_88 = {1{`RANDOM}};
  divReg_4_status_5 = _RAND_88[0:0];
  _RAND_89 = {1{`RANDOM}};
  divReg_4_status_6 = _RAND_89[0:0];
  _RAND_90 = {1{`RANDOM}};
  divReg_4_status_7 = _RAND_90[0:0];
  _RAND_91 = {1{`RANDOM}};
  divReg_4_op = _RAND_91[4:0];
  _RAND_92 = {1{`RANDOM}};
  divReg_4_valid = _RAND_92[0:0];
  _RAND_93 = {1{`RANDOM}};
  divReg_4_exMantA = _RAND_93[25:0];
  _RAND_94 = {1{`RANDOM}};
  divReg_5_mantissaA = _RAND_94[25:0];
  _RAND_95 = {1{`RANDOM}};
  divReg_5_mantissaB = _RAND_95[25:0];
  _RAND_96 = {1{`RANDOM}};
  divReg_5_mantissaC = _RAND_96[25:0];
  _RAND_97 = {1{`RANDOM}};
  divReg_5_exponentA = _RAND_97[7:0];
  _RAND_98 = {1{`RANDOM}};
  divReg_5_exponentB = _RAND_98[7:0];
  _RAND_99 = {1{`RANDOM}};
  divReg_5_signBitA = _RAND_99[0:0];
  _RAND_100 = {1{`RANDOM}};
  divReg_5_signBitB = _RAND_100[0:0];
  _RAND_101 = {1{`RANDOM}};
  divReg_5_faultFlags = _RAND_101[11:0];
  _RAND_102 = {1{`RANDOM}};
  divReg_5_status_0 = _RAND_102[0:0];
  _RAND_103 = {1{`RANDOM}};
  divReg_5_status_1 = _RAND_103[0:0];
  _RAND_104 = {1{`RANDOM}};
  divReg_5_status_2 = _RAND_104[0:0];
  _RAND_105 = {1{`RANDOM}};
  divReg_5_status_3 = _RAND_105[0:0];
  _RAND_106 = {1{`RANDOM}};
  divReg_5_status_4 = _RAND_106[0:0];
  _RAND_107 = {1{`RANDOM}};
  divReg_5_status_5 = _RAND_107[0:0];
  _RAND_108 = {1{`RANDOM}};
  divReg_5_status_6 = _RAND_108[0:0];
  _RAND_109 = {1{`RANDOM}};
  divReg_5_status_7 = _RAND_109[0:0];
  _RAND_110 = {1{`RANDOM}};
  divReg_5_op = _RAND_110[4:0];
  _RAND_111 = {1{`RANDOM}};
  divReg_5_valid = _RAND_111[0:0];
  _RAND_112 = {1{`RANDOM}};
  divReg_5_exMantA = _RAND_112[25:0];
  _RAND_113 = {1{`RANDOM}};
  divReg_6_mantissaA = _RAND_113[25:0];
  _RAND_114 = {1{`RANDOM}};
  divReg_6_mantissaB = _RAND_114[25:0];
  _RAND_115 = {1{`RANDOM}};
  divReg_6_mantissaC = _RAND_115[25:0];
  _RAND_116 = {1{`RANDOM}};
  divReg_6_exponentA = _RAND_116[7:0];
  _RAND_117 = {1{`RANDOM}};
  divReg_6_exponentB = _RAND_117[7:0];
  _RAND_118 = {1{`RANDOM}};
  divReg_6_signBitA = _RAND_118[0:0];
  _RAND_119 = {1{`RANDOM}};
  divReg_6_signBitB = _RAND_119[0:0];
  _RAND_120 = {1{`RANDOM}};
  divReg_6_faultFlags = _RAND_120[11:0];
  _RAND_121 = {1{`RANDOM}};
  divReg_6_status_0 = _RAND_121[0:0];
  _RAND_122 = {1{`RANDOM}};
  divReg_6_status_1 = _RAND_122[0:0];
  _RAND_123 = {1{`RANDOM}};
  divReg_6_status_2 = _RAND_123[0:0];
  _RAND_124 = {1{`RANDOM}};
  divReg_6_status_3 = _RAND_124[0:0];
  _RAND_125 = {1{`RANDOM}};
  divReg_6_status_4 = _RAND_125[0:0];
  _RAND_126 = {1{`RANDOM}};
  divReg_6_status_5 = _RAND_126[0:0];
  _RAND_127 = {1{`RANDOM}};
  divReg_6_status_6 = _RAND_127[0:0];
  _RAND_128 = {1{`RANDOM}};
  divReg_6_status_7 = _RAND_128[0:0];
  _RAND_129 = {1{`RANDOM}};
  divReg_6_op = _RAND_129[4:0];
  _RAND_130 = {1{`RANDOM}};
  divReg_6_valid = _RAND_130[0:0];
  _RAND_131 = {1{`RANDOM}};
  divReg_6_exMantA = _RAND_131[25:0];
  _RAND_132 = {1{`RANDOM}};
  divReg_7_mantissaA = _RAND_132[25:0];
  _RAND_133 = {1{`RANDOM}};
  divReg_7_mantissaB = _RAND_133[25:0];
  _RAND_134 = {1{`RANDOM}};
  divReg_7_mantissaC = _RAND_134[25:0];
  _RAND_135 = {1{`RANDOM}};
  divReg_7_exponentA = _RAND_135[7:0];
  _RAND_136 = {1{`RANDOM}};
  divReg_7_exponentB = _RAND_136[7:0];
  _RAND_137 = {1{`RANDOM}};
  divReg_7_signBitA = _RAND_137[0:0];
  _RAND_138 = {1{`RANDOM}};
  divReg_7_signBitB = _RAND_138[0:0];
  _RAND_139 = {1{`RANDOM}};
  divReg_7_faultFlags = _RAND_139[11:0];
  _RAND_140 = {1{`RANDOM}};
  divReg_7_status_0 = _RAND_140[0:0];
  _RAND_141 = {1{`RANDOM}};
  divReg_7_status_1 = _RAND_141[0:0];
  _RAND_142 = {1{`RANDOM}};
  divReg_7_status_2 = _RAND_142[0:0];
  _RAND_143 = {1{`RANDOM}};
  divReg_7_status_3 = _RAND_143[0:0];
  _RAND_144 = {1{`RANDOM}};
  divReg_7_status_4 = _RAND_144[0:0];
  _RAND_145 = {1{`RANDOM}};
  divReg_7_status_5 = _RAND_145[0:0];
  _RAND_146 = {1{`RANDOM}};
  divReg_7_status_6 = _RAND_146[0:0];
  _RAND_147 = {1{`RANDOM}};
  divReg_7_status_7 = _RAND_147[0:0];
  _RAND_148 = {1{`RANDOM}};
  divReg_7_op = _RAND_148[4:0];
  _RAND_149 = {1{`RANDOM}};
  divReg_7_valid = _RAND_149[0:0];
  _RAND_150 = {1{`RANDOM}};
  divReg_7_exMantA = _RAND_150[25:0];
  _RAND_151 = {1{`RANDOM}};
  divReg_8_mantissaA = _RAND_151[25:0];
  _RAND_152 = {1{`RANDOM}};
  divReg_8_mantissaB = _RAND_152[25:0];
  _RAND_153 = {1{`RANDOM}};
  divReg_8_mantissaC = _RAND_153[25:0];
  _RAND_154 = {1{`RANDOM}};
  divReg_8_exponentA = _RAND_154[7:0];
  _RAND_155 = {1{`RANDOM}};
  divReg_8_exponentB = _RAND_155[7:0];
  _RAND_156 = {1{`RANDOM}};
  divReg_8_signBitA = _RAND_156[0:0];
  _RAND_157 = {1{`RANDOM}};
  divReg_8_signBitB = _RAND_157[0:0];
  _RAND_158 = {1{`RANDOM}};
  divReg_8_faultFlags = _RAND_158[11:0];
  _RAND_159 = {1{`RANDOM}};
  divReg_8_status_0 = _RAND_159[0:0];
  _RAND_160 = {1{`RANDOM}};
  divReg_8_status_1 = _RAND_160[0:0];
  _RAND_161 = {1{`RANDOM}};
  divReg_8_status_2 = _RAND_161[0:0];
  _RAND_162 = {1{`RANDOM}};
  divReg_8_status_3 = _RAND_162[0:0];
  _RAND_163 = {1{`RANDOM}};
  divReg_8_status_4 = _RAND_163[0:0];
  _RAND_164 = {1{`RANDOM}};
  divReg_8_status_5 = _RAND_164[0:0];
  _RAND_165 = {1{`RANDOM}};
  divReg_8_status_6 = _RAND_165[0:0];
  _RAND_166 = {1{`RANDOM}};
  divReg_8_status_7 = _RAND_166[0:0];
  _RAND_167 = {1{`RANDOM}};
  divReg_8_op = _RAND_167[4:0];
  _RAND_168 = {1{`RANDOM}};
  divReg_8_valid = _RAND_168[0:0];
  _RAND_169 = {1{`RANDOM}};
  divReg_8_exMantA = _RAND_169[25:0];
  _RAND_170 = {1{`RANDOM}};
  divReg_9_mantissaA = _RAND_170[25:0];
  _RAND_171 = {1{`RANDOM}};
  divReg_9_mantissaB = _RAND_171[25:0];
  _RAND_172 = {1{`RANDOM}};
  divReg_9_mantissaC = _RAND_172[25:0];
  _RAND_173 = {1{`RANDOM}};
  divReg_9_exponentA = _RAND_173[7:0];
  _RAND_174 = {1{`RANDOM}};
  divReg_9_exponentB = _RAND_174[7:0];
  _RAND_175 = {1{`RANDOM}};
  divReg_9_signBitA = _RAND_175[0:0];
  _RAND_176 = {1{`RANDOM}};
  divReg_9_signBitB = _RAND_176[0:0];
  _RAND_177 = {1{`RANDOM}};
  divReg_9_faultFlags = _RAND_177[11:0];
  _RAND_178 = {1{`RANDOM}};
  divReg_9_status_0 = _RAND_178[0:0];
  _RAND_179 = {1{`RANDOM}};
  divReg_9_status_1 = _RAND_179[0:0];
  _RAND_180 = {1{`RANDOM}};
  divReg_9_status_2 = _RAND_180[0:0];
  _RAND_181 = {1{`RANDOM}};
  divReg_9_status_3 = _RAND_181[0:0];
  _RAND_182 = {1{`RANDOM}};
  divReg_9_status_4 = _RAND_182[0:0];
  _RAND_183 = {1{`RANDOM}};
  divReg_9_status_5 = _RAND_183[0:0];
  _RAND_184 = {1{`RANDOM}};
  divReg_9_status_6 = _RAND_184[0:0];
  _RAND_185 = {1{`RANDOM}};
  divReg_9_status_7 = _RAND_185[0:0];
  _RAND_186 = {1{`RANDOM}};
  divReg_9_op = _RAND_186[4:0];
  _RAND_187 = {1{`RANDOM}};
  divReg_9_valid = _RAND_187[0:0];
  _RAND_188 = {1{`RANDOM}};
  divReg_9_exMantA = _RAND_188[25:0];
  _RAND_189 = {1{`RANDOM}};
  divReg_10_mantissaA = _RAND_189[25:0];
  _RAND_190 = {1{`RANDOM}};
  divReg_10_mantissaB = _RAND_190[25:0];
  _RAND_191 = {1{`RANDOM}};
  divReg_10_mantissaC = _RAND_191[25:0];
  _RAND_192 = {1{`RANDOM}};
  divReg_10_exponentA = _RAND_192[7:0];
  _RAND_193 = {1{`RANDOM}};
  divReg_10_exponentB = _RAND_193[7:0];
  _RAND_194 = {1{`RANDOM}};
  divReg_10_signBitA = _RAND_194[0:0];
  _RAND_195 = {1{`RANDOM}};
  divReg_10_signBitB = _RAND_195[0:0];
  _RAND_196 = {1{`RANDOM}};
  divReg_10_faultFlags = _RAND_196[11:0];
  _RAND_197 = {1{`RANDOM}};
  divReg_10_status_0 = _RAND_197[0:0];
  _RAND_198 = {1{`RANDOM}};
  divReg_10_status_1 = _RAND_198[0:0];
  _RAND_199 = {1{`RANDOM}};
  divReg_10_status_2 = _RAND_199[0:0];
  _RAND_200 = {1{`RANDOM}};
  divReg_10_status_3 = _RAND_200[0:0];
  _RAND_201 = {1{`RANDOM}};
  divReg_10_status_4 = _RAND_201[0:0];
  _RAND_202 = {1{`RANDOM}};
  divReg_10_status_5 = _RAND_202[0:0];
  _RAND_203 = {1{`RANDOM}};
  divReg_10_status_6 = _RAND_203[0:0];
  _RAND_204 = {1{`RANDOM}};
  divReg_10_status_7 = _RAND_204[0:0];
  _RAND_205 = {1{`RANDOM}};
  divReg_10_op = _RAND_205[4:0];
  _RAND_206 = {1{`RANDOM}};
  divReg_10_valid = _RAND_206[0:0];
  _RAND_207 = {1{`RANDOM}};
  divReg_10_exMantA = _RAND_207[25:0];
  _RAND_208 = {1{`RANDOM}};
  divReg_11_mantissaA = _RAND_208[25:0];
  _RAND_209 = {1{`RANDOM}};
  divReg_11_mantissaB = _RAND_209[25:0];
  _RAND_210 = {1{`RANDOM}};
  divReg_11_mantissaC = _RAND_210[25:0];
  _RAND_211 = {1{`RANDOM}};
  divReg_11_exponentA = _RAND_211[7:0];
  _RAND_212 = {1{`RANDOM}};
  divReg_11_exponentB = _RAND_212[7:0];
  _RAND_213 = {1{`RANDOM}};
  divReg_11_signBitA = _RAND_213[0:0];
  _RAND_214 = {1{`RANDOM}};
  divReg_11_signBitB = _RAND_214[0:0];
  _RAND_215 = {1{`RANDOM}};
  divReg_11_faultFlags = _RAND_215[11:0];
  _RAND_216 = {1{`RANDOM}};
  divReg_11_status_0 = _RAND_216[0:0];
  _RAND_217 = {1{`RANDOM}};
  divReg_11_status_1 = _RAND_217[0:0];
  _RAND_218 = {1{`RANDOM}};
  divReg_11_status_2 = _RAND_218[0:0];
  _RAND_219 = {1{`RANDOM}};
  divReg_11_status_3 = _RAND_219[0:0];
  _RAND_220 = {1{`RANDOM}};
  divReg_11_status_4 = _RAND_220[0:0];
  _RAND_221 = {1{`RANDOM}};
  divReg_11_status_5 = _RAND_221[0:0];
  _RAND_222 = {1{`RANDOM}};
  divReg_11_status_6 = _RAND_222[0:0];
  _RAND_223 = {1{`RANDOM}};
  divReg_11_status_7 = _RAND_223[0:0];
  _RAND_224 = {1{`RANDOM}};
  divReg_11_op = _RAND_224[4:0];
  _RAND_225 = {1{`RANDOM}};
  divReg_11_valid = _RAND_225[0:0];
  _RAND_226 = {1{`RANDOM}};
  divReg_11_exMantA = _RAND_226[25:0];
  _RAND_227 = {1{`RANDOM}};
  divReg_12_mantissaA = _RAND_227[25:0];
  _RAND_228 = {1{`RANDOM}};
  divReg_12_mantissaB = _RAND_228[25:0];
  _RAND_229 = {1{`RANDOM}};
  divReg_12_mantissaC = _RAND_229[25:0];
  _RAND_230 = {1{`RANDOM}};
  divReg_12_exponentA = _RAND_230[7:0];
  _RAND_231 = {1{`RANDOM}};
  divReg_12_exponentB = _RAND_231[7:0];
  _RAND_232 = {1{`RANDOM}};
  divReg_12_signBitA = _RAND_232[0:0];
  _RAND_233 = {1{`RANDOM}};
  divReg_12_signBitB = _RAND_233[0:0];
  _RAND_234 = {1{`RANDOM}};
  divReg_12_faultFlags = _RAND_234[11:0];
  _RAND_235 = {1{`RANDOM}};
  divReg_12_status_0 = _RAND_235[0:0];
  _RAND_236 = {1{`RANDOM}};
  divReg_12_status_1 = _RAND_236[0:0];
  _RAND_237 = {1{`RANDOM}};
  divReg_12_status_2 = _RAND_237[0:0];
  _RAND_238 = {1{`RANDOM}};
  divReg_12_status_3 = _RAND_238[0:0];
  _RAND_239 = {1{`RANDOM}};
  divReg_12_status_4 = _RAND_239[0:0];
  _RAND_240 = {1{`RANDOM}};
  divReg_12_status_5 = _RAND_240[0:0];
  _RAND_241 = {1{`RANDOM}};
  divReg_12_status_6 = _RAND_241[0:0];
  _RAND_242 = {1{`RANDOM}};
  divReg_12_status_7 = _RAND_242[0:0];
  _RAND_243 = {1{`RANDOM}};
  divReg_12_op = _RAND_243[4:0];
  _RAND_244 = {1{`RANDOM}};
  divReg_12_valid = _RAND_244[0:0];
  _RAND_245 = {1{`RANDOM}};
  divReg_12_exMantA = _RAND_245[25:0];
  _RAND_246 = {1{`RANDOM}};
  divReg_13_mantissaA = _RAND_246[25:0];
  _RAND_247 = {1{`RANDOM}};
  divReg_13_mantissaB = _RAND_247[25:0];
  _RAND_248 = {1{`RANDOM}};
  divReg_13_mantissaC = _RAND_248[25:0];
  _RAND_249 = {1{`RANDOM}};
  divReg_13_exponentA = _RAND_249[7:0];
  _RAND_250 = {1{`RANDOM}};
  divReg_13_exponentB = _RAND_250[7:0];
  _RAND_251 = {1{`RANDOM}};
  divReg_13_signBitA = _RAND_251[0:0];
  _RAND_252 = {1{`RANDOM}};
  divReg_13_signBitB = _RAND_252[0:0];
  _RAND_253 = {1{`RANDOM}};
  divReg_13_faultFlags = _RAND_253[11:0];
  _RAND_254 = {1{`RANDOM}};
  divReg_13_status_0 = _RAND_254[0:0];
  _RAND_255 = {1{`RANDOM}};
  divReg_13_status_1 = _RAND_255[0:0];
  _RAND_256 = {1{`RANDOM}};
  divReg_13_status_2 = _RAND_256[0:0];
  _RAND_257 = {1{`RANDOM}};
  divReg_13_status_3 = _RAND_257[0:0];
  _RAND_258 = {1{`RANDOM}};
  divReg_13_status_4 = _RAND_258[0:0];
  _RAND_259 = {1{`RANDOM}};
  divReg_13_status_5 = _RAND_259[0:0];
  _RAND_260 = {1{`RANDOM}};
  divReg_13_status_6 = _RAND_260[0:0];
  _RAND_261 = {1{`RANDOM}};
  divReg_13_status_7 = _RAND_261[0:0];
  _RAND_262 = {1{`RANDOM}};
  divReg_13_op = _RAND_262[4:0];
  _RAND_263 = {1{`RANDOM}};
  divReg_13_valid = _RAND_263[0:0];
  _RAND_264 = {1{`RANDOM}};
  divReg_13_exMantA = _RAND_264[25:0];
  _RAND_265 = {1{`RANDOM}};
  divReg_14_mantissaA = _RAND_265[25:0];
  _RAND_266 = {1{`RANDOM}};
  divReg_14_mantissaB = _RAND_266[25:0];
  _RAND_267 = {1{`RANDOM}};
  divReg_14_mantissaC = _RAND_267[25:0];
  _RAND_268 = {1{`RANDOM}};
  divReg_14_exponentA = _RAND_268[7:0];
  _RAND_269 = {1{`RANDOM}};
  divReg_14_exponentB = _RAND_269[7:0];
  _RAND_270 = {1{`RANDOM}};
  divReg_14_signBitA = _RAND_270[0:0];
  _RAND_271 = {1{`RANDOM}};
  divReg_14_signBitB = _RAND_271[0:0];
  _RAND_272 = {1{`RANDOM}};
  divReg_14_faultFlags = _RAND_272[11:0];
  _RAND_273 = {1{`RANDOM}};
  divReg_14_status_0 = _RAND_273[0:0];
  _RAND_274 = {1{`RANDOM}};
  divReg_14_status_1 = _RAND_274[0:0];
  _RAND_275 = {1{`RANDOM}};
  divReg_14_status_2 = _RAND_275[0:0];
  _RAND_276 = {1{`RANDOM}};
  divReg_14_status_3 = _RAND_276[0:0];
  _RAND_277 = {1{`RANDOM}};
  divReg_14_status_4 = _RAND_277[0:0];
  _RAND_278 = {1{`RANDOM}};
  divReg_14_status_5 = _RAND_278[0:0];
  _RAND_279 = {1{`RANDOM}};
  divReg_14_status_6 = _RAND_279[0:0];
  _RAND_280 = {1{`RANDOM}};
  divReg_14_status_7 = _RAND_280[0:0];
  _RAND_281 = {1{`RANDOM}};
  divReg_14_op = _RAND_281[4:0];
  _RAND_282 = {1{`RANDOM}};
  divReg_14_valid = _RAND_282[0:0];
  _RAND_283 = {1{`RANDOM}};
  divReg_14_exMantA = _RAND_283[25:0];
  _RAND_284 = {1{`RANDOM}};
  divReg_15_mantissaA = _RAND_284[25:0];
  _RAND_285 = {1{`RANDOM}};
  divReg_15_mantissaB = _RAND_285[25:0];
  _RAND_286 = {1{`RANDOM}};
  divReg_15_mantissaC = _RAND_286[25:0];
  _RAND_287 = {1{`RANDOM}};
  divReg_15_exponentA = _RAND_287[7:0];
  _RAND_288 = {1{`RANDOM}};
  divReg_15_exponentB = _RAND_288[7:0];
  _RAND_289 = {1{`RANDOM}};
  divReg_15_signBitA = _RAND_289[0:0];
  _RAND_290 = {1{`RANDOM}};
  divReg_15_signBitB = _RAND_290[0:0];
  _RAND_291 = {1{`RANDOM}};
  divReg_15_faultFlags = _RAND_291[11:0];
  _RAND_292 = {1{`RANDOM}};
  divReg_15_status_0 = _RAND_292[0:0];
  _RAND_293 = {1{`RANDOM}};
  divReg_15_status_1 = _RAND_293[0:0];
  _RAND_294 = {1{`RANDOM}};
  divReg_15_status_2 = _RAND_294[0:0];
  _RAND_295 = {1{`RANDOM}};
  divReg_15_status_3 = _RAND_295[0:0];
  _RAND_296 = {1{`RANDOM}};
  divReg_15_status_4 = _RAND_296[0:0];
  _RAND_297 = {1{`RANDOM}};
  divReg_15_status_5 = _RAND_297[0:0];
  _RAND_298 = {1{`RANDOM}};
  divReg_15_status_6 = _RAND_298[0:0];
  _RAND_299 = {1{`RANDOM}};
  divReg_15_status_7 = _RAND_299[0:0];
  _RAND_300 = {1{`RANDOM}};
  divReg_15_op = _RAND_300[4:0];
  _RAND_301 = {1{`RANDOM}};
  divReg_15_valid = _RAND_301[0:0];
  _RAND_302 = {1{`RANDOM}};
  divReg_15_exMantA = _RAND_302[25:0];
  _RAND_303 = {1{`RANDOM}};
  divReg_16_mantissaA = _RAND_303[25:0];
  _RAND_304 = {1{`RANDOM}};
  divReg_16_mantissaB = _RAND_304[25:0];
  _RAND_305 = {1{`RANDOM}};
  divReg_16_mantissaC = _RAND_305[25:0];
  _RAND_306 = {1{`RANDOM}};
  divReg_16_exponentA = _RAND_306[7:0];
  _RAND_307 = {1{`RANDOM}};
  divReg_16_exponentB = _RAND_307[7:0];
  _RAND_308 = {1{`RANDOM}};
  divReg_16_signBitA = _RAND_308[0:0];
  _RAND_309 = {1{`RANDOM}};
  divReg_16_signBitB = _RAND_309[0:0];
  _RAND_310 = {1{`RANDOM}};
  divReg_16_faultFlags = _RAND_310[11:0];
  _RAND_311 = {1{`RANDOM}};
  divReg_16_status_0 = _RAND_311[0:0];
  _RAND_312 = {1{`RANDOM}};
  divReg_16_status_1 = _RAND_312[0:0];
  _RAND_313 = {1{`RANDOM}};
  divReg_16_status_2 = _RAND_313[0:0];
  _RAND_314 = {1{`RANDOM}};
  divReg_16_status_3 = _RAND_314[0:0];
  _RAND_315 = {1{`RANDOM}};
  divReg_16_status_4 = _RAND_315[0:0];
  _RAND_316 = {1{`RANDOM}};
  divReg_16_status_5 = _RAND_316[0:0];
  _RAND_317 = {1{`RANDOM}};
  divReg_16_status_6 = _RAND_317[0:0];
  _RAND_318 = {1{`RANDOM}};
  divReg_16_status_7 = _RAND_318[0:0];
  _RAND_319 = {1{`RANDOM}};
  divReg_16_op = _RAND_319[4:0];
  _RAND_320 = {1{`RANDOM}};
  divReg_16_valid = _RAND_320[0:0];
  _RAND_321 = {1{`RANDOM}};
  divReg_16_exMantA = _RAND_321[25:0];
  _RAND_322 = {1{`RANDOM}};
  divReg_17_mantissaA = _RAND_322[25:0];
  _RAND_323 = {1{`RANDOM}};
  divReg_17_mantissaB = _RAND_323[25:0];
  _RAND_324 = {1{`RANDOM}};
  divReg_17_mantissaC = _RAND_324[25:0];
  _RAND_325 = {1{`RANDOM}};
  divReg_17_exponentA = _RAND_325[7:0];
  _RAND_326 = {1{`RANDOM}};
  divReg_17_exponentB = _RAND_326[7:0];
  _RAND_327 = {1{`RANDOM}};
  divReg_17_signBitA = _RAND_327[0:0];
  _RAND_328 = {1{`RANDOM}};
  divReg_17_signBitB = _RAND_328[0:0];
  _RAND_329 = {1{`RANDOM}};
  divReg_17_faultFlags = _RAND_329[11:0];
  _RAND_330 = {1{`RANDOM}};
  divReg_17_status_0 = _RAND_330[0:0];
  _RAND_331 = {1{`RANDOM}};
  divReg_17_status_1 = _RAND_331[0:0];
  _RAND_332 = {1{`RANDOM}};
  divReg_17_status_2 = _RAND_332[0:0];
  _RAND_333 = {1{`RANDOM}};
  divReg_17_status_3 = _RAND_333[0:0];
  _RAND_334 = {1{`RANDOM}};
  divReg_17_status_4 = _RAND_334[0:0];
  _RAND_335 = {1{`RANDOM}};
  divReg_17_status_5 = _RAND_335[0:0];
  _RAND_336 = {1{`RANDOM}};
  divReg_17_status_6 = _RAND_336[0:0];
  _RAND_337 = {1{`RANDOM}};
  divReg_17_status_7 = _RAND_337[0:0];
  _RAND_338 = {1{`RANDOM}};
  divReg_17_op = _RAND_338[4:0];
  _RAND_339 = {1{`RANDOM}};
  divReg_17_valid = _RAND_339[0:0];
  _RAND_340 = {1{`RANDOM}};
  divReg_17_exMantA = _RAND_340[25:0];
  _RAND_341 = {1{`RANDOM}};
  divReg_18_mantissaA = _RAND_341[25:0];
  _RAND_342 = {1{`RANDOM}};
  divReg_18_mantissaB = _RAND_342[25:0];
  _RAND_343 = {1{`RANDOM}};
  divReg_18_mantissaC = _RAND_343[25:0];
  _RAND_344 = {1{`RANDOM}};
  divReg_18_exponentA = _RAND_344[7:0];
  _RAND_345 = {1{`RANDOM}};
  divReg_18_exponentB = _RAND_345[7:0];
  _RAND_346 = {1{`RANDOM}};
  divReg_18_signBitA = _RAND_346[0:0];
  _RAND_347 = {1{`RANDOM}};
  divReg_18_signBitB = _RAND_347[0:0];
  _RAND_348 = {1{`RANDOM}};
  divReg_18_faultFlags = _RAND_348[11:0];
  _RAND_349 = {1{`RANDOM}};
  divReg_18_status_0 = _RAND_349[0:0];
  _RAND_350 = {1{`RANDOM}};
  divReg_18_status_1 = _RAND_350[0:0];
  _RAND_351 = {1{`RANDOM}};
  divReg_18_status_2 = _RAND_351[0:0];
  _RAND_352 = {1{`RANDOM}};
  divReg_18_status_3 = _RAND_352[0:0];
  _RAND_353 = {1{`RANDOM}};
  divReg_18_status_4 = _RAND_353[0:0];
  _RAND_354 = {1{`RANDOM}};
  divReg_18_status_5 = _RAND_354[0:0];
  _RAND_355 = {1{`RANDOM}};
  divReg_18_status_6 = _RAND_355[0:0];
  _RAND_356 = {1{`RANDOM}};
  divReg_18_status_7 = _RAND_356[0:0];
  _RAND_357 = {1{`RANDOM}};
  divReg_18_op = _RAND_357[4:0];
  _RAND_358 = {1{`RANDOM}};
  divReg_18_valid = _RAND_358[0:0];
  _RAND_359 = {1{`RANDOM}};
  divReg_18_exMantA = _RAND_359[25:0];
  _RAND_360 = {1{`RANDOM}};
  divReg_19_mantissaA = _RAND_360[25:0];
  _RAND_361 = {1{`RANDOM}};
  divReg_19_mantissaB = _RAND_361[25:0];
  _RAND_362 = {1{`RANDOM}};
  divReg_19_mantissaC = _RAND_362[25:0];
  _RAND_363 = {1{`RANDOM}};
  divReg_19_exponentA = _RAND_363[7:0];
  _RAND_364 = {1{`RANDOM}};
  divReg_19_exponentB = _RAND_364[7:0];
  _RAND_365 = {1{`RANDOM}};
  divReg_19_signBitA = _RAND_365[0:0];
  _RAND_366 = {1{`RANDOM}};
  divReg_19_signBitB = _RAND_366[0:0];
  _RAND_367 = {1{`RANDOM}};
  divReg_19_faultFlags = _RAND_367[11:0];
  _RAND_368 = {1{`RANDOM}};
  divReg_19_status_0 = _RAND_368[0:0];
  _RAND_369 = {1{`RANDOM}};
  divReg_19_status_1 = _RAND_369[0:0];
  _RAND_370 = {1{`RANDOM}};
  divReg_19_status_2 = _RAND_370[0:0];
  _RAND_371 = {1{`RANDOM}};
  divReg_19_status_3 = _RAND_371[0:0];
  _RAND_372 = {1{`RANDOM}};
  divReg_19_status_4 = _RAND_372[0:0];
  _RAND_373 = {1{`RANDOM}};
  divReg_19_status_5 = _RAND_373[0:0];
  _RAND_374 = {1{`RANDOM}};
  divReg_19_status_6 = _RAND_374[0:0];
  _RAND_375 = {1{`RANDOM}};
  divReg_19_status_7 = _RAND_375[0:0];
  _RAND_376 = {1{`RANDOM}};
  divReg_19_op = _RAND_376[4:0];
  _RAND_377 = {1{`RANDOM}};
  divReg_19_valid = _RAND_377[0:0];
  _RAND_378 = {1{`RANDOM}};
  divReg_19_exMantA = _RAND_378[25:0];
  _RAND_379 = {1{`RANDOM}};
  divReg_20_mantissaA = _RAND_379[25:0];
  _RAND_380 = {1{`RANDOM}};
  divReg_20_mantissaB = _RAND_380[25:0];
  _RAND_381 = {1{`RANDOM}};
  divReg_20_mantissaC = _RAND_381[25:0];
  _RAND_382 = {1{`RANDOM}};
  divReg_20_exponentA = _RAND_382[7:0];
  _RAND_383 = {1{`RANDOM}};
  divReg_20_exponentB = _RAND_383[7:0];
  _RAND_384 = {1{`RANDOM}};
  divReg_20_signBitA = _RAND_384[0:0];
  _RAND_385 = {1{`RANDOM}};
  divReg_20_signBitB = _RAND_385[0:0];
  _RAND_386 = {1{`RANDOM}};
  divReg_20_faultFlags = _RAND_386[11:0];
  _RAND_387 = {1{`RANDOM}};
  divReg_20_status_0 = _RAND_387[0:0];
  _RAND_388 = {1{`RANDOM}};
  divReg_20_status_1 = _RAND_388[0:0];
  _RAND_389 = {1{`RANDOM}};
  divReg_20_status_2 = _RAND_389[0:0];
  _RAND_390 = {1{`RANDOM}};
  divReg_20_status_3 = _RAND_390[0:0];
  _RAND_391 = {1{`RANDOM}};
  divReg_20_status_4 = _RAND_391[0:0];
  _RAND_392 = {1{`RANDOM}};
  divReg_20_status_5 = _RAND_392[0:0];
  _RAND_393 = {1{`RANDOM}};
  divReg_20_status_6 = _RAND_393[0:0];
  _RAND_394 = {1{`RANDOM}};
  divReg_20_status_7 = _RAND_394[0:0];
  _RAND_395 = {1{`RANDOM}};
  divReg_20_op = _RAND_395[4:0];
  _RAND_396 = {1{`RANDOM}};
  divReg_20_valid = _RAND_396[0:0];
  _RAND_397 = {1{`RANDOM}};
  divReg_20_exMantA = _RAND_397[25:0];
  _RAND_398 = {1{`RANDOM}};
  divReg_21_mantissaA = _RAND_398[25:0];
  _RAND_399 = {1{`RANDOM}};
  divReg_21_mantissaB = _RAND_399[25:0];
  _RAND_400 = {1{`RANDOM}};
  divReg_21_mantissaC = _RAND_400[25:0];
  _RAND_401 = {1{`RANDOM}};
  divReg_21_exponentA = _RAND_401[7:0];
  _RAND_402 = {1{`RANDOM}};
  divReg_21_exponentB = _RAND_402[7:0];
  _RAND_403 = {1{`RANDOM}};
  divReg_21_signBitA = _RAND_403[0:0];
  _RAND_404 = {1{`RANDOM}};
  divReg_21_signBitB = _RAND_404[0:0];
  _RAND_405 = {1{`RANDOM}};
  divReg_21_faultFlags = _RAND_405[11:0];
  _RAND_406 = {1{`RANDOM}};
  divReg_21_status_0 = _RAND_406[0:0];
  _RAND_407 = {1{`RANDOM}};
  divReg_21_status_1 = _RAND_407[0:0];
  _RAND_408 = {1{`RANDOM}};
  divReg_21_status_2 = _RAND_408[0:0];
  _RAND_409 = {1{`RANDOM}};
  divReg_21_status_3 = _RAND_409[0:0];
  _RAND_410 = {1{`RANDOM}};
  divReg_21_status_4 = _RAND_410[0:0];
  _RAND_411 = {1{`RANDOM}};
  divReg_21_status_5 = _RAND_411[0:0];
  _RAND_412 = {1{`RANDOM}};
  divReg_21_status_6 = _RAND_412[0:0];
  _RAND_413 = {1{`RANDOM}};
  divReg_21_status_7 = _RAND_413[0:0];
  _RAND_414 = {1{`RANDOM}};
  divReg_21_op = _RAND_414[4:0];
  _RAND_415 = {1{`RANDOM}};
  divReg_21_valid = _RAND_415[0:0];
  _RAND_416 = {1{`RANDOM}};
  divReg_21_exMantA = _RAND_416[25:0];
  _RAND_417 = {1{`RANDOM}};
  divReg_22_mantissaA = _RAND_417[25:0];
  _RAND_418 = {1{`RANDOM}};
  divReg_22_mantissaB = _RAND_418[25:0];
  _RAND_419 = {1{`RANDOM}};
  divReg_22_mantissaC = _RAND_419[25:0];
  _RAND_420 = {1{`RANDOM}};
  divReg_22_exponentA = _RAND_420[7:0];
  _RAND_421 = {1{`RANDOM}};
  divReg_22_exponentB = _RAND_421[7:0];
  _RAND_422 = {1{`RANDOM}};
  divReg_22_signBitA = _RAND_422[0:0];
  _RAND_423 = {1{`RANDOM}};
  divReg_22_signBitB = _RAND_423[0:0];
  _RAND_424 = {1{`RANDOM}};
  divReg_22_faultFlags = _RAND_424[11:0];
  _RAND_425 = {1{`RANDOM}};
  divReg_22_status_0 = _RAND_425[0:0];
  _RAND_426 = {1{`RANDOM}};
  divReg_22_status_1 = _RAND_426[0:0];
  _RAND_427 = {1{`RANDOM}};
  divReg_22_status_2 = _RAND_427[0:0];
  _RAND_428 = {1{`RANDOM}};
  divReg_22_status_3 = _RAND_428[0:0];
  _RAND_429 = {1{`RANDOM}};
  divReg_22_status_4 = _RAND_429[0:0];
  _RAND_430 = {1{`RANDOM}};
  divReg_22_status_5 = _RAND_430[0:0];
  _RAND_431 = {1{`RANDOM}};
  divReg_22_status_6 = _RAND_431[0:0];
  _RAND_432 = {1{`RANDOM}};
  divReg_22_status_7 = _RAND_432[0:0];
  _RAND_433 = {1{`RANDOM}};
  divReg_22_op = _RAND_433[4:0];
  _RAND_434 = {1{`RANDOM}};
  divReg_22_valid = _RAND_434[0:0];
  _RAND_435 = {1{`RANDOM}};
  divReg_22_exMantA = _RAND_435[25:0];
  _RAND_436 = {1{`RANDOM}};
  divReg_23_mantissaA = _RAND_436[25:0];
  _RAND_437 = {1{`RANDOM}};
  divReg_23_mantissaB = _RAND_437[25:0];
  _RAND_438 = {1{`RANDOM}};
  divReg_23_mantissaC = _RAND_438[25:0];
  _RAND_439 = {1{`RANDOM}};
  divReg_23_exponentA = _RAND_439[7:0];
  _RAND_440 = {1{`RANDOM}};
  divReg_23_exponentB = _RAND_440[7:0];
  _RAND_441 = {1{`RANDOM}};
  divReg_23_signBitA = _RAND_441[0:0];
  _RAND_442 = {1{`RANDOM}};
  divReg_23_signBitB = _RAND_442[0:0];
  _RAND_443 = {1{`RANDOM}};
  divReg_23_faultFlags = _RAND_443[11:0];
  _RAND_444 = {1{`RANDOM}};
  divReg_23_status_0 = _RAND_444[0:0];
  _RAND_445 = {1{`RANDOM}};
  divReg_23_status_1 = _RAND_445[0:0];
  _RAND_446 = {1{`RANDOM}};
  divReg_23_status_2 = _RAND_446[0:0];
  _RAND_447 = {1{`RANDOM}};
  divReg_23_status_3 = _RAND_447[0:0];
  _RAND_448 = {1{`RANDOM}};
  divReg_23_status_4 = _RAND_448[0:0];
  _RAND_449 = {1{`RANDOM}};
  divReg_23_status_5 = _RAND_449[0:0];
  _RAND_450 = {1{`RANDOM}};
  divReg_23_status_6 = _RAND_450[0:0];
  _RAND_451 = {1{`RANDOM}};
  divReg_23_status_7 = _RAND_451[0:0];
  _RAND_452 = {1{`RANDOM}};
  divReg_23_op = _RAND_452[4:0];
  _RAND_453 = {1{`RANDOM}};
  divReg_23_valid = _RAND_453[0:0];
  _RAND_454 = {1{`RANDOM}};
  divReg_23_exMantA = _RAND_454[25:0];
  _RAND_455 = {1{`RANDOM}};
  divReg_24_mantissaA = _RAND_455[25:0];
  _RAND_456 = {1{`RANDOM}};
  divReg_24_mantissaB = _RAND_456[25:0];
  _RAND_457 = {1{`RANDOM}};
  divReg_24_mantissaC = _RAND_457[25:0];
  _RAND_458 = {1{`RANDOM}};
  divReg_24_exponentA = _RAND_458[7:0];
  _RAND_459 = {1{`RANDOM}};
  divReg_24_exponentB = _RAND_459[7:0];
  _RAND_460 = {1{`RANDOM}};
  divReg_24_signBitA = _RAND_460[0:0];
  _RAND_461 = {1{`RANDOM}};
  divReg_24_signBitB = _RAND_461[0:0];
  _RAND_462 = {1{`RANDOM}};
  divReg_24_faultFlags = _RAND_462[11:0];
  _RAND_463 = {1{`RANDOM}};
  divReg_24_status_0 = _RAND_463[0:0];
  _RAND_464 = {1{`RANDOM}};
  divReg_24_status_1 = _RAND_464[0:0];
  _RAND_465 = {1{`RANDOM}};
  divReg_24_status_2 = _RAND_465[0:0];
  _RAND_466 = {1{`RANDOM}};
  divReg_24_status_3 = _RAND_466[0:0];
  _RAND_467 = {1{`RANDOM}};
  divReg_24_status_4 = _RAND_467[0:0];
  _RAND_468 = {1{`RANDOM}};
  divReg_24_status_5 = _RAND_468[0:0];
  _RAND_469 = {1{`RANDOM}};
  divReg_24_status_6 = _RAND_469[0:0];
  _RAND_470 = {1{`RANDOM}};
  divReg_24_status_7 = _RAND_470[0:0];
  _RAND_471 = {1{`RANDOM}};
  divReg_24_op = _RAND_471[4:0];
  _RAND_472 = {1{`RANDOM}};
  divReg_24_valid = _RAND_472[0:0];
  _RAND_473 = {1{`RANDOM}};
  divReg_24_exMantA = _RAND_473[25:0];
  _RAND_474 = {1{`RANDOM}};
  divReg_25_mantissaB = _RAND_474[25:0];
  _RAND_475 = {1{`RANDOM}};
  divReg_25_mantissaC = _RAND_475[25:0];
  _RAND_476 = {1{`RANDOM}};
  divReg_25_exponentA = _RAND_476[7:0];
  _RAND_477 = {1{`RANDOM}};
  divReg_25_exponentC = _RAND_477[7:0];
  _RAND_478 = {1{`RANDOM}};
  divReg_25_signBitA = _RAND_478[0:0];
  _RAND_479 = {1{`RANDOM}};
  divReg_25_signBitB = _RAND_479[0:0];
  _RAND_480 = {1{`RANDOM}};
  divReg_25_signBitC = _RAND_480[0:0];
  _RAND_481 = {1{`RANDOM}};
  divReg_25_faultFlags = _RAND_481[11:0];
  _RAND_482 = {1{`RANDOM}};
  divReg_25_status_0 = _RAND_482[0:0];
  _RAND_483 = {1{`RANDOM}};
  divReg_25_status_1 = _RAND_483[0:0];
  _RAND_484 = {1{`RANDOM}};
  divReg_25_status_2 = _RAND_484[0:0];
  _RAND_485 = {1{`RANDOM}};
  divReg_25_status_3 = _RAND_485[0:0];
  _RAND_486 = {1{`RANDOM}};
  divReg_25_status_4 = _RAND_486[0:0];
  _RAND_487 = {1{`RANDOM}};
  divReg_25_status_5 = _RAND_487[0:0];
  _RAND_488 = {1{`RANDOM}};
  divReg_25_status_6 = _RAND_488[0:0];
  _RAND_489 = {1{`RANDOM}};
  divReg_25_status_7 = _RAND_489[0:0];
  _RAND_490 = {1{`RANDOM}};
  divReg_25_op = _RAND_490[4:0];
  _RAND_491 = {1{`RANDOM}};
  divReg_25_valid = _RAND_491[0:0];
  _RAND_492 = {1{`RANDOM}};
  divReg_25_exMantA = _RAND_492[25:0];
`endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
module SPFPSQRT(
  input         clock,
  input         reset,
  input         io_in_valid,
  input  [25:0] io_in_bits_a_mantissa,
  input  [7:0]  io_in_bits_a_exponent,
  input         io_in_bits_a_sign,
  input  [4:0]  io_in_bits_op,
  input  [11:0] io_in_bits_fflags,
  output        io_out_valid,
  output [25:0] io_out_bits_result_mantissa,
  output [7:0]  io_out_bits_result_exponent,
  output        io_out_bits_result_sign,
  output        io_out_bits_fcsr_4,
  output [4:0]  io_out_bits_op,
  output        io_out_bits_sqrtInProc
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [63:0] _RAND_27;
  reg [63:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [63:0] _RAND_31;
  reg [63:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [63:0] _RAND_35;
  reg [63:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [63:0] _RAND_39;
  reg [63:0] _RAND_40;
  reg [31:0] _RAND_41;
  reg [31:0] _RAND_42;
  reg [63:0] _RAND_43;
  reg [63:0] _RAND_44;
  reg [31:0] _RAND_45;
  reg [31:0] _RAND_46;
  reg [63:0] _RAND_47;
  reg [63:0] _RAND_48;
  reg [31:0] _RAND_49;
  reg [31:0] _RAND_50;
  reg [63:0] _RAND_51;
  reg [63:0] _RAND_52;
  reg [31:0] _RAND_53;
  reg [31:0] _RAND_54;
  reg [63:0] _RAND_55;
  reg [63:0] _RAND_56;
  reg [31:0] _RAND_57;
  reg [31:0] _RAND_58;
  reg [63:0] _RAND_59;
  reg [63:0] _RAND_60;
  reg [31:0] _RAND_61;
  reg [31:0] _RAND_62;
  reg [63:0] _RAND_63;
  reg [63:0] _RAND_64;
  reg [31:0] _RAND_65;
  reg [31:0] _RAND_66;
  reg [63:0] _RAND_67;
  reg [63:0] _RAND_68;
  reg [31:0] _RAND_69;
  reg [31:0] _RAND_70;
  reg [63:0] _RAND_71;
  reg [63:0] _RAND_72;
  reg [31:0] _RAND_73;
  reg [31:0] _RAND_74;
  reg [63:0] _RAND_75;
  reg [63:0] _RAND_76;
  reg [31:0] _RAND_77;
  reg [31:0] _RAND_78;
  reg [63:0] _RAND_79;
  reg [63:0] _RAND_80;
  reg [31:0] _RAND_81;
  reg [31:0] _RAND_82;
  reg [63:0] _RAND_83;
  reg [63:0] _RAND_84;
  reg [31:0] _RAND_85;
  reg [31:0] _RAND_86;
  reg [63:0] _RAND_87;
  reg [63:0] _RAND_88;
  reg [31:0] _RAND_89;
  reg [31:0] _RAND_90;
  reg [63:0] _RAND_91;
  reg [63:0] _RAND_92;
  reg [31:0] _RAND_93;
  reg [31:0] _RAND_94;
  reg [63:0] _RAND_95;
  reg [63:0] _RAND_96;
  reg [31:0] _RAND_97;
  reg [31:0] _RAND_98;
  reg [63:0] _RAND_99;
  reg [63:0] _RAND_100;
  reg [31:0] _RAND_101;
  reg [31:0] _RAND_102;
  reg [63:0] _RAND_103;
  reg [63:0] _RAND_104;
  reg [31:0] _RAND_105;
  reg [31:0] _RAND_106;
  reg [63:0] _RAND_107;
  reg [63:0] _RAND_108;
  reg [31:0] _RAND_109;
  reg [31:0] _RAND_110;
  reg [63:0] _RAND_111;
  reg [63:0] _RAND_112;
  reg [31:0] _RAND_113;
  reg [31:0] _RAND_114;
  reg [63:0] _RAND_115;
  reg [63:0] _RAND_116;
  reg [31:0] _RAND_117;
  reg [31:0] _RAND_118;
  reg [63:0] _RAND_119;
  reg [63:0] _RAND_120;
  reg [31:0] _RAND_121;
  reg [31:0] _RAND_122;
  reg [63:0] _RAND_123;
  reg [63:0] _RAND_124;
  reg [31:0] _RAND_125;
  reg [31:0] _RAND_126;
  reg [63:0] _RAND_127;
  reg [63:0] _RAND_128;
  reg [31:0] _RAND_129;
  reg [31:0] _RAND_130;
  reg [63:0] _RAND_131;
  reg [31:0] _RAND_132;
  reg [31:0] _RAND_133;
  reg [31:0] _RAND_134;
  reg [31:0] _RAND_135;
  reg [31:0] _RAND_136;
  reg [31:0] _RAND_137;
  reg [31:0] _RAND_138;
  reg [31:0] _RAND_139;
  reg [31:0] _RAND_140;
  reg [31:0] _RAND_141;
  reg [31:0] _RAND_142;
  reg [31:0] _RAND_143;
  reg [31:0] _RAND_144;
  reg [31:0] _RAND_145;
  reg [31:0] _RAND_146;
  reg [31:0] _RAND_147;
  reg [31:0] _RAND_148;
  reg [31:0] _RAND_149;
  reg [31:0] _RAND_150;
  reg [31:0] _RAND_151;
  reg [31:0] _RAND_152;
  reg [31:0] _RAND_153;
  reg [31:0] _RAND_154;
  reg [31:0] _RAND_155;
  reg [31:0] _RAND_156;
  reg [31:0] _RAND_157;
  reg [31:0] _RAND_158;
  reg [31:0] _RAND_159;
  reg [31:0] _RAND_160;
  reg [31:0] _RAND_161;
  reg [31:0] _RAND_162;
  reg [31:0] _RAND_163;
  reg [31:0] _RAND_164;
  reg [31:0] _RAND_165;
  reg [31:0] _RAND_166;
  reg [31:0] _RAND_167;
  reg [31:0] _RAND_168;
  reg [31:0] _RAND_169;
  reg [31:0] _RAND_170;
  reg [31:0] _RAND_171;
  reg [31:0] _RAND_172;
  reg [31:0] _RAND_173;
  reg [31:0] _RAND_174;
  reg [31:0] _RAND_175;
  reg [31:0] _RAND_176;
  reg [31:0] _RAND_177;
  reg [31:0] _RAND_178;
  reg [31:0] _RAND_179;
  reg [31:0] _RAND_180;
  reg [31:0] _RAND_181;
  reg [31:0] _RAND_182;
  reg [31:0] _RAND_183;
  reg [31:0] _RAND_184;
  reg [31:0] _RAND_185;
  reg [31:0] _RAND_186;
  reg [31:0] _RAND_187;
  reg [31:0] _RAND_188;
  reg [31:0] _RAND_189;
  reg [31:0] _RAND_190;
  reg [31:0] _RAND_191;
  reg [31:0] _RAND_192;
  reg [31:0] _RAND_193;
  reg [31:0] _RAND_194;
  reg [31:0] _RAND_195;
  reg [31:0] _RAND_196;
  reg [31:0] _RAND_197;
  reg [31:0] _RAND_198;
  reg [31:0] _RAND_199;
  reg [31:0] _RAND_200;
  reg [31:0] _RAND_201;
  reg [31:0] _RAND_202;
  reg [31:0] _RAND_203;
  reg [31:0] _RAND_204;
  reg [31:0] _RAND_205;
  reg [31:0] _RAND_206;
  reg [31:0] _RAND_207;
  reg [31:0] _RAND_208;
  reg [31:0] _RAND_209;
  reg [31:0] _RAND_210;
  reg [31:0] _RAND_211;
  reg [31:0] _RAND_212;
  reg [31:0] _RAND_213;
  reg [31:0] _RAND_214;
  reg [31:0] _RAND_215;
  reg [31:0] _RAND_216;
  reg [31:0] _RAND_217;
  reg [31:0] _RAND_218;
  reg [31:0] _RAND_219;
  reg [31:0] _RAND_220;
  reg [31:0] _RAND_221;
  reg [31:0] _RAND_222;
  reg [31:0] _RAND_223;
  reg [31:0] _RAND_224;
  reg [31:0] _RAND_225;
  reg [31:0] _RAND_226;
  reg [31:0] _RAND_227;
  reg [31:0] _RAND_228;
  reg [31:0] _RAND_229;
  reg [31:0] _RAND_230;
  reg [31:0] _RAND_231;
  reg [31:0] _RAND_232;
  reg [31:0] _RAND_233;
  reg [31:0] _RAND_234;
  reg [31:0] _RAND_235;
  reg [31:0] _RAND_236;
  reg [31:0] _RAND_237;
  reg [31:0] _RAND_238;
  reg [31:0] _RAND_239;
  reg [31:0] _RAND_240;
  reg [31:0] _RAND_241;
`endif // RANDOMIZE_REG_INIT
  reg [4:0] opReg_0; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_1; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_2; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_3; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_4; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_5; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_6; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_7; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_8; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_9; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_10; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_11; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_12; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_13; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_14; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_15; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_16; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_17; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_18; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_19; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_20; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_21; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_22; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_23; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_24; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_25; // @[SPFPSQRT.scala 60:22]
  reg [4:0] opReg_26; // @[SPFPSQRT.scala 60:22]
  wire  skipZeros = io_in_bits_a_exponent[0] & io_in_bits_a_mantissa[22:0] == 23'h0; // @[SPFPSQRT.scala 76:36]
  wire [37:0] extMantissa = {3'h1,io_in_bits_a_mantissa[22:0],12'h0}; // @[Cat.scala 33:92]
  reg [37:0] loopReg_0_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_0_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_0_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_0_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_1_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_1_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_1_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_1_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_2_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_2_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_2_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_2_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_3_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_3_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_3_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_3_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_4_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_4_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_4_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_4_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_5_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_5_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_5_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_5_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_6_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_6_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_6_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_6_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_7_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_7_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_7_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_7_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_8_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_8_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_8_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_8_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_9_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_9_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_9_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_9_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_10_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_10_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_10_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_10_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_11_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_11_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_11_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_11_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_12_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_12_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_12_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_12_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_13_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_13_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_13_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_13_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_14_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_14_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_14_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_14_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_15_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_15_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_15_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_15_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_16_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_16_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_16_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_16_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_17_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_17_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_17_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_17_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_18_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_18_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_18_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_18_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_19_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_19_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_19_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_19_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_20_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_20_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_20_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_20_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_21_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_21_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_21_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_21_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_22_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_22_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_22_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_22_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_23_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_23_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_23_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_23_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_24_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_24_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_24_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_24_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_25_nX; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_25_nY; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_25_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_25_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [37:0] loopReg_26_nX; // @[SPFPSQRT.scala 121:24]
  reg  loopReg_26_valid; // @[SPFPSQRT.scala 121:24]
  reg [7:0] loopReg_26_sqrtExp; // @[SPFPSQRT.scala 121:24]
  reg [25:0] noLoopReg_0_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_0_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_0_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_0_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_1_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_1_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_1_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_1_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_2_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_2_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_2_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_2_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_3_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_3_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_3_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_3_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_4_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_4_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_4_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_4_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_5_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_5_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_5_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_5_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_6_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_6_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_6_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_6_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_7_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_7_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_7_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_7_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_8_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_8_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_8_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_8_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_9_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_9_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_9_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_9_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_10_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_10_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_10_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_10_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_11_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_11_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_11_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_11_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_12_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_12_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_12_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_12_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_13_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_13_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_13_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_13_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_14_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_14_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_14_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_14_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_15_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_15_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_15_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_15_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_16_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_16_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_16_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_16_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_17_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_17_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_17_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_17_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_18_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_18_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_18_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_18_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_19_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_19_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_19_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_19_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_20_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_20_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_20_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_20_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_21_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_21_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_21_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_21_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_22_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_22_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_22_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_22_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_23_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_23_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_23_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_23_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_24_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_24_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_24_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_24_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_25_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_25_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_25_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_25_valid; // @[SPFPSQRT.scala 122:26]
  reg [25:0] noLoopReg_26_mantissa; // @[SPFPSQRT.scala 122:26]
  reg [7:0] noLoopReg_26_exponent; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_26_sign; // @[SPFPSQRT.scala 122:26]
  reg  noLoopReg_26_valid; // @[SPFPSQRT.scala 122:26]
  wire  zeroMant = io_in_bits_fflags[9]; // @[SPFPSQRT.scala 144:24]
  wire  maxExp = io_in_bits_a_exponent == 8'hff; // @[SPFPSQRT.scala 145:31]
  wire  _T_1 = $signed(io_in_bits_a_exponent) == 8'sh0; // @[SPFPSQRT.scala 161:15]
  wire  _T_2 = $signed(io_in_bits_a_exponent) == 8'sh0 & zeroMant; // @[SPFPSQRT.scala 161:23]
  wire [22:0] _GEN_0 = $signed(io_in_bits_a_exponent) == 8'sh0 & zeroMant ? 23'h0 : 23'h400000; // @[SPFPSQRT.scala 161:36 164:29 180:29]
  wire  _T_8 = ~io_in_bits_a_mantissa[22]; // @[SPFPSQRT.scala 211:12]
  wire [22:0] _GEN_127 = ~io_in_bits_a_mantissa[22] ? 23'h200000 : 23'h400000; // @[SPFPSQRT.scala 211:24 214:31 231:31]
  wire [22:0] _GEN_246 = ~zeroMant ? _GEN_127 : 23'h0; // @[SPFPSQRT.scala 210:21 247:29]
  wire  _GEN_361 = ~zeroMant & _T_8; // @[SPFPSQRT.scala 210:21 38:27]
  wire [7:0] _GEN_2 = io_in_bits_a_exponent % 8'h2; // @[SPFPSQRT.scala 264:23]
  wire [7:0] _sqrtExp_T_2 = io_in_bits_a_exponent - 8'h80; // @[SPFPSQRT.scala 268:43]
  wire [7:0] _sqrtExp_T_3 = {{1'd0}, _sqrtExp_T_2[7:1]}; // @[SPFPSQRT.scala 268:52]
  wire [7:0] _sqrtExp_T_5 = 8'h80 + _sqrtExp_T_3; // @[SPFPSQRT.scala 268:26]
  wire [7:0] _sqrtExp_T_8 = 8'h80 - io_in_bits_a_exponent; // @[SPFPSQRT.scala 270:36]
  wire [7:0] _sqrtExp_T_9 = {{1'd0}, _sqrtExp_T_8[7:1]}; // @[SPFPSQRT.scala 270:51]
  wire [7:0] _sqrtExp_T_11 = 8'h80 - _sqrtExp_T_9; // @[SPFPSQRT.scala 270:26]
  wire [7:0] _GEN_365 = io_in_bits_a_exponent >= 8'h80 ? _sqrtExp_T_5 : _sqrtExp_T_11; // @[SPFPSQRT.scala 267:33 268:17 270:17]
  wire [37:0] _nX_0_T_1 = {{1'd0}, extMantissa[37:1]}; // @[SPFPSQRT.scala 272:43]
  wire [37:0] _nX_0_T_4 = $signed(_nX_0_T_1) + 38'sh200000000; // @[SPFPSQRT.scala 272:47]
  wire [37:0] _nY_0_T_4 = $signed(_nX_0_T_1) - 38'sh200000000; // @[SPFPSQRT.scala 273:47]
  wire [7:0] _sqrtExp_T_14 = io_in_bits_a_exponent - 8'h7f; // @[SPFPSQRT.scala 278:42]
  wire [7:0] _sqrtExp_T_15 = {{1'd0}, _sqrtExp_T_14[7:1]}; // @[SPFPSQRT.scala 278:51]
  wire [7:0] _sqrtExp_T_17 = 8'h7f + _sqrtExp_T_15; // @[SPFPSQRT.scala 278:26]
  wire [7:0] _sqrtExp_T_20 = 8'h7f - io_in_bits_a_exponent; // @[SPFPSQRT.scala 280:36]
  wire [7:0] _sqrtExp_T_21 = {{1'd0}, _sqrtExp_T_20[7:1]}; // @[SPFPSQRT.scala 280:51]
  wire [7:0] _sqrtExp_T_23 = 8'h7f - _sqrtExp_T_21; // @[SPFPSQRT.scala 280:26]
  wire [7:0] _GEN_366 = io_in_bits_a_exponent >= 8'h7f ? _sqrtExp_T_17 : _sqrtExp_T_23; // @[SPFPSQRT.scala 277:34 278:17 280:17]
  wire [37:0] _nX_0_T_5 = {3'h1,io_in_bits_a_mantissa[22:0],12'h0}; // @[SPFPSQRT.scala 282:34]
  wire [37:0] _nX_0_T_8 = $signed(_nX_0_T_5) + 38'sh200000000; // @[SPFPSQRT.scala 282:37]
  wire [37:0] _nY_0_T_8 = $signed(_nX_0_T_5) - 38'sh200000000; // @[SPFPSQRT.scala 283:37]
  wire [7:0] _GEN_367 = _GEN_2[1:0] == 2'h0 ? _GEN_365 : _GEN_366; // @[SPFPSQRT.scala 264:38]
  wire [37:0] _GEN_368 = _GEN_2[1:0] == 2'h0 ? $signed(_nX_0_T_4) : $signed(_nX_0_T_8); // @[SPFPSQRT.scala 264:38 272:13 282:13]
  wire [37:0] _GEN_369 = _GEN_2[1:0] == 2'h0 ? $signed(_nY_0_T_4) : $signed(_nY_0_T_8); // @[SPFPSQRT.scala 264:38 273:13 283:13]
  wire [37:0] _GEN_532 = ~skipZeros ? $signed(loopReg_0_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 300:16 115:26]
  wire [37:0] _GEN_1031 = maxExp ? $signed(38'sh0) : $signed(_GEN_532); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1419 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1031); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_0 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1419); // @[SPFPSQRT.scala 160:23 115:26]
  wire [36:0] _GEN_2075 = nXReg_0[37:1]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_0_T = {{1{_GEN_2075[36]}},_GEN_2075}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_533 = ~skipZeros ? $signed(loopReg_0_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 301:16 116:26]
  wire [37:0] _GEN_1032 = maxExp ? $signed(38'sh0) : $signed(_GEN_533); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1420 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1032); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_0 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1420); // @[SPFPSQRT.scala 160:23 116:26]
  wire [36:0] _GEN_2076 = nYReg_0[37:1]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_0_T = {{1{_GEN_2076[36]}},_GEN_2076}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_535 = ~skipZeros ? $signed(_tmpY_0_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1034 = maxExp ? $signed(38'sh0) : $signed(_GEN_535); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1422 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1034); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_0 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1422); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_1_T_2 = $signed(nXReg_0) + $signed(tmpY_0); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_534 = ~skipZeros ? $signed(_tmpX_0_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1033 = maxExp ? $signed(38'sh0) : $signed(_GEN_534); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1421 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1033); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_0 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1421); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_1_T_2 = $signed(nYReg_0) + $signed(tmpX_0); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_1_T_5 = $signed(nXReg_0) - $signed(tmpY_0); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_1_T_5 = $signed(nYReg_0) - $signed(tmpX_0); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_370 = nYReg_0[37] ? $signed(_nX_1_T_2) : $signed(_nX_1_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_371 = nYReg_0[37] ? $signed(_nY_1_T_2) : $signed(_nY_1_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_536 = ~skipZeros ? $signed(_GEN_370) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1035 = maxExp ? $signed(38'sh0) : $signed(_GEN_536); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1423 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1035); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_1 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1423); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_537 = ~skipZeros ? $signed(_GEN_371) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1036 = maxExp ? $signed(38'sh0) : $signed(_GEN_537); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1424 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1036); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_1 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1424); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_540 = ~skipZeros ? $signed(loopReg_1_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1039 = maxExp ? $signed(38'sh0) : $signed(_GEN_540); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1427 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1039); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_1 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1427); // @[SPFPSQRT.scala 160:23 115:26]
  wire [35:0] _GEN_2077 = nXReg_1[37:2]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_1_T = {{2{_GEN_2077[35]}},_GEN_2077}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_541 = ~skipZeros ? $signed(loopReg_1_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1040 = maxExp ? $signed(38'sh0) : $signed(_GEN_541); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1428 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1040); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_1 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1428); // @[SPFPSQRT.scala 160:23 116:26]
  wire [35:0] _GEN_2078 = nYReg_1[37:2]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_1_T = {{2{_GEN_2078[35]}},_GEN_2078}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_545 = ~skipZeros ? $signed(_tmpY_1_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1044 = maxExp ? $signed(38'sh0) : $signed(_GEN_545); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1432 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1044); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_1 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1432); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_2_T_2 = $signed(nXReg_1) + $signed(tmpY_1); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_544 = ~skipZeros ? $signed(_tmpX_1_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1043 = maxExp ? $signed(38'sh0) : $signed(_GEN_544); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1431 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1043); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_1 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1431); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_2_T_2 = $signed(nYReg_1) + $signed(tmpX_1); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_2_T_5 = $signed(nXReg_1) - $signed(tmpY_1); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_2_T_5 = $signed(nYReg_1) - $signed(tmpX_1); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_376 = nYReg_1[37] ? $signed(_nX_2_T_2) : $signed(_nX_2_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_377 = nYReg_1[37] ? $signed(_nY_2_T_2) : $signed(_nY_2_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_546 = ~skipZeros ? $signed(_GEN_376) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1045 = maxExp ? $signed(38'sh0) : $signed(_GEN_546); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1433 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1045); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_2 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1433); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_547 = ~skipZeros ? $signed(_GEN_377) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1046 = maxExp ? $signed(38'sh0) : $signed(_GEN_547); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1434 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1046); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_2 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1434); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_550 = ~skipZeros ? $signed(loopReg_2_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1049 = maxExp ? $signed(38'sh0) : $signed(_GEN_550); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1437 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1049); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_2 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1437); // @[SPFPSQRT.scala 160:23 115:26]
  wire [34:0] _GEN_2079 = nXReg_2[37:3]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_2_T = {{3{_GEN_2079[34]}},_GEN_2079}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_551 = ~skipZeros ? $signed(loopReg_2_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1050 = maxExp ? $signed(38'sh0) : $signed(_GEN_551); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1438 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1050); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_2 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1438); // @[SPFPSQRT.scala 160:23 116:26]
  wire [34:0] _GEN_2080 = nYReg_2[37:3]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_2_T = {{3{_GEN_2080[34]}},_GEN_2080}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_555 = ~skipZeros ? $signed(_tmpY_2_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1054 = maxExp ? $signed(38'sh0) : $signed(_GEN_555); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1442 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1054); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_2 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1442); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_3_T_2 = $signed(nXReg_2) + $signed(tmpY_2); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_554 = ~skipZeros ? $signed(_tmpX_2_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1053 = maxExp ? $signed(38'sh0) : $signed(_GEN_554); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1441 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1053); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_2 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1441); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_3_T_2 = $signed(nYReg_2) + $signed(tmpX_2); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_3_T_5 = $signed(nXReg_2) - $signed(tmpY_2); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_3_T_5 = $signed(nYReg_2) - $signed(tmpX_2); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_382 = nYReg_2[37] ? $signed(_nX_3_T_2) : $signed(_nX_3_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_383 = nYReg_2[37] ? $signed(_nY_3_T_2) : $signed(_nY_3_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_556 = ~skipZeros ? $signed(_GEN_382) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1055 = maxExp ? $signed(38'sh0) : $signed(_GEN_556); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1443 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1055); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_3 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1443); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_557 = ~skipZeros ? $signed(_GEN_383) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1056 = maxExp ? $signed(38'sh0) : $signed(_GEN_557); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1444 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1056); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_3 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1444); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_560 = ~skipZeros ? $signed(loopReg_3_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1059 = maxExp ? $signed(38'sh0) : $signed(_GEN_560); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1447 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1059); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_3 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1447); // @[SPFPSQRT.scala 160:23 115:26]
  wire [33:0] _GEN_2081 = nXReg_3[37:4]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_3_T = {{4{_GEN_2081[33]}},_GEN_2081}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_561 = ~skipZeros ? $signed(loopReg_3_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1060 = maxExp ? $signed(38'sh0) : $signed(_GEN_561); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1448 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1060); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_3 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1448); // @[SPFPSQRT.scala 160:23 116:26]
  wire [33:0] _GEN_2082 = nYReg_3[37:4]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_3_T = {{4{_GEN_2082[33]}},_GEN_2082}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_565 = ~skipZeros ? $signed(_tmpY_3_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1064 = maxExp ? $signed(38'sh0) : $signed(_GEN_565); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1452 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1064); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_3 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1452); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_4_T_2 = $signed(nXReg_3) + $signed(tmpY_3); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_564 = ~skipZeros ? $signed(_tmpX_3_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1063 = maxExp ? $signed(38'sh0) : $signed(_GEN_564); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1451 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1063); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_3 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1451); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_4_T_2 = $signed(nYReg_3) + $signed(tmpX_3); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_4_T_5 = $signed(nXReg_3) - $signed(tmpY_3); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_4_T_5 = $signed(nYReg_3) - $signed(tmpX_3); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_388 = nYReg_3[37] ? $signed(_nX_4_T_2) : $signed(_nX_4_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_389 = nYReg_3[37] ? $signed(_nY_4_T_2) : $signed(_nY_4_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_566 = ~skipZeros ? $signed(_GEN_388) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1065 = maxExp ? $signed(38'sh0) : $signed(_GEN_566); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1453 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1065); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_4 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1453); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_567 = ~skipZeros ? $signed(_GEN_389) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1066 = maxExp ? $signed(38'sh0) : $signed(_GEN_567); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1454 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1066); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_4 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1454); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_570 = ~skipZeros ? $signed(loopReg_4_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1069 = maxExp ? $signed(38'sh0) : $signed(_GEN_570); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1457 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1069); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_4 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1457); // @[SPFPSQRT.scala 160:23 115:26]
  wire [33:0] _GEN_2083 = nXReg_4[37:4]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_4_T = {{4{_GEN_2083[33]}},_GEN_2083}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_571 = ~skipZeros ? $signed(loopReg_4_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1070 = maxExp ? $signed(38'sh0) : $signed(_GEN_571); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1458 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1070); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_4 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1458); // @[SPFPSQRT.scala 160:23 116:26]
  wire [33:0] _GEN_2084 = nYReg_4[37:4]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_4_T = {{4{_GEN_2084[33]}},_GEN_2084}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_575 = ~skipZeros ? $signed(_tmpY_4_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1074 = maxExp ? $signed(38'sh0) : $signed(_GEN_575); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1462 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1074); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_4 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1462); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_5_T_2 = $signed(nXReg_4) + $signed(tmpY_4); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_574 = ~skipZeros ? $signed(_tmpX_4_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1073 = maxExp ? $signed(38'sh0) : $signed(_GEN_574); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1461 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1073); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_4 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1461); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_5_T_2 = $signed(nYReg_4) + $signed(tmpX_4); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_5_T_5 = $signed(nXReg_4) - $signed(tmpY_4); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_5_T_5 = $signed(nYReg_4) - $signed(tmpX_4); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_394 = nYReg_4[37] ? $signed(_nX_5_T_2) : $signed(_nX_5_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_395 = nYReg_4[37] ? $signed(_nY_5_T_2) : $signed(_nY_5_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_576 = ~skipZeros ? $signed(_GEN_394) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1075 = maxExp ? $signed(38'sh0) : $signed(_GEN_576); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1463 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1075); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_5 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1463); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_577 = ~skipZeros ? $signed(_GEN_395) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1076 = maxExp ? $signed(38'sh0) : $signed(_GEN_577); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1464 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1076); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_5 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1464); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_580 = ~skipZeros ? $signed(loopReg_5_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1079 = maxExp ? $signed(38'sh0) : $signed(_GEN_580); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1467 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1079); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_5 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1467); // @[SPFPSQRT.scala 160:23 115:26]
  wire [32:0] _GEN_2085 = nXReg_5[37:5]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_5_T = {{5{_GEN_2085[32]}},_GEN_2085}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_581 = ~skipZeros ? $signed(loopReg_5_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1080 = maxExp ? $signed(38'sh0) : $signed(_GEN_581); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1468 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1080); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_5 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1468); // @[SPFPSQRT.scala 160:23 116:26]
  wire [32:0] _GEN_2086 = nYReg_5[37:5]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_5_T = {{5{_GEN_2086[32]}},_GEN_2086}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_585 = ~skipZeros ? $signed(_tmpY_5_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1084 = maxExp ? $signed(38'sh0) : $signed(_GEN_585); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1472 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1084); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_5 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1472); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_6_T_2 = $signed(nXReg_5) + $signed(tmpY_5); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_584 = ~skipZeros ? $signed(_tmpX_5_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1083 = maxExp ? $signed(38'sh0) : $signed(_GEN_584); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1471 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1083); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_5 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1471); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_6_T_2 = $signed(nYReg_5) + $signed(tmpX_5); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_6_T_5 = $signed(nXReg_5) - $signed(tmpY_5); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_6_T_5 = $signed(nYReg_5) - $signed(tmpX_5); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_400 = nYReg_5[37] ? $signed(_nX_6_T_2) : $signed(_nX_6_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_401 = nYReg_5[37] ? $signed(_nY_6_T_2) : $signed(_nY_6_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_586 = ~skipZeros ? $signed(_GEN_400) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1085 = maxExp ? $signed(38'sh0) : $signed(_GEN_586); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1473 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1085); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_6 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1473); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_587 = ~skipZeros ? $signed(_GEN_401) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1086 = maxExp ? $signed(38'sh0) : $signed(_GEN_587); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1474 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1086); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_6 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1474); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_590 = ~skipZeros ? $signed(loopReg_6_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1089 = maxExp ? $signed(38'sh0) : $signed(_GEN_590); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1477 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1089); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_6 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1477); // @[SPFPSQRT.scala 160:23 115:26]
  wire [31:0] _GEN_2087 = nXReg_6[37:6]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_6_T = {{6{_GEN_2087[31]}},_GEN_2087}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_591 = ~skipZeros ? $signed(loopReg_6_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1090 = maxExp ? $signed(38'sh0) : $signed(_GEN_591); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1478 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1090); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_6 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1478); // @[SPFPSQRT.scala 160:23 116:26]
  wire [31:0] _GEN_2088 = nYReg_6[37:6]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_6_T = {{6{_GEN_2088[31]}},_GEN_2088}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_595 = ~skipZeros ? $signed(_tmpY_6_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1094 = maxExp ? $signed(38'sh0) : $signed(_GEN_595); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1482 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1094); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_6 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1482); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_7_T_2 = $signed(nXReg_6) + $signed(tmpY_6); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_594 = ~skipZeros ? $signed(_tmpX_6_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1093 = maxExp ? $signed(38'sh0) : $signed(_GEN_594); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1481 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1093); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_6 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1481); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_7_T_2 = $signed(nYReg_6) + $signed(tmpX_6); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_7_T_5 = $signed(nXReg_6) - $signed(tmpY_6); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_7_T_5 = $signed(nYReg_6) - $signed(tmpX_6); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_406 = nYReg_6[37] ? $signed(_nX_7_T_2) : $signed(_nX_7_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_407 = nYReg_6[37] ? $signed(_nY_7_T_2) : $signed(_nY_7_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_596 = ~skipZeros ? $signed(_GEN_406) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1095 = maxExp ? $signed(38'sh0) : $signed(_GEN_596); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1483 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1095); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_7 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1483); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_597 = ~skipZeros ? $signed(_GEN_407) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1096 = maxExp ? $signed(38'sh0) : $signed(_GEN_597); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1484 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1096); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_7 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1484); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_600 = ~skipZeros ? $signed(loopReg_7_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1099 = maxExp ? $signed(38'sh0) : $signed(_GEN_600); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1487 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1099); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_7 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1487); // @[SPFPSQRT.scala 160:23 115:26]
  wire [30:0] _GEN_2089 = nXReg_7[37:7]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_7_T = {{7{_GEN_2089[30]}},_GEN_2089}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_601 = ~skipZeros ? $signed(loopReg_7_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1100 = maxExp ? $signed(38'sh0) : $signed(_GEN_601); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1488 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1100); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_7 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1488); // @[SPFPSQRT.scala 160:23 116:26]
  wire [30:0] _GEN_2090 = nYReg_7[37:7]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_7_T = {{7{_GEN_2090[30]}},_GEN_2090}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_605 = ~skipZeros ? $signed(_tmpY_7_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1104 = maxExp ? $signed(38'sh0) : $signed(_GEN_605); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1492 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1104); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_7 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1492); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_8_T_2 = $signed(nXReg_7) + $signed(tmpY_7); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_604 = ~skipZeros ? $signed(_tmpX_7_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1103 = maxExp ? $signed(38'sh0) : $signed(_GEN_604); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1491 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1103); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_7 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1491); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_8_T_2 = $signed(nYReg_7) + $signed(tmpX_7); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_8_T_5 = $signed(nXReg_7) - $signed(tmpY_7); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_8_T_5 = $signed(nYReg_7) - $signed(tmpX_7); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_412 = nYReg_7[37] ? $signed(_nX_8_T_2) : $signed(_nX_8_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_413 = nYReg_7[37] ? $signed(_nY_8_T_2) : $signed(_nY_8_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_606 = ~skipZeros ? $signed(_GEN_412) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1105 = maxExp ? $signed(38'sh0) : $signed(_GEN_606); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1493 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1105); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_8 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1493); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_607 = ~skipZeros ? $signed(_GEN_413) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1106 = maxExp ? $signed(38'sh0) : $signed(_GEN_607); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1494 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1106); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_8 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1494); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_610 = ~skipZeros ? $signed(loopReg_8_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1109 = maxExp ? $signed(38'sh0) : $signed(_GEN_610); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1497 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1109); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_8 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1497); // @[SPFPSQRT.scala 160:23 115:26]
  wire [29:0] _GEN_2091 = nXReg_8[37:8]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_8_T = {{8{_GEN_2091[29]}},_GEN_2091}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_611 = ~skipZeros ? $signed(loopReg_8_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1110 = maxExp ? $signed(38'sh0) : $signed(_GEN_611); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1498 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1110); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_8 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1498); // @[SPFPSQRT.scala 160:23 116:26]
  wire [29:0] _GEN_2092 = nYReg_8[37:8]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_8_T = {{8{_GEN_2092[29]}},_GEN_2092}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_615 = ~skipZeros ? $signed(_tmpY_8_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1114 = maxExp ? $signed(38'sh0) : $signed(_GEN_615); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1502 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1114); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_8 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1502); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_9_T_2 = $signed(nXReg_8) + $signed(tmpY_8); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_614 = ~skipZeros ? $signed(_tmpX_8_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1113 = maxExp ? $signed(38'sh0) : $signed(_GEN_614); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1501 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1113); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_8 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1501); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_9_T_2 = $signed(nYReg_8) + $signed(tmpX_8); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_9_T_5 = $signed(nXReg_8) - $signed(tmpY_8); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_9_T_5 = $signed(nYReg_8) - $signed(tmpX_8); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_418 = nYReg_8[37] ? $signed(_nX_9_T_2) : $signed(_nX_9_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_419 = nYReg_8[37] ? $signed(_nY_9_T_2) : $signed(_nY_9_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_616 = ~skipZeros ? $signed(_GEN_418) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1115 = maxExp ? $signed(38'sh0) : $signed(_GEN_616); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1503 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1115); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_9 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1503); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_617 = ~skipZeros ? $signed(_GEN_419) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1116 = maxExp ? $signed(38'sh0) : $signed(_GEN_617); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1504 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1116); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_9 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1504); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_620 = ~skipZeros ? $signed(loopReg_9_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1119 = maxExp ? $signed(38'sh0) : $signed(_GEN_620); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1507 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1119); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_9 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1507); // @[SPFPSQRT.scala 160:23 115:26]
  wire [28:0] _GEN_2093 = nXReg_9[37:9]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_9_T = {{9{_GEN_2093[28]}},_GEN_2093}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_621 = ~skipZeros ? $signed(loopReg_9_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1120 = maxExp ? $signed(38'sh0) : $signed(_GEN_621); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1508 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1120); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_9 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1508); // @[SPFPSQRT.scala 160:23 116:26]
  wire [28:0] _GEN_2094 = nYReg_9[37:9]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_9_T = {{9{_GEN_2094[28]}},_GEN_2094}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_625 = ~skipZeros ? $signed(_tmpY_9_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1124 = maxExp ? $signed(38'sh0) : $signed(_GEN_625); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1512 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1124); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_9 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1512); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_10_T_2 = $signed(nXReg_9) + $signed(tmpY_9); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_624 = ~skipZeros ? $signed(_tmpX_9_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1123 = maxExp ? $signed(38'sh0) : $signed(_GEN_624); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1511 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1123); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_9 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1511); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_10_T_2 = $signed(nYReg_9) + $signed(tmpX_9); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_10_T_5 = $signed(nXReg_9) - $signed(tmpY_9); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_10_T_5 = $signed(nYReg_9) - $signed(tmpX_9); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_424 = nYReg_9[37] ? $signed(_nX_10_T_2) : $signed(_nX_10_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_425 = nYReg_9[37] ? $signed(_nY_10_T_2) : $signed(_nY_10_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_626 = ~skipZeros ? $signed(_GEN_424) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1125 = maxExp ? $signed(38'sh0) : $signed(_GEN_626); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1513 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1125); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_10 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1513); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_627 = ~skipZeros ? $signed(_GEN_425) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1126 = maxExp ? $signed(38'sh0) : $signed(_GEN_627); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1514 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1126); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_10 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1514); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_630 = ~skipZeros ? $signed(loopReg_10_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1129 = maxExp ? $signed(38'sh0) : $signed(_GEN_630); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1517 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1129); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_10 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1517); // @[SPFPSQRT.scala 160:23 115:26]
  wire [27:0] _GEN_2095 = nXReg_10[37:10]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_10_T = {{10{_GEN_2095[27]}},_GEN_2095}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_631 = ~skipZeros ? $signed(loopReg_10_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1130 = maxExp ? $signed(38'sh0) : $signed(_GEN_631); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1518 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1130); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_10 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1518); // @[SPFPSQRT.scala 160:23 116:26]
  wire [27:0] _GEN_2096 = nYReg_10[37:10]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_10_T = {{10{_GEN_2096[27]}},_GEN_2096}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_635 = ~skipZeros ? $signed(_tmpY_10_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1134 = maxExp ? $signed(38'sh0) : $signed(_GEN_635); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1522 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1134); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_10 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1522); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_11_T_2 = $signed(nXReg_10) + $signed(tmpY_10); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_634 = ~skipZeros ? $signed(_tmpX_10_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1133 = maxExp ? $signed(38'sh0) : $signed(_GEN_634); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1521 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1133); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_10 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1521); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_11_T_2 = $signed(nYReg_10) + $signed(tmpX_10); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_11_T_5 = $signed(nXReg_10) - $signed(tmpY_10); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_11_T_5 = $signed(nYReg_10) - $signed(tmpX_10); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_430 = nYReg_10[37] ? $signed(_nX_11_T_2) : $signed(_nX_11_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_431 = nYReg_10[37] ? $signed(_nY_11_T_2) : $signed(_nY_11_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_636 = ~skipZeros ? $signed(_GEN_430) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1135 = maxExp ? $signed(38'sh0) : $signed(_GEN_636); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1523 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1135); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_11 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1523); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_637 = ~skipZeros ? $signed(_GEN_431) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1136 = maxExp ? $signed(38'sh0) : $signed(_GEN_637); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1524 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1136); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_11 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1524); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_640 = ~skipZeros ? $signed(loopReg_11_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1139 = maxExp ? $signed(38'sh0) : $signed(_GEN_640); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1527 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1139); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_11 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1527); // @[SPFPSQRT.scala 160:23 115:26]
  wire [26:0] _GEN_2097 = nXReg_11[37:11]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_11_T = {{11{_GEN_2097[26]}},_GEN_2097}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_641 = ~skipZeros ? $signed(loopReg_11_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1140 = maxExp ? $signed(38'sh0) : $signed(_GEN_641); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1528 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1140); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_11 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1528); // @[SPFPSQRT.scala 160:23 116:26]
  wire [26:0] _GEN_2098 = nYReg_11[37:11]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_11_T = {{11{_GEN_2098[26]}},_GEN_2098}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_645 = ~skipZeros ? $signed(_tmpY_11_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1144 = maxExp ? $signed(38'sh0) : $signed(_GEN_645); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1532 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1144); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_11 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1532); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_12_T_2 = $signed(nXReg_11) + $signed(tmpY_11); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_644 = ~skipZeros ? $signed(_tmpX_11_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1143 = maxExp ? $signed(38'sh0) : $signed(_GEN_644); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1531 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1143); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_11 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1531); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_12_T_2 = $signed(nYReg_11) + $signed(tmpX_11); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_12_T_5 = $signed(nXReg_11) - $signed(tmpY_11); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_12_T_5 = $signed(nYReg_11) - $signed(tmpX_11); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_436 = nYReg_11[37] ? $signed(_nX_12_T_2) : $signed(_nX_12_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_437 = nYReg_11[37] ? $signed(_nY_12_T_2) : $signed(_nY_12_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_646 = ~skipZeros ? $signed(_GEN_436) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1145 = maxExp ? $signed(38'sh0) : $signed(_GEN_646); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1533 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1145); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_12 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1533); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_647 = ~skipZeros ? $signed(_GEN_437) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1146 = maxExp ? $signed(38'sh0) : $signed(_GEN_647); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1534 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1146); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_12 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1534); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_650 = ~skipZeros ? $signed(loopReg_12_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1149 = maxExp ? $signed(38'sh0) : $signed(_GEN_650); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1537 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1149); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_12 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1537); // @[SPFPSQRT.scala 160:23 115:26]
  wire [25:0] _GEN_2099 = nXReg_12[37:12]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_12_T = {{12{_GEN_2099[25]}},_GEN_2099}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_651 = ~skipZeros ? $signed(loopReg_12_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1150 = maxExp ? $signed(38'sh0) : $signed(_GEN_651); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1538 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1150); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_12 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1538); // @[SPFPSQRT.scala 160:23 116:26]
  wire [25:0] _GEN_2100 = nYReg_12[37:12]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_12_T = {{12{_GEN_2100[25]}},_GEN_2100}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_655 = ~skipZeros ? $signed(_tmpY_12_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1154 = maxExp ? $signed(38'sh0) : $signed(_GEN_655); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1542 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1154); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_12 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1542); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_13_T_2 = $signed(nXReg_12) + $signed(tmpY_12); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_654 = ~skipZeros ? $signed(_tmpX_12_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1153 = maxExp ? $signed(38'sh0) : $signed(_GEN_654); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1541 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1153); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_12 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1541); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_13_T_2 = $signed(nYReg_12) + $signed(tmpX_12); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_13_T_5 = $signed(nXReg_12) - $signed(tmpY_12); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_13_T_5 = $signed(nYReg_12) - $signed(tmpX_12); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_442 = nYReg_12[37] ? $signed(_nX_13_T_2) : $signed(_nX_13_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_443 = nYReg_12[37] ? $signed(_nY_13_T_2) : $signed(_nY_13_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_656 = ~skipZeros ? $signed(_GEN_442) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1155 = maxExp ? $signed(38'sh0) : $signed(_GEN_656); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1543 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1155); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_13 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1543); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_657 = ~skipZeros ? $signed(_GEN_443) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1156 = maxExp ? $signed(38'sh0) : $signed(_GEN_657); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1544 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1156); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_13 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1544); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_660 = ~skipZeros ? $signed(loopReg_13_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1159 = maxExp ? $signed(38'sh0) : $signed(_GEN_660); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1547 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1159); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_13 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1547); // @[SPFPSQRT.scala 160:23 115:26]
  wire [25:0] _GEN_2101 = nXReg_13[37:12]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_13_T = {{12{_GEN_2101[25]}},_GEN_2101}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_661 = ~skipZeros ? $signed(loopReg_13_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1160 = maxExp ? $signed(38'sh0) : $signed(_GEN_661); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1548 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1160); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_13 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1548); // @[SPFPSQRT.scala 160:23 116:26]
  wire [25:0] _GEN_2102 = nYReg_13[37:12]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_13_T = {{12{_GEN_2102[25]}},_GEN_2102}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_665 = ~skipZeros ? $signed(_tmpY_13_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1164 = maxExp ? $signed(38'sh0) : $signed(_GEN_665); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1552 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1164); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_13 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1552); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_14_T_2 = $signed(nXReg_13) + $signed(tmpY_13); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_664 = ~skipZeros ? $signed(_tmpX_13_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1163 = maxExp ? $signed(38'sh0) : $signed(_GEN_664); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1551 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1163); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_13 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1551); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_14_T_2 = $signed(nYReg_13) + $signed(tmpX_13); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_14_T_5 = $signed(nXReg_13) - $signed(tmpY_13); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_14_T_5 = $signed(nYReg_13) - $signed(tmpX_13); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_448 = nYReg_13[37] ? $signed(_nX_14_T_2) : $signed(_nX_14_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_449 = nYReg_13[37] ? $signed(_nY_14_T_2) : $signed(_nY_14_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_666 = ~skipZeros ? $signed(_GEN_448) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1165 = maxExp ? $signed(38'sh0) : $signed(_GEN_666); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1553 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1165); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_14 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1553); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_667 = ~skipZeros ? $signed(_GEN_449) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1166 = maxExp ? $signed(38'sh0) : $signed(_GEN_667); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1554 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1166); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_14 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1554); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_670 = ~skipZeros ? $signed(loopReg_14_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1169 = maxExp ? $signed(38'sh0) : $signed(_GEN_670); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1557 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1169); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_14 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1557); // @[SPFPSQRT.scala 160:23 115:26]
  wire [24:0] _GEN_2103 = nXReg_14[37:13]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_14_T = {{13{_GEN_2103[24]}},_GEN_2103}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_671 = ~skipZeros ? $signed(loopReg_14_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1170 = maxExp ? $signed(38'sh0) : $signed(_GEN_671); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1558 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1170); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_14 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1558); // @[SPFPSQRT.scala 160:23 116:26]
  wire [24:0] _GEN_2104 = nYReg_14[37:13]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_14_T = {{13{_GEN_2104[24]}},_GEN_2104}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_675 = ~skipZeros ? $signed(_tmpY_14_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1174 = maxExp ? $signed(38'sh0) : $signed(_GEN_675); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1562 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1174); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_14 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1562); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_15_T_2 = $signed(nXReg_14) + $signed(tmpY_14); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_674 = ~skipZeros ? $signed(_tmpX_14_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1173 = maxExp ? $signed(38'sh0) : $signed(_GEN_674); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1561 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1173); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_14 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1561); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_15_T_2 = $signed(nYReg_14) + $signed(tmpX_14); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_15_T_5 = $signed(nXReg_14) - $signed(tmpY_14); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_15_T_5 = $signed(nYReg_14) - $signed(tmpX_14); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_454 = nYReg_14[37] ? $signed(_nX_15_T_2) : $signed(_nX_15_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_455 = nYReg_14[37] ? $signed(_nY_15_T_2) : $signed(_nY_15_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_676 = ~skipZeros ? $signed(_GEN_454) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1175 = maxExp ? $signed(38'sh0) : $signed(_GEN_676); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1563 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1175); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_15 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1563); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_677 = ~skipZeros ? $signed(_GEN_455) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1176 = maxExp ? $signed(38'sh0) : $signed(_GEN_677); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1564 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1176); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_15 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1564); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_680 = ~skipZeros ? $signed(loopReg_15_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1179 = maxExp ? $signed(38'sh0) : $signed(_GEN_680); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1567 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1179); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_15 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1567); // @[SPFPSQRT.scala 160:23 115:26]
  wire [23:0] _GEN_2105 = nXReg_15[37:14]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_15_T = {{14{_GEN_2105[23]}},_GEN_2105}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_681 = ~skipZeros ? $signed(loopReg_15_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1180 = maxExp ? $signed(38'sh0) : $signed(_GEN_681); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1568 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1180); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_15 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1568); // @[SPFPSQRT.scala 160:23 116:26]
  wire [23:0] _GEN_2106 = nYReg_15[37:14]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_15_T = {{14{_GEN_2106[23]}},_GEN_2106}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_685 = ~skipZeros ? $signed(_tmpY_15_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1184 = maxExp ? $signed(38'sh0) : $signed(_GEN_685); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1572 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1184); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_15 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1572); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_16_T_2 = $signed(nXReg_15) + $signed(tmpY_15); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_684 = ~skipZeros ? $signed(_tmpX_15_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1183 = maxExp ? $signed(38'sh0) : $signed(_GEN_684); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1571 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1183); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_15 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1571); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_16_T_2 = $signed(nYReg_15) + $signed(tmpX_15); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_16_T_5 = $signed(nXReg_15) - $signed(tmpY_15); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_16_T_5 = $signed(nYReg_15) - $signed(tmpX_15); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_460 = nYReg_15[37] ? $signed(_nX_16_T_2) : $signed(_nX_16_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_461 = nYReg_15[37] ? $signed(_nY_16_T_2) : $signed(_nY_16_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_686 = ~skipZeros ? $signed(_GEN_460) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1185 = maxExp ? $signed(38'sh0) : $signed(_GEN_686); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1573 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1185); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_16 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1573); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_687 = ~skipZeros ? $signed(_GEN_461) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1186 = maxExp ? $signed(38'sh0) : $signed(_GEN_687); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1574 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1186); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_16 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1574); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_690 = ~skipZeros ? $signed(loopReg_16_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1189 = maxExp ? $signed(38'sh0) : $signed(_GEN_690); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1577 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1189); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_16 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1577); // @[SPFPSQRT.scala 160:23 115:26]
  wire [22:0] _GEN_2107 = nXReg_16[37:15]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_16_T = {{15{_GEN_2107[22]}},_GEN_2107}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_691 = ~skipZeros ? $signed(loopReg_16_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1190 = maxExp ? $signed(38'sh0) : $signed(_GEN_691); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1578 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1190); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_16 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1578); // @[SPFPSQRT.scala 160:23 116:26]
  wire [22:0] _GEN_2108 = nYReg_16[37:15]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_16_T = {{15{_GEN_2108[22]}},_GEN_2108}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_695 = ~skipZeros ? $signed(_tmpY_16_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1194 = maxExp ? $signed(38'sh0) : $signed(_GEN_695); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1582 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1194); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_16 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1582); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_17_T_2 = $signed(nXReg_16) + $signed(tmpY_16); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_694 = ~skipZeros ? $signed(_tmpX_16_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1193 = maxExp ? $signed(38'sh0) : $signed(_GEN_694); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1581 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1193); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_16 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1581); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_17_T_2 = $signed(nYReg_16) + $signed(tmpX_16); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_17_T_5 = $signed(nXReg_16) - $signed(tmpY_16); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_17_T_5 = $signed(nYReg_16) - $signed(tmpX_16); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_466 = nYReg_16[37] ? $signed(_nX_17_T_2) : $signed(_nX_17_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_467 = nYReg_16[37] ? $signed(_nY_17_T_2) : $signed(_nY_17_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_696 = ~skipZeros ? $signed(_GEN_466) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1195 = maxExp ? $signed(38'sh0) : $signed(_GEN_696); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1583 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1195); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_17 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1583); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_697 = ~skipZeros ? $signed(_GEN_467) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1196 = maxExp ? $signed(38'sh0) : $signed(_GEN_697); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1584 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1196); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_17 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1584); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_700 = ~skipZeros ? $signed(loopReg_17_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1199 = maxExp ? $signed(38'sh0) : $signed(_GEN_700); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1587 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1199); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_17 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1587); // @[SPFPSQRT.scala 160:23 115:26]
  wire [21:0] _GEN_2109 = nXReg_17[37:16]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_17_T = {{16{_GEN_2109[21]}},_GEN_2109}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_701 = ~skipZeros ? $signed(loopReg_17_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1200 = maxExp ? $signed(38'sh0) : $signed(_GEN_701); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1588 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1200); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_17 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1588); // @[SPFPSQRT.scala 160:23 116:26]
  wire [21:0] _GEN_2110 = nYReg_17[37:16]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_17_T = {{16{_GEN_2110[21]}},_GEN_2110}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_705 = ~skipZeros ? $signed(_tmpY_17_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1204 = maxExp ? $signed(38'sh0) : $signed(_GEN_705); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1592 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1204); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_17 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1592); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_18_T_2 = $signed(nXReg_17) + $signed(tmpY_17); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_704 = ~skipZeros ? $signed(_tmpX_17_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1203 = maxExp ? $signed(38'sh0) : $signed(_GEN_704); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1591 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1203); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_17 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1591); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_18_T_2 = $signed(nYReg_17) + $signed(tmpX_17); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_18_T_5 = $signed(nXReg_17) - $signed(tmpY_17); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_18_T_5 = $signed(nYReg_17) - $signed(tmpX_17); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_472 = nYReg_17[37] ? $signed(_nX_18_T_2) : $signed(_nX_18_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_473 = nYReg_17[37] ? $signed(_nY_18_T_2) : $signed(_nY_18_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_706 = ~skipZeros ? $signed(_GEN_472) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1205 = maxExp ? $signed(38'sh0) : $signed(_GEN_706); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1593 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1205); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_18 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1593); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_707 = ~skipZeros ? $signed(_GEN_473) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1206 = maxExp ? $signed(38'sh0) : $signed(_GEN_707); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1594 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1206); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_18 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1594); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_710 = ~skipZeros ? $signed(loopReg_18_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1209 = maxExp ? $signed(38'sh0) : $signed(_GEN_710); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1597 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1209); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_18 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1597); // @[SPFPSQRT.scala 160:23 115:26]
  wire [20:0] _GEN_2111 = nXReg_18[37:17]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_18_T = {{17{_GEN_2111[20]}},_GEN_2111}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_711 = ~skipZeros ? $signed(loopReg_18_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1210 = maxExp ? $signed(38'sh0) : $signed(_GEN_711); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1598 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1210); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_18 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1598); // @[SPFPSQRT.scala 160:23 116:26]
  wire [20:0] _GEN_2112 = nYReg_18[37:17]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_18_T = {{17{_GEN_2112[20]}},_GEN_2112}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_715 = ~skipZeros ? $signed(_tmpY_18_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1214 = maxExp ? $signed(38'sh0) : $signed(_GEN_715); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1602 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1214); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_18 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1602); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_19_T_2 = $signed(nXReg_18) + $signed(tmpY_18); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_714 = ~skipZeros ? $signed(_tmpX_18_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1213 = maxExp ? $signed(38'sh0) : $signed(_GEN_714); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1601 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1213); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_18 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1601); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_19_T_2 = $signed(nYReg_18) + $signed(tmpX_18); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_19_T_5 = $signed(nXReg_18) - $signed(tmpY_18); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_19_T_5 = $signed(nYReg_18) - $signed(tmpX_18); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_478 = nYReg_18[37] ? $signed(_nX_19_T_2) : $signed(_nX_19_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_479 = nYReg_18[37] ? $signed(_nY_19_T_2) : $signed(_nY_19_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_716 = ~skipZeros ? $signed(_GEN_478) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1215 = maxExp ? $signed(38'sh0) : $signed(_GEN_716); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1603 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1215); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_19 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1603); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_717 = ~skipZeros ? $signed(_GEN_479) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1216 = maxExp ? $signed(38'sh0) : $signed(_GEN_717); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1604 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1216); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_19 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1604); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_720 = ~skipZeros ? $signed(loopReg_19_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1219 = maxExp ? $signed(38'sh0) : $signed(_GEN_720); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1607 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1219); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_19 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1607); // @[SPFPSQRT.scala 160:23 115:26]
  wire [19:0] _GEN_2113 = nXReg_19[37:18]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_19_T = {{18{_GEN_2113[19]}},_GEN_2113}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_721 = ~skipZeros ? $signed(loopReg_19_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1220 = maxExp ? $signed(38'sh0) : $signed(_GEN_721); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1608 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1220); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_19 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1608); // @[SPFPSQRT.scala 160:23 116:26]
  wire [19:0] _GEN_2114 = nYReg_19[37:18]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_19_T = {{18{_GEN_2114[19]}},_GEN_2114}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_725 = ~skipZeros ? $signed(_tmpY_19_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1224 = maxExp ? $signed(38'sh0) : $signed(_GEN_725); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1612 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1224); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_19 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1612); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_20_T_2 = $signed(nXReg_19) + $signed(tmpY_19); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_724 = ~skipZeros ? $signed(_tmpX_19_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1223 = maxExp ? $signed(38'sh0) : $signed(_GEN_724); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1611 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1223); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_19 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1611); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_20_T_2 = $signed(nYReg_19) + $signed(tmpX_19); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_20_T_5 = $signed(nXReg_19) - $signed(tmpY_19); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_20_T_5 = $signed(nYReg_19) - $signed(tmpX_19); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_484 = nYReg_19[37] ? $signed(_nX_20_T_2) : $signed(_nX_20_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_485 = nYReg_19[37] ? $signed(_nY_20_T_2) : $signed(_nY_20_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_726 = ~skipZeros ? $signed(_GEN_484) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1225 = maxExp ? $signed(38'sh0) : $signed(_GEN_726); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1613 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1225); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_20 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1613); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_727 = ~skipZeros ? $signed(_GEN_485) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1226 = maxExp ? $signed(38'sh0) : $signed(_GEN_727); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1614 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1226); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_20 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1614); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_730 = ~skipZeros ? $signed(loopReg_20_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1229 = maxExp ? $signed(38'sh0) : $signed(_GEN_730); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1617 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1229); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_20 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1617); // @[SPFPSQRT.scala 160:23 115:26]
  wire [18:0] _GEN_2115 = nXReg_20[37:19]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_20_T = {{19{_GEN_2115[18]}},_GEN_2115}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_731 = ~skipZeros ? $signed(loopReg_20_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1230 = maxExp ? $signed(38'sh0) : $signed(_GEN_731); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1618 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1230); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_20 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1618); // @[SPFPSQRT.scala 160:23 116:26]
  wire [18:0] _GEN_2116 = nYReg_20[37:19]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_20_T = {{19{_GEN_2116[18]}},_GEN_2116}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_735 = ~skipZeros ? $signed(_tmpY_20_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1234 = maxExp ? $signed(38'sh0) : $signed(_GEN_735); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1622 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1234); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_20 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1622); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_21_T_2 = $signed(nXReg_20) + $signed(tmpY_20); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_734 = ~skipZeros ? $signed(_tmpX_20_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1233 = maxExp ? $signed(38'sh0) : $signed(_GEN_734); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1621 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1233); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_20 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1621); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_21_T_2 = $signed(nYReg_20) + $signed(tmpX_20); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_21_T_5 = $signed(nXReg_20) - $signed(tmpY_20); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_21_T_5 = $signed(nYReg_20) - $signed(tmpX_20); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_490 = nYReg_20[37] ? $signed(_nX_21_T_2) : $signed(_nX_21_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_491 = nYReg_20[37] ? $signed(_nY_21_T_2) : $signed(_nY_21_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_736 = ~skipZeros ? $signed(_GEN_490) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1235 = maxExp ? $signed(38'sh0) : $signed(_GEN_736); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1623 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1235); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_21 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1623); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_737 = ~skipZeros ? $signed(_GEN_491) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1236 = maxExp ? $signed(38'sh0) : $signed(_GEN_737); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1624 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1236); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_21 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1624); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_740 = ~skipZeros ? $signed(loopReg_21_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1239 = maxExp ? $signed(38'sh0) : $signed(_GEN_740); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1627 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1239); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_21 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1627); // @[SPFPSQRT.scala 160:23 115:26]
  wire [17:0] _GEN_2117 = nXReg_21[37:20]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_21_T = {{20{_GEN_2117[17]}},_GEN_2117}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_741 = ~skipZeros ? $signed(loopReg_21_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1240 = maxExp ? $signed(38'sh0) : $signed(_GEN_741); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1628 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1240); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_21 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1628); // @[SPFPSQRT.scala 160:23 116:26]
  wire [17:0] _GEN_2118 = nYReg_21[37:20]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_21_T = {{20{_GEN_2118[17]}},_GEN_2118}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_745 = ~skipZeros ? $signed(_tmpY_21_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1244 = maxExp ? $signed(38'sh0) : $signed(_GEN_745); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1632 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1244); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_21 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1632); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_22_T_2 = $signed(nXReg_21) + $signed(tmpY_21); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_744 = ~skipZeros ? $signed(_tmpX_21_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1243 = maxExp ? $signed(38'sh0) : $signed(_GEN_744); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1631 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1243); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_21 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1631); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_22_T_2 = $signed(nYReg_21) + $signed(tmpX_21); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_22_T_5 = $signed(nXReg_21) - $signed(tmpY_21); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_22_T_5 = $signed(nYReg_21) - $signed(tmpX_21); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_496 = nYReg_21[37] ? $signed(_nX_22_T_2) : $signed(_nX_22_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_497 = nYReg_21[37] ? $signed(_nY_22_T_2) : $signed(_nY_22_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_746 = ~skipZeros ? $signed(_GEN_496) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1245 = maxExp ? $signed(38'sh0) : $signed(_GEN_746); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1633 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1245); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_22 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1633); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_747 = ~skipZeros ? $signed(_GEN_497) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1246 = maxExp ? $signed(38'sh0) : $signed(_GEN_747); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1634 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1246); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_22 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1634); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_750 = ~skipZeros ? $signed(loopReg_22_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1249 = maxExp ? $signed(38'sh0) : $signed(_GEN_750); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1637 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1249); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_22 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1637); // @[SPFPSQRT.scala 160:23 115:26]
  wire [16:0] _GEN_2119 = nXReg_22[37:21]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_22_T = {{21{_GEN_2119[16]}},_GEN_2119}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_751 = ~skipZeros ? $signed(loopReg_22_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1250 = maxExp ? $signed(38'sh0) : $signed(_GEN_751); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1638 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1250); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_22 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1638); // @[SPFPSQRT.scala 160:23 116:26]
  wire [16:0] _GEN_2120 = nYReg_22[37:21]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_22_T = {{21{_GEN_2120[16]}},_GEN_2120}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_755 = ~skipZeros ? $signed(_tmpY_22_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1254 = maxExp ? $signed(38'sh0) : $signed(_GEN_755); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1642 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1254); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_22 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1642); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_23_T_2 = $signed(nXReg_22) + $signed(tmpY_22); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_754 = ~skipZeros ? $signed(_tmpX_22_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1253 = maxExp ? $signed(38'sh0) : $signed(_GEN_754); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1641 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1253); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_22 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1641); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_23_T_2 = $signed(nYReg_22) + $signed(tmpX_22); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_23_T_5 = $signed(nXReg_22) - $signed(tmpY_22); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_23_T_5 = $signed(nYReg_22) - $signed(tmpX_22); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_502 = nYReg_22[37] ? $signed(_nX_23_T_2) : $signed(_nX_23_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_503 = nYReg_22[37] ? $signed(_nY_23_T_2) : $signed(_nY_23_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_756 = ~skipZeros ? $signed(_GEN_502) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1255 = maxExp ? $signed(38'sh0) : $signed(_GEN_756); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1643 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1255); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_23 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1643); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_757 = ~skipZeros ? $signed(_GEN_503) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1256 = maxExp ? $signed(38'sh0) : $signed(_GEN_757); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1644 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1256); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_23 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1644); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_760 = ~skipZeros ? $signed(loopReg_23_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1259 = maxExp ? $signed(38'sh0) : $signed(_GEN_760); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1647 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1259); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_23 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1647); // @[SPFPSQRT.scala 160:23 115:26]
  wire [15:0] _GEN_2121 = nXReg_23[37:22]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_23_T = {{22{_GEN_2121[15]}},_GEN_2121}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_761 = ~skipZeros ? $signed(loopReg_23_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1260 = maxExp ? $signed(38'sh0) : $signed(_GEN_761); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1648 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1260); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_23 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1648); // @[SPFPSQRT.scala 160:23 116:26]
  wire [15:0] _GEN_2122 = nYReg_23[37:22]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_23_T = {{22{_GEN_2122[15]}},_GEN_2122}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_765 = ~skipZeros ? $signed(_tmpY_23_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1264 = maxExp ? $signed(38'sh0) : $signed(_GEN_765); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1652 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1264); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_23 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1652); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_24_T_2 = $signed(nXReg_23) + $signed(tmpY_23); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_764 = ~skipZeros ? $signed(_tmpX_23_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1263 = maxExp ? $signed(38'sh0) : $signed(_GEN_764); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1651 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1263); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_23 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1651); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_24_T_2 = $signed(nYReg_23) + $signed(tmpX_23); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_24_T_5 = $signed(nXReg_23) - $signed(tmpY_23); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_24_T_5 = $signed(nYReg_23) - $signed(tmpX_23); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_508 = nYReg_23[37] ? $signed(_nX_24_T_2) : $signed(_nX_24_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_509 = nYReg_23[37] ? $signed(_nY_24_T_2) : $signed(_nY_24_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_766 = ~skipZeros ? $signed(_GEN_508) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1265 = maxExp ? $signed(38'sh0) : $signed(_GEN_766); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1653 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1265); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_24 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1653); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_767 = ~skipZeros ? $signed(_GEN_509) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1266 = maxExp ? $signed(38'sh0) : $signed(_GEN_767); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1654 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1266); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_24 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1654); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_770 = ~skipZeros ? $signed(loopReg_24_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1269 = maxExp ? $signed(38'sh0) : $signed(_GEN_770); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1657 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1269); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_24 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1657); // @[SPFPSQRT.scala 160:23 115:26]
  wire [14:0] _GEN_2123 = nXReg_24[37:23]; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _tmpX_24_T = {{23{_GEN_2123[14]}},_GEN_2123}; // @[SPFPSQRT.scala 305:34]
  wire [37:0] _GEN_771 = ~skipZeros ? $signed(loopReg_24_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1270 = maxExp ? $signed(38'sh0) : $signed(_GEN_771); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1658 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1270); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_24 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1658); // @[SPFPSQRT.scala 160:23 116:26]
  wire [14:0] _GEN_2124 = nYReg_24[37:23]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_24_T = {{23{_GEN_2124[14]}},_GEN_2124}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_775 = ~skipZeros ? $signed(_tmpY_24_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1274 = maxExp ? $signed(38'sh0) : $signed(_GEN_775); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1662 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1274); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_24 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1662); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_25_T_2 = $signed(nXReg_24) + $signed(tmpY_24); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _GEN_774 = ~skipZeros ? $signed(_tmpX_24_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 305:20 112:25]
  wire [37:0] _GEN_1273 = maxExp ? $signed(38'sh0) : $signed(_GEN_774); // @[SPFPSQRT.scala 209:23 112:25]
  wire [37:0] _GEN_1661 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1273); // @[SPFPSQRT.scala 112:25 194:41]
  wire [37:0] tmpX_24 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1661); // @[SPFPSQRT.scala 160:23 112:25]
  wire [37:0] _nY_25_T_2 = $signed(nYReg_24) + $signed(tmpX_24); // @[SPFPSQRT.scala 317:31]
  wire [37:0] _nX_25_T_5 = $signed(nXReg_24) - $signed(tmpY_24); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _nY_25_T_5 = $signed(nYReg_24) - $signed(tmpX_24); // @[SPFPSQRT.scala 325:31]
  wire [37:0] _GEN_514 = nYReg_24[37] ? $signed(_nX_25_T_2) : $signed(_nX_25_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_515 = nYReg_24[37] ? $signed(_nY_25_T_2) : $signed(_nY_25_T_5); // @[SPFPSQRT.scala 314:50 317:17 325:17]
  wire [37:0] _GEN_776 = ~skipZeros ? $signed(_GEN_514) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1275 = maxExp ? $signed(38'sh0) : $signed(_GEN_776); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1663 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1275); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_25 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1663); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_777 = ~skipZeros ? $signed(_GEN_515) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 111:23]
  wire [37:0] _GEN_1276 = maxExp ? $signed(38'sh0) : $signed(_GEN_777); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1664 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1276); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_25 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1664); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_780 = ~skipZeros ? $signed(loopReg_25_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1279 = maxExp ? $signed(38'sh0) : $signed(_GEN_780); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1667 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1279); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_25 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1667); // @[SPFPSQRT.scala 160:23 115:26]
  wire [37:0] _GEN_781 = ~skipZeros ? $signed(loopReg_25_nY) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 116:26]
  wire [37:0] _GEN_1280 = maxExp ? $signed(38'sh0) : $signed(_GEN_781); // @[SPFPSQRT.scala 209:23 116:26]
  wire [37:0] _GEN_1668 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1280); // @[SPFPSQRT.scala 116:26 194:41]
  wire [37:0] nYReg_25 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1668); // @[SPFPSQRT.scala 160:23 116:26]
  wire [13:0] _GEN_2126 = nYReg_25[37:24]; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _tmpY_25_T = {{24{_GEN_2126[13]}},_GEN_2126}; // @[SPFPSQRT.scala 306:34]
  wire [37:0] _GEN_785 = ~skipZeros ? $signed(_tmpY_25_T) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 306:20 113:25]
  wire [37:0] _GEN_1284 = maxExp ? $signed(38'sh0) : $signed(_GEN_785); // @[SPFPSQRT.scala 209:23 113:25]
  wire [37:0] _GEN_1672 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1284); // @[SPFPSQRT.scala 113:25 194:41]
  wire [37:0] tmpY_25 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1672); // @[SPFPSQRT.scala 160:23 113:25]
  wire [37:0] _nX_26_T_2 = $signed(nXReg_25) + $signed(tmpY_25); // @[SPFPSQRT.scala 316:31]
  wire [37:0] _nX_26_T_5 = $signed(nXReg_25) - $signed(tmpY_25); // @[SPFPSQRT.scala 324:31]
  wire [37:0] _GEN_520 = nYReg_25[37] ? $signed(_nX_26_T_2) : $signed(_nX_26_T_5); // @[SPFPSQRT.scala 314:50 316:17 324:17]
  wire [37:0] _GEN_786 = ~skipZeros ? $signed(_GEN_520) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 110:23]
  wire [37:0] _GEN_1285 = maxExp ? $signed(38'sh0) : $signed(_GEN_786); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1673 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1285); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_26 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1673); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_790 = ~skipZeros ? $signed(loopReg_26_nX) : $signed(38'sh0); // @[SPFPSQRT.scala 293:22 115:26]
  wire [37:0] _GEN_1289 = maxExp ? $signed(38'sh0) : $signed(_GEN_790); // @[SPFPSQRT.scala 209:23 115:26]
  wire [37:0] _GEN_1677 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1289); // @[SPFPSQRT.scala 115:26 194:41]
  wire [37:0] nXReg_26 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1677); // @[SPFPSQRT.scala 160:23 115:26]
  wire [75:0] mulRes = 38'sh9a8f43900 * $signed(nXReg_26); // @[SPFPSQRT.scala 340:31]
  wire [31:0] _tempMant_T = mulRes[75:44]; // @[SPFPSQRT.scala 344:30]
  wire [28:0] tempMant = _tempMant_T[28:0]; // @[SPFPSQRT.scala 344:61]
  wire  sticky = |mulRes[44:0]; // @[SPFPSQRT.scala 348:53]
  wire [29:0] _finalMant_T = {tempMant, 1'h0}; // @[SPFPSQRT.scala 357:32]
  wire [7:0] _finalExp_T_4 = $signed(loopReg_26_sqrtExp) - 8'sh1; // @[SPFPSQRT.scala 358:78]
  wire [27:0] finalMant = ~tempMant[26] ? _finalMant_T[28:1] : tempMant[28:1]; // @[SPFPSQRT.scala 354:41 357:19 361:19]
  wire [27:0] _io_out_bits_result_mantissa_T_1 = {finalMant[26:0],sticky}; // @[Cat.scala 33:92]
  wire [7:0] _io_out_bits_result_exponent_T = ~tempMant[26] ? _finalExp_T_4 : loopReg_26_sqrtExp; // @[SPFPSQRT.scala 365:47]
  wire [7:0] _GEN_1024 = maxExp ? 8'h0 : _GEN_367; // @[SPFPSQRT.scala 209:23 119:29]
  wire [7:0] _GEN_1412 = zeroMant & _T_1 ? 8'h0 : _GEN_1024; // @[SPFPSQRT.scala 119:29 194:41]
  wire [7:0] sqrtExp = io_in_bits_a_sign ? 8'h0 : _GEN_1412; // @[SPFPSQRT.scala 160:23 119:29]
  wire [7:0] _noLoopReg_0_exponent_T_4 = io_in_bits_a_sign ? 8'h0 : _GEN_1412; // @[SPFPSQRT.scala 371:40]
  wire [37:0] _GEN_1025 = maxExp ? $signed(38'sh0) : $signed(_GEN_368); // @[SPFPSQRT.scala 110:23 209:23]
  wire [37:0] _GEN_1413 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1025); // @[SPFPSQRT.scala 110:23 194:41]
  wire [37:0] nX_0 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1413); // @[SPFPSQRT.scala 110:23 160:23]
  wire [37:0] _GEN_528 = ~skipZeros ? $signed(nX_0) : $signed(loopReg_0_nX); // @[SPFPSQRT.scala 293:22 295:21 121:24]
  wire [37:0] _GEN_1026 = maxExp ? $signed(38'sh0) : $signed(_GEN_369); // @[SPFPSQRT.scala 111:23 209:23]
  wire [37:0] _GEN_1414 = zeroMant & _T_1 ? $signed(38'sh0) : $signed(_GEN_1026); // @[SPFPSQRT.scala 111:23 194:41]
  wire [37:0] nY_0 = io_in_bits_a_sign ? $signed(38'sh0) : $signed(_GEN_1414); // @[SPFPSQRT.scala 111:23 160:23]
  wire [37:0] _GEN_529 = ~skipZeros ? $signed(nY_0) : $signed(loopReg_0_nY); // @[SPFPSQRT.scala 293:22 296:21 121:24]
  wire  _GEN_530 = ~skipZeros ? io_in_valid : loopReg_0_valid; // @[SPFPSQRT.scala 293:22 121:24 297:24]
  wire [7:0] _GEN_531 = ~skipZeros ? sqrtExp : loopReg_0_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 298:26]
  wire [37:0] _GEN_538 = ~skipZeros ? $signed(nX_1) : $signed(loopReg_1_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_539 = ~skipZeros ? $signed(nY_1) : $signed(loopReg_1_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_542 = ~skipZeros ? loopReg_0_valid : loopReg_1_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_543 = ~skipZeros ? loopReg_0_sqrtExp : loopReg_1_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_548 = ~skipZeros ? $signed(nX_2) : $signed(loopReg_2_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_549 = ~skipZeros ? $signed(nY_2) : $signed(loopReg_2_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_552 = ~skipZeros ? loopReg_1_valid : loopReg_2_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_553 = ~skipZeros ? loopReg_1_sqrtExp : loopReg_2_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_558 = ~skipZeros ? $signed(nX_3) : $signed(loopReg_3_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_559 = ~skipZeros ? $signed(nY_3) : $signed(loopReg_3_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_562 = ~skipZeros ? loopReg_2_valid : loopReg_3_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_563 = ~skipZeros ? loopReg_2_sqrtExp : loopReg_3_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_568 = ~skipZeros ? $signed(nX_4) : $signed(loopReg_4_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_569 = ~skipZeros ? $signed(nY_4) : $signed(loopReg_4_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_572 = ~skipZeros ? loopReg_3_valid : loopReg_4_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_573 = ~skipZeros ? loopReg_3_sqrtExp : loopReg_4_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_578 = ~skipZeros ? $signed(nX_5) : $signed(loopReg_5_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_579 = ~skipZeros ? $signed(nY_5) : $signed(loopReg_5_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_582 = ~skipZeros ? loopReg_4_valid : loopReg_5_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_583 = ~skipZeros ? loopReg_4_sqrtExp : loopReg_5_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_588 = ~skipZeros ? $signed(nX_6) : $signed(loopReg_6_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_589 = ~skipZeros ? $signed(nY_6) : $signed(loopReg_6_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_592 = ~skipZeros ? loopReg_5_valid : loopReg_6_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_593 = ~skipZeros ? loopReg_5_sqrtExp : loopReg_6_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_598 = ~skipZeros ? $signed(nX_7) : $signed(loopReg_7_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_599 = ~skipZeros ? $signed(nY_7) : $signed(loopReg_7_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_602 = ~skipZeros ? loopReg_6_valid : loopReg_7_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_603 = ~skipZeros ? loopReg_6_sqrtExp : loopReg_7_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_608 = ~skipZeros ? $signed(nX_8) : $signed(loopReg_8_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_609 = ~skipZeros ? $signed(nY_8) : $signed(loopReg_8_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_612 = ~skipZeros ? loopReg_7_valid : loopReg_8_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_613 = ~skipZeros ? loopReg_7_sqrtExp : loopReg_8_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_618 = ~skipZeros ? $signed(nX_9) : $signed(loopReg_9_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_619 = ~skipZeros ? $signed(nY_9) : $signed(loopReg_9_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_622 = ~skipZeros ? loopReg_8_valid : loopReg_9_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_623 = ~skipZeros ? loopReg_8_sqrtExp : loopReg_9_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_628 = ~skipZeros ? $signed(nX_10) : $signed(loopReg_10_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_629 = ~skipZeros ? $signed(nY_10) : $signed(loopReg_10_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_632 = ~skipZeros ? loopReg_9_valid : loopReg_10_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_633 = ~skipZeros ? loopReg_9_sqrtExp : loopReg_10_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_638 = ~skipZeros ? $signed(nX_11) : $signed(loopReg_11_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_639 = ~skipZeros ? $signed(nY_11) : $signed(loopReg_11_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_642 = ~skipZeros ? loopReg_10_valid : loopReg_11_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_643 = ~skipZeros ? loopReg_10_sqrtExp : loopReg_11_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_648 = ~skipZeros ? $signed(nX_12) : $signed(loopReg_12_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_649 = ~skipZeros ? $signed(nY_12) : $signed(loopReg_12_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_652 = ~skipZeros ? loopReg_11_valid : loopReg_12_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_653 = ~skipZeros ? loopReg_11_sqrtExp : loopReg_12_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_658 = ~skipZeros ? $signed(nX_13) : $signed(loopReg_13_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_659 = ~skipZeros ? $signed(nY_13) : $signed(loopReg_13_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_662 = ~skipZeros ? loopReg_12_valid : loopReg_13_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_663 = ~skipZeros ? loopReg_12_sqrtExp : loopReg_13_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_668 = ~skipZeros ? $signed(nX_14) : $signed(loopReg_14_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_669 = ~skipZeros ? $signed(nY_14) : $signed(loopReg_14_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_672 = ~skipZeros ? loopReg_13_valid : loopReg_14_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_673 = ~skipZeros ? loopReg_13_sqrtExp : loopReg_14_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_678 = ~skipZeros ? $signed(nX_15) : $signed(loopReg_15_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_679 = ~skipZeros ? $signed(nY_15) : $signed(loopReg_15_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_682 = ~skipZeros ? loopReg_14_valid : loopReg_15_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_683 = ~skipZeros ? loopReg_14_sqrtExp : loopReg_15_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_688 = ~skipZeros ? $signed(nX_16) : $signed(loopReg_16_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_689 = ~skipZeros ? $signed(nY_16) : $signed(loopReg_16_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_692 = ~skipZeros ? loopReg_15_valid : loopReg_16_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_693 = ~skipZeros ? loopReg_15_sqrtExp : loopReg_16_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_698 = ~skipZeros ? $signed(nX_17) : $signed(loopReg_17_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_699 = ~skipZeros ? $signed(nY_17) : $signed(loopReg_17_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_702 = ~skipZeros ? loopReg_16_valid : loopReg_17_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_703 = ~skipZeros ? loopReg_16_sqrtExp : loopReg_17_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_708 = ~skipZeros ? $signed(nX_18) : $signed(loopReg_18_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_709 = ~skipZeros ? $signed(nY_18) : $signed(loopReg_18_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_712 = ~skipZeros ? loopReg_17_valid : loopReg_18_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_713 = ~skipZeros ? loopReg_17_sqrtExp : loopReg_18_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_718 = ~skipZeros ? $signed(nX_19) : $signed(loopReg_19_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_719 = ~skipZeros ? $signed(nY_19) : $signed(loopReg_19_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_722 = ~skipZeros ? loopReg_18_valid : loopReg_19_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_723 = ~skipZeros ? loopReg_18_sqrtExp : loopReg_19_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_728 = ~skipZeros ? $signed(nX_20) : $signed(loopReg_20_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_729 = ~skipZeros ? $signed(nY_20) : $signed(loopReg_20_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_732 = ~skipZeros ? loopReg_19_valid : loopReg_20_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_733 = ~skipZeros ? loopReg_19_sqrtExp : loopReg_20_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_738 = ~skipZeros ? $signed(nX_21) : $signed(loopReg_21_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_739 = ~skipZeros ? $signed(nY_21) : $signed(loopReg_21_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_742 = ~skipZeros ? loopReg_20_valid : loopReg_21_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_743 = ~skipZeros ? loopReg_20_sqrtExp : loopReg_21_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_748 = ~skipZeros ? $signed(nX_22) : $signed(loopReg_22_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_749 = ~skipZeros ? $signed(nY_22) : $signed(loopReg_22_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_752 = ~skipZeros ? loopReg_21_valid : loopReg_22_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_753 = ~skipZeros ? loopReg_21_sqrtExp : loopReg_22_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_758 = ~skipZeros ? $signed(nX_23) : $signed(loopReg_23_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_759 = ~skipZeros ? $signed(nY_23) : $signed(loopReg_23_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_762 = ~skipZeros ? loopReg_22_valid : loopReg_23_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_763 = ~skipZeros ? loopReg_22_sqrtExp : loopReg_23_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_768 = ~skipZeros ? $signed(nX_24) : $signed(loopReg_24_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_769 = ~skipZeros ? $signed(nY_24) : $signed(loopReg_24_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_772 = ~skipZeros ? loopReg_23_valid : loopReg_24_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_773 = ~skipZeros ? loopReg_23_sqrtExp : loopReg_24_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_778 = ~skipZeros ? $signed(nX_25) : $signed(loopReg_25_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire [37:0] _GEN_779 = ~skipZeros ? $signed(nY_25) : $signed(loopReg_25_nY); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_782 = ~skipZeros ? loopReg_24_valid : loopReg_25_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_783 = ~skipZeros ? loopReg_24_sqrtExp : loopReg_25_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [37:0] _GEN_788 = ~skipZeros ? $signed(nX_26) : $signed(loopReg_26_nX); // @[SPFPSQRT.scala 293:22 121:24]
  wire  _GEN_792 = ~skipZeros ? loopReg_25_valid : loopReg_26_valid; // @[SPFPSQRT.scala 293:22 121:24 331:26]
  wire [7:0] _GEN_793 = ~skipZeros ? loopReg_25_sqrtExp : loopReg_26_sqrtExp; // @[SPFPSQRT.scala 293:22 121:24 332:28]
  wire [25:0] _GEN_794 = ~skipZeros ? {{3'd0}, _io_out_bits_result_mantissa_T_1[25:3]} : noLoopReg_26_mantissa; // @[SPFPSQRT.scala 293:22 364:35 379:35]
  wire [7:0] _GEN_795 = ~skipZeros ? $signed(_io_out_bits_result_exponent_T) : $signed(noLoopReg_26_exponent); // @[SPFPSQRT.scala 293:22 365:35 380:35]
  wire  _GEN_796 = ~skipZeros ? 1'h0 : noLoopReg_26_sign; // @[SPFPSQRT.scala 293:22 366:35 381:35]
  wire [25:0] _GEN_797 = ~skipZeros ? noLoopReg_0_mantissa : 26'h0; // @[SPFPSQRT.scala 293:22 122:26 370:29]
  wire [7:0] _GEN_798 = ~skipZeros ? $signed(noLoopReg_0_exponent) : $signed(_noLoopReg_0_exponent_T_4); // @[SPFPSQRT.scala 293:22 122:26 371:29]
  wire  _GEN_799 = ~skipZeros & noLoopReg_0_sign; // @[SPFPSQRT.scala 293:22 122:26 372:29]
  wire  _GEN_800 = ~skipZeros ? noLoopReg_0_valid : io_in_valid; // @[SPFPSQRT.scala 293:22 122:26 373:29]
  wire [25:0] _GEN_801 = ~skipZeros ? noLoopReg_1_mantissa : noLoopReg_0_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_802 = ~skipZeros ? $signed(noLoopReg_1_exponent) : $signed(noLoopReg_0_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_803 = ~skipZeros ? noLoopReg_1_sign : noLoopReg_0_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_804 = ~skipZeros ? noLoopReg_1_valid : noLoopReg_0_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_805 = ~skipZeros ? noLoopReg_2_mantissa : noLoopReg_1_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_806 = ~skipZeros ? $signed(noLoopReg_2_exponent) : $signed(noLoopReg_1_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_807 = ~skipZeros ? noLoopReg_2_sign : noLoopReg_1_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_808 = ~skipZeros ? noLoopReg_2_valid : noLoopReg_1_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_809 = ~skipZeros ? noLoopReg_3_mantissa : noLoopReg_2_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_810 = ~skipZeros ? $signed(noLoopReg_3_exponent) : $signed(noLoopReg_2_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_811 = ~skipZeros ? noLoopReg_3_sign : noLoopReg_2_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_812 = ~skipZeros ? noLoopReg_3_valid : noLoopReg_2_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_813 = ~skipZeros ? noLoopReg_4_mantissa : noLoopReg_3_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_814 = ~skipZeros ? $signed(noLoopReg_4_exponent) : $signed(noLoopReg_3_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_815 = ~skipZeros ? noLoopReg_4_sign : noLoopReg_3_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_816 = ~skipZeros ? noLoopReg_4_valid : noLoopReg_3_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_817 = ~skipZeros ? noLoopReg_5_mantissa : noLoopReg_4_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_818 = ~skipZeros ? $signed(noLoopReg_5_exponent) : $signed(noLoopReg_4_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_819 = ~skipZeros ? noLoopReg_5_sign : noLoopReg_4_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_820 = ~skipZeros ? noLoopReg_5_valid : noLoopReg_4_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_821 = ~skipZeros ? noLoopReg_6_mantissa : noLoopReg_5_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_822 = ~skipZeros ? $signed(noLoopReg_6_exponent) : $signed(noLoopReg_5_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_823 = ~skipZeros ? noLoopReg_6_sign : noLoopReg_5_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_824 = ~skipZeros ? noLoopReg_6_valid : noLoopReg_5_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_825 = ~skipZeros ? noLoopReg_7_mantissa : noLoopReg_6_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_826 = ~skipZeros ? $signed(noLoopReg_7_exponent) : $signed(noLoopReg_6_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_827 = ~skipZeros ? noLoopReg_7_sign : noLoopReg_6_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_828 = ~skipZeros ? noLoopReg_7_valid : noLoopReg_6_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_829 = ~skipZeros ? noLoopReg_8_mantissa : noLoopReg_7_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_830 = ~skipZeros ? $signed(noLoopReg_8_exponent) : $signed(noLoopReg_7_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_831 = ~skipZeros ? noLoopReg_8_sign : noLoopReg_7_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_832 = ~skipZeros ? noLoopReg_8_valid : noLoopReg_7_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_833 = ~skipZeros ? noLoopReg_9_mantissa : noLoopReg_8_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_834 = ~skipZeros ? $signed(noLoopReg_9_exponent) : $signed(noLoopReg_8_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_835 = ~skipZeros ? noLoopReg_9_sign : noLoopReg_8_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_836 = ~skipZeros ? noLoopReg_9_valid : noLoopReg_8_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_837 = ~skipZeros ? noLoopReg_10_mantissa : noLoopReg_9_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_838 = ~skipZeros ? $signed(noLoopReg_10_exponent) : $signed(noLoopReg_9_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_839 = ~skipZeros ? noLoopReg_10_sign : noLoopReg_9_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_840 = ~skipZeros ? noLoopReg_10_valid : noLoopReg_9_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_841 = ~skipZeros ? noLoopReg_11_mantissa : noLoopReg_10_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_842 = ~skipZeros ? $signed(noLoopReg_11_exponent) : $signed(noLoopReg_10_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_843 = ~skipZeros ? noLoopReg_11_sign : noLoopReg_10_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_844 = ~skipZeros ? noLoopReg_11_valid : noLoopReg_10_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_845 = ~skipZeros ? noLoopReg_12_mantissa : noLoopReg_11_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_846 = ~skipZeros ? $signed(noLoopReg_12_exponent) : $signed(noLoopReg_11_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_847 = ~skipZeros ? noLoopReg_12_sign : noLoopReg_11_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_848 = ~skipZeros ? noLoopReg_12_valid : noLoopReg_11_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_849 = ~skipZeros ? noLoopReg_13_mantissa : noLoopReg_12_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_850 = ~skipZeros ? $signed(noLoopReg_13_exponent) : $signed(noLoopReg_12_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_851 = ~skipZeros ? noLoopReg_13_sign : noLoopReg_12_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_852 = ~skipZeros ? noLoopReg_13_valid : noLoopReg_12_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_853 = ~skipZeros ? noLoopReg_14_mantissa : noLoopReg_13_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_854 = ~skipZeros ? $signed(noLoopReg_14_exponent) : $signed(noLoopReg_13_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_855 = ~skipZeros ? noLoopReg_14_sign : noLoopReg_13_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_856 = ~skipZeros ? noLoopReg_14_valid : noLoopReg_13_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_857 = ~skipZeros ? noLoopReg_15_mantissa : noLoopReg_14_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_858 = ~skipZeros ? $signed(noLoopReg_15_exponent) : $signed(noLoopReg_14_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_859 = ~skipZeros ? noLoopReg_15_sign : noLoopReg_14_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_860 = ~skipZeros ? noLoopReg_15_valid : noLoopReg_14_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_861 = ~skipZeros ? noLoopReg_16_mantissa : noLoopReg_15_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_862 = ~skipZeros ? $signed(noLoopReg_16_exponent) : $signed(noLoopReg_15_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_863 = ~skipZeros ? noLoopReg_16_sign : noLoopReg_15_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_864 = ~skipZeros ? noLoopReg_16_valid : noLoopReg_15_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_865 = ~skipZeros ? noLoopReg_17_mantissa : noLoopReg_16_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_866 = ~skipZeros ? $signed(noLoopReg_17_exponent) : $signed(noLoopReg_16_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_867 = ~skipZeros ? noLoopReg_17_sign : noLoopReg_16_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_868 = ~skipZeros ? noLoopReg_17_valid : noLoopReg_16_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_869 = ~skipZeros ? noLoopReg_18_mantissa : noLoopReg_17_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_870 = ~skipZeros ? $signed(noLoopReg_18_exponent) : $signed(noLoopReg_17_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_871 = ~skipZeros ? noLoopReg_18_sign : noLoopReg_17_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_872 = ~skipZeros ? noLoopReg_18_valid : noLoopReg_17_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_873 = ~skipZeros ? noLoopReg_19_mantissa : noLoopReg_18_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_874 = ~skipZeros ? $signed(noLoopReg_19_exponent) : $signed(noLoopReg_18_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_875 = ~skipZeros ? noLoopReg_19_sign : noLoopReg_18_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_876 = ~skipZeros ? noLoopReg_19_valid : noLoopReg_18_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_877 = ~skipZeros ? noLoopReg_20_mantissa : noLoopReg_19_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_878 = ~skipZeros ? $signed(noLoopReg_20_exponent) : $signed(noLoopReg_19_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_879 = ~skipZeros ? noLoopReg_20_sign : noLoopReg_19_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_880 = ~skipZeros ? noLoopReg_20_valid : noLoopReg_19_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_881 = ~skipZeros ? noLoopReg_21_mantissa : noLoopReg_20_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_882 = ~skipZeros ? $signed(noLoopReg_21_exponent) : $signed(noLoopReg_20_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_883 = ~skipZeros ? noLoopReg_21_sign : noLoopReg_20_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_884 = ~skipZeros ? noLoopReg_21_valid : noLoopReg_20_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_885 = ~skipZeros ? noLoopReg_22_mantissa : noLoopReg_21_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_886 = ~skipZeros ? $signed(noLoopReg_22_exponent) : $signed(noLoopReg_21_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_887 = ~skipZeros ? noLoopReg_22_sign : noLoopReg_21_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_888 = ~skipZeros ? noLoopReg_22_valid : noLoopReg_21_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_889 = ~skipZeros ? noLoopReg_23_mantissa : noLoopReg_22_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_890 = ~skipZeros ? $signed(noLoopReg_23_exponent) : $signed(noLoopReg_22_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_891 = ~skipZeros ? noLoopReg_23_sign : noLoopReg_22_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_892 = ~skipZeros ? noLoopReg_23_valid : noLoopReg_22_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_893 = ~skipZeros ? noLoopReg_24_mantissa : noLoopReg_23_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_894 = ~skipZeros ? $signed(noLoopReg_24_exponent) : $signed(noLoopReg_23_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_895 = ~skipZeros ? noLoopReg_24_sign : noLoopReg_23_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_896 = ~skipZeros ? noLoopReg_24_valid : noLoopReg_23_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_897 = ~skipZeros ? noLoopReg_25_mantissa : noLoopReg_24_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_898 = ~skipZeros ? $signed(noLoopReg_25_exponent) : $signed(noLoopReg_24_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_899 = ~skipZeros ? noLoopReg_25_sign : noLoopReg_24_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_900 = ~skipZeros ? noLoopReg_25_valid : noLoopReg_24_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_901 = ~skipZeros ? noLoopReg_26_mantissa : noLoopReg_25_mantissa; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [7:0] _GEN_902 = ~skipZeros ? $signed(noLoopReg_26_exponent) : $signed(noLoopReg_25_exponent); // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_903 = ~skipZeros ? noLoopReg_26_sign : noLoopReg_25_sign; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire  _GEN_904 = ~skipZeros ? noLoopReg_26_valid : noLoopReg_25_valid; // @[SPFPSQRT.scala 293:22 122:26 376:24]
  wire [25:0] _GEN_1013 = maxExp ? noLoopReg_26_mantissa : _GEN_794; // @[SPFPSQRT.scala 209:23]
  wire [7:0] _GEN_1014 = maxExp ? $signed(noLoopReg_26_exponent) : $signed(_GEN_795); // @[SPFPSQRT.scala 209:23]
  wire  _GEN_1015 = maxExp ? noLoopReg_26_sign : _GEN_796; // @[SPFPSQRT.scala 209:23]
  wire  _GEN_1020 = maxExp & _GEN_361; // @[SPFPSQRT.scala 209:23 38:27]
  wire [25:0] _GEN_1401 = zeroMant & _T_1 ? noLoopReg_26_mantissa : _GEN_1013; // @[SPFPSQRT.scala 194:41 206:33]
  wire [7:0] _GEN_1402 = zeroMant & _T_1 ? $signed(noLoopReg_26_exponent) : $signed(_GEN_1014); // @[SPFPSQRT.scala 194:41 207:33]
  wire  _GEN_1403 = zeroMant & _T_1 ? noLoopReg_26_sign : _GEN_1015; // @[SPFPSQRT.scala 194:41 208:33]
  wire  _GEN_1408 = zeroMant & _T_1 ? 1'h0 : _GEN_1020; // @[SPFPSQRT.scala 194:41 38:27]
  wire  _io_out_bits_sqrtInProc_T_51 = loopReg_0_valid | loopReg_1_valid | loopReg_2_valid | loopReg_3_valid |
    loopReg_4_valid | loopReg_5_valid | loopReg_6_valid | loopReg_7_valid | loopReg_8_valid | loopReg_9_valid |
    loopReg_10_valid | loopReg_11_valid | loopReg_12_valid | loopReg_13_valid | loopReg_14_valid | loopReg_15_valid |
    loopReg_16_valid | loopReg_17_valid | loopReg_18_valid | loopReg_19_valid | loopReg_20_valid | loopReg_21_valid |
    loopReg_22_valid | loopReg_23_valid | loopReg_24_valid | loopReg_25_valid | loopReg_26_valid; // @[SPFPSQRT.scala 393:98]
  assign io_out_valid = noLoopReg_26_valid | loopReg_26_valid; // @[SPFPSQRT.scala 388:47]
  assign io_out_bits_result_mantissa = io_in_bits_a_sign ? noLoopReg_26_mantissa : _GEN_1401; // @[SPFPSQRT.scala 160:23]
  assign io_out_bits_result_exponent = io_in_bits_a_sign ? $signed(noLoopReg_26_exponent) : $signed(_GEN_1402); // @[SPFPSQRT.scala 160:23]
  assign io_out_bits_result_sign = io_in_bits_a_sign ? noLoopReg_26_sign : _GEN_1403; // @[SPFPSQRT.scala 160:23]
  assign io_out_bits_fcsr_4 = io_in_bits_a_sign | _GEN_1408; // @[SPFPSQRT.scala 160:23]
  assign io_out_bits_op = opReg_26; // @[SPFPSQRT.scala 385:18]
  assign io_out_bits_sqrtInProc = noLoopReg_0_valid | noLoopReg_1_valid | noLoopReg_2_valid | noLoopReg_3_valid |
    noLoopReg_4_valid | noLoopReg_5_valid | noLoopReg_6_valid | noLoopReg_7_valid | noLoopReg_8_valid |
    noLoopReg_9_valid | noLoopReg_10_valid | noLoopReg_11_valid | noLoopReg_12_valid | noLoopReg_13_valid |
    noLoopReg_14_valid | noLoopReg_15_valid | noLoopReg_16_valid | noLoopReg_17_valid | noLoopReg_18_valid |
    noLoopReg_19_valid | noLoopReg_20_valid | noLoopReg_21_valid | noLoopReg_22_valid | noLoopReg_23_valid |
    noLoopReg_24_valid | noLoopReg_25_valid | noLoopReg_26_valid | _io_out_bits_sqrtInProc_T_51; // @[SPFPSQRT.scala 393:66]
  always @(posedge clock) begin
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_0 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_0 <= io_in_bits_op; // @[SPFPSQRT.scala 61:12]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_1 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_1 <= opReg_0; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_2 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_2 <= opReg_1; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_3 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_3 <= opReg_2; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_4 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_4 <= opReg_3; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_5 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_5 <= opReg_4; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_6 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_6 <= opReg_5; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_7 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_7 <= opReg_6; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_8 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_8 <= opReg_7; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_9 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_9 <= opReg_8; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_10 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_10 <= opReg_9; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_11 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_11 <= opReg_10; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_12 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_12 <= opReg_11; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_13 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_13 <= opReg_12; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_14 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_14 <= opReg_13; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_15 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_15 <= opReg_14; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_16 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_16 <= opReg_15; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_17 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_17 <= opReg_16; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_18 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_18 <= opReg_17; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_19 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_19 <= opReg_18; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_20 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_20 <= opReg_19; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_21 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_21 <= opReg_20; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_22 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_22 <= opReg_21; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_23 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_23 <= opReg_22; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_24 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_24 <= opReg_23; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_25 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_25 <= opReg_24; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 60:22]
      opReg_26 <= 5'h0; // @[SPFPSQRT.scala 60:22]
    end else begin
      opReg_26 <= opReg_25; // @[SPFPSQRT.scala 63:16]
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_0_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_0_nX <= _GEN_528;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_0_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_0_nY <= _GEN_529;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_0_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_0_valid <= _GEN_530;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_0_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_0_sqrtExp <= _GEN_531;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_1_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_1_nX <= _GEN_538;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_1_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_1_nY <= _GEN_539;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_1_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_1_valid <= _GEN_542;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_1_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_1_sqrtExp <= _GEN_543;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_2_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_2_nX <= _GEN_548;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_2_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_2_nY <= _GEN_549;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_2_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_2_valid <= _GEN_552;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_2_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_2_sqrtExp <= _GEN_553;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_3_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_3_nX <= _GEN_558;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_3_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_3_nY <= _GEN_559;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_3_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_3_valid <= _GEN_562;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_3_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_3_sqrtExp <= _GEN_563;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_4_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_4_nX <= _GEN_568;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_4_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_4_nY <= _GEN_569;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_4_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_4_valid <= _GEN_572;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_4_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_4_sqrtExp <= _GEN_573;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_5_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_5_nX <= _GEN_578;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_5_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_5_nY <= _GEN_579;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_5_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_5_valid <= _GEN_582;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_5_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_5_sqrtExp <= _GEN_583;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_6_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_6_nX <= _GEN_588;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_6_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_6_nY <= _GEN_589;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_6_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_6_valid <= _GEN_592;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_6_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_6_sqrtExp <= _GEN_593;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_7_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_7_nX <= _GEN_598;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_7_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_7_nY <= _GEN_599;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_7_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_7_valid <= _GEN_602;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_7_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_7_sqrtExp <= _GEN_603;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_8_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_8_nX <= _GEN_608;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_8_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_8_nY <= _GEN_609;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_8_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_8_valid <= _GEN_612;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_8_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_8_sqrtExp <= _GEN_613;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_9_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_9_nX <= _GEN_618;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_9_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_9_nY <= _GEN_619;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_9_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_9_valid <= _GEN_622;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_9_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_9_sqrtExp <= _GEN_623;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_10_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_10_nX <= _GEN_628;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_10_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_10_nY <= _GEN_629;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_10_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_10_valid <= _GEN_632;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_10_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_10_sqrtExp <= _GEN_633;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_11_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_11_nX <= _GEN_638;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_11_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_11_nY <= _GEN_639;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_11_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_11_valid <= _GEN_642;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_11_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_11_sqrtExp <= _GEN_643;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_12_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_12_nX <= _GEN_648;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_12_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_12_nY <= _GEN_649;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_12_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_12_valid <= _GEN_652;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_12_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_12_sqrtExp <= _GEN_653;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_13_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_13_nX <= _GEN_658;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_13_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_13_nY <= _GEN_659;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_13_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_13_valid <= _GEN_662;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_13_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_13_sqrtExp <= _GEN_663;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_14_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_14_nX <= _GEN_668;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_14_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_14_nY <= _GEN_669;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_14_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_14_valid <= _GEN_672;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_14_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_14_sqrtExp <= _GEN_673;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_15_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_15_nX <= _GEN_678;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_15_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_15_nY <= _GEN_679;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_15_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_15_valid <= _GEN_682;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_15_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_15_sqrtExp <= _GEN_683;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_16_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_16_nX <= _GEN_688;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_16_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_16_nY <= _GEN_689;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_16_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_16_valid <= _GEN_692;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_16_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_16_sqrtExp <= _GEN_693;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_17_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_17_nX <= _GEN_698;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_17_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_17_nY <= _GEN_699;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_17_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_17_valid <= _GEN_702;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_17_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_17_sqrtExp <= _GEN_703;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_18_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_18_nX <= _GEN_708;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_18_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_18_nY <= _GEN_709;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_18_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_18_valid <= _GEN_712;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_18_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_18_sqrtExp <= _GEN_713;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_19_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_19_nX <= _GEN_718;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_19_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_19_nY <= _GEN_719;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_19_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_19_valid <= _GEN_722;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_19_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_19_sqrtExp <= _GEN_723;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_20_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_20_nX <= _GEN_728;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_20_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_20_nY <= _GEN_729;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_20_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_20_valid <= _GEN_732;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_20_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_20_sqrtExp <= _GEN_733;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_21_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_21_nX <= _GEN_738;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_21_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_21_nY <= _GEN_739;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_21_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_21_valid <= _GEN_742;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_21_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_21_sqrtExp <= _GEN_743;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_22_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_22_nX <= _GEN_748;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_22_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_22_nY <= _GEN_749;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_22_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_22_valid <= _GEN_752;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_22_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_22_sqrtExp <= _GEN_753;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_23_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_23_nX <= _GEN_758;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_23_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_23_nY <= _GEN_759;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_23_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_23_valid <= _GEN_762;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_23_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_23_sqrtExp <= _GEN_763;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_24_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_24_nX <= _GEN_768;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_24_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_24_nY <= _GEN_769;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_24_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_24_valid <= _GEN_772;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_24_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_24_sqrtExp <= _GEN_773;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_25_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_25_nX <= _GEN_778;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_25_nY <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_25_nY <= _GEN_779;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_25_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_25_valid <= _GEN_782;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_25_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_25_sqrtExp <= _GEN_783;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_26_nX <= 38'sh0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_26_nX <= _GEN_788;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_26_valid <= 1'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_26_valid <= _GEN_792;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 121:24]
      loopReg_26_sqrtExp <= 8'h0; // @[SPFPSQRT.scala 121:24]
    end else if (!(io_in_bits_a_sign)) begin // @[SPFPSQRT.scala 160:23]
      if (!(zeroMant & _T_1)) begin // @[SPFPSQRT.scala 194:41]
        if (!(maxExp)) begin // @[SPFPSQRT.scala 209:23]
          loopReg_26_sqrtExp <= _GEN_793;
        end
      end
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_0_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_0_mantissa <= {{3'd0}, _GEN_0};
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_0_mantissa <= 26'h0; // @[SPFPSQRT.scala 197:27]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_0_mantissa <= {{3'd0}, _GEN_246};
    end else begin
      noLoopReg_0_mantissa <= _GEN_797;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_0_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      if ($signed(io_in_bits_a_exponent) == 8'sh0 & zeroMant) begin // @[SPFPSQRT.scala 161:36]
        noLoopReg_0_exponent <= 8'sh0; // @[SPFPSQRT.scala 165:29]
      end else begin
        noLoopReg_0_exponent <= -8'sh1; // @[SPFPSQRT.scala 181:29]
      end
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_0_exponent <= 8'sh0; // @[SPFPSQRT.scala 198:27]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_0_exponent <= -8'sh1;
    end else begin
      noLoopReg_0_exponent <= _GEN_798;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_0_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_0_sign <= _T_2;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_0_sign <= 1'h0; // @[SPFPSQRT.scala 199:27]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_0_sign <= 1'h0;
    end else begin
      noLoopReg_0_sign <= _GEN_799;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_0_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_0_valid <= io_in_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_0_valid <= io_in_valid; // @[SPFPSQRT.scala 200:27]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_0_valid <= io_in_valid;
    end else begin
      noLoopReg_0_valid <= _GEN_800;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_1_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_1_mantissa <= noLoopReg_0_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_1_mantissa <= noLoopReg_0_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_1_mantissa <= noLoopReg_0_mantissa;
    end else begin
      noLoopReg_1_mantissa <= _GEN_801;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_1_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_1_exponent <= noLoopReg_0_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_1_exponent <= noLoopReg_0_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_1_exponent <= noLoopReg_0_exponent;
    end else begin
      noLoopReg_1_exponent <= _GEN_802;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_1_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_1_sign <= noLoopReg_0_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_1_sign <= noLoopReg_0_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_1_sign <= noLoopReg_0_sign;
    end else begin
      noLoopReg_1_sign <= _GEN_803;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_1_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_1_valid <= noLoopReg_0_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_1_valid <= noLoopReg_0_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_1_valid <= noLoopReg_0_valid;
    end else begin
      noLoopReg_1_valid <= _GEN_804;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_2_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_2_mantissa <= noLoopReg_1_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_2_mantissa <= noLoopReg_1_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_2_mantissa <= noLoopReg_1_mantissa;
    end else begin
      noLoopReg_2_mantissa <= _GEN_805;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_2_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_2_exponent <= noLoopReg_1_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_2_exponent <= noLoopReg_1_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_2_exponent <= noLoopReg_1_exponent;
    end else begin
      noLoopReg_2_exponent <= _GEN_806;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_2_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_2_sign <= noLoopReg_1_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_2_sign <= noLoopReg_1_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_2_sign <= noLoopReg_1_sign;
    end else begin
      noLoopReg_2_sign <= _GEN_807;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_2_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_2_valid <= noLoopReg_1_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_2_valid <= noLoopReg_1_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_2_valid <= noLoopReg_1_valid;
    end else begin
      noLoopReg_2_valid <= _GEN_808;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_3_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_3_mantissa <= noLoopReg_2_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_3_mantissa <= noLoopReg_2_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_3_mantissa <= noLoopReg_2_mantissa;
    end else begin
      noLoopReg_3_mantissa <= _GEN_809;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_3_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_3_exponent <= noLoopReg_2_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_3_exponent <= noLoopReg_2_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_3_exponent <= noLoopReg_2_exponent;
    end else begin
      noLoopReg_3_exponent <= _GEN_810;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_3_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_3_sign <= noLoopReg_2_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_3_sign <= noLoopReg_2_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_3_sign <= noLoopReg_2_sign;
    end else begin
      noLoopReg_3_sign <= _GEN_811;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_3_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_3_valid <= noLoopReg_2_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_3_valid <= noLoopReg_2_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_3_valid <= noLoopReg_2_valid;
    end else begin
      noLoopReg_3_valid <= _GEN_812;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_4_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_4_mantissa <= noLoopReg_3_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_4_mantissa <= noLoopReg_3_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_4_mantissa <= noLoopReg_3_mantissa;
    end else begin
      noLoopReg_4_mantissa <= _GEN_813;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_4_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_4_exponent <= noLoopReg_3_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_4_exponent <= noLoopReg_3_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_4_exponent <= noLoopReg_3_exponent;
    end else begin
      noLoopReg_4_exponent <= _GEN_814;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_4_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_4_sign <= noLoopReg_3_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_4_sign <= noLoopReg_3_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_4_sign <= noLoopReg_3_sign;
    end else begin
      noLoopReg_4_sign <= _GEN_815;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_4_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_4_valid <= noLoopReg_3_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_4_valid <= noLoopReg_3_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_4_valid <= noLoopReg_3_valid;
    end else begin
      noLoopReg_4_valid <= _GEN_816;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_5_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_5_mantissa <= noLoopReg_4_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_5_mantissa <= noLoopReg_4_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_5_mantissa <= noLoopReg_4_mantissa;
    end else begin
      noLoopReg_5_mantissa <= _GEN_817;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_5_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_5_exponent <= noLoopReg_4_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_5_exponent <= noLoopReg_4_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_5_exponent <= noLoopReg_4_exponent;
    end else begin
      noLoopReg_5_exponent <= _GEN_818;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_5_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_5_sign <= noLoopReg_4_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_5_sign <= noLoopReg_4_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_5_sign <= noLoopReg_4_sign;
    end else begin
      noLoopReg_5_sign <= _GEN_819;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_5_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_5_valid <= noLoopReg_4_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_5_valid <= noLoopReg_4_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_5_valid <= noLoopReg_4_valid;
    end else begin
      noLoopReg_5_valid <= _GEN_820;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_6_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_6_mantissa <= noLoopReg_5_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_6_mantissa <= noLoopReg_5_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_6_mantissa <= noLoopReg_5_mantissa;
    end else begin
      noLoopReg_6_mantissa <= _GEN_821;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_6_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_6_exponent <= noLoopReg_5_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_6_exponent <= noLoopReg_5_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_6_exponent <= noLoopReg_5_exponent;
    end else begin
      noLoopReg_6_exponent <= _GEN_822;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_6_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_6_sign <= noLoopReg_5_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_6_sign <= noLoopReg_5_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_6_sign <= noLoopReg_5_sign;
    end else begin
      noLoopReg_6_sign <= _GEN_823;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_6_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_6_valid <= noLoopReg_5_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_6_valid <= noLoopReg_5_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_6_valid <= noLoopReg_5_valid;
    end else begin
      noLoopReg_6_valid <= _GEN_824;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_7_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_7_mantissa <= noLoopReg_6_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_7_mantissa <= noLoopReg_6_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_7_mantissa <= noLoopReg_6_mantissa;
    end else begin
      noLoopReg_7_mantissa <= _GEN_825;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_7_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_7_exponent <= noLoopReg_6_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_7_exponent <= noLoopReg_6_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_7_exponent <= noLoopReg_6_exponent;
    end else begin
      noLoopReg_7_exponent <= _GEN_826;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_7_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_7_sign <= noLoopReg_6_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_7_sign <= noLoopReg_6_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_7_sign <= noLoopReg_6_sign;
    end else begin
      noLoopReg_7_sign <= _GEN_827;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_7_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_7_valid <= noLoopReg_6_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_7_valid <= noLoopReg_6_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_7_valid <= noLoopReg_6_valid;
    end else begin
      noLoopReg_7_valid <= _GEN_828;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_8_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_8_mantissa <= noLoopReg_7_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_8_mantissa <= noLoopReg_7_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_8_mantissa <= noLoopReg_7_mantissa;
    end else begin
      noLoopReg_8_mantissa <= _GEN_829;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_8_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_8_exponent <= noLoopReg_7_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_8_exponent <= noLoopReg_7_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_8_exponent <= noLoopReg_7_exponent;
    end else begin
      noLoopReg_8_exponent <= _GEN_830;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_8_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_8_sign <= noLoopReg_7_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_8_sign <= noLoopReg_7_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_8_sign <= noLoopReg_7_sign;
    end else begin
      noLoopReg_8_sign <= _GEN_831;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_8_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_8_valid <= noLoopReg_7_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_8_valid <= noLoopReg_7_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_8_valid <= noLoopReg_7_valid;
    end else begin
      noLoopReg_8_valid <= _GEN_832;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_9_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_9_mantissa <= noLoopReg_8_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_9_mantissa <= noLoopReg_8_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_9_mantissa <= noLoopReg_8_mantissa;
    end else begin
      noLoopReg_9_mantissa <= _GEN_833;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_9_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_9_exponent <= noLoopReg_8_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_9_exponent <= noLoopReg_8_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_9_exponent <= noLoopReg_8_exponent;
    end else begin
      noLoopReg_9_exponent <= _GEN_834;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_9_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_9_sign <= noLoopReg_8_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_9_sign <= noLoopReg_8_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_9_sign <= noLoopReg_8_sign;
    end else begin
      noLoopReg_9_sign <= _GEN_835;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_9_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_9_valid <= noLoopReg_8_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_9_valid <= noLoopReg_8_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_9_valid <= noLoopReg_8_valid;
    end else begin
      noLoopReg_9_valid <= _GEN_836;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_10_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_10_mantissa <= noLoopReg_9_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_10_mantissa <= noLoopReg_9_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_10_mantissa <= noLoopReg_9_mantissa;
    end else begin
      noLoopReg_10_mantissa <= _GEN_837;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_10_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_10_exponent <= noLoopReg_9_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_10_exponent <= noLoopReg_9_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_10_exponent <= noLoopReg_9_exponent;
    end else begin
      noLoopReg_10_exponent <= _GEN_838;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_10_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_10_sign <= noLoopReg_9_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_10_sign <= noLoopReg_9_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_10_sign <= noLoopReg_9_sign;
    end else begin
      noLoopReg_10_sign <= _GEN_839;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_10_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_10_valid <= noLoopReg_9_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_10_valid <= noLoopReg_9_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_10_valid <= noLoopReg_9_valid;
    end else begin
      noLoopReg_10_valid <= _GEN_840;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_11_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_11_mantissa <= noLoopReg_10_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_11_mantissa <= noLoopReg_10_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_11_mantissa <= noLoopReg_10_mantissa;
    end else begin
      noLoopReg_11_mantissa <= _GEN_841;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_11_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_11_exponent <= noLoopReg_10_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_11_exponent <= noLoopReg_10_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_11_exponent <= noLoopReg_10_exponent;
    end else begin
      noLoopReg_11_exponent <= _GEN_842;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_11_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_11_sign <= noLoopReg_10_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_11_sign <= noLoopReg_10_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_11_sign <= noLoopReg_10_sign;
    end else begin
      noLoopReg_11_sign <= _GEN_843;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_11_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_11_valid <= noLoopReg_10_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_11_valid <= noLoopReg_10_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_11_valid <= noLoopReg_10_valid;
    end else begin
      noLoopReg_11_valid <= _GEN_844;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_12_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_12_mantissa <= noLoopReg_11_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_12_mantissa <= noLoopReg_11_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_12_mantissa <= noLoopReg_11_mantissa;
    end else begin
      noLoopReg_12_mantissa <= _GEN_845;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_12_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_12_exponent <= noLoopReg_11_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_12_exponent <= noLoopReg_11_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_12_exponent <= noLoopReg_11_exponent;
    end else begin
      noLoopReg_12_exponent <= _GEN_846;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_12_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_12_sign <= noLoopReg_11_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_12_sign <= noLoopReg_11_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_12_sign <= noLoopReg_11_sign;
    end else begin
      noLoopReg_12_sign <= _GEN_847;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_12_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_12_valid <= noLoopReg_11_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_12_valid <= noLoopReg_11_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_12_valid <= noLoopReg_11_valid;
    end else begin
      noLoopReg_12_valid <= _GEN_848;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_13_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_13_mantissa <= noLoopReg_12_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_13_mantissa <= noLoopReg_12_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_13_mantissa <= noLoopReg_12_mantissa;
    end else begin
      noLoopReg_13_mantissa <= _GEN_849;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_13_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_13_exponent <= noLoopReg_12_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_13_exponent <= noLoopReg_12_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_13_exponent <= noLoopReg_12_exponent;
    end else begin
      noLoopReg_13_exponent <= _GEN_850;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_13_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_13_sign <= noLoopReg_12_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_13_sign <= noLoopReg_12_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_13_sign <= noLoopReg_12_sign;
    end else begin
      noLoopReg_13_sign <= _GEN_851;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_13_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_13_valid <= noLoopReg_12_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_13_valid <= noLoopReg_12_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_13_valid <= noLoopReg_12_valid;
    end else begin
      noLoopReg_13_valid <= _GEN_852;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_14_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_14_mantissa <= noLoopReg_13_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_14_mantissa <= noLoopReg_13_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_14_mantissa <= noLoopReg_13_mantissa;
    end else begin
      noLoopReg_14_mantissa <= _GEN_853;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_14_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_14_exponent <= noLoopReg_13_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_14_exponent <= noLoopReg_13_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_14_exponent <= noLoopReg_13_exponent;
    end else begin
      noLoopReg_14_exponent <= _GEN_854;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_14_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_14_sign <= noLoopReg_13_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_14_sign <= noLoopReg_13_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_14_sign <= noLoopReg_13_sign;
    end else begin
      noLoopReg_14_sign <= _GEN_855;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_14_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_14_valid <= noLoopReg_13_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_14_valid <= noLoopReg_13_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_14_valid <= noLoopReg_13_valid;
    end else begin
      noLoopReg_14_valid <= _GEN_856;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_15_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_15_mantissa <= noLoopReg_14_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_15_mantissa <= noLoopReg_14_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_15_mantissa <= noLoopReg_14_mantissa;
    end else begin
      noLoopReg_15_mantissa <= _GEN_857;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_15_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_15_exponent <= noLoopReg_14_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_15_exponent <= noLoopReg_14_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_15_exponent <= noLoopReg_14_exponent;
    end else begin
      noLoopReg_15_exponent <= _GEN_858;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_15_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_15_sign <= noLoopReg_14_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_15_sign <= noLoopReg_14_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_15_sign <= noLoopReg_14_sign;
    end else begin
      noLoopReg_15_sign <= _GEN_859;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_15_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_15_valid <= noLoopReg_14_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_15_valid <= noLoopReg_14_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_15_valid <= noLoopReg_14_valid;
    end else begin
      noLoopReg_15_valid <= _GEN_860;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_16_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_16_mantissa <= noLoopReg_15_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_16_mantissa <= noLoopReg_15_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_16_mantissa <= noLoopReg_15_mantissa;
    end else begin
      noLoopReg_16_mantissa <= _GEN_861;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_16_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_16_exponent <= noLoopReg_15_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_16_exponent <= noLoopReg_15_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_16_exponent <= noLoopReg_15_exponent;
    end else begin
      noLoopReg_16_exponent <= _GEN_862;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_16_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_16_sign <= noLoopReg_15_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_16_sign <= noLoopReg_15_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_16_sign <= noLoopReg_15_sign;
    end else begin
      noLoopReg_16_sign <= _GEN_863;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_16_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_16_valid <= noLoopReg_15_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_16_valid <= noLoopReg_15_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_16_valid <= noLoopReg_15_valid;
    end else begin
      noLoopReg_16_valid <= _GEN_864;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_17_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_17_mantissa <= noLoopReg_16_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_17_mantissa <= noLoopReg_16_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_17_mantissa <= noLoopReg_16_mantissa;
    end else begin
      noLoopReg_17_mantissa <= _GEN_865;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_17_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_17_exponent <= noLoopReg_16_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_17_exponent <= noLoopReg_16_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_17_exponent <= noLoopReg_16_exponent;
    end else begin
      noLoopReg_17_exponent <= _GEN_866;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_17_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_17_sign <= noLoopReg_16_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_17_sign <= noLoopReg_16_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_17_sign <= noLoopReg_16_sign;
    end else begin
      noLoopReg_17_sign <= _GEN_867;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_17_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_17_valid <= noLoopReg_16_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_17_valid <= noLoopReg_16_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_17_valid <= noLoopReg_16_valid;
    end else begin
      noLoopReg_17_valid <= _GEN_868;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_18_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_18_mantissa <= noLoopReg_17_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_18_mantissa <= noLoopReg_17_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_18_mantissa <= noLoopReg_17_mantissa;
    end else begin
      noLoopReg_18_mantissa <= _GEN_869;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_18_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_18_exponent <= noLoopReg_17_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_18_exponent <= noLoopReg_17_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_18_exponent <= noLoopReg_17_exponent;
    end else begin
      noLoopReg_18_exponent <= _GEN_870;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_18_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_18_sign <= noLoopReg_17_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_18_sign <= noLoopReg_17_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_18_sign <= noLoopReg_17_sign;
    end else begin
      noLoopReg_18_sign <= _GEN_871;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_18_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_18_valid <= noLoopReg_17_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_18_valid <= noLoopReg_17_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_18_valid <= noLoopReg_17_valid;
    end else begin
      noLoopReg_18_valid <= _GEN_872;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_19_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_19_mantissa <= noLoopReg_18_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_19_mantissa <= noLoopReg_18_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_19_mantissa <= noLoopReg_18_mantissa;
    end else begin
      noLoopReg_19_mantissa <= _GEN_873;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_19_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_19_exponent <= noLoopReg_18_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_19_exponent <= noLoopReg_18_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_19_exponent <= noLoopReg_18_exponent;
    end else begin
      noLoopReg_19_exponent <= _GEN_874;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_19_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_19_sign <= noLoopReg_18_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_19_sign <= noLoopReg_18_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_19_sign <= noLoopReg_18_sign;
    end else begin
      noLoopReg_19_sign <= _GEN_875;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_19_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_19_valid <= noLoopReg_18_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_19_valid <= noLoopReg_18_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_19_valid <= noLoopReg_18_valid;
    end else begin
      noLoopReg_19_valid <= _GEN_876;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_20_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_20_mantissa <= noLoopReg_19_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_20_mantissa <= noLoopReg_19_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_20_mantissa <= noLoopReg_19_mantissa;
    end else begin
      noLoopReg_20_mantissa <= _GEN_877;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_20_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_20_exponent <= noLoopReg_19_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_20_exponent <= noLoopReg_19_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_20_exponent <= noLoopReg_19_exponent;
    end else begin
      noLoopReg_20_exponent <= _GEN_878;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_20_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_20_sign <= noLoopReg_19_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_20_sign <= noLoopReg_19_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_20_sign <= noLoopReg_19_sign;
    end else begin
      noLoopReg_20_sign <= _GEN_879;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_20_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_20_valid <= noLoopReg_19_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_20_valid <= noLoopReg_19_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_20_valid <= noLoopReg_19_valid;
    end else begin
      noLoopReg_20_valid <= _GEN_880;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_21_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_21_mantissa <= noLoopReg_20_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_21_mantissa <= noLoopReg_20_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_21_mantissa <= noLoopReg_20_mantissa;
    end else begin
      noLoopReg_21_mantissa <= _GEN_881;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_21_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_21_exponent <= noLoopReg_20_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_21_exponent <= noLoopReg_20_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_21_exponent <= noLoopReg_20_exponent;
    end else begin
      noLoopReg_21_exponent <= _GEN_882;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_21_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_21_sign <= noLoopReg_20_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_21_sign <= noLoopReg_20_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_21_sign <= noLoopReg_20_sign;
    end else begin
      noLoopReg_21_sign <= _GEN_883;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_21_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_21_valid <= noLoopReg_20_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_21_valid <= noLoopReg_20_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_21_valid <= noLoopReg_20_valid;
    end else begin
      noLoopReg_21_valid <= _GEN_884;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_22_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_22_mantissa <= noLoopReg_21_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_22_mantissa <= noLoopReg_21_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_22_mantissa <= noLoopReg_21_mantissa;
    end else begin
      noLoopReg_22_mantissa <= _GEN_885;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_22_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_22_exponent <= noLoopReg_21_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_22_exponent <= noLoopReg_21_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_22_exponent <= noLoopReg_21_exponent;
    end else begin
      noLoopReg_22_exponent <= _GEN_886;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_22_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_22_sign <= noLoopReg_21_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_22_sign <= noLoopReg_21_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_22_sign <= noLoopReg_21_sign;
    end else begin
      noLoopReg_22_sign <= _GEN_887;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_22_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_22_valid <= noLoopReg_21_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_22_valid <= noLoopReg_21_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_22_valid <= noLoopReg_21_valid;
    end else begin
      noLoopReg_22_valid <= _GEN_888;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_23_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_23_mantissa <= noLoopReg_22_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_23_mantissa <= noLoopReg_22_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_23_mantissa <= noLoopReg_22_mantissa;
    end else begin
      noLoopReg_23_mantissa <= _GEN_889;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_23_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_23_exponent <= noLoopReg_22_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_23_exponent <= noLoopReg_22_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_23_exponent <= noLoopReg_22_exponent;
    end else begin
      noLoopReg_23_exponent <= _GEN_890;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_23_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_23_sign <= noLoopReg_22_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_23_sign <= noLoopReg_22_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_23_sign <= noLoopReg_22_sign;
    end else begin
      noLoopReg_23_sign <= _GEN_891;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_23_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_23_valid <= noLoopReg_22_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_23_valid <= noLoopReg_22_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_23_valid <= noLoopReg_22_valid;
    end else begin
      noLoopReg_23_valid <= _GEN_892;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_24_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_24_mantissa <= noLoopReg_23_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_24_mantissa <= noLoopReg_23_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_24_mantissa <= noLoopReg_23_mantissa;
    end else begin
      noLoopReg_24_mantissa <= _GEN_893;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_24_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_24_exponent <= noLoopReg_23_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_24_exponent <= noLoopReg_23_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_24_exponent <= noLoopReg_23_exponent;
    end else begin
      noLoopReg_24_exponent <= _GEN_894;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_24_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_24_sign <= noLoopReg_23_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_24_sign <= noLoopReg_23_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_24_sign <= noLoopReg_23_sign;
    end else begin
      noLoopReg_24_sign <= _GEN_895;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_24_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_24_valid <= noLoopReg_23_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_24_valid <= noLoopReg_23_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_24_valid <= noLoopReg_23_valid;
    end else begin
      noLoopReg_24_valid <= _GEN_896;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_25_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_25_mantissa <= noLoopReg_24_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_25_mantissa <= noLoopReg_24_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_25_mantissa <= noLoopReg_24_mantissa;
    end else begin
      noLoopReg_25_mantissa <= _GEN_897;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_25_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_25_exponent <= noLoopReg_24_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_25_exponent <= noLoopReg_24_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_25_exponent <= noLoopReg_24_exponent;
    end else begin
      noLoopReg_25_exponent <= _GEN_898;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_25_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_25_sign <= noLoopReg_24_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_25_sign <= noLoopReg_24_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_25_sign <= noLoopReg_24_sign;
    end else begin
      noLoopReg_25_sign <= _GEN_899;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_25_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_25_valid <= noLoopReg_24_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_25_valid <= noLoopReg_24_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_25_valid <= noLoopReg_24_valid;
    end else begin
      noLoopReg_25_valid <= _GEN_900;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_26_mantissa <= 26'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_26_mantissa <= noLoopReg_25_mantissa;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_26_mantissa <= noLoopReg_25_mantissa; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_26_mantissa <= noLoopReg_25_mantissa;
    end else begin
      noLoopReg_26_mantissa <= _GEN_901;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_26_exponent <= 8'sh0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_26_exponent <= noLoopReg_25_exponent;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_26_exponent <= noLoopReg_25_exponent; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_26_exponent <= noLoopReg_25_exponent;
    end else begin
      noLoopReg_26_exponent <= _GEN_902;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_26_sign <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_26_sign <= noLoopReg_25_sign;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_26_sign <= noLoopReg_25_sign; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_26_sign <= noLoopReg_25_sign;
    end else begin
      noLoopReg_26_sign <= _GEN_903;
    end
    if (reset) begin // @[SPFPSQRT.scala 122:26]
      noLoopReg_26_valid <= 1'h0; // @[SPFPSQRT.scala 122:26]
    end else if (io_in_bits_a_sign) begin // @[SPFPSQRT.scala 160:23]
      noLoopReg_26_valid <= noLoopReg_25_valid;
    end else if (zeroMant & _T_1) begin // @[SPFPSQRT.scala 194:41]
      noLoopReg_26_valid <= noLoopReg_25_valid; // @[SPFPSQRT.scala 203:22]
    end else if (maxExp) begin // @[SPFPSQRT.scala 209:23]
      noLoopReg_26_valid <= noLoopReg_25_valid;
    end else begin
      noLoopReg_26_valid <= _GEN_904;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  opReg_0 = _RAND_0[4:0];
  _RAND_1 = {1{`RANDOM}};
  opReg_1 = _RAND_1[4:0];
  _RAND_2 = {1{`RANDOM}};
  opReg_2 = _RAND_2[4:0];
  _RAND_3 = {1{`RANDOM}};
  opReg_3 = _RAND_3[4:0];
  _RAND_4 = {1{`RANDOM}};
  opReg_4 = _RAND_4[4:0];
  _RAND_5 = {1{`RANDOM}};
  opReg_5 = _RAND_5[4:0];
  _RAND_6 = {1{`RANDOM}};
  opReg_6 = _RAND_6[4:0];
  _RAND_7 = {1{`RANDOM}};
  opReg_7 = _RAND_7[4:0];
  _RAND_8 = {1{`RANDOM}};
  opReg_8 = _RAND_8[4:0];
  _RAND_9 = {1{`RANDOM}};
  opReg_9 = _RAND_9[4:0];
  _RAND_10 = {1{`RANDOM}};
  opReg_10 = _RAND_10[4:0];
  _RAND_11 = {1{`RANDOM}};
  opReg_11 = _RAND_11[4:0];
  _RAND_12 = {1{`RANDOM}};
  opReg_12 = _RAND_12[4:0];
  _RAND_13 = {1{`RANDOM}};
  opReg_13 = _RAND_13[4:0];
  _RAND_14 = {1{`RANDOM}};
  opReg_14 = _RAND_14[4:0];
  _RAND_15 = {1{`RANDOM}};
  opReg_15 = _RAND_15[4:0];
  _RAND_16 = {1{`RANDOM}};
  opReg_16 = _RAND_16[4:0];
  _RAND_17 = {1{`RANDOM}};
  opReg_17 = _RAND_17[4:0];
  _RAND_18 = {1{`RANDOM}};
  opReg_18 = _RAND_18[4:0];
  _RAND_19 = {1{`RANDOM}};
  opReg_19 = _RAND_19[4:0];
  _RAND_20 = {1{`RANDOM}};
  opReg_20 = _RAND_20[4:0];
  _RAND_21 = {1{`RANDOM}};
  opReg_21 = _RAND_21[4:0];
  _RAND_22 = {1{`RANDOM}};
  opReg_22 = _RAND_22[4:0];
  _RAND_23 = {1{`RANDOM}};
  opReg_23 = _RAND_23[4:0];
  _RAND_24 = {1{`RANDOM}};
  opReg_24 = _RAND_24[4:0];
  _RAND_25 = {1{`RANDOM}};
  opReg_25 = _RAND_25[4:0];
  _RAND_26 = {1{`RANDOM}};
  opReg_26 = _RAND_26[4:0];
  _RAND_27 = {2{`RANDOM}};
  loopReg_0_nX = _RAND_27[37:0];
  _RAND_28 = {2{`RANDOM}};
  loopReg_0_nY = _RAND_28[37:0];
  _RAND_29 = {1{`RANDOM}};
  loopReg_0_valid = _RAND_29[0:0];
  _RAND_30 = {1{`RANDOM}};
  loopReg_0_sqrtExp = _RAND_30[7:0];
  _RAND_31 = {2{`RANDOM}};
  loopReg_1_nX = _RAND_31[37:0];
  _RAND_32 = {2{`RANDOM}};
  loopReg_1_nY = _RAND_32[37:0];
  _RAND_33 = {1{`RANDOM}};
  loopReg_1_valid = _RAND_33[0:0];
  _RAND_34 = {1{`RANDOM}};
  loopReg_1_sqrtExp = _RAND_34[7:0];
  _RAND_35 = {2{`RANDOM}};
  loopReg_2_nX = _RAND_35[37:0];
  _RAND_36 = {2{`RANDOM}};
  loopReg_2_nY = _RAND_36[37:0];
  _RAND_37 = {1{`RANDOM}};
  loopReg_2_valid = _RAND_37[0:0];
  _RAND_38 = {1{`RANDOM}};
  loopReg_2_sqrtExp = _RAND_38[7:0];
  _RAND_39 = {2{`RANDOM}};
  loopReg_3_nX = _RAND_39[37:0];
  _RAND_40 = {2{`RANDOM}};
  loopReg_3_nY = _RAND_40[37:0];
  _RAND_41 = {1{`RANDOM}};
  loopReg_3_valid = _RAND_41[0:0];
  _RAND_42 = {1{`RANDOM}};
  loopReg_3_sqrtExp = _RAND_42[7:0];
  _RAND_43 = {2{`RANDOM}};
  loopReg_4_nX = _RAND_43[37:0];
  _RAND_44 = {2{`RANDOM}};
  loopReg_4_nY = _RAND_44[37:0];
  _RAND_45 = {1{`RANDOM}};
  loopReg_4_valid = _RAND_45[0:0];
  _RAND_46 = {1{`RANDOM}};
  loopReg_4_sqrtExp = _RAND_46[7:0];
  _RAND_47 = {2{`RANDOM}};
  loopReg_5_nX = _RAND_47[37:0];
  _RAND_48 = {2{`RANDOM}};
  loopReg_5_nY = _RAND_48[37:0];
  _RAND_49 = {1{`RANDOM}};
  loopReg_5_valid = _RAND_49[0:0];
  _RAND_50 = {1{`RANDOM}};
  loopReg_5_sqrtExp = _RAND_50[7:0];
  _RAND_51 = {2{`RANDOM}};
  loopReg_6_nX = _RAND_51[37:0];
  _RAND_52 = {2{`RANDOM}};
  loopReg_6_nY = _RAND_52[37:0];
  _RAND_53 = {1{`RANDOM}};
  loopReg_6_valid = _RAND_53[0:0];
  _RAND_54 = {1{`RANDOM}};
  loopReg_6_sqrtExp = _RAND_54[7:0];
  _RAND_55 = {2{`RANDOM}};
  loopReg_7_nX = _RAND_55[37:0];
  _RAND_56 = {2{`RANDOM}};
  loopReg_7_nY = _RAND_56[37:0];
  _RAND_57 = {1{`RANDOM}};
  loopReg_7_valid = _RAND_57[0:0];
  _RAND_58 = {1{`RANDOM}};
  loopReg_7_sqrtExp = _RAND_58[7:0];
  _RAND_59 = {2{`RANDOM}};
  loopReg_8_nX = _RAND_59[37:0];
  _RAND_60 = {2{`RANDOM}};
  loopReg_8_nY = _RAND_60[37:0];
  _RAND_61 = {1{`RANDOM}};
  loopReg_8_valid = _RAND_61[0:0];
  _RAND_62 = {1{`RANDOM}};
  loopReg_8_sqrtExp = _RAND_62[7:0];
  _RAND_63 = {2{`RANDOM}};
  loopReg_9_nX = _RAND_63[37:0];
  _RAND_64 = {2{`RANDOM}};
  loopReg_9_nY = _RAND_64[37:0];
  _RAND_65 = {1{`RANDOM}};
  loopReg_9_valid = _RAND_65[0:0];
  _RAND_66 = {1{`RANDOM}};
  loopReg_9_sqrtExp = _RAND_66[7:0];
  _RAND_67 = {2{`RANDOM}};
  loopReg_10_nX = _RAND_67[37:0];
  _RAND_68 = {2{`RANDOM}};
  loopReg_10_nY = _RAND_68[37:0];
  _RAND_69 = {1{`RANDOM}};
  loopReg_10_valid = _RAND_69[0:0];
  _RAND_70 = {1{`RANDOM}};
  loopReg_10_sqrtExp = _RAND_70[7:0];
  _RAND_71 = {2{`RANDOM}};
  loopReg_11_nX = _RAND_71[37:0];
  _RAND_72 = {2{`RANDOM}};
  loopReg_11_nY = _RAND_72[37:0];
  _RAND_73 = {1{`RANDOM}};
  loopReg_11_valid = _RAND_73[0:0];
  _RAND_74 = {1{`RANDOM}};
  loopReg_11_sqrtExp = _RAND_74[7:0];
  _RAND_75 = {2{`RANDOM}};
  loopReg_12_nX = _RAND_75[37:0];
  _RAND_76 = {2{`RANDOM}};
  loopReg_12_nY = _RAND_76[37:0];
  _RAND_77 = {1{`RANDOM}};
  loopReg_12_valid = _RAND_77[0:0];
  _RAND_78 = {1{`RANDOM}};
  loopReg_12_sqrtExp = _RAND_78[7:0];
  _RAND_79 = {2{`RANDOM}};
  loopReg_13_nX = _RAND_79[37:0];
  _RAND_80 = {2{`RANDOM}};
  loopReg_13_nY = _RAND_80[37:0];
  _RAND_81 = {1{`RANDOM}};
  loopReg_13_valid = _RAND_81[0:0];
  _RAND_82 = {1{`RANDOM}};
  loopReg_13_sqrtExp = _RAND_82[7:0];
  _RAND_83 = {2{`RANDOM}};
  loopReg_14_nX = _RAND_83[37:0];
  _RAND_84 = {2{`RANDOM}};
  loopReg_14_nY = _RAND_84[37:0];
  _RAND_85 = {1{`RANDOM}};
  loopReg_14_valid = _RAND_85[0:0];
  _RAND_86 = {1{`RANDOM}};
  loopReg_14_sqrtExp = _RAND_86[7:0];
  _RAND_87 = {2{`RANDOM}};
  loopReg_15_nX = _RAND_87[37:0];
  _RAND_88 = {2{`RANDOM}};
  loopReg_15_nY = _RAND_88[37:0];
  _RAND_89 = {1{`RANDOM}};
  loopReg_15_valid = _RAND_89[0:0];
  _RAND_90 = {1{`RANDOM}};
  loopReg_15_sqrtExp = _RAND_90[7:0];
  _RAND_91 = {2{`RANDOM}};
  loopReg_16_nX = _RAND_91[37:0];
  _RAND_92 = {2{`RANDOM}};
  loopReg_16_nY = _RAND_92[37:0];
  _RAND_93 = {1{`RANDOM}};
  loopReg_16_valid = _RAND_93[0:0];
  _RAND_94 = {1{`RANDOM}};
  loopReg_16_sqrtExp = _RAND_94[7:0];
  _RAND_95 = {2{`RANDOM}};
  loopReg_17_nX = _RAND_95[37:0];
  _RAND_96 = {2{`RANDOM}};
  loopReg_17_nY = _RAND_96[37:0];
  _RAND_97 = {1{`RANDOM}};
  loopReg_17_valid = _RAND_97[0:0];
  _RAND_98 = {1{`RANDOM}};
  loopReg_17_sqrtExp = _RAND_98[7:0];
  _RAND_99 = {2{`RANDOM}};
  loopReg_18_nX = _RAND_99[37:0];
  _RAND_100 = {2{`RANDOM}};
  loopReg_18_nY = _RAND_100[37:0];
  _RAND_101 = {1{`RANDOM}};
  loopReg_18_valid = _RAND_101[0:0];
  _RAND_102 = {1{`RANDOM}};
  loopReg_18_sqrtExp = _RAND_102[7:0];
  _RAND_103 = {2{`RANDOM}};
  loopReg_19_nX = _RAND_103[37:0];
  _RAND_104 = {2{`RANDOM}};
  loopReg_19_nY = _RAND_104[37:0];
  _RAND_105 = {1{`RANDOM}};
  loopReg_19_valid = _RAND_105[0:0];
  _RAND_106 = {1{`RANDOM}};
  loopReg_19_sqrtExp = _RAND_106[7:0];
  _RAND_107 = {2{`RANDOM}};
  loopReg_20_nX = _RAND_107[37:0];
  _RAND_108 = {2{`RANDOM}};
  loopReg_20_nY = _RAND_108[37:0];
  _RAND_109 = {1{`RANDOM}};
  loopReg_20_valid = _RAND_109[0:0];
  _RAND_110 = {1{`RANDOM}};
  loopReg_20_sqrtExp = _RAND_110[7:0];
  _RAND_111 = {2{`RANDOM}};
  loopReg_21_nX = _RAND_111[37:0];
  _RAND_112 = {2{`RANDOM}};
  loopReg_21_nY = _RAND_112[37:0];
  _RAND_113 = {1{`RANDOM}};
  loopReg_21_valid = _RAND_113[0:0];
  _RAND_114 = {1{`RANDOM}};
  loopReg_21_sqrtExp = _RAND_114[7:0];
  _RAND_115 = {2{`RANDOM}};
  loopReg_22_nX = _RAND_115[37:0];
  _RAND_116 = {2{`RANDOM}};
  loopReg_22_nY = _RAND_116[37:0];
  _RAND_117 = {1{`RANDOM}};
  loopReg_22_valid = _RAND_117[0:0];
  _RAND_118 = {1{`RANDOM}};
  loopReg_22_sqrtExp = _RAND_118[7:0];
  _RAND_119 = {2{`RANDOM}};
  loopReg_23_nX = _RAND_119[37:0];
  _RAND_120 = {2{`RANDOM}};
  loopReg_23_nY = _RAND_120[37:0];
  _RAND_121 = {1{`RANDOM}};
  loopReg_23_valid = _RAND_121[0:0];
  _RAND_122 = {1{`RANDOM}};
  loopReg_23_sqrtExp = _RAND_122[7:0];
  _RAND_123 = {2{`RANDOM}};
  loopReg_24_nX = _RAND_123[37:0];
  _RAND_124 = {2{`RANDOM}};
  loopReg_24_nY = _RAND_124[37:0];
  _RAND_125 = {1{`RANDOM}};
  loopReg_24_valid = _RAND_125[0:0];
  _RAND_126 = {1{`RANDOM}};
  loopReg_24_sqrtExp = _RAND_126[7:0];
  _RAND_127 = {2{`RANDOM}};
  loopReg_25_nX = _RAND_127[37:0];
  _RAND_128 = {2{`RANDOM}};
  loopReg_25_nY = _RAND_128[37:0];
  _RAND_129 = {1{`RANDOM}};
  loopReg_25_valid = _RAND_129[0:0];
  _RAND_130 = {1{`RANDOM}};
  loopReg_25_sqrtExp = _RAND_130[7:0];
  _RAND_131 = {2{`RANDOM}};
  loopReg_26_nX = _RAND_131[37:0];
  _RAND_132 = {1{`RANDOM}};
  loopReg_26_valid = _RAND_132[0:0];
  _RAND_133 = {1{`RANDOM}};
  loopReg_26_sqrtExp = _RAND_133[7:0];
  _RAND_134 = {1{`RANDOM}};
  noLoopReg_0_mantissa = _RAND_134[25:0];
  _RAND_135 = {1{`RANDOM}};
  noLoopReg_0_exponent = _RAND_135[7:0];
  _RAND_136 = {1{`RANDOM}};
  noLoopReg_0_sign = _RAND_136[0:0];
  _RAND_137 = {1{`RANDOM}};
  noLoopReg_0_valid = _RAND_137[0:0];
  _RAND_138 = {1{`RANDOM}};
  noLoopReg_1_mantissa = _RAND_138[25:0];
  _RAND_139 = {1{`RANDOM}};
  noLoopReg_1_exponent = _RAND_139[7:0];
  _RAND_140 = {1{`RANDOM}};
  noLoopReg_1_sign = _RAND_140[0:0];
  _RAND_141 = {1{`RANDOM}};
  noLoopReg_1_valid = _RAND_141[0:0];
  _RAND_142 = {1{`RANDOM}};
  noLoopReg_2_mantissa = _RAND_142[25:0];
  _RAND_143 = {1{`RANDOM}};
  noLoopReg_2_exponent = _RAND_143[7:0];
  _RAND_144 = {1{`RANDOM}};
  noLoopReg_2_sign = _RAND_144[0:0];
  _RAND_145 = {1{`RANDOM}};
  noLoopReg_2_valid = _RAND_145[0:0];
  _RAND_146 = {1{`RANDOM}};
  noLoopReg_3_mantissa = _RAND_146[25:0];
  _RAND_147 = {1{`RANDOM}};
  noLoopReg_3_exponent = _RAND_147[7:0];
  _RAND_148 = {1{`RANDOM}};
  noLoopReg_3_sign = _RAND_148[0:0];
  _RAND_149 = {1{`RANDOM}};
  noLoopReg_3_valid = _RAND_149[0:0];
  _RAND_150 = {1{`RANDOM}};
  noLoopReg_4_mantissa = _RAND_150[25:0];
  _RAND_151 = {1{`RANDOM}};
  noLoopReg_4_exponent = _RAND_151[7:0];
  _RAND_152 = {1{`RANDOM}};
  noLoopReg_4_sign = _RAND_152[0:0];
  _RAND_153 = {1{`RANDOM}};
  noLoopReg_4_valid = _RAND_153[0:0];
  _RAND_154 = {1{`RANDOM}};
  noLoopReg_5_mantissa = _RAND_154[25:0];
  _RAND_155 = {1{`RANDOM}};
  noLoopReg_5_exponent = _RAND_155[7:0];
  _RAND_156 = {1{`RANDOM}};
  noLoopReg_5_sign = _RAND_156[0:0];
  _RAND_157 = {1{`RANDOM}};
  noLoopReg_5_valid = _RAND_157[0:0];
  _RAND_158 = {1{`RANDOM}};
  noLoopReg_6_mantissa = _RAND_158[25:0];
  _RAND_159 = {1{`RANDOM}};
  noLoopReg_6_exponent = _RAND_159[7:0];
  _RAND_160 = {1{`RANDOM}};
  noLoopReg_6_sign = _RAND_160[0:0];
  _RAND_161 = {1{`RANDOM}};
  noLoopReg_6_valid = _RAND_161[0:0];
  _RAND_162 = {1{`RANDOM}};
  noLoopReg_7_mantissa = _RAND_162[25:0];
  _RAND_163 = {1{`RANDOM}};
  noLoopReg_7_exponent = _RAND_163[7:0];
  _RAND_164 = {1{`RANDOM}};
  noLoopReg_7_sign = _RAND_164[0:0];
  _RAND_165 = {1{`RANDOM}};
  noLoopReg_7_valid = _RAND_165[0:0];
  _RAND_166 = {1{`RANDOM}};
  noLoopReg_8_mantissa = _RAND_166[25:0];
  _RAND_167 = {1{`RANDOM}};
  noLoopReg_8_exponent = _RAND_167[7:0];
  _RAND_168 = {1{`RANDOM}};
  noLoopReg_8_sign = _RAND_168[0:0];
  _RAND_169 = {1{`RANDOM}};
  noLoopReg_8_valid = _RAND_169[0:0];
  _RAND_170 = {1{`RANDOM}};
  noLoopReg_9_mantissa = _RAND_170[25:0];
  _RAND_171 = {1{`RANDOM}};
  noLoopReg_9_exponent = _RAND_171[7:0];
  _RAND_172 = {1{`RANDOM}};
  noLoopReg_9_sign = _RAND_172[0:0];
  _RAND_173 = {1{`RANDOM}};
  noLoopReg_9_valid = _RAND_173[0:0];
  _RAND_174 = {1{`RANDOM}};
  noLoopReg_10_mantissa = _RAND_174[25:0];
  _RAND_175 = {1{`RANDOM}};
  noLoopReg_10_exponent = _RAND_175[7:0];
  _RAND_176 = {1{`RANDOM}};
  noLoopReg_10_sign = _RAND_176[0:0];
  _RAND_177 = {1{`RANDOM}};
  noLoopReg_10_valid = _RAND_177[0:0];
  _RAND_178 = {1{`RANDOM}};
  noLoopReg_11_mantissa = _RAND_178[25:0];
  _RAND_179 = {1{`RANDOM}};
  noLoopReg_11_exponent = _RAND_179[7:0];
  _RAND_180 = {1{`RANDOM}};
  noLoopReg_11_sign = _RAND_180[0:0];
  _RAND_181 = {1{`RANDOM}};
  noLoopReg_11_valid = _RAND_181[0:0];
  _RAND_182 = {1{`RANDOM}};
  noLoopReg_12_mantissa = _RAND_182[25:0];
  _RAND_183 = {1{`RANDOM}};
  noLoopReg_12_exponent = _RAND_183[7:0];
  _RAND_184 = {1{`RANDOM}};
  noLoopReg_12_sign = _RAND_184[0:0];
  _RAND_185 = {1{`RANDOM}};
  noLoopReg_12_valid = _RAND_185[0:0];
  _RAND_186 = {1{`RANDOM}};
  noLoopReg_13_mantissa = _RAND_186[25:0];
  _RAND_187 = {1{`RANDOM}};
  noLoopReg_13_exponent = _RAND_187[7:0];
  _RAND_188 = {1{`RANDOM}};
  noLoopReg_13_sign = _RAND_188[0:0];
  _RAND_189 = {1{`RANDOM}};
  noLoopReg_13_valid = _RAND_189[0:0];
  _RAND_190 = {1{`RANDOM}};
  noLoopReg_14_mantissa = _RAND_190[25:0];
  _RAND_191 = {1{`RANDOM}};
  noLoopReg_14_exponent = _RAND_191[7:0];
  _RAND_192 = {1{`RANDOM}};
  noLoopReg_14_sign = _RAND_192[0:0];
  _RAND_193 = {1{`RANDOM}};
  noLoopReg_14_valid = _RAND_193[0:0];
  _RAND_194 = {1{`RANDOM}};
  noLoopReg_15_mantissa = _RAND_194[25:0];
  _RAND_195 = {1{`RANDOM}};
  noLoopReg_15_exponent = _RAND_195[7:0];
  _RAND_196 = {1{`RANDOM}};
  noLoopReg_15_sign = _RAND_196[0:0];
  _RAND_197 = {1{`RANDOM}};
  noLoopReg_15_valid = _RAND_197[0:0];
  _RAND_198 = {1{`RANDOM}};
  noLoopReg_16_mantissa = _RAND_198[25:0];
  _RAND_199 = {1{`RANDOM}};
  noLoopReg_16_exponent = _RAND_199[7:0];
  _RAND_200 = {1{`RANDOM}};
  noLoopReg_16_sign = _RAND_200[0:0];
  _RAND_201 = {1{`RANDOM}};
  noLoopReg_16_valid = _RAND_201[0:0];
  _RAND_202 = {1{`RANDOM}};
  noLoopReg_17_mantissa = _RAND_202[25:0];
  _RAND_203 = {1{`RANDOM}};
  noLoopReg_17_exponent = _RAND_203[7:0];
  _RAND_204 = {1{`RANDOM}};
  noLoopReg_17_sign = _RAND_204[0:0];
  _RAND_205 = {1{`RANDOM}};
  noLoopReg_17_valid = _RAND_205[0:0];
  _RAND_206 = {1{`RANDOM}};
  noLoopReg_18_mantissa = _RAND_206[25:0];
  _RAND_207 = {1{`RANDOM}};
  noLoopReg_18_exponent = _RAND_207[7:0];
  _RAND_208 = {1{`RANDOM}};
  noLoopReg_18_sign = _RAND_208[0:0];
  _RAND_209 = {1{`RANDOM}};
  noLoopReg_18_valid = _RAND_209[0:0];
  _RAND_210 = {1{`RANDOM}};
  noLoopReg_19_mantissa = _RAND_210[25:0];
  _RAND_211 = {1{`RANDOM}};
  noLoopReg_19_exponent = _RAND_211[7:0];
  _RAND_212 = {1{`RANDOM}};
  noLoopReg_19_sign = _RAND_212[0:0];
  _RAND_213 = {1{`RANDOM}};
  noLoopReg_19_valid = _RAND_213[0:0];
  _RAND_214 = {1{`RANDOM}};
  noLoopReg_20_mantissa = _RAND_214[25:0];
  _RAND_215 = {1{`RANDOM}};
  noLoopReg_20_exponent = _RAND_215[7:0];
  _RAND_216 = {1{`RANDOM}};
  noLoopReg_20_sign = _RAND_216[0:0];
  _RAND_217 = {1{`RANDOM}};
  noLoopReg_20_valid = _RAND_217[0:0];
  _RAND_218 = {1{`RANDOM}};
  noLoopReg_21_mantissa = _RAND_218[25:0];
  _RAND_219 = {1{`RANDOM}};
  noLoopReg_21_exponent = _RAND_219[7:0];
  _RAND_220 = {1{`RANDOM}};
  noLoopReg_21_sign = _RAND_220[0:0];
  _RAND_221 = {1{`RANDOM}};
  noLoopReg_21_valid = _RAND_221[0:0];
  _RAND_222 = {1{`RANDOM}};
  noLoopReg_22_mantissa = _RAND_222[25:0];
  _RAND_223 = {1{`RANDOM}};
  noLoopReg_22_exponent = _RAND_223[7:0];
  _RAND_224 = {1{`RANDOM}};
  noLoopReg_22_sign = _RAND_224[0:0];
  _RAND_225 = {1{`RANDOM}};
  noLoopReg_22_valid = _RAND_225[0:0];
  _RAND_226 = {1{`RANDOM}};
  noLoopReg_23_mantissa = _RAND_226[25:0];
  _RAND_227 = {1{`RANDOM}};
  noLoopReg_23_exponent = _RAND_227[7:0];
  _RAND_228 = {1{`RANDOM}};
  noLoopReg_23_sign = _RAND_228[0:0];
  _RAND_229 = {1{`RANDOM}};
  noLoopReg_23_valid = _RAND_229[0:0];
  _RAND_230 = {1{`RANDOM}};
  noLoopReg_24_mantissa = _RAND_230[25:0];
  _RAND_231 = {1{`RANDOM}};
  noLoopReg_24_exponent = _RAND_231[7:0];
  _RAND_232 = {1{`RANDOM}};
  noLoopReg_24_sign = _RAND_232[0:0];
  _RAND_233 = {1{`RANDOM}};
  noLoopReg_24_valid = _RAND_233[0:0];
  _RAND_234 = {1{`RANDOM}};
  noLoopReg_25_mantissa = _RAND_234[25:0];
  _RAND_235 = {1{`RANDOM}};
  noLoopReg_25_exponent = _RAND_235[7:0];
  _RAND_236 = {1{`RANDOM}};
  noLoopReg_25_sign = _RAND_236[0:0];
  _RAND_237 = {1{`RANDOM}};
  noLoopReg_25_valid = _RAND_237[0:0];
  _RAND_238 = {1{`RANDOM}};
  noLoopReg_26_mantissa = _RAND_238[25:0];
  _RAND_239 = {1{`RANDOM}};
  noLoopReg_26_exponent = _RAND_239[7:0];
  _RAND_240 = {1{`RANDOM}};
  noLoopReg_26_sign = _RAND_240[0:0];
  _RAND_241 = {1{`RANDOM}};
  noLoopReg_26_valid = _RAND_241[0:0];
`endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
module SPFPComp(
  input         io_in_valid,
  input  [25:0] io_in_bits_a_mantissa,
  input  [7:0]  io_in_bits_a_exponent,
  input         io_in_bits_a_sign,
  input  [25:0] io_in_bits_b_mantissa,
  input  [7:0]  io_in_bits_b_exponent,
  input         io_in_bits_b_sign,
  input  [4:0]  io_in_bits_op,
  input         io_in_bits_fcsr_0,
  input         io_in_bits_fcsr_1,
  input         io_in_bits_fcsr_2,
  input         io_in_bits_fcsr_3,
  input         io_in_bits_fcsr_4,
  input         io_in_bits_fcsr_5,
  input         io_in_bits_fcsr_6,
  input         io_in_bits_fcsr_7,
  input  [11:0] io_in_bits_fflags,
  output        io_out_valid,
  output [25:0] io_out_bits_result_mantissa,
  output [7:0]  io_out_bits_result_exponent,
  output        io_out_bits_result_sign,
  output        io_out_bits_fcsr_0,
  output        io_out_bits_fcsr_1,
  output        io_out_bits_fcsr_2,
  output        io_out_bits_fcsr_3,
  output        io_out_bits_fcsr_4,
  output        io_out_bits_fcsr_5,
  output        io_out_bits_fcsr_6,
  output        io_out_bits_fcsr_7,
  output [4:0]  io_out_bits_op
);
  wire  isMin = io_in_bits_op == 5'h5; // @[SPFPComp.scala 25:33]
  wire  zeroA = io_in_bits_fflags[6] & io_in_bits_fflags[9]; // @[SPFPComp.scala 26:38]
  wire  zeroB = io_in_bits_fflags[7] & io_in_bits_fflags[10]; // @[SPFPComp.scala 27:38]
  wire  aNaN = io_in_bits_fflags[3] & io_in_bits_fflags[0]; // @[SPFPComp.scala 28:38]
  wire  bNaN = io_in_bits_fflags[4] & io_in_bits_fflags[1]; // @[SPFPComp.scala 29:38]
  wire  _T_3 = aNaN | bNaN; // @[SPFPComp.scala 54:16]
  wire  _T_6 = ~io_in_bits_a_mantissa[22]; // @[SPFPComp.scala 56:15]
  wire  _T_8 = ~io_in_bits_b_mantissa[22]; // @[SPFPComp.scala 56:45]
  wire  _T_9 = ~io_in_bits_a_mantissa[22] | ~io_in_bits_b_mantissa[22]; // @[SPFPComp.scala 56:42]
  wire  _GEN_12 = ~io_in_bits_a_mantissa[22] | ~io_in_bits_b_mantissa[22] | io_in_bits_fcsr_4; // @[SPFPComp.scala 22:6 56:73]
  wire  _GEN_28 = _T_8 | io_in_bits_fcsr_4; // @[SPFPComp.scala 63:43 22:6]
  wire  _GEN_44 = _T_6 | io_in_bits_fcsr_4; // @[SPFPComp.scala 68:42 22:6]
  wire  _GEN_52 = io_in_bits_fflags[3] ? _GEN_44 : io_in_bits_fcsr_4; // @[SPFPComp.scala 67:28 22:6]
  wire [25:0] _GEN_56 = io_in_bits_fflags[3] ? io_in_bits_b_mantissa : 26'h0; // @[SPFPComp.scala 67:28 71:28 48:31]
  wire [7:0] _GEN_57 = io_in_bits_fflags[3] ? $signed(io_in_bits_b_exponent) : $signed(8'sh0); // @[SPFPComp.scala 67:28 71:28 49:31]
  wire  _GEN_58 = io_in_bits_fflags[3] & io_in_bits_b_sign; // @[SPFPComp.scala 50:27 67:28 71:28]
  wire  _GEN_63 = io_in_bits_fflags[4] ? _GEN_28 : _GEN_52; // @[SPFPComp.scala 62:28]
  wire [25:0] _GEN_67 = io_in_bits_fflags[4] ? io_in_bits_a_mantissa : _GEN_56; // @[SPFPComp.scala 62:28 66:28]
  wire [7:0] _GEN_68 = io_in_bits_fflags[4] ? $signed(io_in_bits_a_exponent) : $signed(_GEN_57); // @[SPFPComp.scala 62:28 66:28]
  wire  _GEN_69 = io_in_bits_fflags[4] ? io_in_bits_a_sign : _GEN_58; // @[SPFPComp.scala 62:28 66:28]
  wire  _GEN_74 = io_in_bits_fflags[3] & io_in_bits_fflags[4] ? _GEN_12 : _GEN_63; // @[SPFPComp.scala 55:33]
  wire [25:0] _GEN_78 = io_in_bits_fflags[3] & io_in_bits_fflags[4] ? 26'h400000 : _GEN_67; // @[SPFPComp.scala 55:33 59:37]
  wire [8:0] _GEN_79 = io_in_bits_fflags[3] & io_in_bits_fflags[4] ? $signed(9'sh80) : $signed({{1{_GEN_68[7]}},_GEN_68}
    ); // @[SPFPComp.scala 55:33 60:37]
  wire  _GEN_80 = io_in_bits_fflags[3] & io_in_bits_fflags[4] ? 1'h0 : _GEN_69; // @[SPFPComp.scala 55:33 61:37]
  wire  _T_19 = ~io_in_bits_b_sign; // @[SPFPComp.scala 73:33]
  wire  _T_20 = io_in_bits_a_sign & ~io_in_bits_b_sign; // @[SPFPComp.scala 73:30]
  wire [25:0] _io_out_bits_result_T_mantissa = isMin ? io_in_bits_a_mantissa : io_in_bits_b_mantissa; // @[SPFPComp.scala 74:32]
  wire [7:0] _io_out_bits_result_T_exponent = isMin ? $signed(io_in_bits_a_exponent) : $signed(io_in_bits_b_exponent); // @[SPFPComp.scala 74:32]
  wire  _io_out_bits_result_T_sign = isMin ? io_in_bits_a_sign : io_in_bits_b_sign; // @[SPFPComp.scala 74:32]
  wire  _T_23 = ~io_in_bits_a_sign; // @[SPFPComp.scala 75:33]
  wire [25:0] _io_out_bits_result_T_1_mantissa = isMin ? io_in_bits_b_mantissa : io_in_bits_a_mantissa; // @[SPFPComp.scala 76:32]
  wire [7:0] _io_out_bits_result_T_1_exponent = isMin ? $signed(io_in_bits_b_exponent) : $signed(io_in_bits_a_exponent); // @[SPFPComp.scala 76:32]
  wire  _io_out_bits_result_T_1_sign = isMin ? io_in_bits_b_sign : io_in_bits_a_sign; // @[SPFPComp.scala 76:32]
  wire  _T_28 = $signed(io_in_bits_a_exponent) == $signed(io_in_bits_b_exponent); // @[SPFPComp.scala 78:17]
  wire  _T_29 = io_in_bits_a_mantissa == io_in_bits_b_mantissa; // @[SPFPComp.scala 79:20]
  wire  _T_30 = io_in_bits_a_mantissa < io_in_bits_b_mantissa; // @[SPFPComp.scala 79:39]
  wire  _T_31 = io_in_bits_a_mantissa == io_in_bits_b_mantissa | io_in_bits_a_mantissa < io_in_bits_b_mantissa; // @[SPFPComp.scala 79:30]
  wire [25:0] _GEN_81 = io_in_bits_a_mantissa == io_in_bits_b_mantissa | io_in_bits_a_mantissa < io_in_bits_b_mantissa
     ? _io_out_bits_result_T_1_mantissa : _io_out_bits_result_T_mantissa; // @[SPFPComp.scala 79:48 80:30 82:30]
  wire [7:0] _GEN_82 = io_in_bits_a_mantissa == io_in_bits_b_mantissa | io_in_bits_a_mantissa < io_in_bits_b_mantissa ?
    $signed(_io_out_bits_result_T_1_exponent) : $signed(_io_out_bits_result_T_exponent); // @[SPFPComp.scala 79:48 80:30 82:30]
  wire  _GEN_83 = io_in_bits_a_mantissa == io_in_bits_b_mantissa | io_in_bits_a_mantissa < io_in_bits_b_mantissa ?
    _io_out_bits_result_T_1_sign : _io_out_bits_result_T_sign; // @[SPFPComp.scala 79:48 80:30 82:30]
  wire  _T_32 = $signed(io_in_bits_a_exponent) > $signed(io_in_bits_b_exponent); // @[SPFPComp.scala 84:25]
  wire [25:0] _GEN_84 = $signed(io_in_bits_a_exponent) > $signed(io_in_bits_b_exponent) ? _io_out_bits_result_T_mantissa
     : _io_out_bits_result_T_1_mantissa; // @[SPFPComp.scala 84:33 85:28 87:28]
  wire [7:0] _GEN_85 = $signed(io_in_bits_a_exponent) > $signed(io_in_bits_b_exponent) ? $signed(
    _io_out_bits_result_T_exponent) : $signed(_io_out_bits_result_T_1_exponent); // @[SPFPComp.scala 84:33 85:28 87:28]
  wire  _GEN_86 = $signed(io_in_bits_a_exponent) > $signed(io_in_bits_b_exponent) ? _io_out_bits_result_T_sign :
    _io_out_bits_result_T_1_sign; // @[SPFPComp.scala 84:33 85:28 87:28]
  wire [25:0] _GEN_87 = $signed(io_in_bits_a_exponent) == $signed(io_in_bits_b_exponent) ? _GEN_81 : _GEN_84; // @[SPFPComp.scala 78:27]
  wire [7:0] _GEN_88 = $signed(io_in_bits_a_exponent) == $signed(io_in_bits_b_exponent) ? $signed(_GEN_82) : $signed(
    _GEN_85); // @[SPFPComp.scala 78:27]
  wire  _GEN_89 = $signed(io_in_bits_a_exponent) == $signed(io_in_bits_b_exponent) ? _GEN_83 : _GEN_86; // @[SPFPComp.scala 78:27]
  wire  _T_40 = io_in_bits_a_mantissa > io_in_bits_b_mantissa; // @[SPFPComp.scala 91:39]
  wire [25:0] _GEN_90 = _T_29 | io_in_bits_a_mantissa > io_in_bits_b_mantissa ? _io_out_bits_result_T_1_mantissa :
    _io_out_bits_result_T_mantissa; // @[SPFPComp.scala 91:48 92:30 94:30]
  wire [7:0] _GEN_91 = _T_29 | io_in_bits_a_mantissa > io_in_bits_b_mantissa ? $signed(_io_out_bits_result_T_1_exponent)
     : $signed(_io_out_bits_result_T_exponent); // @[SPFPComp.scala 91:48 92:30 94:30]
  wire  _GEN_92 = _T_29 | io_in_bits_a_mantissa > io_in_bits_b_mantissa ? _io_out_bits_result_T_1_sign :
    _io_out_bits_result_T_sign; // @[SPFPComp.scala 91:48 92:30 94:30]
  wire [25:0] _GEN_93 = _T_32 ? _io_out_bits_result_T_1_mantissa : _io_out_bits_result_T_mantissa; // @[SPFPComp.scala 96:32 97:28 99:28]
  wire [7:0] _GEN_94 = _T_32 ? $signed(_io_out_bits_result_T_1_exponent) : $signed(_io_out_bits_result_T_exponent); // @[SPFPComp.scala 96:32 97:28 99:28]
  wire  _GEN_95 = _T_32 ? _io_out_bits_result_T_1_sign : _io_out_bits_result_T_sign; // @[SPFPComp.scala 96:32 97:28 99:28]
  wire [25:0] _GEN_96 = _T_28 ? _GEN_90 : _GEN_93; // @[SPFPComp.scala 90:27]
  wire [7:0] _GEN_97 = _T_28 ? $signed(_GEN_91) : $signed(_GEN_94); // @[SPFPComp.scala 90:27]
  wire  _GEN_98 = _T_28 ? _GEN_92 : _GEN_95; // @[SPFPComp.scala 90:27]
  wire [25:0] _GEN_99 = _T_23 & _T_19 ? _GEN_96 : 26'h0; // @[SPFPComp.scala 48:31 89:49]
  wire [7:0] _GEN_100 = _T_23 & _T_19 ? $signed(_GEN_97) : $signed(8'sh0); // @[SPFPComp.scala 49:31 89:49]
  wire  _GEN_101 = _T_23 & _T_19 & _GEN_98; // @[SPFPComp.scala 50:27 89:49]
  wire [25:0] _GEN_102 = io_in_bits_b_sign & io_in_bits_a_sign ? _GEN_87 : _GEN_99; // @[SPFPComp.scala 77:47]
  wire [7:0] _GEN_103 = io_in_bits_b_sign & io_in_bits_a_sign ? $signed(_GEN_88) : $signed(_GEN_100); // @[SPFPComp.scala 77:47]
  wire  _GEN_104 = io_in_bits_b_sign & io_in_bits_a_sign ? _GEN_89 : _GEN_101; // @[SPFPComp.scala 77:47]
  wire [25:0] _GEN_105 = io_in_bits_b_sign & ~io_in_bits_a_sign ? _io_out_bits_result_T_1_mantissa : _GEN_102; // @[SPFPComp.scala 75:50 76:26]
  wire [7:0] _GEN_106 = io_in_bits_b_sign & ~io_in_bits_a_sign ? $signed(_io_out_bits_result_T_1_exponent) : $signed(
    _GEN_103); // @[SPFPComp.scala 75:50 76:26]
  wire  _GEN_107 = io_in_bits_b_sign & ~io_in_bits_a_sign ? _io_out_bits_result_T_1_sign : _GEN_104; // @[SPFPComp.scala 75:50 76:26]
  wire [25:0] _GEN_108 = io_in_bits_a_sign & ~io_in_bits_b_sign ? _io_out_bits_result_T_mantissa : _GEN_105; // @[SPFPComp.scala 73:50 74:26]
  wire [7:0] _GEN_109 = io_in_bits_a_sign & ~io_in_bits_b_sign ? $signed(_io_out_bits_result_T_exponent) : $signed(
    _GEN_106); // @[SPFPComp.scala 73:50 74:26]
  wire  _GEN_110 = io_in_bits_a_sign & ~io_in_bits_b_sign ? _io_out_bits_result_T_sign : _GEN_107; // @[SPFPComp.scala 73:50 74:26]
  wire  _GEN_115 = aNaN | bNaN ? _GEN_74 : io_in_bits_fcsr_4; // @[SPFPComp.scala 54:25 22:6]
  wire [25:0] _GEN_119 = aNaN | bNaN ? _GEN_78 : _GEN_108; // @[SPFPComp.scala 54:25]
  wire [8:0] _GEN_120 = aNaN | bNaN ? $signed(_GEN_79) : $signed({{1{_GEN_109[7]}},_GEN_109}); // @[SPFPComp.scala 54:25]
  wire  _GEN_121 = aNaN | bNaN ? _GEN_80 : _GEN_110; // @[SPFPComp.scala 54:25]
  wire  _GEN_126 = isMin | io_in_bits_op == 5'h6 ? _GEN_115 : io_in_bits_fcsr_4; // @[SPFPComp.scala 22:6 53:87]
  wire [25:0] _GEN_130 = isMin | io_in_bits_op == 5'h6 ? _GEN_119 : 26'h0; // @[SPFPComp.scala 48:31 53:87]
  wire [8:0] _GEN_131 = isMin | io_in_bits_op == 5'h6 ? $signed(_GEN_120) : $signed(9'sh0); // @[SPFPComp.scala 49:31 53:87]
  wire  _GEN_132 = (isMin | io_in_bits_op == 5'h6) & _GEN_121; // @[SPFPComp.scala 50:27 53:87]
  wire  _GEN_145 = _T_9 | _GEN_126; // @[SPFPComp.scala 113:71]
  wire  _GEN_161 = _T_8 | _GEN_126; // @[SPFPComp.scala 120:41]
  wire  _GEN_177 = _T_6 | _GEN_126; // @[SPFPComp.scala 127:40]
  wire  _T_57 = zeroA & zeroB; // @[SPFPComp.scala 133:23]
  wire  _T_58 = io_in_bits_a_sign == io_in_bits_b_sign; // @[SPFPComp.scala 133:43]
  wire [7:0] _GEN_182 = _T_58 & _T_28 & _T_29 ? $signed(8'sh7f) : $signed(8'sh0); // @[SPFPComp.scala 137:69 139:35 143:35]
  wire [7:0] _GEN_184 = zeroA & zeroB & ~(io_in_bits_a_sign == io_in_bits_b_sign) ? $signed(8'sh0) : $signed(_GEN_182); // @[SPFPComp.scala 133:54 135:35]
  wire  _GEN_189 = aNaN ? _GEN_177 : _GEN_126; // @[SPFPComp.scala 126:23]
  wire [7:0] _GEN_194 = aNaN ? $signed(8'sh0) : $signed(_GEN_184); // @[SPFPComp.scala 126:23 131:35]
  wire  _GEN_199 = bNaN ? _GEN_161 : _GEN_189; // @[SPFPComp.scala 119:23]
  wire [7:0] _GEN_204 = bNaN ? $signed(8'sh0) : $signed(_GEN_194); // @[SPFPComp.scala 119:23 124:35]
  wire  _GEN_209 = aNaN & bNaN ? _GEN_145 : _GEN_199; // @[SPFPComp.scala 112:25]
  wire [7:0] _GEN_214 = aNaN & bNaN ? $signed(8'sh0) : $signed(_GEN_204); // @[SPFPComp.scala 112:25 117:35]
  wire  _GEN_219 = io_in_bits_op == 5'hb ? _GEN_209 : _GEN_126; // @[SPFPComp.scala 104:49]
  wire [25:0] _GEN_223 = io_in_bits_op == 5'hb ? 26'h0 : _GEN_130; // @[SPFPComp.scala 104:49]
  wire [8:0] _GEN_224 = io_in_bits_op == 5'hb ? $signed({{1{_GEN_214[7]}},_GEN_214}) : $signed(_GEN_131); // @[SPFPComp.scala 104:49]
  wire  _GEN_225 = io_in_bits_op == 5'hb ? 1'h0 : _GEN_132; // @[SPFPComp.scala 104:49]
  wire [25:0] _GEN_234 = _T_30 ? 26'h0 : _GEN_223; // @[SPFPComp.scala 177:41 178:43]
  wire [8:0] _GEN_235 = _T_30 ? $signed(9'sh7f) : $signed(_GEN_224); // @[SPFPComp.scala 177:41 179:43]
  wire  _GEN_236 = _T_30 ? 1'h0 : _GEN_225; // @[SPFPComp.scala 177:41 180:43]
  wire [25:0] _GEN_237 = _T_29 ? 26'h0 : _GEN_234; // @[SPFPComp.scala 173:35 174:43]
  wire [8:0] _GEN_238 = _T_29 ? $signed(9'sh0) : $signed(_GEN_235); // @[SPFPComp.scala 173:35 175:43]
  wire  _GEN_239 = _T_29 ? 1'h0 : _GEN_236; // @[SPFPComp.scala 173:35 176:43]
  wire [7:0] _GEN_241 = $signed(io_in_bits_a_exponent) < $signed(io_in_bits_b_exponent) ? $signed(8'sh7f) : $signed(8'sh0
    ); // @[SPFPComp.scala 182:37 184:41 188:41]
  wire [25:0] _GEN_242 = _T_28 ? _GEN_237 : 26'h0; // @[SPFPComp.scala 172:31]
  wire [8:0] _GEN_243 = _T_28 ? $signed(_GEN_238) : $signed({{1{_GEN_241[7]}},_GEN_241}); // @[SPFPComp.scala 172:31]
  wire  _GEN_244 = _T_28 & _GEN_239; // @[SPFPComp.scala 172:31]
  wire [7:0] _GEN_246 = _T_40 ? $signed(8'sh7f) : $signed(8'sh0); // @[SPFPComp.scala 193:33 195:43 199:43]
  wire [7:0] _GEN_248 = _T_32 ? $signed(8'sh7f) : $signed(8'sh0); // @[SPFPComp.scala 202:37 204:41 208:41]
  wire [7:0] _GEN_250 = _T_28 ? $signed(_GEN_246) : $signed(_GEN_248); // @[SPFPComp.scala 192:31]
  wire [25:0] _GEN_251 = io_in_bits_a_sign ? 26'h0 : _GEN_223; // @[SPFPComp.scala 191:35]
  wire [8:0] _GEN_252 = io_in_bits_a_sign ? $signed({{1{_GEN_250[7]}},_GEN_250}) : $signed(_GEN_224); // @[SPFPComp.scala 191:35]
  wire  _GEN_253 = io_in_bits_a_sign ? 1'h0 : _GEN_225; // @[SPFPComp.scala 191:35]
  wire [25:0] _GEN_254 = _T_23 ? _GEN_242 : _GEN_251; // @[SPFPComp.scala 171:29]
  wire [8:0] _GEN_255 = _T_23 ? $signed(_GEN_243) : $signed(_GEN_252); // @[SPFPComp.scala 171:29]
  wire  _GEN_256 = _T_23 ? _GEN_244 : _GEN_253; // @[SPFPComp.scala 171:29]
  wire [7:0] _GEN_258 = _T_20 ? $signed(8'sh7f) : $signed(8'sh0); // @[SPFPComp.scala 212:50 214:37 218:37]
  wire [25:0] _GEN_259 = _T_58 ? _GEN_254 : 26'h0; // @[SPFPComp.scala 170:29]
  wire [8:0] _GEN_260 = _T_58 ? $signed(_GEN_255) : $signed({{1{_GEN_258[7]}},_GEN_258}); // @[SPFPComp.scala 170:29]
  wire  _GEN_261 = _T_58 & _GEN_256; // @[SPFPComp.scala 170:29]
  wire [25:0] _GEN_262 = _T_57 ? 26'h0 : _GEN_259; // @[SPFPComp.scala 164:33 166:35]
  wire [8:0] _GEN_263 = _T_57 ? $signed(9'sh7f) : $signed(_GEN_260); // @[SPFPComp.scala 164:33 167:35]
  wire  _GEN_264 = _T_57 ? 1'h0 : _GEN_261; // @[SPFPComp.scala 164:33 168:35]
  wire [25:0] _GEN_265 = _T_3 ? 26'h0 : _GEN_262; // @[SPFPComp.scala 158:24 160:35]
  wire [8:0] _GEN_266 = _T_3 ? $signed(9'sh0) : $signed(_GEN_263); // @[SPFPComp.scala 158:24 161:35]
  wire  _GEN_267 = _T_3 ? 1'h0 : _GEN_264; // @[SPFPComp.scala 158:24 162:35]
  wire  _GEN_272 = _T_3 | _GEN_219; // @[SPFPComp.scala 158:24]
  wire [25:0] _GEN_276 = io_in_bits_op == 5'hc ? _GEN_265 : _GEN_223; // @[SPFPComp.scala 148:49]
  wire [8:0] _GEN_277 = io_in_bits_op == 5'hc ? $signed(_GEN_266) : $signed(_GEN_224); // @[SPFPComp.scala 148:49]
  wire  _GEN_278 = io_in_bits_op == 5'hc ? _GEN_267 : _GEN_225; // @[SPFPComp.scala 148:49]
  wire  _GEN_283 = io_in_bits_op == 5'hc ? _GEN_272 : _GEN_219; // @[SPFPComp.scala 148:49]
  wire [7:0] _GEN_296 = _T_31 ? $signed(8'sh7f) : $signed(8'sh0); // @[SPFPComp.scala 250:52 252:43 256:43]
  wire [7:0] _GEN_300 = _T_28 ? $signed(_GEN_296) : $signed(_GEN_241); // @[SPFPComp.scala 249:31]
  wire [7:0] _GEN_302 = _T_30 ? $signed(8'sh0) : $signed(8'sh7f); // @[SPFPComp.scala 270:34 272:43 276:43]
  wire [7:0] _GEN_306 = _T_28 ? $signed(_GEN_302) : $signed(_GEN_248); // @[SPFPComp.scala 269:31]
  wire [25:0] _GEN_307 = io_in_bits_a_sign ? 26'h0 : _GEN_276; // @[SPFPComp.scala 268:35]
  wire [8:0] _GEN_308 = io_in_bits_a_sign ? $signed({{1{_GEN_306[7]}},_GEN_306}) : $signed(_GEN_277); // @[SPFPComp.scala 268:35]
  wire  _GEN_309 = io_in_bits_a_sign ? 1'h0 : _GEN_278; // @[SPFPComp.scala 268:35]
  wire [25:0] _GEN_310 = _T_23 ? 26'h0 : _GEN_307; // @[SPFPComp.scala 248:29]
  wire [8:0] _GEN_311 = _T_23 ? $signed({{1{_GEN_300[7]}},_GEN_300}) : $signed(_GEN_308); // @[SPFPComp.scala 248:29]
  wire  _GEN_312 = _T_23 ? 1'h0 : _GEN_309; // @[SPFPComp.scala 248:29]
  wire [25:0] _GEN_315 = _T_58 ? _GEN_310 : 26'h0; // @[SPFPComp.scala 247:29]
  wire [8:0] _GEN_316 = _T_58 ? $signed(_GEN_311) : $signed({{1{_GEN_258[7]}},_GEN_258}); // @[SPFPComp.scala 247:29]
  wire  _GEN_317 = _T_58 & _GEN_312; // @[SPFPComp.scala 247:29]
  wire [25:0] _GEN_318 = _T_57 ? 26'h0 : _GEN_315; // @[SPFPComp.scala 241:33 243:35]
  wire [8:0] _GEN_319 = _T_57 ? $signed(9'sh7f) : $signed(_GEN_316); // @[SPFPComp.scala 241:33 244:35]
  wire  _GEN_320 = _T_57 ? 1'h0 : _GEN_317; // @[SPFPComp.scala 241:33 245:35]
  wire [25:0] _GEN_321 = _T_3 ? 26'h0 : _GEN_318; // @[SPFPComp.scala 235:24 237:35]
  wire [8:0] _GEN_322 = _T_3 ? $signed(9'sh0) : $signed(_GEN_319); // @[SPFPComp.scala 235:24 238:35]
  wire  _GEN_323 = _T_3 ? 1'h0 : _GEN_320; // @[SPFPComp.scala 235:24 239:35]
  wire  _GEN_328 = _T_3 | _GEN_283; // @[SPFPComp.scala 235:24]
  wire [8:0] _GEN_333 = io_in_bits_op == 5'hd ? $signed(_GEN_322) : $signed(_GEN_277); // @[SPFPComp.scala 224:49]
  assign io_out_valid = io_in_valid; // @[SPFPComp.scala 304:16]
  assign io_out_bits_result_mantissa = io_in_bits_op == 5'hd ? _GEN_321 : _GEN_276; // @[SPFPComp.scala 224:49]
  assign io_out_bits_result_exponent = _GEN_333[7:0];
  assign io_out_bits_result_sign = io_in_bits_op == 5'hd ? _GEN_323 : _GEN_278; // @[SPFPComp.scala 224:49]
  assign io_out_bits_fcsr_0 = io_in_bits_fcsr_0; // @[SPFPComp.scala 224:49]
  assign io_out_bits_fcsr_1 = io_in_bits_fcsr_1; // @[SPFPComp.scala 224:49]
  assign io_out_bits_fcsr_2 = io_in_bits_fcsr_2; // @[SPFPComp.scala 224:49]
  assign io_out_bits_fcsr_3 = io_in_bits_fcsr_3; // @[SPFPComp.scala 224:49]
  assign io_out_bits_fcsr_4 = io_in_bits_op == 5'hd ? _GEN_328 : _GEN_283; // @[SPFPComp.scala 224:49]
  assign io_out_bits_fcsr_5 = io_in_bits_fcsr_5; // @[SPFPComp.scala 224:49]
  assign io_out_bits_fcsr_6 = io_in_bits_fcsr_6; // @[SPFPComp.scala 224:49]
  assign io_out_bits_fcsr_7 = io_in_bits_fcsr_7; // @[SPFPComp.scala 224:49]
  assign io_out_bits_op = io_in_bits_op; // @[SPFPComp.scala 302:18]
endmodule
module SPFPSGNJ(
  input         io_in_valid,
  input  [25:0] io_in_bits_a_mantissa,
  input  [7:0]  io_in_bits_a_exponent,
  input         io_in_bits_a_sign,
  input         io_in_bits_b_sign,
  input  [4:0]  io_in_bits_op,
  input         io_in_bits_fcsr_0,
  input         io_in_bits_fcsr_1,
  input         io_in_bits_fcsr_2,
  input         io_in_bits_fcsr_3,
  input         io_in_bits_fcsr_4,
  input         io_in_bits_fcsr_5,
  input         io_in_bits_fcsr_6,
  input         io_in_bits_fcsr_7,
  output        io_out_valid,
  output [25:0] io_out_bits_result_mantissa,
  output [7:0]  io_out_bits_result_exponent,
  output        io_out_bits_result_sign,
  output        io_out_bits_fcsr_0,
  output        io_out_bits_fcsr_1,
  output        io_out_bits_fcsr_2,
  output        io_out_bits_fcsr_3,
  output        io_out_bits_fcsr_4,
  output        io_out_bits_fcsr_5,
  output        io_out_bits_fcsr_6,
  output        io_out_bits_fcsr_7,
  output [4:0]  io_out_bits_op
);
  wire  signFSGNJN = ~io_in_bits_b_sign; // @[SPFPSGNJ.scala 25:20]
  wire  signFSGNJX = io_in_bits_a_sign ^ io_in_bits_b_sign; // @[SPFPSGNJ.scala 26:38]
  wire  _GEN_0 = io_in_bits_op == 5'h13 ? signFSGNJN : signFSGNJX; // @[SPFPSGNJ.scala 30:53 31:27 33:27]
  assign io_out_valid = io_in_valid; // @[SPFPSGNJ.scala 41:16]
  assign io_out_bits_result_mantissa = io_in_bits_a_mantissa; // @[SPFPSGNJ.scala 20:31]
  assign io_out_bits_result_exponent = io_in_bits_a_exponent; // @[SPFPSGNJ.scala 21:31]
  assign io_out_bits_result_sign = io_in_bits_op == 5'h12 ? io_in_bits_b_sign : _GEN_0; // @[SPFPSGNJ.scala 28:45 29:27]
  assign io_out_bits_fcsr_0 = io_in_bits_fcsr_0; // @[SPFPSGNJ.scala 38:20]
  assign io_out_bits_fcsr_1 = io_in_bits_fcsr_1; // @[SPFPSGNJ.scala 38:20]
  assign io_out_bits_fcsr_2 = io_in_bits_fcsr_2; // @[SPFPSGNJ.scala 38:20]
  assign io_out_bits_fcsr_3 = io_in_bits_fcsr_3; // @[SPFPSGNJ.scala 38:20]
  assign io_out_bits_fcsr_4 = io_in_bits_fcsr_4; // @[SPFPSGNJ.scala 38:20]
  assign io_out_bits_fcsr_5 = io_in_bits_fcsr_5; // @[SPFPSGNJ.scala 38:20]
  assign io_out_bits_fcsr_6 = io_in_bits_fcsr_6; // @[SPFPSGNJ.scala 38:20]
  assign io_out_bits_fcsr_7 = io_in_bits_fcsr_7; // @[SPFPSGNJ.scala 38:20]
  assign io_out_bits_op = io_in_bits_op; // @[SPFPSGNJ.scala 39:18]
endmodule
module SPFPClass(
  input         io_in_valid,
  input  [25:0] io_in_bits_a_mantissa,
  input         io_in_bits_a_sign,
  input  [4:0]  io_in_bits_op,
  input         io_in_bits_fcsr_0,
  input         io_in_bits_fcsr_1,
  input         io_in_bits_fcsr_2,
  input         io_in_bits_fcsr_3,
  input         io_in_bits_fcsr_4,
  input         io_in_bits_fcsr_5,
  input         io_in_bits_fcsr_6,
  input         io_in_bits_fcsr_7,
  input  [11:0] io_in_bits_fflags,
  output        io_out_valid,
  output [25:0] io_out_bits_result_mantissa,
  output        io_out_bits_fcsr_0,
  output        io_out_bits_fcsr_1,
  output        io_out_bits_fcsr_2,
  output        io_out_bits_fcsr_3,
  output        io_out_bits_fcsr_4,
  output        io_out_bits_fcsr_5,
  output        io_out_bits_fcsr_6,
  output        io_out_bits_fcsr_7,
  output [4:0]  io_out_bits_op
);
  wire  isPos = ~io_in_bits_a_sign; // @[SPFPClass.scala 16:23]
  wire  zeroMant = io_in_bits_fflags[9]; // @[SPFPClass.scala 45:35]
  wire  zeroExp = io_in_bits_fflags[6]; // @[SPFPClass.scala 46:35]
  wire  maxExp = io_in_bits_fflags[3]; // @[SPFPClass.scala 47:35]
  wire  _isNZero_T = zeroMant & zeroExp; // @[SPFPClass.scala 56:34]
  wire  _isNZero_T_1 = ~isPos; // @[SPFPClass.scala 56:49]
  wire  isNZero = zeroMant & zeroExp & ~isPos; // @[SPFPClass.scala 56:46]
  wire  _isNNormal_T = ~maxExp; // @[SPFPClass.scala 57:25]
  wire  _isNNormal_T_2 = ~io_in_bits_a_mantissa[23]; // @[SPFPClass.scala 57:37]
  wire  isNNormal = ~maxExp & ~io_in_bits_a_mantissa[23] & _isNZero_T_1 & ~isNZero; // @[SPFPClass.scala 57:74]
  wire  isNSubnormal = zeroExp & io_in_bits_a_mantissa[23] & _isNZero_T_1; // @[SPFPClass.scala 58:63]
  wire  _isNInf_T = maxExp & zeroMant; // @[SPFPClass.scala 59:34]
  wire  isNInf = maxExp & zeroMant & _isNZero_T_1; // @[SPFPClass.scala 59:47]
  wire  isPZero = _isNZero_T & isPos; // @[SPFPClass.scala 60:47]
  wire  isPInf = _isNInf_T & isPos; // @[SPFPClass.scala 61:47]
  wire  isPSubnormal = zeroExp & _isNNormal_T_2 & isPos & ~isPZero; // @[SPFPClass.scala 62:73]
  wire  isPNormal = _isNNormal_T & io_in_bits_a_mantissa[23] & isPos; // @[SPFPClass.scala 63:64]
  wire  _isSNaN_T_4 = ~(isPInf | isNInf); // @[SPFPClass.scala 64:67]
  wire  isSNaN = maxExp & ~io_in_bits_a_mantissa[22] & ~(isPInf | isNInf); // @[SPFPClass.scala 64:64]
  wire  isQNaN = maxExp & io_in_bits_a_mantissa[22] & _isSNaN_T_4 & ~isSNaN; // @[SPFPClass.scala 65:87]
  wire [4:0] io_out_bits_result_mantissa_lo = {isPZero,isNZero,isNSubnormal,isNNormal,isNInf}; // @[Cat.scala 33:92]
  wire [20:0] io_out_bits_result_mantissa_hi = {16'h0,isQNaN,isSNaN,isPInf,isPNormal,isPSubnormal}; // @[Cat.scala 33:92]
  assign io_out_valid = io_in_valid; // @[SPFPClass.scala 89:16]
  assign io_out_bits_result_mantissa = {io_out_bits_result_mantissa_hi,io_out_bits_result_mantissa_lo}; // @[Cat.scala 33:92]
  assign io_out_bits_fcsr_0 = io_in_bits_fcsr_0; // @[SPFPClass.scala 86:20]
  assign io_out_bits_fcsr_1 = io_in_bits_fcsr_1; // @[SPFPClass.scala 86:20]
  assign io_out_bits_fcsr_2 = io_in_bits_fcsr_2; // @[SPFPClass.scala 86:20]
  assign io_out_bits_fcsr_3 = io_in_bits_fcsr_3; // @[SPFPClass.scala 86:20]
  assign io_out_bits_fcsr_4 = io_in_bits_fcsr_4; // @[SPFPClass.scala 86:20]
  assign io_out_bits_fcsr_5 = io_in_bits_fcsr_5; // @[SPFPClass.scala 86:20]
  assign io_out_bits_fcsr_6 = io_in_bits_fcsr_6; // @[SPFPClass.scala 86:20]
  assign io_out_bits_fcsr_7 = io_in_bits_fcsr_7; // @[SPFPClass.scala 86:20]
  assign io_out_bits_op = io_in_bits_op; // @[SPFPClass.scala 87:18]
endmodule
module SPFPPass(
  input         io_in_valid,
  input  [25:0] io_in_bits_a_mantissa,
  input  [7:0]  io_in_bits_a_exponent,
  input         io_in_bits_a_sign,
  input  [4:0]  io_in_bits_op,
  input         io_in_bits_fcsr_0,
  input         io_in_bits_fcsr_1,
  input         io_in_bits_fcsr_2,
  input         io_in_bits_fcsr_3,
  input         io_in_bits_fcsr_4,
  input         io_in_bits_fcsr_5,
  input         io_in_bits_fcsr_6,
  input         io_in_bits_fcsr_7,
  output        io_out_valid,
  output [25:0] io_out_bits_result_mantissa,
  output [7:0]  io_out_bits_result_exponent,
  output        io_out_bits_result_sign,
  output        io_out_bits_fcsr_0,
  output        io_out_bits_fcsr_1,
  output        io_out_bits_fcsr_2,
  output        io_out_bits_fcsr_3,
  output        io_out_bits_fcsr_4,
  output        io_out_bits_fcsr_5,
  output        io_out_bits_fcsr_6,
  output        io_out_bits_fcsr_7,
  output [4:0]  io_out_bits_op
);
  assign io_out_valid = io_in_valid; // @[SPFPPass.scala 19:26]
  assign io_out_bits_result_mantissa = io_in_bits_a_mantissa; // @[SPFPPass.scala 14:26]
  assign io_out_bits_result_exponent = io_in_bits_a_exponent; // @[SPFPPass.scala 14:26]
  assign io_out_bits_result_sign = io_in_bits_a_sign; // @[SPFPPass.scala 14:26]
  assign io_out_bits_fcsr_0 = io_in_bits_fcsr_0; // @[SPFPPass.scala 15:26]
  assign io_out_bits_fcsr_1 = io_in_bits_fcsr_1; // @[SPFPPass.scala 15:26]
  assign io_out_bits_fcsr_2 = io_in_bits_fcsr_2; // @[SPFPPass.scala 15:26]
  assign io_out_bits_fcsr_3 = io_in_bits_fcsr_3; // @[SPFPPass.scala 15:26]
  assign io_out_bits_fcsr_4 = io_in_bits_fcsr_4; // @[SPFPPass.scala 15:26]
  assign io_out_bits_fcsr_5 = io_in_bits_fcsr_5; // @[SPFPPass.scala 15:26]
  assign io_out_bits_fcsr_6 = io_in_bits_fcsr_6; // @[SPFPPass.scala 15:26]
  assign io_out_bits_fcsr_7 = io_in_bits_fcsr_7; // @[SPFPPass.scala 15:26]
  assign io_out_bits_op = io_in_bits_op; // @[SPFPPass.scala 17:26]
endmodule
module FPUExecutionUnit(
  input         clock,
  input         reset,
  output        io_in_ready,
  input         io_in_valid,
  input  [25:0] io_in_bits_a_mantissa,
  input  [7:0]  io_in_bits_a_exponent,
  input         io_in_bits_a_sign,
  input  [25:0] io_in_bits_b_mantissa,
  input  [7:0]  io_in_bits_b_exponent,
  input         io_in_bits_b_sign,
  input  [25:0] io_in_bits_c_mantissa,
  input  [7:0]  io_in_bits_c_exponent,
  input         io_in_bits_c_sign,
  input  [4:0]  io_in_bits_op,
  input         io_in_bits_fcsr_0,
  input         io_in_bits_fcsr_1,
  input         io_in_bits_fcsr_2,
  input         io_in_bits_fcsr_3,
  input         io_in_bits_fcsr_4,
  input         io_in_bits_fcsr_5,
  input         io_in_bits_fcsr_6,
  input         io_in_bits_fcsr_7,
  input  [11:0] io_in_bits_fflags,
  input         io_in_bits_control_adder,
  input         io_in_bits_control_multiplier,
  input         io_in_bits_control_fused,
  input         io_in_bits_control_divider,
  input         io_in_bits_control_comparator,
  input         io_in_bits_control_squareRoot,
  input         io_in_bits_control_bitInject,
  input         io_in_bits_control_classify,
  input         io_in_bits_stall_stall,
  input  [4:0]  io_in_bits_stall_op,
  input         io_in_bits_stall_pass,
  output        io_out_valid,
  output [25:0] io_out_bits_result_mantissa,
  output [7:0]  io_out_bits_result_exponent,
  output        io_out_bits_result_sign,
  output        io_out_bits_fcsr_0,
  output        io_out_bits_fcsr_1,
  output        io_out_bits_fcsr_2,
  output        io_out_bits_fcsr_3,
  output        io_out_bits_fcsr_4,
  output        io_out_bits_fcsr_5,
  output        io_out_bits_fcsr_6,
  output        io_out_bits_fcsr_7,
  output [4:0]  io_out_bits_op,
  output [4:0]  io_out_bits_stall_op,
  output        io_out_bits_stall_pass,
  output        io_out_bits_stall_inProc
);
  wire  executorAdd_io_in_valid; // @[FPUExecutionUnit.scala 36:30]
  wire [25:0] executorAdd_io_in_bits_a_mantissa; // @[FPUExecutionUnit.scala 36:30]
  wire [7:0] executorAdd_io_in_bits_a_exponent; // @[FPUExecutionUnit.scala 36:30]
  wire  executorAdd_io_in_bits_a_sign; // @[FPUExecutionUnit.scala 36:30]
  wire [25:0] executorAdd_io_in_bits_b_mantissa; // @[FPUExecutionUnit.scala 36:30]
  wire [7:0] executorAdd_io_in_bits_b_exponent; // @[FPUExecutionUnit.scala 36:30]
  wire  executorAdd_io_in_bits_b_sign; // @[FPUExecutionUnit.scala 36:30]
  wire [4:0] executorAdd_io_in_bits_op; // @[FPUExecutionUnit.scala 36:30]
  wire  executorAdd_io_in_bits_fcsr_0; // @[FPUExecutionUnit.scala 36:30]
  wire  executorAdd_io_in_bits_fcsr_1; // @[FPUExecutionUnit.scala 36:30]
  wire  executorAdd_io_in_bits_fcsr_2; // @[FPUExecutionUnit.scala 36:30]
  wire  executorAdd_io_in_bits_fcsr_3; // @[FPUExecutionUnit.scala 36:30]
  wire  executorAdd_io_in_bits_fcsr_4; // @[FPUExecutionUnit.scala 36:30]
  wire  executorAdd_io_in_bits_fcsr_5; // @[FPUExecutionUnit.scala 36:30]
  wire  executorAdd_io_in_bits_fcsr_6; // @[FPUExecutionUnit.scala 36:30]
  wire  executorAdd_io_in_bits_fcsr_7; // @[FPUExecutionUnit.scala 36:30]
  wire [11:0] executorAdd_io_in_bits_fflags; // @[FPUExecutionUnit.scala 36:30]
  wire  executorAdd_io_out_valid; // @[FPUExecutionUnit.scala 36:30]
  wire [25:0] executorAdd_io_out_bits_result_mantissa; // @[FPUExecutionUnit.scala 36:30]
  wire [7:0] executorAdd_io_out_bits_result_exponent; // @[FPUExecutionUnit.scala 36:30]
  wire  executorAdd_io_out_bits_result_sign; // @[FPUExecutionUnit.scala 36:30]
  wire  executorAdd_io_out_bits_fcsr_0; // @[FPUExecutionUnit.scala 36:30]
  wire  executorAdd_io_out_bits_fcsr_1; // @[FPUExecutionUnit.scala 36:30]
  wire  executorAdd_io_out_bits_fcsr_2; // @[FPUExecutionUnit.scala 36:30]
  wire  executorAdd_io_out_bits_fcsr_3; // @[FPUExecutionUnit.scala 36:30]
  wire  executorAdd_io_out_bits_fcsr_4; // @[FPUExecutionUnit.scala 36:30]
  wire  executorAdd_io_out_bits_fcsr_5; // @[FPUExecutionUnit.scala 36:30]
  wire  executorAdd_io_out_bits_fcsr_6; // @[FPUExecutionUnit.scala 36:30]
  wire  executorAdd_io_out_bits_fcsr_7; // @[FPUExecutionUnit.scala 36:30]
  wire [4:0] executorAdd_io_out_bits_op; // @[FPUExecutionUnit.scala 36:30]
  wire  executorMul_clock; // @[FPUExecutionUnit.scala 37:30]
  wire  executorMul_reset; // @[FPUExecutionUnit.scala 37:30]
  wire  executorMul_io_in_valid; // @[FPUExecutionUnit.scala 37:30]
  wire [25:0] executorMul_io_in_bits_a_mantissa; // @[FPUExecutionUnit.scala 37:30]
  wire [7:0] executorMul_io_in_bits_a_exponent; // @[FPUExecutionUnit.scala 37:30]
  wire  executorMul_io_in_bits_a_sign; // @[FPUExecutionUnit.scala 37:30]
  wire [25:0] executorMul_io_in_bits_b_mantissa; // @[FPUExecutionUnit.scala 37:30]
  wire [7:0] executorMul_io_in_bits_b_exponent; // @[FPUExecutionUnit.scala 37:30]
  wire  executorMul_io_in_bits_b_sign; // @[FPUExecutionUnit.scala 37:30]
  wire [25:0] executorMul_io_in_bits_c_mantissa; // @[FPUExecutionUnit.scala 37:30]
  wire [7:0] executorMul_io_in_bits_c_exponent; // @[FPUExecutionUnit.scala 37:30]
  wire  executorMul_io_in_bits_c_sign; // @[FPUExecutionUnit.scala 37:30]
  wire [4:0] executorMul_io_in_bits_op; // @[FPUExecutionUnit.scala 37:30]
  wire  executorMul_io_in_bits_fcsr_0; // @[FPUExecutionUnit.scala 37:30]
  wire  executorMul_io_in_bits_fcsr_1; // @[FPUExecutionUnit.scala 37:30]
  wire  executorMul_io_in_bits_fcsr_2; // @[FPUExecutionUnit.scala 37:30]
  wire  executorMul_io_in_bits_fcsr_3; // @[FPUExecutionUnit.scala 37:30]
  wire  executorMul_io_in_bits_fcsr_4; // @[FPUExecutionUnit.scala 37:30]
  wire  executorMul_io_in_bits_fcsr_5; // @[FPUExecutionUnit.scala 37:30]
  wire  executorMul_io_in_bits_fcsr_6; // @[FPUExecutionUnit.scala 37:30]
  wire  executorMul_io_in_bits_fcsr_7; // @[FPUExecutionUnit.scala 37:30]
  wire [11:0] executorMul_io_in_bits_fflags; // @[FPUExecutionUnit.scala 37:30]
  wire  executorMul_io_out_valid; // @[FPUExecutionUnit.scala 37:30]
  wire [25:0] executorMul_io_out_bits_result_mantissa; // @[FPUExecutionUnit.scala 37:30]
  wire [7:0] executorMul_io_out_bits_result_exponent; // @[FPUExecutionUnit.scala 37:30]
  wire  executorMul_io_out_bits_result_sign; // @[FPUExecutionUnit.scala 37:30]
  wire  executorMul_io_out_bits_fcsr_0; // @[FPUExecutionUnit.scala 37:30]
  wire  executorMul_io_out_bits_fcsr_1; // @[FPUExecutionUnit.scala 37:30]
  wire  executorMul_io_out_bits_fcsr_2; // @[FPUExecutionUnit.scala 37:30]
  wire  executorMul_io_out_bits_fcsr_3; // @[FPUExecutionUnit.scala 37:30]
  wire  executorMul_io_out_bits_fcsr_4; // @[FPUExecutionUnit.scala 37:30]
  wire  executorMul_io_out_bits_fcsr_5; // @[FPUExecutionUnit.scala 37:30]
  wire  executorMul_io_out_bits_fcsr_6; // @[FPUExecutionUnit.scala 37:30]
  wire  executorMul_io_out_bits_fcsr_7; // @[FPUExecutionUnit.scala 37:30]
  wire [4:0] executorMul_io_out_bits_op; // @[FPUExecutionUnit.scala 37:30]
  wire  executorMul_io_out_bits_mulInProc; // @[FPUExecutionUnit.scala 37:30]
  wire  executorDiv_clock; // @[FPUExecutionUnit.scala 38:30]
  wire  executorDiv_reset; // @[FPUExecutionUnit.scala 38:30]
  wire  executorDiv_io_in_valid; // @[FPUExecutionUnit.scala 38:30]
  wire [25:0] executorDiv_io_in_bits_a_mantissa; // @[FPUExecutionUnit.scala 38:30]
  wire [7:0] executorDiv_io_in_bits_a_exponent; // @[FPUExecutionUnit.scala 38:30]
  wire  executorDiv_io_in_bits_a_sign; // @[FPUExecutionUnit.scala 38:30]
  wire [25:0] executorDiv_io_in_bits_b_mantissa; // @[FPUExecutionUnit.scala 38:30]
  wire [7:0] executorDiv_io_in_bits_b_exponent; // @[FPUExecutionUnit.scala 38:30]
  wire  executorDiv_io_in_bits_b_sign; // @[FPUExecutionUnit.scala 38:30]
  wire [4:0] executorDiv_io_in_bits_op; // @[FPUExecutionUnit.scala 38:30]
  wire  executorDiv_io_in_bits_fcsr_0; // @[FPUExecutionUnit.scala 38:30]
  wire  executorDiv_io_in_bits_fcsr_1; // @[FPUExecutionUnit.scala 38:30]
  wire  executorDiv_io_in_bits_fcsr_2; // @[FPUExecutionUnit.scala 38:30]
  wire  executorDiv_io_in_bits_fcsr_3; // @[FPUExecutionUnit.scala 38:30]
  wire  executorDiv_io_in_bits_fcsr_4; // @[FPUExecutionUnit.scala 38:30]
  wire  executorDiv_io_in_bits_fcsr_5; // @[FPUExecutionUnit.scala 38:30]
  wire  executorDiv_io_in_bits_fcsr_6; // @[FPUExecutionUnit.scala 38:30]
  wire  executorDiv_io_in_bits_fcsr_7; // @[FPUExecutionUnit.scala 38:30]
  wire [11:0] executorDiv_io_in_bits_fflags; // @[FPUExecutionUnit.scala 38:30]
  wire  executorDiv_io_out_valid; // @[FPUExecutionUnit.scala 38:30]
  wire [25:0] executorDiv_io_out_bits_result_mantissa; // @[FPUExecutionUnit.scala 38:30]
  wire [7:0] executorDiv_io_out_bits_result_exponent; // @[FPUExecutionUnit.scala 38:30]
  wire  executorDiv_io_out_bits_result_sign; // @[FPUExecutionUnit.scala 38:30]
  wire  executorDiv_io_out_bits_fcsr_0; // @[FPUExecutionUnit.scala 38:30]
  wire  executorDiv_io_out_bits_fcsr_1; // @[FPUExecutionUnit.scala 38:30]
  wire  executorDiv_io_out_bits_fcsr_2; // @[FPUExecutionUnit.scala 38:30]
  wire  executorDiv_io_out_bits_fcsr_3; // @[FPUExecutionUnit.scala 38:30]
  wire  executorDiv_io_out_bits_fcsr_4; // @[FPUExecutionUnit.scala 38:30]
  wire  executorDiv_io_out_bits_fcsr_5; // @[FPUExecutionUnit.scala 38:30]
  wire  executorDiv_io_out_bits_fcsr_6; // @[FPUExecutionUnit.scala 38:30]
  wire  executorDiv_io_out_bits_fcsr_7; // @[FPUExecutionUnit.scala 38:30]
  wire [4:0] executorDiv_io_out_bits_op; // @[FPUExecutionUnit.scala 38:30]
  wire  executorDiv_io_out_bits_divInProc; // @[FPUExecutionUnit.scala 38:30]
  wire  executorSqrt_clock; // @[FPUExecutionUnit.scala 39:30]
  wire  executorSqrt_reset; // @[FPUExecutionUnit.scala 39:30]
  wire  executorSqrt_io_in_valid; // @[FPUExecutionUnit.scala 39:30]
  wire [25:0] executorSqrt_io_in_bits_a_mantissa; // @[FPUExecutionUnit.scala 39:30]
  wire [7:0] executorSqrt_io_in_bits_a_exponent; // @[FPUExecutionUnit.scala 39:30]
  wire  executorSqrt_io_in_bits_a_sign; // @[FPUExecutionUnit.scala 39:30]
  wire [4:0] executorSqrt_io_in_bits_op; // @[FPUExecutionUnit.scala 39:30]
  wire [11:0] executorSqrt_io_in_bits_fflags; // @[FPUExecutionUnit.scala 39:30]
  wire  executorSqrt_io_out_valid; // @[FPUExecutionUnit.scala 39:30]
  wire [25:0] executorSqrt_io_out_bits_result_mantissa; // @[FPUExecutionUnit.scala 39:30]
  wire [7:0] executorSqrt_io_out_bits_result_exponent; // @[FPUExecutionUnit.scala 39:30]
  wire  executorSqrt_io_out_bits_result_sign; // @[FPUExecutionUnit.scala 39:30]
  wire  executorSqrt_io_out_bits_fcsr_4; // @[FPUExecutionUnit.scala 39:30]
  wire [4:0] executorSqrt_io_out_bits_op; // @[FPUExecutionUnit.scala 39:30]
  wire  executorSqrt_io_out_bits_sqrtInProc; // @[FPUExecutionUnit.scala 39:30]
  wire  executorComp_io_in_valid; // @[FPUExecutionUnit.scala 40:30]
  wire [25:0] executorComp_io_in_bits_a_mantissa; // @[FPUExecutionUnit.scala 40:30]
  wire [7:0] executorComp_io_in_bits_a_exponent; // @[FPUExecutionUnit.scala 40:30]
  wire  executorComp_io_in_bits_a_sign; // @[FPUExecutionUnit.scala 40:30]
  wire [25:0] executorComp_io_in_bits_b_mantissa; // @[FPUExecutionUnit.scala 40:30]
  wire [7:0] executorComp_io_in_bits_b_exponent; // @[FPUExecutionUnit.scala 40:30]
  wire  executorComp_io_in_bits_b_sign; // @[FPUExecutionUnit.scala 40:30]
  wire [4:0] executorComp_io_in_bits_op; // @[FPUExecutionUnit.scala 40:30]
  wire  executorComp_io_in_bits_fcsr_0; // @[FPUExecutionUnit.scala 40:30]
  wire  executorComp_io_in_bits_fcsr_1; // @[FPUExecutionUnit.scala 40:30]
  wire  executorComp_io_in_bits_fcsr_2; // @[FPUExecutionUnit.scala 40:30]
  wire  executorComp_io_in_bits_fcsr_3; // @[FPUExecutionUnit.scala 40:30]
  wire  executorComp_io_in_bits_fcsr_4; // @[FPUExecutionUnit.scala 40:30]
  wire  executorComp_io_in_bits_fcsr_5; // @[FPUExecutionUnit.scala 40:30]
  wire  executorComp_io_in_bits_fcsr_6; // @[FPUExecutionUnit.scala 40:30]
  wire  executorComp_io_in_bits_fcsr_7; // @[FPUExecutionUnit.scala 40:30]
  wire [11:0] executorComp_io_in_bits_fflags; // @[FPUExecutionUnit.scala 40:30]
  wire  executorComp_io_out_valid; // @[FPUExecutionUnit.scala 40:30]
  wire [25:0] executorComp_io_out_bits_result_mantissa; // @[FPUExecutionUnit.scala 40:30]
  wire [7:0] executorComp_io_out_bits_result_exponent; // @[FPUExecutionUnit.scala 40:30]
  wire  executorComp_io_out_bits_result_sign; // @[FPUExecutionUnit.scala 40:30]
  wire  executorComp_io_out_bits_fcsr_0; // @[FPUExecutionUnit.scala 40:30]
  wire  executorComp_io_out_bits_fcsr_1; // @[FPUExecutionUnit.scala 40:30]
  wire  executorComp_io_out_bits_fcsr_2; // @[FPUExecutionUnit.scala 40:30]
  wire  executorComp_io_out_bits_fcsr_3; // @[FPUExecutionUnit.scala 40:30]
  wire  executorComp_io_out_bits_fcsr_4; // @[FPUExecutionUnit.scala 40:30]
  wire  executorComp_io_out_bits_fcsr_5; // @[FPUExecutionUnit.scala 40:30]
  wire  executorComp_io_out_bits_fcsr_6; // @[FPUExecutionUnit.scala 40:30]
  wire  executorComp_io_out_bits_fcsr_7; // @[FPUExecutionUnit.scala 40:30]
  wire [4:0] executorComp_io_out_bits_op; // @[FPUExecutionUnit.scala 40:30]
  wire  executorSgn_io_in_valid; // @[FPUExecutionUnit.scala 41:30]
  wire [25:0] executorSgn_io_in_bits_a_mantissa; // @[FPUExecutionUnit.scala 41:30]
  wire [7:0] executorSgn_io_in_bits_a_exponent; // @[FPUExecutionUnit.scala 41:30]
  wire  executorSgn_io_in_bits_a_sign; // @[FPUExecutionUnit.scala 41:30]
  wire  executorSgn_io_in_bits_b_sign; // @[FPUExecutionUnit.scala 41:30]
  wire [4:0] executorSgn_io_in_bits_op; // @[FPUExecutionUnit.scala 41:30]
  wire  executorSgn_io_in_bits_fcsr_0; // @[FPUExecutionUnit.scala 41:30]
  wire  executorSgn_io_in_bits_fcsr_1; // @[FPUExecutionUnit.scala 41:30]
  wire  executorSgn_io_in_bits_fcsr_2; // @[FPUExecutionUnit.scala 41:30]
  wire  executorSgn_io_in_bits_fcsr_3; // @[FPUExecutionUnit.scala 41:30]
  wire  executorSgn_io_in_bits_fcsr_4; // @[FPUExecutionUnit.scala 41:30]
  wire  executorSgn_io_in_bits_fcsr_5; // @[FPUExecutionUnit.scala 41:30]
  wire  executorSgn_io_in_bits_fcsr_6; // @[FPUExecutionUnit.scala 41:30]
  wire  executorSgn_io_in_bits_fcsr_7; // @[FPUExecutionUnit.scala 41:30]
  wire  executorSgn_io_out_valid; // @[FPUExecutionUnit.scala 41:30]
  wire [25:0] executorSgn_io_out_bits_result_mantissa; // @[FPUExecutionUnit.scala 41:30]
  wire [7:0] executorSgn_io_out_bits_result_exponent; // @[FPUExecutionUnit.scala 41:30]
  wire  executorSgn_io_out_bits_result_sign; // @[FPUExecutionUnit.scala 41:30]
  wire  executorSgn_io_out_bits_fcsr_0; // @[FPUExecutionUnit.scala 41:30]
  wire  executorSgn_io_out_bits_fcsr_1; // @[FPUExecutionUnit.scala 41:30]
  wire  executorSgn_io_out_bits_fcsr_2; // @[FPUExecutionUnit.scala 41:30]
  wire  executorSgn_io_out_bits_fcsr_3; // @[FPUExecutionUnit.scala 41:30]
  wire  executorSgn_io_out_bits_fcsr_4; // @[FPUExecutionUnit.scala 41:30]
  wire  executorSgn_io_out_bits_fcsr_5; // @[FPUExecutionUnit.scala 41:30]
  wire  executorSgn_io_out_bits_fcsr_6; // @[FPUExecutionUnit.scala 41:30]
  wire  executorSgn_io_out_bits_fcsr_7; // @[FPUExecutionUnit.scala 41:30]
  wire [4:0] executorSgn_io_out_bits_op; // @[FPUExecutionUnit.scala 41:30]
  wire  executorClass_io_in_valid; // @[FPUExecutionUnit.scala 42:30]
  wire [25:0] executorClass_io_in_bits_a_mantissa; // @[FPUExecutionUnit.scala 42:30]
  wire  executorClass_io_in_bits_a_sign; // @[FPUExecutionUnit.scala 42:30]
  wire [4:0] executorClass_io_in_bits_op; // @[FPUExecutionUnit.scala 42:30]
  wire  executorClass_io_in_bits_fcsr_0; // @[FPUExecutionUnit.scala 42:30]
  wire  executorClass_io_in_bits_fcsr_1; // @[FPUExecutionUnit.scala 42:30]
  wire  executorClass_io_in_bits_fcsr_2; // @[FPUExecutionUnit.scala 42:30]
  wire  executorClass_io_in_bits_fcsr_3; // @[FPUExecutionUnit.scala 42:30]
  wire  executorClass_io_in_bits_fcsr_4; // @[FPUExecutionUnit.scala 42:30]
  wire  executorClass_io_in_bits_fcsr_5; // @[FPUExecutionUnit.scala 42:30]
  wire  executorClass_io_in_bits_fcsr_6; // @[FPUExecutionUnit.scala 42:30]
  wire  executorClass_io_in_bits_fcsr_7; // @[FPUExecutionUnit.scala 42:30]
  wire [11:0] executorClass_io_in_bits_fflags; // @[FPUExecutionUnit.scala 42:30]
  wire  executorClass_io_out_valid; // @[FPUExecutionUnit.scala 42:30]
  wire [25:0] executorClass_io_out_bits_result_mantissa; // @[FPUExecutionUnit.scala 42:30]
  wire  executorClass_io_out_bits_fcsr_0; // @[FPUExecutionUnit.scala 42:30]
  wire  executorClass_io_out_bits_fcsr_1; // @[FPUExecutionUnit.scala 42:30]
  wire  executorClass_io_out_bits_fcsr_2; // @[FPUExecutionUnit.scala 42:30]
  wire  executorClass_io_out_bits_fcsr_3; // @[FPUExecutionUnit.scala 42:30]
  wire  executorClass_io_out_bits_fcsr_4; // @[FPUExecutionUnit.scala 42:30]
  wire  executorClass_io_out_bits_fcsr_5; // @[FPUExecutionUnit.scala 42:30]
  wire  executorClass_io_out_bits_fcsr_6; // @[FPUExecutionUnit.scala 42:30]
  wire  executorClass_io_out_bits_fcsr_7; // @[FPUExecutionUnit.scala 42:30]
  wire [4:0] executorClass_io_out_bits_op; // @[FPUExecutionUnit.scala 42:30]
  wire  passthrough_io_in_valid; // @[FPUExecutionUnit.scala 43:30]
  wire [25:0] passthrough_io_in_bits_a_mantissa; // @[FPUExecutionUnit.scala 43:30]
  wire [7:0] passthrough_io_in_bits_a_exponent; // @[FPUExecutionUnit.scala 43:30]
  wire  passthrough_io_in_bits_a_sign; // @[FPUExecutionUnit.scala 43:30]
  wire [4:0] passthrough_io_in_bits_op; // @[FPUExecutionUnit.scala 43:30]
  wire  passthrough_io_in_bits_fcsr_0; // @[FPUExecutionUnit.scala 43:30]
  wire  passthrough_io_in_bits_fcsr_1; // @[FPUExecutionUnit.scala 43:30]
  wire  passthrough_io_in_bits_fcsr_2; // @[FPUExecutionUnit.scala 43:30]
  wire  passthrough_io_in_bits_fcsr_3; // @[FPUExecutionUnit.scala 43:30]
  wire  passthrough_io_in_bits_fcsr_4; // @[FPUExecutionUnit.scala 43:30]
  wire  passthrough_io_in_bits_fcsr_5; // @[FPUExecutionUnit.scala 43:30]
  wire  passthrough_io_in_bits_fcsr_6; // @[FPUExecutionUnit.scala 43:30]
  wire  passthrough_io_in_bits_fcsr_7; // @[FPUExecutionUnit.scala 43:30]
  wire  passthrough_io_out_valid; // @[FPUExecutionUnit.scala 43:30]
  wire [25:0] passthrough_io_out_bits_result_mantissa; // @[FPUExecutionUnit.scala 43:30]
  wire [7:0] passthrough_io_out_bits_result_exponent; // @[FPUExecutionUnit.scala 43:30]
  wire  passthrough_io_out_bits_result_sign; // @[FPUExecutionUnit.scala 43:30]
  wire  passthrough_io_out_bits_fcsr_0; // @[FPUExecutionUnit.scala 43:30]
  wire  passthrough_io_out_bits_fcsr_1; // @[FPUExecutionUnit.scala 43:30]
  wire  passthrough_io_out_bits_fcsr_2; // @[FPUExecutionUnit.scala 43:30]
  wire  passthrough_io_out_bits_fcsr_3; // @[FPUExecutionUnit.scala 43:30]
  wire  passthrough_io_out_bits_fcsr_4; // @[FPUExecutionUnit.scala 43:30]
  wire  passthrough_io_out_bits_fcsr_5; // @[FPUExecutionUnit.scala 43:30]
  wire  passthrough_io_out_bits_fcsr_6; // @[FPUExecutionUnit.scala 43:30]
  wire  passthrough_io_out_bits_fcsr_7; // @[FPUExecutionUnit.scala 43:30]
  wire [4:0] passthrough_io_out_bits_op; // @[FPUExecutionUnit.scala 43:30]
  wire  _T_1 = ~io_in_bits_stall_stall; // @[FPUExecutionUnit.scala 82:10]
  wire  _T_2 = ~io_in_bits_stall_stall & io_in_bits_stall_pass; // @[FPUExecutionUnit.scala 82:34]
  wire  _T_3 = io_in_bits_stall_stall & io_in_bits_stall_pass | _T_2; // @[FPUExecutionUnit.scala 81:59]
  wire [25:0] _GEN_0 = io_in_bits_control_classify ? io_in_bits_a_mantissa : 26'h0; // @[FPUExecutionUnit.scala 132:46 133:40 45:27]
  wire  _GEN_2 = io_in_bits_control_classify & io_in_bits_a_sign; // @[FPUExecutionUnit.scala 132:46 133:40 45:27]
  wire [4:0] _GEN_9 = io_in_bits_control_classify ? io_in_bits_op : 5'h16; // @[FPUExecutionUnit.scala 132:46 136:40 45:27]
  wire  _GEN_10 = io_in_bits_control_classify & io_in_bits_fcsr_0; // @[FPUExecutionUnit.scala 132:46 137:40 45:27]
  wire  _GEN_11 = io_in_bits_control_classify & io_in_bits_fcsr_1; // @[FPUExecutionUnit.scala 132:46 137:40 45:27]
  wire  _GEN_12 = io_in_bits_control_classify & io_in_bits_fcsr_2; // @[FPUExecutionUnit.scala 132:46 137:40 45:27]
  wire  _GEN_13 = io_in_bits_control_classify & io_in_bits_fcsr_3; // @[FPUExecutionUnit.scala 132:46 137:40 45:27]
  wire  _GEN_14 = io_in_bits_control_classify & io_in_bits_fcsr_4; // @[FPUExecutionUnit.scala 132:46 137:40 45:27]
  wire  _GEN_15 = io_in_bits_control_classify & io_in_bits_fcsr_5; // @[FPUExecutionUnit.scala 132:46 137:40 45:27]
  wire  _GEN_16 = io_in_bits_control_classify & io_in_bits_fcsr_6; // @[FPUExecutionUnit.scala 132:46 137:40 45:27]
  wire  _GEN_17 = io_in_bits_control_classify & io_in_bits_fcsr_7; // @[FPUExecutionUnit.scala 132:46 137:40 45:27]
  wire [11:0] _GEN_18 = io_in_bits_control_classify ? io_in_bits_fflags : 12'h0; // @[FPUExecutionUnit.scala 132:46 138:40 45:27]
  wire  _GEN_19 = io_in_bits_control_classify & io_in_valid; // @[FPUExecutionUnit.scala 132:46 139:40 45:27]
  wire [25:0] _GEN_20 = io_in_bits_control_classify ? 26'h0 : io_in_bits_a_mantissa; // @[FPUExecutionUnit.scala 132:46 45:27 141:38]
  wire [7:0] _GEN_21 = io_in_bits_control_classify ? $signed(8'sh0) : $signed(io_in_bits_a_exponent); // @[FPUExecutionUnit.scala 132:46 45:27 141:38]
  wire  _GEN_22 = io_in_bits_control_classify ? 1'h0 : io_in_bits_a_sign; // @[FPUExecutionUnit.scala 132:46 45:27 141:38]
  wire [4:0] _GEN_29 = io_in_bits_control_classify ? 5'h16 : io_in_bits_op; // @[FPUExecutionUnit.scala 132:46 45:27 144:38]
  wire  _GEN_30 = io_in_bits_control_classify ? 1'h0 : io_in_bits_fcsr_0; // @[FPUExecutionUnit.scala 132:46 45:27 145:38]
  wire  _GEN_31 = io_in_bits_control_classify ? 1'h0 : io_in_bits_fcsr_1; // @[FPUExecutionUnit.scala 132:46 45:27 145:38]
  wire  _GEN_32 = io_in_bits_control_classify ? 1'h0 : io_in_bits_fcsr_2; // @[FPUExecutionUnit.scala 132:46 45:27 145:38]
  wire  _GEN_33 = io_in_bits_control_classify ? 1'h0 : io_in_bits_fcsr_3; // @[FPUExecutionUnit.scala 132:46 45:27 145:38]
  wire  _GEN_34 = io_in_bits_control_classify ? 1'h0 : io_in_bits_fcsr_4; // @[FPUExecutionUnit.scala 132:46 45:27 145:38]
  wire  _GEN_35 = io_in_bits_control_classify ? 1'h0 : io_in_bits_fcsr_5; // @[FPUExecutionUnit.scala 132:46 45:27 145:38]
  wire  _GEN_36 = io_in_bits_control_classify ? 1'h0 : io_in_bits_fcsr_6; // @[FPUExecutionUnit.scala 132:46 45:27 145:38]
  wire  _GEN_37 = io_in_bits_control_classify ? 1'h0 : io_in_bits_fcsr_7; // @[FPUExecutionUnit.scala 132:46 45:27 145:38]
  wire  _GEN_38 = io_in_bits_control_classify ? 1'h0 : io_in_valid; // @[FPUExecutionUnit.scala 132:46 45:27 146:38]
  wire [25:0] _GEN_40 = io_in_bits_control_bitInject ? io_in_bits_a_mantissa : 26'h0; // @[FPUExecutionUnit.scala 124:47 125:38 45:27]
  wire [7:0] _GEN_41 = io_in_bits_control_bitInject ? $signed(io_in_bits_a_exponent) : $signed(8'sh0); // @[FPUExecutionUnit.scala 124:47 125:38 45:27]
  wire  _GEN_42 = io_in_bits_control_bitInject & io_in_bits_a_sign; // @[FPUExecutionUnit.scala 124:47 125:38 45:27]
  wire  _GEN_45 = io_in_bits_control_bitInject & io_in_bits_b_sign; // @[FPUExecutionUnit.scala 124:47 126:38 45:27]
  wire [4:0] _GEN_49 = io_in_bits_control_bitInject ? io_in_bits_op : 5'h16; // @[FPUExecutionUnit.scala 124:47 128:38 45:27]
  wire  _GEN_50 = io_in_bits_control_bitInject & io_in_bits_fcsr_0; // @[FPUExecutionUnit.scala 124:47 129:38 45:27]
  wire  _GEN_51 = io_in_bits_control_bitInject & io_in_bits_fcsr_1; // @[FPUExecutionUnit.scala 124:47 129:38 45:27]
  wire  _GEN_52 = io_in_bits_control_bitInject & io_in_bits_fcsr_2; // @[FPUExecutionUnit.scala 124:47 129:38 45:27]
  wire  _GEN_53 = io_in_bits_control_bitInject & io_in_bits_fcsr_3; // @[FPUExecutionUnit.scala 124:47 129:38 45:27]
  wire  _GEN_54 = io_in_bits_control_bitInject & io_in_bits_fcsr_4; // @[FPUExecutionUnit.scala 124:47 129:38 45:27]
  wire  _GEN_55 = io_in_bits_control_bitInject & io_in_bits_fcsr_5; // @[FPUExecutionUnit.scala 124:47 129:38 45:27]
  wire  _GEN_56 = io_in_bits_control_bitInject & io_in_bits_fcsr_6; // @[FPUExecutionUnit.scala 124:47 129:38 45:27]
  wire  _GEN_57 = io_in_bits_control_bitInject & io_in_bits_fcsr_7; // @[FPUExecutionUnit.scala 124:47 129:38 45:27]
  wire  _GEN_59 = io_in_bits_control_bitInject & io_in_valid; // @[FPUExecutionUnit.scala 124:47 131:38 45:27]
  wire [25:0] _GEN_60 = io_in_bits_control_bitInject ? 26'h0 : _GEN_0; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire  _GEN_62 = io_in_bits_control_bitInject ? 1'h0 : _GEN_2; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire [4:0] _GEN_69 = io_in_bits_control_bitInject ? 5'h16 : _GEN_9; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire  _GEN_70 = io_in_bits_control_bitInject ? 1'h0 : _GEN_10; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire  _GEN_71 = io_in_bits_control_bitInject ? 1'h0 : _GEN_11; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire  _GEN_72 = io_in_bits_control_bitInject ? 1'h0 : _GEN_12; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire  _GEN_73 = io_in_bits_control_bitInject ? 1'h0 : _GEN_13; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire  _GEN_74 = io_in_bits_control_bitInject ? 1'h0 : _GEN_14; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire  _GEN_75 = io_in_bits_control_bitInject ? 1'h0 : _GEN_15; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire  _GEN_76 = io_in_bits_control_bitInject ? 1'h0 : _GEN_16; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire  _GEN_77 = io_in_bits_control_bitInject ? 1'h0 : _GEN_17; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire [11:0] _GEN_78 = io_in_bits_control_bitInject ? 12'h0 : _GEN_18; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire  _GEN_79 = io_in_bits_control_bitInject ? 1'h0 : _GEN_19; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire [25:0] _GEN_80 = io_in_bits_control_bitInject ? 26'h0 : _GEN_20; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire [7:0] _GEN_81 = io_in_bits_control_bitInject ? $signed(8'sh0) : $signed(_GEN_21); // @[FPUExecutionUnit.scala 124:47 45:27]
  wire  _GEN_82 = io_in_bits_control_bitInject ? 1'h0 : _GEN_22; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire [4:0] _GEN_89 = io_in_bits_control_bitInject ? 5'h16 : _GEN_29; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire  _GEN_90 = io_in_bits_control_bitInject ? 1'h0 : _GEN_30; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire  _GEN_91 = io_in_bits_control_bitInject ? 1'h0 : _GEN_31; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire  _GEN_92 = io_in_bits_control_bitInject ? 1'h0 : _GEN_32; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire  _GEN_93 = io_in_bits_control_bitInject ? 1'h0 : _GEN_33; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire  _GEN_94 = io_in_bits_control_bitInject ? 1'h0 : _GEN_34; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire  _GEN_95 = io_in_bits_control_bitInject ? 1'h0 : _GEN_35; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire  _GEN_96 = io_in_bits_control_bitInject ? 1'h0 : _GEN_36; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire  _GEN_97 = io_in_bits_control_bitInject ? 1'h0 : _GEN_37; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire  _GEN_98 = io_in_bits_control_bitInject ? 1'h0 : _GEN_38; // @[FPUExecutionUnit.scala 124:47 45:27]
  wire [25:0] _GEN_100 = io_in_bits_control_squareRoot ? io_in_bits_a_mantissa : 26'h0; // @[FPUExecutionUnit.scala 116:49 117:39 45:27]
  wire [7:0] _GEN_101 = io_in_bits_control_squareRoot ? $signed(io_in_bits_a_exponent) : $signed(8'sh0); // @[FPUExecutionUnit.scala 116:49 117:39 45:27]
  wire  _GEN_102 = io_in_bits_control_squareRoot & io_in_bits_a_sign; // @[FPUExecutionUnit.scala 116:49 117:39 45:27]
  wire [4:0] _GEN_109 = io_in_bits_control_squareRoot ? io_in_bits_op : 5'h16; // @[FPUExecutionUnit.scala 116:49 120:39 45:27]
  wire [11:0] _GEN_118 = io_in_bits_control_squareRoot ? io_in_bits_fflags : 12'h0; // @[FPUExecutionUnit.scala 116:49 122:39 45:27]
  wire  _GEN_119 = io_in_bits_control_squareRoot & io_in_valid; // @[FPUExecutionUnit.scala 116:49 123:39 45:27]
  wire [25:0] _GEN_120 = io_in_bits_control_squareRoot ? 26'h0 : _GEN_40; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire [7:0] _GEN_121 = io_in_bits_control_squareRoot ? $signed(8'sh0) : $signed(_GEN_41); // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_122 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_42; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_125 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_45; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire [4:0] _GEN_129 = io_in_bits_control_squareRoot ? 5'h16 : _GEN_49; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_130 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_50; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_131 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_51; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_132 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_52; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_133 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_53; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_134 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_54; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_135 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_55; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_136 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_56; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_137 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_57; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_139 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_59; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire [25:0] _GEN_140 = io_in_bits_control_squareRoot ? 26'h0 : _GEN_60; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_142 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_62; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire [4:0] _GEN_149 = io_in_bits_control_squareRoot ? 5'h16 : _GEN_69; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_150 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_70; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_151 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_71; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_152 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_72; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_153 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_73; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_154 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_74; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_155 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_75; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_156 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_76; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_157 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_77; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire [11:0] _GEN_158 = io_in_bits_control_squareRoot ? 12'h0 : _GEN_78; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_159 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_79; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire [25:0] _GEN_160 = io_in_bits_control_squareRoot ? 26'h0 : _GEN_80; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire [7:0] _GEN_161 = io_in_bits_control_squareRoot ? $signed(8'sh0) : $signed(_GEN_81); // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_162 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_82; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire [4:0] _GEN_169 = io_in_bits_control_squareRoot ? 5'h16 : _GEN_89; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_170 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_90; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_171 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_91; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_172 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_92; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_173 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_93; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_174 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_94; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_175 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_95; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_176 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_96; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_177 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_97; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire  _GEN_178 = io_in_bits_control_squareRoot ? 1'h0 : _GEN_98; // @[FPUExecutionUnit.scala 116:49 45:27]
  wire [25:0] _GEN_180 = io_in_bits_control_comparator ? io_in_bits_a_mantissa : 26'h0; // @[FPUExecutionUnit.scala 108:49 109:39 45:27]
  wire [7:0] _GEN_181 = io_in_bits_control_comparator ? $signed(io_in_bits_a_exponent) : $signed(8'sh0); // @[FPUExecutionUnit.scala 108:49 109:39 45:27]
  wire  _GEN_182 = io_in_bits_control_comparator & io_in_bits_a_sign; // @[FPUExecutionUnit.scala 108:49 109:39 45:27]
  wire [25:0] _GEN_183 = io_in_bits_control_comparator ? io_in_bits_b_mantissa : 26'h0; // @[FPUExecutionUnit.scala 108:49 110:39 45:27]
  wire [7:0] _GEN_184 = io_in_bits_control_comparator ? $signed(io_in_bits_b_exponent) : $signed(8'sh0); // @[FPUExecutionUnit.scala 108:49 110:39 45:27]
  wire  _GEN_185 = io_in_bits_control_comparator & io_in_bits_b_sign; // @[FPUExecutionUnit.scala 108:49 110:39 45:27]
  wire [4:0] _GEN_189 = io_in_bits_control_comparator ? io_in_bits_op : 5'h16; // @[FPUExecutionUnit.scala 108:49 112:39 45:27]
  wire  _GEN_190 = io_in_bits_control_comparator & io_in_bits_fcsr_0; // @[FPUExecutionUnit.scala 108:49 113:39 45:27]
  wire  _GEN_191 = io_in_bits_control_comparator & io_in_bits_fcsr_1; // @[FPUExecutionUnit.scala 108:49 113:39 45:27]
  wire  _GEN_192 = io_in_bits_control_comparator & io_in_bits_fcsr_2; // @[FPUExecutionUnit.scala 108:49 113:39 45:27]
  wire  _GEN_193 = io_in_bits_control_comparator & io_in_bits_fcsr_3; // @[FPUExecutionUnit.scala 108:49 113:39 45:27]
  wire  _GEN_194 = io_in_bits_control_comparator & io_in_bits_fcsr_4; // @[FPUExecutionUnit.scala 108:49 113:39 45:27]
  wire  _GEN_195 = io_in_bits_control_comparator & io_in_bits_fcsr_5; // @[FPUExecutionUnit.scala 108:49 113:39 45:27]
  wire  _GEN_196 = io_in_bits_control_comparator & io_in_bits_fcsr_6; // @[FPUExecutionUnit.scala 108:49 113:39 45:27]
  wire  _GEN_197 = io_in_bits_control_comparator & io_in_bits_fcsr_7; // @[FPUExecutionUnit.scala 108:49 113:39 45:27]
  wire [11:0] _GEN_198 = io_in_bits_control_comparator ? io_in_bits_fflags : 12'h0; // @[FPUExecutionUnit.scala 108:49 114:39 45:27]
  wire  _GEN_199 = io_in_bits_control_comparator & io_in_valid; // @[FPUExecutionUnit.scala 108:49 115:39 45:27]
  wire [25:0] _GEN_200 = io_in_bits_control_comparator ? 26'h0 : _GEN_100; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire [7:0] _GEN_201 = io_in_bits_control_comparator ? $signed(8'sh0) : $signed(_GEN_101); // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_202 = io_in_bits_control_comparator ? 1'h0 : _GEN_102; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire [4:0] _GEN_209 = io_in_bits_control_comparator ? 5'h16 : _GEN_109; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire [11:0] _GEN_218 = io_in_bits_control_comparator ? 12'h0 : _GEN_118; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_219 = io_in_bits_control_comparator ? 1'h0 : _GEN_119; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire [25:0] _GEN_220 = io_in_bits_control_comparator ? 26'h0 : _GEN_120; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire [7:0] _GEN_221 = io_in_bits_control_comparator ? $signed(8'sh0) : $signed(_GEN_121); // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_222 = io_in_bits_control_comparator ? 1'h0 : _GEN_122; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_225 = io_in_bits_control_comparator ? 1'h0 : _GEN_125; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire [4:0] _GEN_229 = io_in_bits_control_comparator ? 5'h16 : _GEN_129; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_230 = io_in_bits_control_comparator ? 1'h0 : _GEN_130; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_231 = io_in_bits_control_comparator ? 1'h0 : _GEN_131; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_232 = io_in_bits_control_comparator ? 1'h0 : _GEN_132; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_233 = io_in_bits_control_comparator ? 1'h0 : _GEN_133; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_234 = io_in_bits_control_comparator ? 1'h0 : _GEN_134; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_235 = io_in_bits_control_comparator ? 1'h0 : _GEN_135; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_236 = io_in_bits_control_comparator ? 1'h0 : _GEN_136; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_237 = io_in_bits_control_comparator ? 1'h0 : _GEN_137; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_239 = io_in_bits_control_comparator ? 1'h0 : _GEN_139; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire [25:0] _GEN_240 = io_in_bits_control_comparator ? 26'h0 : _GEN_140; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_242 = io_in_bits_control_comparator ? 1'h0 : _GEN_142; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire [4:0] _GEN_249 = io_in_bits_control_comparator ? 5'h16 : _GEN_149; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_250 = io_in_bits_control_comparator ? 1'h0 : _GEN_150; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_251 = io_in_bits_control_comparator ? 1'h0 : _GEN_151; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_252 = io_in_bits_control_comparator ? 1'h0 : _GEN_152; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_253 = io_in_bits_control_comparator ? 1'h0 : _GEN_153; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_254 = io_in_bits_control_comparator ? 1'h0 : _GEN_154; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_255 = io_in_bits_control_comparator ? 1'h0 : _GEN_155; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_256 = io_in_bits_control_comparator ? 1'h0 : _GEN_156; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_257 = io_in_bits_control_comparator ? 1'h0 : _GEN_157; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire [11:0] _GEN_258 = io_in_bits_control_comparator ? 12'h0 : _GEN_158; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_259 = io_in_bits_control_comparator ? 1'h0 : _GEN_159; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire [25:0] _GEN_260 = io_in_bits_control_comparator ? 26'h0 : _GEN_160; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire [7:0] _GEN_261 = io_in_bits_control_comparator ? $signed(8'sh0) : $signed(_GEN_161); // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_262 = io_in_bits_control_comparator ? 1'h0 : _GEN_162; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire [4:0] _GEN_269 = io_in_bits_control_comparator ? 5'h16 : _GEN_169; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_270 = io_in_bits_control_comparator ? 1'h0 : _GEN_170; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_271 = io_in_bits_control_comparator ? 1'h0 : _GEN_171; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_272 = io_in_bits_control_comparator ? 1'h0 : _GEN_172; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_273 = io_in_bits_control_comparator ? 1'h0 : _GEN_173; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_274 = io_in_bits_control_comparator ? 1'h0 : _GEN_174; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_275 = io_in_bits_control_comparator ? 1'h0 : _GEN_175; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_276 = io_in_bits_control_comparator ? 1'h0 : _GEN_176; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_277 = io_in_bits_control_comparator ? 1'h0 : _GEN_177; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire  _GEN_278 = io_in_bits_control_comparator ? 1'h0 : _GEN_178; // @[FPUExecutionUnit.scala 108:49 45:27]
  wire [25:0] _GEN_280 = io_in_bits_control_divider ? io_in_bits_a_mantissa : 26'h0; // @[FPUExecutionUnit.scala 100:46 101:38 45:27]
  wire [7:0] _GEN_281 = io_in_bits_control_divider ? $signed(io_in_bits_a_exponent) : $signed(8'sh0); // @[FPUExecutionUnit.scala 100:46 101:38 45:27]
  wire  _GEN_282 = io_in_bits_control_divider & io_in_bits_a_sign; // @[FPUExecutionUnit.scala 100:46 101:38 45:27]
  wire [25:0] _GEN_283 = io_in_bits_control_divider ? io_in_bits_b_mantissa : 26'h0; // @[FPUExecutionUnit.scala 100:46 102:38 45:27]
  wire [7:0] _GEN_284 = io_in_bits_control_divider ? $signed(io_in_bits_b_exponent) : $signed(8'sh0); // @[FPUExecutionUnit.scala 100:46 102:38 45:27]
  wire  _GEN_285 = io_in_bits_control_divider & io_in_bits_b_sign; // @[FPUExecutionUnit.scala 100:46 102:38 45:27]
  wire [4:0] _GEN_289 = io_in_bits_control_divider ? io_in_bits_op : 5'h16; // @[FPUExecutionUnit.scala 100:46 104:38 45:27]
  wire  _GEN_290 = io_in_bits_control_divider & io_in_bits_fcsr_0; // @[FPUExecutionUnit.scala 100:46 105:38 45:27]
  wire  _GEN_291 = io_in_bits_control_divider & io_in_bits_fcsr_1; // @[FPUExecutionUnit.scala 100:46 105:38 45:27]
  wire  _GEN_292 = io_in_bits_control_divider & io_in_bits_fcsr_2; // @[FPUExecutionUnit.scala 100:46 105:38 45:27]
  wire  _GEN_293 = io_in_bits_control_divider & io_in_bits_fcsr_3; // @[FPUExecutionUnit.scala 100:46 105:38 45:27]
  wire  _GEN_294 = io_in_bits_control_divider & io_in_bits_fcsr_4; // @[FPUExecutionUnit.scala 100:46 105:38 45:27]
  wire  _GEN_295 = io_in_bits_control_divider & io_in_bits_fcsr_5; // @[FPUExecutionUnit.scala 100:46 105:38 45:27]
  wire  _GEN_296 = io_in_bits_control_divider & io_in_bits_fcsr_6; // @[FPUExecutionUnit.scala 100:46 105:38 45:27]
  wire  _GEN_297 = io_in_bits_control_divider & io_in_bits_fcsr_7; // @[FPUExecutionUnit.scala 100:46 105:38 45:27]
  wire [11:0] _GEN_298 = io_in_bits_control_divider ? io_in_bits_fflags : 12'h0; // @[FPUExecutionUnit.scala 100:46 106:38 45:27]
  wire  _GEN_299 = io_in_bits_control_divider & io_in_valid; // @[FPUExecutionUnit.scala 100:46 107:38 45:27]
  wire [25:0] _GEN_300 = io_in_bits_control_divider ? 26'h0 : _GEN_180; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire [7:0] _GEN_301 = io_in_bits_control_divider ? $signed(8'sh0) : $signed(_GEN_181); // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_302 = io_in_bits_control_divider ? 1'h0 : _GEN_182; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire [25:0] _GEN_303 = io_in_bits_control_divider ? 26'h0 : _GEN_183; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire [7:0] _GEN_304 = io_in_bits_control_divider ? $signed(8'sh0) : $signed(_GEN_184); // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_305 = io_in_bits_control_divider ? 1'h0 : _GEN_185; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire [4:0] _GEN_309 = io_in_bits_control_divider ? 5'h16 : _GEN_189; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_310 = io_in_bits_control_divider ? 1'h0 : _GEN_190; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_311 = io_in_bits_control_divider ? 1'h0 : _GEN_191; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_312 = io_in_bits_control_divider ? 1'h0 : _GEN_192; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_313 = io_in_bits_control_divider ? 1'h0 : _GEN_193; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_314 = io_in_bits_control_divider ? 1'h0 : _GEN_194; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_315 = io_in_bits_control_divider ? 1'h0 : _GEN_195; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_316 = io_in_bits_control_divider ? 1'h0 : _GEN_196; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_317 = io_in_bits_control_divider ? 1'h0 : _GEN_197; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire [11:0] _GEN_318 = io_in_bits_control_divider ? 12'h0 : _GEN_198; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_319 = io_in_bits_control_divider ? 1'h0 : _GEN_199; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire [25:0] _GEN_320 = io_in_bits_control_divider ? 26'h0 : _GEN_200; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire [7:0] _GEN_321 = io_in_bits_control_divider ? $signed(8'sh0) : $signed(_GEN_201); // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_322 = io_in_bits_control_divider ? 1'h0 : _GEN_202; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire [4:0] _GEN_329 = io_in_bits_control_divider ? 5'h16 : _GEN_209; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire [11:0] _GEN_338 = io_in_bits_control_divider ? 12'h0 : _GEN_218; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_339 = io_in_bits_control_divider ? 1'h0 : _GEN_219; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire [25:0] _GEN_340 = io_in_bits_control_divider ? 26'h0 : _GEN_220; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire [7:0] _GEN_341 = io_in_bits_control_divider ? $signed(8'sh0) : $signed(_GEN_221); // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_342 = io_in_bits_control_divider ? 1'h0 : _GEN_222; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_345 = io_in_bits_control_divider ? 1'h0 : _GEN_225; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire [4:0] _GEN_349 = io_in_bits_control_divider ? 5'h16 : _GEN_229; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_350 = io_in_bits_control_divider ? 1'h0 : _GEN_230; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_351 = io_in_bits_control_divider ? 1'h0 : _GEN_231; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_352 = io_in_bits_control_divider ? 1'h0 : _GEN_232; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_353 = io_in_bits_control_divider ? 1'h0 : _GEN_233; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_354 = io_in_bits_control_divider ? 1'h0 : _GEN_234; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_355 = io_in_bits_control_divider ? 1'h0 : _GEN_235; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_356 = io_in_bits_control_divider ? 1'h0 : _GEN_236; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_357 = io_in_bits_control_divider ? 1'h0 : _GEN_237; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_359 = io_in_bits_control_divider ? 1'h0 : _GEN_239; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire [25:0] _GEN_360 = io_in_bits_control_divider ? 26'h0 : _GEN_240; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_362 = io_in_bits_control_divider ? 1'h0 : _GEN_242; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire [4:0] _GEN_369 = io_in_bits_control_divider ? 5'h16 : _GEN_249; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_370 = io_in_bits_control_divider ? 1'h0 : _GEN_250; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_371 = io_in_bits_control_divider ? 1'h0 : _GEN_251; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_372 = io_in_bits_control_divider ? 1'h0 : _GEN_252; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_373 = io_in_bits_control_divider ? 1'h0 : _GEN_253; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_374 = io_in_bits_control_divider ? 1'h0 : _GEN_254; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_375 = io_in_bits_control_divider ? 1'h0 : _GEN_255; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_376 = io_in_bits_control_divider ? 1'h0 : _GEN_256; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_377 = io_in_bits_control_divider ? 1'h0 : _GEN_257; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire [11:0] _GEN_378 = io_in_bits_control_divider ? 12'h0 : _GEN_258; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_379 = io_in_bits_control_divider ? 1'h0 : _GEN_259; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire [25:0] _GEN_380 = io_in_bits_control_divider ? 26'h0 : _GEN_260; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire [7:0] _GEN_381 = io_in_bits_control_divider ? $signed(8'sh0) : $signed(_GEN_261); // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_382 = io_in_bits_control_divider ? 1'h0 : _GEN_262; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire [4:0] _GEN_389 = io_in_bits_control_divider ? 5'h16 : _GEN_269; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_390 = io_in_bits_control_divider ? 1'h0 : _GEN_270; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_391 = io_in_bits_control_divider ? 1'h0 : _GEN_271; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_392 = io_in_bits_control_divider ? 1'h0 : _GEN_272; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_393 = io_in_bits_control_divider ? 1'h0 : _GEN_273; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_394 = io_in_bits_control_divider ? 1'h0 : _GEN_274; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_395 = io_in_bits_control_divider ? 1'h0 : _GEN_275; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_396 = io_in_bits_control_divider ? 1'h0 : _GEN_276; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_397 = io_in_bits_control_divider ? 1'h0 : _GEN_277; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire  _GEN_398 = io_in_bits_control_divider ? 1'h0 : _GEN_278; // @[FPUExecutionUnit.scala 100:46 45:27]
  wire [25:0] _GEN_400 = io_in_bits_control_multiplier | io_in_bits_control_fused ? io_in_bits_a_mantissa : 26'h0; // @[FPUExecutionUnit.scala 45:27 92:76 93:38]
  wire [7:0] _GEN_401 = io_in_bits_control_multiplier | io_in_bits_control_fused ? $signed(io_in_bits_a_exponent) :
    $signed(8'sh0); // @[FPUExecutionUnit.scala 45:27 92:76 93:38]
  wire  _GEN_402 = (io_in_bits_control_multiplier | io_in_bits_control_fused) & io_in_bits_a_sign; // @[FPUExecutionUnit.scala 45:27 92:76 93:38]
  wire [25:0] _GEN_403 = io_in_bits_control_multiplier | io_in_bits_control_fused ? io_in_bits_b_mantissa : 26'h0; // @[FPUExecutionUnit.scala 45:27 92:76 94:38]
  wire [7:0] _GEN_404 = io_in_bits_control_multiplier | io_in_bits_control_fused ? $signed(io_in_bits_b_exponent) :
    $signed(8'sh0); // @[FPUExecutionUnit.scala 45:27 92:76 94:38]
  wire  _GEN_405 = (io_in_bits_control_multiplier | io_in_bits_control_fused) & io_in_bits_b_sign; // @[FPUExecutionUnit.scala 45:27 92:76 94:38]
  wire [25:0] _GEN_406 = io_in_bits_control_multiplier | io_in_bits_control_fused ? io_in_bits_c_mantissa : 26'h0; // @[FPUExecutionUnit.scala 45:27 92:76 95:38]
  wire [7:0] _GEN_407 = io_in_bits_control_multiplier | io_in_bits_control_fused ? $signed(io_in_bits_c_exponent) :
    $signed(8'sh0); // @[FPUExecutionUnit.scala 45:27 92:76 95:38]
  wire  _GEN_408 = (io_in_bits_control_multiplier | io_in_bits_control_fused) & io_in_bits_c_sign; // @[FPUExecutionUnit.scala 45:27 92:76 95:38]
  wire [4:0] _GEN_409 = io_in_bits_control_multiplier | io_in_bits_control_fused ? io_in_bits_op : 5'h16; // @[FPUExecutionUnit.scala 45:27 92:76 96:38]
  wire  _GEN_410 = (io_in_bits_control_multiplier | io_in_bits_control_fused) & io_in_bits_fcsr_0; // @[FPUExecutionUnit.scala 45:27 92:76 97:38]
  wire  _GEN_411 = (io_in_bits_control_multiplier | io_in_bits_control_fused) & io_in_bits_fcsr_1; // @[FPUExecutionUnit.scala 45:27 92:76 97:38]
  wire  _GEN_412 = (io_in_bits_control_multiplier | io_in_bits_control_fused) & io_in_bits_fcsr_2; // @[FPUExecutionUnit.scala 45:27 92:76 97:38]
  wire  _GEN_413 = (io_in_bits_control_multiplier | io_in_bits_control_fused) & io_in_bits_fcsr_3; // @[FPUExecutionUnit.scala 45:27 92:76 97:38]
  wire  _GEN_414 = (io_in_bits_control_multiplier | io_in_bits_control_fused) & io_in_bits_fcsr_4; // @[FPUExecutionUnit.scala 45:27 92:76 97:38]
  wire  _GEN_415 = (io_in_bits_control_multiplier | io_in_bits_control_fused) & io_in_bits_fcsr_5; // @[FPUExecutionUnit.scala 45:27 92:76 97:38]
  wire  _GEN_416 = (io_in_bits_control_multiplier | io_in_bits_control_fused) & io_in_bits_fcsr_6; // @[FPUExecutionUnit.scala 45:27 92:76 97:38]
  wire  _GEN_417 = (io_in_bits_control_multiplier | io_in_bits_control_fused) & io_in_bits_fcsr_7; // @[FPUExecutionUnit.scala 45:27 92:76 97:38]
  wire [11:0] _GEN_418 = io_in_bits_control_multiplier | io_in_bits_control_fused ? io_in_bits_fflags : 12'h0; // @[FPUExecutionUnit.scala 45:27 92:76 98:38]
  wire  _GEN_419 = (io_in_bits_control_multiplier | io_in_bits_control_fused) & io_in_valid; // @[FPUExecutionUnit.scala 45:27 92:76 99:38]
  wire [25:0] _GEN_420 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 26'h0 : _GEN_280; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [7:0] _GEN_421 = io_in_bits_control_multiplier | io_in_bits_control_fused ? $signed(8'sh0) : $signed(_GEN_281); // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_422 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_282; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [25:0] _GEN_423 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 26'h0 : _GEN_283; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [7:0] _GEN_424 = io_in_bits_control_multiplier | io_in_bits_control_fused ? $signed(8'sh0) : $signed(_GEN_284); // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_425 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_285; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [4:0] _GEN_429 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 5'h16 : _GEN_289; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_430 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_290; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_431 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_291; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_432 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_292; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_433 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_293; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_434 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_294; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_435 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_295; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_436 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_296; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_437 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_297; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [11:0] _GEN_438 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 12'h0 : _GEN_298; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_439 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_299; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [25:0] _GEN_440 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 26'h0 : _GEN_300; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [7:0] _GEN_441 = io_in_bits_control_multiplier | io_in_bits_control_fused ? $signed(8'sh0) : $signed(_GEN_301); // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_442 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_302; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [25:0] _GEN_443 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 26'h0 : _GEN_303; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [7:0] _GEN_444 = io_in_bits_control_multiplier | io_in_bits_control_fused ? $signed(8'sh0) : $signed(_GEN_304); // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_445 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_305; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [4:0] _GEN_449 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 5'h16 : _GEN_309; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_450 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_310; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_451 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_311; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_452 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_312; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_453 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_313; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_454 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_314; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_455 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_315; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_456 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_316; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_457 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_317; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [11:0] _GEN_458 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 12'h0 : _GEN_318; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_459 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_319; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [25:0] _GEN_460 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 26'h0 : _GEN_320; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [7:0] _GEN_461 = io_in_bits_control_multiplier | io_in_bits_control_fused ? $signed(8'sh0) : $signed(_GEN_321); // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_462 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_322; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [4:0] _GEN_469 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 5'h16 : _GEN_329; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [11:0] _GEN_478 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 12'h0 : _GEN_338; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_479 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_339; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [25:0] _GEN_480 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 26'h0 : _GEN_340; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [7:0] _GEN_481 = io_in_bits_control_multiplier | io_in_bits_control_fused ? $signed(8'sh0) : $signed(_GEN_341); // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_482 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_342; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_485 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_345; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [4:0] _GEN_489 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 5'h16 : _GEN_349; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_490 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_350; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_491 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_351; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_492 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_352; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_493 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_353; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_494 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_354; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_495 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_355; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_496 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_356; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_497 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_357; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_499 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_359; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [25:0] _GEN_500 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 26'h0 : _GEN_360; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_502 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_362; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [4:0] _GEN_509 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 5'h16 : _GEN_369; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_510 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_370; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_511 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_371; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_512 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_372; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_513 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_373; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_514 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_374; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_515 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_375; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_516 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_376; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_517 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_377; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [11:0] _GEN_518 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 12'h0 : _GEN_378; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_519 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_379; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [25:0] _GEN_520 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 26'h0 : _GEN_380; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [7:0] _GEN_521 = io_in_bits_control_multiplier | io_in_bits_control_fused ? $signed(8'sh0) : $signed(_GEN_381); // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_522 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_382; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [4:0] _GEN_529 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 5'h16 : _GEN_389; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_530 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_390; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_531 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_391; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_532 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_392; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_533 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_393; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_534 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_394; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_535 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_395; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_536 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_396; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_537 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_397; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire  _GEN_538 = io_in_bits_control_multiplier | io_in_bits_control_fused ? 1'h0 : _GEN_398; // @[FPUExecutionUnit.scala 45:27 92:76]
  wire [25:0] _GEN_540 = io_in_bits_control_adder ? io_in_bits_a_mantissa : 26'h0; // @[FPUExecutionUnit.scala 45:27 84:37 85:38]
  wire [7:0] _GEN_541 = io_in_bits_control_adder ? $signed(io_in_bits_a_exponent) : $signed(8'sh0); // @[FPUExecutionUnit.scala 45:27 84:37 85:38]
  wire  _GEN_542 = io_in_bits_control_adder & io_in_bits_a_sign; // @[FPUExecutionUnit.scala 45:27 84:37 85:38]
  wire [25:0] _GEN_543 = io_in_bits_control_adder ? io_in_bits_b_mantissa : 26'h0; // @[FPUExecutionUnit.scala 45:27 84:37 86:38]
  wire [7:0] _GEN_544 = io_in_bits_control_adder ? $signed(io_in_bits_b_exponent) : $signed(8'sh0); // @[FPUExecutionUnit.scala 45:27 84:37 86:38]
  wire  _GEN_545 = io_in_bits_control_adder & io_in_bits_b_sign; // @[FPUExecutionUnit.scala 45:27 84:37 86:38]
  wire [4:0] _GEN_549 = io_in_bits_control_adder ? io_in_bits_op : 5'h16; // @[FPUExecutionUnit.scala 45:27 84:37 88:38]
  wire  _GEN_550 = io_in_bits_control_adder & io_in_bits_fcsr_0; // @[FPUExecutionUnit.scala 45:27 84:37 89:38]
  wire  _GEN_551 = io_in_bits_control_adder & io_in_bits_fcsr_1; // @[FPUExecutionUnit.scala 45:27 84:37 89:38]
  wire  _GEN_552 = io_in_bits_control_adder & io_in_bits_fcsr_2; // @[FPUExecutionUnit.scala 45:27 84:37 89:38]
  wire  _GEN_553 = io_in_bits_control_adder & io_in_bits_fcsr_3; // @[FPUExecutionUnit.scala 45:27 84:37 89:38]
  wire  _GEN_554 = io_in_bits_control_adder & io_in_bits_fcsr_4; // @[FPUExecutionUnit.scala 45:27 84:37 89:38]
  wire  _GEN_555 = io_in_bits_control_adder & io_in_bits_fcsr_5; // @[FPUExecutionUnit.scala 45:27 84:37 89:38]
  wire  _GEN_556 = io_in_bits_control_adder & io_in_bits_fcsr_6; // @[FPUExecutionUnit.scala 45:27 84:37 89:38]
  wire  _GEN_557 = io_in_bits_control_adder & io_in_bits_fcsr_7; // @[FPUExecutionUnit.scala 45:27 84:37 89:38]
  wire [11:0] _GEN_558 = io_in_bits_control_adder ? io_in_bits_fflags : 12'h0; // @[FPUExecutionUnit.scala 45:27 84:37 90:38]
  wire  _GEN_559 = io_in_bits_control_adder & io_in_valid; // @[FPUExecutionUnit.scala 45:27 84:37 91:38]
  wire [25:0] _GEN_560 = io_in_bits_control_adder ? 26'h0 : _GEN_400; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [7:0] _GEN_561 = io_in_bits_control_adder ? $signed(8'sh0) : $signed(_GEN_401); // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_562 = io_in_bits_control_adder ? 1'h0 : _GEN_402; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [25:0] _GEN_563 = io_in_bits_control_adder ? 26'h0 : _GEN_403; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [7:0] _GEN_564 = io_in_bits_control_adder ? $signed(8'sh0) : $signed(_GEN_404); // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_565 = io_in_bits_control_adder ? 1'h0 : _GEN_405; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [25:0] _GEN_566 = io_in_bits_control_adder ? 26'h0 : _GEN_406; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [7:0] _GEN_567 = io_in_bits_control_adder ? $signed(8'sh0) : $signed(_GEN_407); // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_568 = io_in_bits_control_adder ? 1'h0 : _GEN_408; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [4:0] _GEN_569 = io_in_bits_control_adder ? 5'h16 : _GEN_409; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_570 = io_in_bits_control_adder ? 1'h0 : _GEN_410; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_571 = io_in_bits_control_adder ? 1'h0 : _GEN_411; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_572 = io_in_bits_control_adder ? 1'h0 : _GEN_412; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_573 = io_in_bits_control_adder ? 1'h0 : _GEN_413; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_574 = io_in_bits_control_adder ? 1'h0 : _GEN_414; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_575 = io_in_bits_control_adder ? 1'h0 : _GEN_415; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_576 = io_in_bits_control_adder ? 1'h0 : _GEN_416; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_577 = io_in_bits_control_adder ? 1'h0 : _GEN_417; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [11:0] _GEN_578 = io_in_bits_control_adder ? 12'h0 : _GEN_418; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_579 = io_in_bits_control_adder ? 1'h0 : _GEN_419; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [25:0] _GEN_580 = io_in_bits_control_adder ? 26'h0 : _GEN_420; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [7:0] _GEN_581 = io_in_bits_control_adder ? $signed(8'sh0) : $signed(_GEN_421); // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_582 = io_in_bits_control_adder ? 1'h0 : _GEN_422; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [25:0] _GEN_583 = io_in_bits_control_adder ? 26'h0 : _GEN_423; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [7:0] _GEN_584 = io_in_bits_control_adder ? $signed(8'sh0) : $signed(_GEN_424); // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_585 = io_in_bits_control_adder ? 1'h0 : _GEN_425; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [4:0] _GEN_589 = io_in_bits_control_adder ? 5'h16 : _GEN_429; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_590 = io_in_bits_control_adder ? 1'h0 : _GEN_430; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_591 = io_in_bits_control_adder ? 1'h0 : _GEN_431; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_592 = io_in_bits_control_adder ? 1'h0 : _GEN_432; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_593 = io_in_bits_control_adder ? 1'h0 : _GEN_433; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_594 = io_in_bits_control_adder ? 1'h0 : _GEN_434; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_595 = io_in_bits_control_adder ? 1'h0 : _GEN_435; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_596 = io_in_bits_control_adder ? 1'h0 : _GEN_436; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_597 = io_in_bits_control_adder ? 1'h0 : _GEN_437; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [11:0] _GEN_598 = io_in_bits_control_adder ? 12'h0 : _GEN_438; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_599 = io_in_bits_control_adder ? 1'h0 : _GEN_439; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [25:0] _GEN_600 = io_in_bits_control_adder ? 26'h0 : _GEN_440; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [7:0] _GEN_601 = io_in_bits_control_adder ? $signed(8'sh0) : $signed(_GEN_441); // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_602 = io_in_bits_control_adder ? 1'h0 : _GEN_442; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [25:0] _GEN_603 = io_in_bits_control_adder ? 26'h0 : _GEN_443; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [7:0] _GEN_604 = io_in_bits_control_adder ? $signed(8'sh0) : $signed(_GEN_444); // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_605 = io_in_bits_control_adder ? 1'h0 : _GEN_445; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [4:0] _GEN_609 = io_in_bits_control_adder ? 5'h16 : _GEN_449; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_610 = io_in_bits_control_adder ? 1'h0 : _GEN_450; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_611 = io_in_bits_control_adder ? 1'h0 : _GEN_451; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_612 = io_in_bits_control_adder ? 1'h0 : _GEN_452; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_613 = io_in_bits_control_adder ? 1'h0 : _GEN_453; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_614 = io_in_bits_control_adder ? 1'h0 : _GEN_454; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_615 = io_in_bits_control_adder ? 1'h0 : _GEN_455; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_616 = io_in_bits_control_adder ? 1'h0 : _GEN_456; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_617 = io_in_bits_control_adder ? 1'h0 : _GEN_457; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [11:0] _GEN_618 = io_in_bits_control_adder ? 12'h0 : _GEN_458; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_619 = io_in_bits_control_adder ? 1'h0 : _GEN_459; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [25:0] _GEN_620 = io_in_bits_control_adder ? 26'h0 : _GEN_460; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [7:0] _GEN_621 = io_in_bits_control_adder ? $signed(8'sh0) : $signed(_GEN_461); // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_622 = io_in_bits_control_adder ? 1'h0 : _GEN_462; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [4:0] _GEN_629 = io_in_bits_control_adder ? 5'h16 : _GEN_469; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [11:0] _GEN_638 = io_in_bits_control_adder ? 12'h0 : _GEN_478; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_639 = io_in_bits_control_adder ? 1'h0 : _GEN_479; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [25:0] _GEN_640 = io_in_bits_control_adder ? 26'h0 : _GEN_480; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [7:0] _GEN_641 = io_in_bits_control_adder ? $signed(8'sh0) : $signed(_GEN_481); // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_642 = io_in_bits_control_adder ? 1'h0 : _GEN_482; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_645 = io_in_bits_control_adder ? 1'h0 : _GEN_485; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [4:0] _GEN_649 = io_in_bits_control_adder ? 5'h16 : _GEN_489; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_650 = io_in_bits_control_adder ? 1'h0 : _GEN_490; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_651 = io_in_bits_control_adder ? 1'h0 : _GEN_491; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_652 = io_in_bits_control_adder ? 1'h0 : _GEN_492; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_653 = io_in_bits_control_adder ? 1'h0 : _GEN_493; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_654 = io_in_bits_control_adder ? 1'h0 : _GEN_494; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_655 = io_in_bits_control_adder ? 1'h0 : _GEN_495; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_656 = io_in_bits_control_adder ? 1'h0 : _GEN_496; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_657 = io_in_bits_control_adder ? 1'h0 : _GEN_497; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_659 = io_in_bits_control_adder ? 1'h0 : _GEN_499; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [25:0] _GEN_660 = io_in_bits_control_adder ? 26'h0 : _GEN_500; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_662 = io_in_bits_control_adder ? 1'h0 : _GEN_502; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [4:0] _GEN_669 = io_in_bits_control_adder ? 5'h16 : _GEN_509; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_670 = io_in_bits_control_adder ? 1'h0 : _GEN_510; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_671 = io_in_bits_control_adder ? 1'h0 : _GEN_511; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_672 = io_in_bits_control_adder ? 1'h0 : _GEN_512; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_673 = io_in_bits_control_adder ? 1'h0 : _GEN_513; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_674 = io_in_bits_control_adder ? 1'h0 : _GEN_514; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_675 = io_in_bits_control_adder ? 1'h0 : _GEN_515; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_676 = io_in_bits_control_adder ? 1'h0 : _GEN_516; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_677 = io_in_bits_control_adder ? 1'h0 : _GEN_517; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [11:0] _GEN_678 = io_in_bits_control_adder ? 12'h0 : _GEN_518; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_679 = io_in_bits_control_adder ? 1'h0 : _GEN_519; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [25:0] _GEN_680 = io_in_bits_control_adder ? 26'h0 : _GEN_520; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [7:0] _GEN_681 = io_in_bits_control_adder ? $signed(8'sh0) : $signed(_GEN_521); // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_682 = io_in_bits_control_adder ? 1'h0 : _GEN_522; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire [4:0] _GEN_689 = io_in_bits_control_adder ? 5'h16 : _GEN_529; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_690 = io_in_bits_control_adder ? 1'h0 : _GEN_530; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_691 = io_in_bits_control_adder ? 1'h0 : _GEN_531; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_692 = io_in_bits_control_adder ? 1'h0 : _GEN_532; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_693 = io_in_bits_control_adder ? 1'h0 : _GEN_533; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_694 = io_in_bits_control_adder ? 1'h0 : _GEN_534; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_695 = io_in_bits_control_adder ? 1'h0 : _GEN_535; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_696 = io_in_bits_control_adder ? 1'h0 : _GEN_536; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_697 = io_in_bits_control_adder ? 1'h0 : _GEN_537; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  _GEN_698 = io_in_bits_control_adder ? 1'h0 : _GEN_538; // @[FPUExecutionUnit.scala 45:27 84:37]
  wire  inProc = executorMul_io_out_bits_mulInProc | executorDiv_io_out_bits_divInProc |
    executorSqrt_io_out_bits_sqrtInProc; // @[FPUExecutionUnit.scala 166:87]
  wire  submoduleOut_0_valid = executorAdd_io_out_valid; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_0_bits_result_sign = executorAdd_io_out_bits_result_sign; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_1_valid = executorMul_io_out_valid; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_1_bits_result_sign = executorMul_io_out_bits_result_sign; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_2_valid = executorDiv_io_out_valid; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_2_bits_result_sign = executorDiv_io_out_bits_result_sign; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_3_valid = executorSqrt_io_out_valid; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_3_bits_result_sign = executorSqrt_io_out_bits_result_sign; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_4_valid = executorComp_io_out_valid; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_4_bits_result_sign = executorComp_io_out_bits_result_sign; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_5_valid = executorSgn_io_out_valid; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_5_bits_result_sign = executorSgn_io_out_bits_result_sign; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_6_valid = executorClass_io_out_valid; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_7_valid = passthrough_io_out_valid; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_7_bits_result_sign = passthrough_io_out_bits_result_sign; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire [7:0] _io_out_bits_result_T_31 = submoduleOut_0_valid ? $signed(executorAdd_io_out_bits_result_exponent) :
    $signed(8'sh0); // @[Mux.scala 27:73]
  wire [7:0] _io_out_bits_result_T_32 = submoduleOut_1_valid ? $signed(executorMul_io_out_bits_result_exponent) :
    $signed(8'sh0); // @[Mux.scala 27:73]
  wire [7:0] _io_out_bits_result_T_33 = submoduleOut_2_valid ? $signed(executorDiv_io_out_bits_result_exponent) :
    $signed(8'sh0); // @[Mux.scala 27:73]
  wire [7:0] _io_out_bits_result_T_34 = submoduleOut_3_valid ? $signed(executorSqrt_io_out_bits_result_exponent) :
    $signed(8'sh0); // @[Mux.scala 27:73]
  wire [7:0] _io_out_bits_result_T_35 = submoduleOut_4_valid ? $signed(executorComp_io_out_bits_result_exponent) :
    $signed(8'sh0); // @[Mux.scala 27:73]
  wire [7:0] _io_out_bits_result_T_36 = submoduleOut_5_valid ? $signed(executorSgn_io_out_bits_result_exponent) :
    $signed(8'sh0); // @[Mux.scala 27:73]
  wire [7:0] _io_out_bits_result_T_38 = submoduleOut_7_valid ? $signed(passthrough_io_out_bits_result_exponent) :
    $signed(8'sh0); // @[Mux.scala 27:73]
  wire [7:0] _io_out_bits_result_T_40 = $signed(_io_out_bits_result_T_31) | $signed(_io_out_bits_result_T_32); // @[Mux.scala 27:73]
  wire [7:0] _io_out_bits_result_T_42 = $signed(_io_out_bits_result_T_40) | $signed(_io_out_bits_result_T_33); // @[Mux.scala 27:73]
  wire [7:0] _io_out_bits_result_T_44 = $signed(_io_out_bits_result_T_42) | $signed(_io_out_bits_result_T_34); // @[Mux.scala 27:73]
  wire [7:0] _io_out_bits_result_T_46 = $signed(_io_out_bits_result_T_44) | $signed(_io_out_bits_result_T_35); // @[Mux.scala 27:73]
  wire [7:0] _io_out_bits_result_T_50 = $signed(_io_out_bits_result_T_46) | $signed(_io_out_bits_result_T_36); // @[Mux.scala 27:73]
  wire [25:0] submoduleOut_0_bits_result_mantissa = executorAdd_io_out_bits_result_mantissa; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire [25:0] _io_out_bits_result_T_55 = submoduleOut_0_valid ? submoduleOut_0_bits_result_mantissa : 26'h0; // @[Mux.scala 27:73]
  wire [25:0] submoduleOut_1_bits_result_mantissa = executorMul_io_out_bits_result_mantissa; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire [25:0] _io_out_bits_result_T_56 = submoduleOut_1_valid ? submoduleOut_1_bits_result_mantissa : 26'h0; // @[Mux.scala 27:73]
  wire [25:0] submoduleOut_2_bits_result_mantissa = executorDiv_io_out_bits_result_mantissa; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire [25:0] _io_out_bits_result_T_57 = submoduleOut_2_valid ? submoduleOut_2_bits_result_mantissa : 26'h0; // @[Mux.scala 27:73]
  wire [25:0] submoduleOut_3_bits_result_mantissa = executorSqrt_io_out_bits_result_mantissa; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire [25:0] _io_out_bits_result_T_58 = submoduleOut_3_valid ? submoduleOut_3_bits_result_mantissa : 26'h0; // @[Mux.scala 27:73]
  wire [25:0] submoduleOut_4_bits_result_mantissa = executorComp_io_out_bits_result_mantissa; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire [25:0] _io_out_bits_result_T_59 = submoduleOut_4_valid ? submoduleOut_4_bits_result_mantissa : 26'h0; // @[Mux.scala 27:73]
  wire [25:0] submoduleOut_5_bits_result_mantissa = executorSgn_io_out_bits_result_mantissa; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire [25:0] _io_out_bits_result_T_60 = submoduleOut_5_valid ? submoduleOut_5_bits_result_mantissa : 26'h0; // @[Mux.scala 27:73]
  wire [25:0] submoduleOut_6_bits_result_mantissa = executorClass_io_out_bits_result_mantissa; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire [25:0] _io_out_bits_result_T_61 = submoduleOut_6_valid ? submoduleOut_6_bits_result_mantissa : 26'h0; // @[Mux.scala 27:73]
  wire [25:0] submoduleOut_7_bits_result_mantissa = passthrough_io_out_bits_result_mantissa; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire [25:0] _io_out_bits_result_T_62 = submoduleOut_7_valid ? submoduleOut_7_bits_result_mantissa : 26'h0; // @[Mux.scala 27:73]
  wire [25:0] _io_out_bits_result_T_63 = _io_out_bits_result_T_55 | _io_out_bits_result_T_56; // @[Mux.scala 27:73]
  wire [25:0] _io_out_bits_result_T_64 = _io_out_bits_result_T_63 | _io_out_bits_result_T_57; // @[Mux.scala 27:73]
  wire [25:0] _io_out_bits_result_T_65 = _io_out_bits_result_T_64 | _io_out_bits_result_T_58; // @[Mux.scala 27:73]
  wire [25:0] _io_out_bits_result_T_66 = _io_out_bits_result_T_65 | _io_out_bits_result_T_59; // @[Mux.scala 27:73]
  wire [25:0] _io_out_bits_result_T_67 = _io_out_bits_result_T_66 | _io_out_bits_result_T_60; // @[Mux.scala 27:73]
  wire [25:0] _io_out_bits_result_T_68 = _io_out_bits_result_T_67 | _io_out_bits_result_T_61; // @[Mux.scala 27:73]
  wire  submoduleOut_0_bits_fcsr_0 = executorAdd_io_out_bits_fcsr_0; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_1_bits_fcsr_0 = executorMul_io_out_bits_fcsr_0; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_2_bits_fcsr_0 = executorDiv_io_out_bits_fcsr_0; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_4_bits_fcsr_0 = executorComp_io_out_bits_fcsr_0; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_5_bits_fcsr_0 = executorSgn_io_out_bits_fcsr_0; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_6_bits_fcsr_0 = executorClass_io_out_bits_fcsr_0; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_7_bits_fcsr_0 = passthrough_io_out_bits_fcsr_0; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_0_bits_fcsr_1 = executorAdd_io_out_bits_fcsr_1; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_1_bits_fcsr_1 = executorMul_io_out_bits_fcsr_1; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_2_bits_fcsr_1 = executorDiv_io_out_bits_fcsr_1; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_4_bits_fcsr_1 = executorComp_io_out_bits_fcsr_1; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_5_bits_fcsr_1 = executorSgn_io_out_bits_fcsr_1; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_6_bits_fcsr_1 = executorClass_io_out_bits_fcsr_1; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_7_bits_fcsr_1 = passthrough_io_out_bits_fcsr_1; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_0_bits_fcsr_2 = executorAdd_io_out_bits_fcsr_2; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_1_bits_fcsr_2 = executorMul_io_out_bits_fcsr_2; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_2_bits_fcsr_2 = executorDiv_io_out_bits_fcsr_2; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_4_bits_fcsr_2 = executorComp_io_out_bits_fcsr_2; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_5_bits_fcsr_2 = executorSgn_io_out_bits_fcsr_2; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_6_bits_fcsr_2 = executorClass_io_out_bits_fcsr_2; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_7_bits_fcsr_2 = passthrough_io_out_bits_fcsr_2; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_0_bits_fcsr_3 = executorAdd_io_out_bits_fcsr_3; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_1_bits_fcsr_3 = executorMul_io_out_bits_fcsr_3; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_2_bits_fcsr_3 = executorDiv_io_out_bits_fcsr_3; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_4_bits_fcsr_3 = executorComp_io_out_bits_fcsr_3; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_5_bits_fcsr_3 = executorSgn_io_out_bits_fcsr_3; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_6_bits_fcsr_3 = executorClass_io_out_bits_fcsr_3; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_7_bits_fcsr_3 = passthrough_io_out_bits_fcsr_3; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_0_bits_fcsr_4 = executorAdd_io_out_bits_fcsr_4; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_1_bits_fcsr_4 = executorMul_io_out_bits_fcsr_4; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_2_bits_fcsr_4 = executorDiv_io_out_bits_fcsr_4; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_3_bits_fcsr_4 = executorSqrt_io_out_bits_fcsr_4; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_4_bits_fcsr_4 = executorComp_io_out_bits_fcsr_4; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_5_bits_fcsr_4 = executorSgn_io_out_bits_fcsr_4; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_6_bits_fcsr_4 = executorClass_io_out_bits_fcsr_4; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_7_bits_fcsr_4 = passthrough_io_out_bits_fcsr_4; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_0_bits_fcsr_5 = executorAdd_io_out_bits_fcsr_5; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_1_bits_fcsr_5 = executorMul_io_out_bits_fcsr_5; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_2_bits_fcsr_5 = executorDiv_io_out_bits_fcsr_5; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_4_bits_fcsr_5 = executorComp_io_out_bits_fcsr_5; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_5_bits_fcsr_5 = executorSgn_io_out_bits_fcsr_5; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_6_bits_fcsr_5 = executorClass_io_out_bits_fcsr_5; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_7_bits_fcsr_5 = passthrough_io_out_bits_fcsr_5; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_0_bits_fcsr_6 = executorAdd_io_out_bits_fcsr_6; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_1_bits_fcsr_6 = executorMul_io_out_bits_fcsr_6; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_2_bits_fcsr_6 = executorDiv_io_out_bits_fcsr_6; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_4_bits_fcsr_6 = executorComp_io_out_bits_fcsr_6; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_5_bits_fcsr_6 = executorSgn_io_out_bits_fcsr_6; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_6_bits_fcsr_6 = executorClass_io_out_bits_fcsr_6; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_7_bits_fcsr_6 = passthrough_io_out_bits_fcsr_6; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_0_bits_fcsr_7 = executorAdd_io_out_bits_fcsr_7; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_1_bits_fcsr_7 = executorMul_io_out_bits_fcsr_7; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_2_bits_fcsr_7 = executorDiv_io_out_bits_fcsr_7; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_4_bits_fcsr_7 = executorComp_io_out_bits_fcsr_7; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_5_bits_fcsr_7 = executorSgn_io_out_bits_fcsr_7; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_6_bits_fcsr_7 = executorClass_io_out_bits_fcsr_7; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire  submoduleOut_7_bits_fcsr_7 = passthrough_io_out_bits_fcsr_7; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire [4:0] submoduleOut_0_bits_op = executorAdd_io_out_bits_op; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire [4:0] _io_out_bits_op_T_1 = submoduleOut_0_valid ? submoduleOut_0_bits_op : 5'h0; // @[Mux.scala 27:73]
  wire [4:0] submoduleOut_1_bits_op = executorMul_io_out_bits_op; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire [4:0] _io_out_bits_op_T_3 = submoduleOut_1_valid ? submoduleOut_1_bits_op : 5'h0; // @[Mux.scala 27:73]
  wire [4:0] submoduleOut_2_bits_op = executorDiv_io_out_bits_op; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire [4:0] _io_out_bits_op_T_5 = submoduleOut_2_valid ? submoduleOut_2_bits_op : 5'h0; // @[Mux.scala 27:73]
  wire [4:0] submoduleOut_3_bits_op = executorSqrt_io_out_bits_op; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire [4:0] _io_out_bits_op_T_7 = submoduleOut_3_valid ? submoduleOut_3_bits_op : 5'h0; // @[Mux.scala 27:73]
  wire [4:0] submoduleOut_4_bits_op = executorComp_io_out_bits_op; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire [4:0] _io_out_bits_op_T_9 = submoduleOut_4_valid ? submoduleOut_4_bits_op : 5'h0; // @[Mux.scala 27:73]
  wire [4:0] submoduleOut_5_bits_op = executorSgn_io_out_bits_op; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire [4:0] _io_out_bits_op_T_11 = submoduleOut_5_valid ? submoduleOut_5_bits_op : 5'h0; // @[Mux.scala 27:73]
  wire [4:0] submoduleOut_6_bits_op = executorClass_io_out_bits_op; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire [4:0] _io_out_bits_op_T_13 = submoduleOut_6_valid ? submoduleOut_6_bits_op : 5'h0; // @[Mux.scala 27:73]
  wire [4:0] submoduleOut_7_bits_op = passthrough_io_out_bits_op; // @[FPUExecutionUnit.scala 153:{29,29}]
  wire [4:0] _io_out_bits_op_T_15 = submoduleOut_7_valid ? submoduleOut_7_bits_op : 5'h0; // @[Mux.scala 27:73]
  wire [4:0] _io_out_bits_op_T_16 = _io_out_bits_op_T_1 | _io_out_bits_op_T_3; // @[Mux.scala 27:73]
  wire [4:0] _io_out_bits_op_T_17 = _io_out_bits_op_T_16 | _io_out_bits_op_T_5; // @[Mux.scala 27:73]
  wire [4:0] _io_out_bits_op_T_18 = _io_out_bits_op_T_17 | _io_out_bits_op_T_7; // @[Mux.scala 27:73]
  wire [4:0] _io_out_bits_op_T_19 = _io_out_bits_op_T_18 | _io_out_bits_op_T_9; // @[Mux.scala 27:73]
  wire [4:0] _io_out_bits_op_T_20 = _io_out_bits_op_T_19 | _io_out_bits_op_T_11; // @[Mux.scala 27:73]
  wire [4:0] _io_out_bits_op_T_21 = _io_out_bits_op_T_20 | _io_out_bits_op_T_13; // @[Mux.scala 27:73]
  SPFPAdd executorAdd ( // @[FPUExecutionUnit.scala 36:30]
    .io_in_valid(executorAdd_io_in_valid),
    .io_in_bits_a_mantissa(executorAdd_io_in_bits_a_mantissa),
    .io_in_bits_a_exponent(executorAdd_io_in_bits_a_exponent),
    .io_in_bits_a_sign(executorAdd_io_in_bits_a_sign),
    .io_in_bits_b_mantissa(executorAdd_io_in_bits_b_mantissa),
    .io_in_bits_b_exponent(executorAdd_io_in_bits_b_exponent),
    .io_in_bits_b_sign(executorAdd_io_in_bits_b_sign),
    .io_in_bits_op(executorAdd_io_in_bits_op),
    .io_in_bits_fcsr_0(executorAdd_io_in_bits_fcsr_0),
    .io_in_bits_fcsr_1(executorAdd_io_in_bits_fcsr_1),
    .io_in_bits_fcsr_2(executorAdd_io_in_bits_fcsr_2),
    .io_in_bits_fcsr_3(executorAdd_io_in_bits_fcsr_3),
    .io_in_bits_fcsr_4(executorAdd_io_in_bits_fcsr_4),
    .io_in_bits_fcsr_5(executorAdd_io_in_bits_fcsr_5),
    .io_in_bits_fcsr_6(executorAdd_io_in_bits_fcsr_6),
    .io_in_bits_fcsr_7(executorAdd_io_in_bits_fcsr_7),
    .io_in_bits_fflags(executorAdd_io_in_bits_fflags),
    .io_out_valid(executorAdd_io_out_valid),
    .io_out_bits_result_mantissa(executorAdd_io_out_bits_result_mantissa),
    .io_out_bits_result_exponent(executorAdd_io_out_bits_result_exponent),
    .io_out_bits_result_sign(executorAdd_io_out_bits_result_sign),
    .io_out_bits_fcsr_0(executorAdd_io_out_bits_fcsr_0),
    .io_out_bits_fcsr_1(executorAdd_io_out_bits_fcsr_1),
    .io_out_bits_fcsr_2(executorAdd_io_out_bits_fcsr_2),
    .io_out_bits_fcsr_3(executorAdd_io_out_bits_fcsr_3),
    .io_out_bits_fcsr_4(executorAdd_io_out_bits_fcsr_4),
    .io_out_bits_fcsr_5(executorAdd_io_out_bits_fcsr_5),
    .io_out_bits_fcsr_6(executorAdd_io_out_bits_fcsr_6),
    .io_out_bits_fcsr_7(executorAdd_io_out_bits_fcsr_7),
    .io_out_bits_op(executorAdd_io_out_bits_op)
  );
  SPFPMul executorMul ( // @[FPUExecutionUnit.scala 37:30]
    .clock(executorMul_clock),
    .reset(executorMul_reset),
    .io_in_valid(executorMul_io_in_valid),
    .io_in_bits_a_mantissa(executorMul_io_in_bits_a_mantissa),
    .io_in_bits_a_exponent(executorMul_io_in_bits_a_exponent),
    .io_in_bits_a_sign(executorMul_io_in_bits_a_sign),
    .io_in_bits_b_mantissa(executorMul_io_in_bits_b_mantissa),
    .io_in_bits_b_exponent(executorMul_io_in_bits_b_exponent),
    .io_in_bits_b_sign(executorMul_io_in_bits_b_sign),
    .io_in_bits_c_mantissa(executorMul_io_in_bits_c_mantissa),
    .io_in_bits_c_exponent(executorMul_io_in_bits_c_exponent),
    .io_in_bits_c_sign(executorMul_io_in_bits_c_sign),
    .io_in_bits_op(executorMul_io_in_bits_op),
    .io_in_bits_fcsr_0(executorMul_io_in_bits_fcsr_0),
    .io_in_bits_fcsr_1(executorMul_io_in_bits_fcsr_1),
    .io_in_bits_fcsr_2(executorMul_io_in_bits_fcsr_2),
    .io_in_bits_fcsr_3(executorMul_io_in_bits_fcsr_3),
    .io_in_bits_fcsr_4(executorMul_io_in_bits_fcsr_4),
    .io_in_bits_fcsr_5(executorMul_io_in_bits_fcsr_5),
    .io_in_bits_fcsr_6(executorMul_io_in_bits_fcsr_6),
    .io_in_bits_fcsr_7(executorMul_io_in_bits_fcsr_7),
    .io_in_bits_fflags(executorMul_io_in_bits_fflags),
    .io_out_valid(executorMul_io_out_valid),
    .io_out_bits_result_mantissa(executorMul_io_out_bits_result_mantissa),
    .io_out_bits_result_exponent(executorMul_io_out_bits_result_exponent),
    .io_out_bits_result_sign(executorMul_io_out_bits_result_sign),
    .io_out_bits_fcsr_0(executorMul_io_out_bits_fcsr_0),
    .io_out_bits_fcsr_1(executorMul_io_out_bits_fcsr_1),
    .io_out_bits_fcsr_2(executorMul_io_out_bits_fcsr_2),
    .io_out_bits_fcsr_3(executorMul_io_out_bits_fcsr_3),
    .io_out_bits_fcsr_4(executorMul_io_out_bits_fcsr_4),
    .io_out_bits_fcsr_5(executorMul_io_out_bits_fcsr_5),
    .io_out_bits_fcsr_6(executorMul_io_out_bits_fcsr_6),
    .io_out_bits_fcsr_7(executorMul_io_out_bits_fcsr_7),
    .io_out_bits_op(executorMul_io_out_bits_op),
    .io_out_bits_mulInProc(executorMul_io_out_bits_mulInProc)
  );
  SPFPDiv executorDiv ( // @[FPUExecutionUnit.scala 38:30]
    .clock(executorDiv_clock),
    .reset(executorDiv_reset),
    .io_in_valid(executorDiv_io_in_valid),
    .io_in_bits_a_mantissa(executorDiv_io_in_bits_a_mantissa),
    .io_in_bits_a_exponent(executorDiv_io_in_bits_a_exponent),
    .io_in_bits_a_sign(executorDiv_io_in_bits_a_sign),
    .io_in_bits_b_mantissa(executorDiv_io_in_bits_b_mantissa),
    .io_in_bits_b_exponent(executorDiv_io_in_bits_b_exponent),
    .io_in_bits_b_sign(executorDiv_io_in_bits_b_sign),
    .io_in_bits_op(executorDiv_io_in_bits_op),
    .io_in_bits_fcsr_0(executorDiv_io_in_bits_fcsr_0),
    .io_in_bits_fcsr_1(executorDiv_io_in_bits_fcsr_1),
    .io_in_bits_fcsr_2(executorDiv_io_in_bits_fcsr_2),
    .io_in_bits_fcsr_3(executorDiv_io_in_bits_fcsr_3),
    .io_in_bits_fcsr_4(executorDiv_io_in_bits_fcsr_4),
    .io_in_bits_fcsr_5(executorDiv_io_in_bits_fcsr_5),
    .io_in_bits_fcsr_6(executorDiv_io_in_bits_fcsr_6),
    .io_in_bits_fcsr_7(executorDiv_io_in_bits_fcsr_7),
    .io_in_bits_fflags(executorDiv_io_in_bits_fflags),
    .io_out_valid(executorDiv_io_out_valid),
    .io_out_bits_result_mantissa(executorDiv_io_out_bits_result_mantissa),
    .io_out_bits_result_exponent(executorDiv_io_out_bits_result_exponent),
    .io_out_bits_result_sign(executorDiv_io_out_bits_result_sign),
    .io_out_bits_fcsr_0(executorDiv_io_out_bits_fcsr_0),
    .io_out_bits_fcsr_1(executorDiv_io_out_bits_fcsr_1),
    .io_out_bits_fcsr_2(executorDiv_io_out_bits_fcsr_2),
    .io_out_bits_fcsr_3(executorDiv_io_out_bits_fcsr_3),
    .io_out_bits_fcsr_4(executorDiv_io_out_bits_fcsr_4),
    .io_out_bits_fcsr_5(executorDiv_io_out_bits_fcsr_5),
    .io_out_bits_fcsr_6(executorDiv_io_out_bits_fcsr_6),
    .io_out_bits_fcsr_7(executorDiv_io_out_bits_fcsr_7),
    .io_out_bits_op(executorDiv_io_out_bits_op),
    .io_out_bits_divInProc(executorDiv_io_out_bits_divInProc)
  );
  SPFPSQRT executorSqrt ( // @[FPUExecutionUnit.scala 39:30]
    .clock(executorSqrt_clock),
    .reset(executorSqrt_reset),
    .io_in_valid(executorSqrt_io_in_valid),
    .io_in_bits_a_mantissa(executorSqrt_io_in_bits_a_mantissa),
    .io_in_bits_a_exponent(executorSqrt_io_in_bits_a_exponent),
    .io_in_bits_a_sign(executorSqrt_io_in_bits_a_sign),
    .io_in_bits_op(executorSqrt_io_in_bits_op),
    .io_in_bits_fflags(executorSqrt_io_in_bits_fflags),
    .io_out_valid(executorSqrt_io_out_valid),
    .io_out_bits_result_mantissa(executorSqrt_io_out_bits_result_mantissa),
    .io_out_bits_result_exponent(executorSqrt_io_out_bits_result_exponent),
    .io_out_bits_result_sign(executorSqrt_io_out_bits_result_sign),
    .io_out_bits_fcsr_4(executorSqrt_io_out_bits_fcsr_4),
    .io_out_bits_op(executorSqrt_io_out_bits_op),
    .io_out_bits_sqrtInProc(executorSqrt_io_out_bits_sqrtInProc)
  );
  SPFPComp executorComp ( // @[FPUExecutionUnit.scala 40:30]
    .io_in_valid(executorComp_io_in_valid),
    .io_in_bits_a_mantissa(executorComp_io_in_bits_a_mantissa),
    .io_in_bits_a_exponent(executorComp_io_in_bits_a_exponent),
    .io_in_bits_a_sign(executorComp_io_in_bits_a_sign),
    .io_in_bits_b_mantissa(executorComp_io_in_bits_b_mantissa),
    .io_in_bits_b_exponent(executorComp_io_in_bits_b_exponent),
    .io_in_bits_b_sign(executorComp_io_in_bits_b_sign),
    .io_in_bits_op(executorComp_io_in_bits_op),
    .io_in_bits_fcsr_0(executorComp_io_in_bits_fcsr_0),
    .io_in_bits_fcsr_1(executorComp_io_in_bits_fcsr_1),
    .io_in_bits_fcsr_2(executorComp_io_in_bits_fcsr_2),
    .io_in_bits_fcsr_3(executorComp_io_in_bits_fcsr_3),
    .io_in_bits_fcsr_4(executorComp_io_in_bits_fcsr_4),
    .io_in_bits_fcsr_5(executorComp_io_in_bits_fcsr_5),
    .io_in_bits_fcsr_6(executorComp_io_in_bits_fcsr_6),
    .io_in_bits_fcsr_7(executorComp_io_in_bits_fcsr_7),
    .io_in_bits_fflags(executorComp_io_in_bits_fflags),
    .io_out_valid(executorComp_io_out_valid),
    .io_out_bits_result_mantissa(executorComp_io_out_bits_result_mantissa),
    .io_out_bits_result_exponent(executorComp_io_out_bits_result_exponent),
    .io_out_bits_result_sign(executorComp_io_out_bits_result_sign),
    .io_out_bits_fcsr_0(executorComp_io_out_bits_fcsr_0),
    .io_out_bits_fcsr_1(executorComp_io_out_bits_fcsr_1),
    .io_out_bits_fcsr_2(executorComp_io_out_bits_fcsr_2),
    .io_out_bits_fcsr_3(executorComp_io_out_bits_fcsr_3),
    .io_out_bits_fcsr_4(executorComp_io_out_bits_fcsr_4),
    .io_out_bits_fcsr_5(executorComp_io_out_bits_fcsr_5),
    .io_out_bits_fcsr_6(executorComp_io_out_bits_fcsr_6),
    .io_out_bits_fcsr_7(executorComp_io_out_bits_fcsr_7),
    .io_out_bits_op(executorComp_io_out_bits_op)
  );
  SPFPSGNJ executorSgn ( // @[FPUExecutionUnit.scala 41:30]
    .io_in_valid(executorSgn_io_in_valid),
    .io_in_bits_a_mantissa(executorSgn_io_in_bits_a_mantissa),
    .io_in_bits_a_exponent(executorSgn_io_in_bits_a_exponent),
    .io_in_bits_a_sign(executorSgn_io_in_bits_a_sign),
    .io_in_bits_b_sign(executorSgn_io_in_bits_b_sign),
    .io_in_bits_op(executorSgn_io_in_bits_op),
    .io_in_bits_fcsr_0(executorSgn_io_in_bits_fcsr_0),
    .io_in_bits_fcsr_1(executorSgn_io_in_bits_fcsr_1),
    .io_in_bits_fcsr_2(executorSgn_io_in_bits_fcsr_2),
    .io_in_bits_fcsr_3(executorSgn_io_in_bits_fcsr_3),
    .io_in_bits_fcsr_4(executorSgn_io_in_bits_fcsr_4),
    .io_in_bits_fcsr_5(executorSgn_io_in_bits_fcsr_5),
    .io_in_bits_fcsr_6(executorSgn_io_in_bits_fcsr_6),
    .io_in_bits_fcsr_7(executorSgn_io_in_bits_fcsr_7),
    .io_out_valid(executorSgn_io_out_valid),
    .io_out_bits_result_mantissa(executorSgn_io_out_bits_result_mantissa),
    .io_out_bits_result_exponent(executorSgn_io_out_bits_result_exponent),
    .io_out_bits_result_sign(executorSgn_io_out_bits_result_sign),
    .io_out_bits_fcsr_0(executorSgn_io_out_bits_fcsr_0),
    .io_out_bits_fcsr_1(executorSgn_io_out_bits_fcsr_1),
    .io_out_bits_fcsr_2(executorSgn_io_out_bits_fcsr_2),
    .io_out_bits_fcsr_3(executorSgn_io_out_bits_fcsr_3),
    .io_out_bits_fcsr_4(executorSgn_io_out_bits_fcsr_4),
    .io_out_bits_fcsr_5(executorSgn_io_out_bits_fcsr_5),
    .io_out_bits_fcsr_6(executorSgn_io_out_bits_fcsr_6),
    .io_out_bits_fcsr_7(executorSgn_io_out_bits_fcsr_7),
    .io_out_bits_op(executorSgn_io_out_bits_op)
  );
  SPFPClass executorClass ( // @[FPUExecutionUnit.scala 42:30]
    .io_in_valid(executorClass_io_in_valid),
    .io_in_bits_a_mantissa(executorClass_io_in_bits_a_mantissa),
    .io_in_bits_a_sign(executorClass_io_in_bits_a_sign),
    .io_in_bits_op(executorClass_io_in_bits_op),
    .io_in_bits_fcsr_0(executorClass_io_in_bits_fcsr_0),
    .io_in_bits_fcsr_1(executorClass_io_in_bits_fcsr_1),
    .io_in_bits_fcsr_2(executorClass_io_in_bits_fcsr_2),
    .io_in_bits_fcsr_3(executorClass_io_in_bits_fcsr_3),
    .io_in_bits_fcsr_4(executorClass_io_in_bits_fcsr_4),
    .io_in_bits_fcsr_5(executorClass_io_in_bits_fcsr_5),
    .io_in_bits_fcsr_6(executorClass_io_in_bits_fcsr_6),
    .io_in_bits_fcsr_7(executorClass_io_in_bits_fcsr_7),
    .io_in_bits_fflags(executorClass_io_in_bits_fflags),
    .io_out_valid(executorClass_io_out_valid),
    .io_out_bits_result_mantissa(executorClass_io_out_bits_result_mantissa),
    .io_out_bits_fcsr_0(executorClass_io_out_bits_fcsr_0),
    .io_out_bits_fcsr_1(executorClass_io_out_bits_fcsr_1),
    .io_out_bits_fcsr_2(executorClass_io_out_bits_fcsr_2),
    .io_out_bits_fcsr_3(executorClass_io_out_bits_fcsr_3),
    .io_out_bits_fcsr_4(executorClass_io_out_bits_fcsr_4),
    .io_out_bits_fcsr_5(executorClass_io_out_bits_fcsr_5),
    .io_out_bits_fcsr_6(executorClass_io_out_bits_fcsr_6),
    .io_out_bits_fcsr_7(executorClass_io_out_bits_fcsr_7),
    .io_out_bits_op(executorClass_io_out_bits_op)
  );
  SPFPPass passthrough ( // @[FPUExecutionUnit.scala 43:30]
    .io_in_valid(passthrough_io_in_valid),
    .io_in_bits_a_mantissa(passthrough_io_in_bits_a_mantissa),
    .io_in_bits_a_exponent(passthrough_io_in_bits_a_exponent),
    .io_in_bits_a_sign(passthrough_io_in_bits_a_sign),
    .io_in_bits_op(passthrough_io_in_bits_op),
    .io_in_bits_fcsr_0(passthrough_io_in_bits_fcsr_0),
    .io_in_bits_fcsr_1(passthrough_io_in_bits_fcsr_1),
    .io_in_bits_fcsr_2(passthrough_io_in_bits_fcsr_2),
    .io_in_bits_fcsr_3(passthrough_io_in_bits_fcsr_3),
    .io_in_bits_fcsr_4(passthrough_io_in_bits_fcsr_4),
    .io_in_bits_fcsr_5(passthrough_io_in_bits_fcsr_5),
    .io_in_bits_fcsr_6(passthrough_io_in_bits_fcsr_6),
    .io_in_bits_fcsr_7(passthrough_io_in_bits_fcsr_7),
    .io_out_valid(passthrough_io_out_valid),
    .io_out_bits_result_mantissa(passthrough_io_out_bits_result_mantissa),
    .io_out_bits_result_exponent(passthrough_io_out_bits_result_exponent),
    .io_out_bits_result_sign(passthrough_io_out_bits_result_sign),
    .io_out_bits_fcsr_0(passthrough_io_out_bits_fcsr_0),
    .io_out_bits_fcsr_1(passthrough_io_out_bits_fcsr_1),
    .io_out_bits_fcsr_2(passthrough_io_out_bits_fcsr_2),
    .io_out_bits_fcsr_3(passthrough_io_out_bits_fcsr_3),
    .io_out_bits_fcsr_4(passthrough_io_out_bits_fcsr_4),
    .io_out_bits_fcsr_5(passthrough_io_out_bits_fcsr_5),
    .io_out_bits_fcsr_6(passthrough_io_out_bits_fcsr_6),
    .io_out_bits_fcsr_7(passthrough_io_out_bits_fcsr_7),
    .io_out_bits_op(passthrough_io_out_bits_op)
  );
  assign io_in_ready = _T_1 & ~inProc | ~io_in_valid; // @[FPUExecutionUnit.scala 168:55]
  assign io_out_valid = submoduleOut_0_valid | submoduleOut_1_valid | submoduleOut_2_valid | submoduleOut_3_valid |
    submoduleOut_4_valid | submoduleOut_5_valid | submoduleOut_6_valid | submoduleOut_7_valid; // @[FPUExecutionUnit.scala 181:65]
  assign io_out_bits_result_mantissa = _io_out_bits_result_T_68 | _io_out_bits_result_T_62; // @[Mux.scala 27:73]
  assign io_out_bits_result_exponent = $signed(_io_out_bits_result_T_50) | $signed(_io_out_bits_result_T_38); // @[Mux.scala 27:73]
  assign io_out_bits_result_sign = submoduleOut_0_valid & submoduleOut_0_bits_result_sign | submoduleOut_1_valid &
    submoduleOut_1_bits_result_sign | submoduleOut_2_valid & submoduleOut_2_bits_result_sign | submoduleOut_3_valid &
    submoduleOut_3_bits_result_sign | submoduleOut_4_valid & submoduleOut_4_bits_result_sign | submoduleOut_5_valid &
    submoduleOut_5_bits_result_sign | submoduleOut_7_valid & submoduleOut_7_bits_result_sign; // @[Mux.scala 27:73]
  assign io_out_bits_fcsr_0 = submoduleOut_0_valid & submoduleOut_0_bits_fcsr_0 | submoduleOut_1_valid &
    submoduleOut_1_bits_fcsr_0 | submoduleOut_2_valid & submoduleOut_2_bits_fcsr_0 | submoduleOut_4_valid &
    submoduleOut_4_bits_fcsr_0 | submoduleOut_5_valid & submoduleOut_5_bits_fcsr_0 | submoduleOut_6_valid &
    submoduleOut_6_bits_fcsr_0 | submoduleOut_7_valid & submoduleOut_7_bits_fcsr_0; // @[Mux.scala 27:73]
  assign io_out_bits_fcsr_1 = submoduleOut_0_valid & submoduleOut_0_bits_fcsr_1 | submoduleOut_1_valid &
    submoduleOut_1_bits_fcsr_1 | submoduleOut_2_valid & submoduleOut_2_bits_fcsr_1 | submoduleOut_4_valid &
    submoduleOut_4_bits_fcsr_1 | submoduleOut_5_valid & submoduleOut_5_bits_fcsr_1 | submoduleOut_6_valid &
    submoduleOut_6_bits_fcsr_1 | submoduleOut_7_valid & submoduleOut_7_bits_fcsr_1; // @[Mux.scala 27:73]
  assign io_out_bits_fcsr_2 = submoduleOut_0_valid & submoduleOut_0_bits_fcsr_2 | submoduleOut_1_valid &
    submoduleOut_1_bits_fcsr_2 | submoduleOut_2_valid & submoduleOut_2_bits_fcsr_2 | submoduleOut_4_valid &
    submoduleOut_4_bits_fcsr_2 | submoduleOut_5_valid & submoduleOut_5_bits_fcsr_2 | submoduleOut_6_valid &
    submoduleOut_6_bits_fcsr_2 | submoduleOut_7_valid & submoduleOut_7_bits_fcsr_2; // @[Mux.scala 27:73]
  assign io_out_bits_fcsr_3 = submoduleOut_0_valid & submoduleOut_0_bits_fcsr_3 | submoduleOut_1_valid &
    submoduleOut_1_bits_fcsr_3 | submoduleOut_2_valid & submoduleOut_2_bits_fcsr_3 | submoduleOut_4_valid &
    submoduleOut_4_bits_fcsr_3 | submoduleOut_5_valid & submoduleOut_5_bits_fcsr_3 | submoduleOut_6_valid &
    submoduleOut_6_bits_fcsr_3 | submoduleOut_7_valid & submoduleOut_7_bits_fcsr_3; // @[Mux.scala 27:73]
  assign io_out_bits_fcsr_4 = submoduleOut_0_valid & submoduleOut_0_bits_fcsr_4 | submoduleOut_1_valid &
    submoduleOut_1_bits_fcsr_4 | submoduleOut_2_valid & submoduleOut_2_bits_fcsr_4 | submoduleOut_3_valid &
    submoduleOut_3_bits_fcsr_4 | submoduleOut_4_valid & submoduleOut_4_bits_fcsr_4 | submoduleOut_5_valid &
    submoduleOut_5_bits_fcsr_4 | submoduleOut_6_valid & submoduleOut_6_bits_fcsr_4 | submoduleOut_7_valid &
    submoduleOut_7_bits_fcsr_4; // @[Mux.scala 27:73]
  assign io_out_bits_fcsr_5 = submoduleOut_0_valid & submoduleOut_0_bits_fcsr_5 | submoduleOut_1_valid &
    submoduleOut_1_bits_fcsr_5 | submoduleOut_2_valid & submoduleOut_2_bits_fcsr_5 | submoduleOut_4_valid &
    submoduleOut_4_bits_fcsr_5 | submoduleOut_5_valid & submoduleOut_5_bits_fcsr_5 | submoduleOut_6_valid &
    submoduleOut_6_bits_fcsr_5 | submoduleOut_7_valid & submoduleOut_7_bits_fcsr_5; // @[Mux.scala 27:73]
  assign io_out_bits_fcsr_6 = submoduleOut_0_valid & submoduleOut_0_bits_fcsr_6 | submoduleOut_1_valid &
    submoduleOut_1_bits_fcsr_6 | submoduleOut_2_valid & submoduleOut_2_bits_fcsr_6 | submoduleOut_4_valid &
    submoduleOut_4_bits_fcsr_6 | submoduleOut_5_valid & submoduleOut_5_bits_fcsr_6 | submoduleOut_6_valid &
    submoduleOut_6_bits_fcsr_6 | submoduleOut_7_valid & submoduleOut_7_bits_fcsr_6; // @[Mux.scala 27:73]
  assign io_out_bits_fcsr_7 = submoduleOut_0_valid & submoduleOut_0_bits_fcsr_7 | submoduleOut_1_valid &
    submoduleOut_1_bits_fcsr_7 | submoduleOut_2_valid & submoduleOut_2_bits_fcsr_7 | submoduleOut_4_valid &
    submoduleOut_4_bits_fcsr_7 | submoduleOut_5_valid & submoduleOut_5_bits_fcsr_7 | submoduleOut_6_valid &
    submoduleOut_6_bits_fcsr_7 | submoduleOut_7_valid & submoduleOut_7_bits_fcsr_7; // @[Mux.scala 27:73]
  assign io_out_bits_op = _io_out_bits_op_T_21 | _io_out_bits_op_T_15; // @[Mux.scala 27:73]
  assign io_out_bits_stall_op = io_in_bits_stall_op; // @[FPUExecutionUnit.scala 79:22]
  assign io_out_bits_stall_pass = _T_3 ? 1'h0 : io_in_bits_stall_pass; // @[FPUExecutionUnit.scala 79:22 82:61 83:29]
  assign io_out_bits_stall_inProc = executorMul_io_out_bits_mulInProc | executorDiv_io_out_bits_divInProc |
    executorSqrt_io_out_bits_sqrtInProc; // @[FPUExecutionUnit.scala 166:87]
  assign executorAdd_io_in_valid = _T_3 & _GEN_559; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorAdd_io_in_bits_a_mantissa = _T_3 ? _GEN_540 : 26'h0; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorAdd_io_in_bits_a_exponent = _T_3 ? $signed(_GEN_541) : $signed(8'sh0); // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorAdd_io_in_bits_a_sign = _T_3 & _GEN_542; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorAdd_io_in_bits_b_mantissa = _T_3 ? _GEN_543 : 26'h0; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorAdd_io_in_bits_b_exponent = _T_3 ? $signed(_GEN_544) : $signed(8'sh0); // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorAdd_io_in_bits_b_sign = _T_3 & _GEN_545; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorAdd_io_in_bits_op = _T_3 ? _GEN_549 : 5'h16; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorAdd_io_in_bits_fcsr_0 = _T_3 & _GEN_550; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorAdd_io_in_bits_fcsr_1 = _T_3 & _GEN_551; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorAdd_io_in_bits_fcsr_2 = _T_3 & _GEN_552; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorAdd_io_in_bits_fcsr_3 = _T_3 & _GEN_553; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorAdd_io_in_bits_fcsr_4 = _T_3 & _GEN_554; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorAdd_io_in_bits_fcsr_5 = _T_3 & _GEN_555; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorAdd_io_in_bits_fcsr_6 = _T_3 & _GEN_556; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorAdd_io_in_bits_fcsr_7 = _T_3 & _GEN_557; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorAdd_io_in_bits_fflags = _T_3 ? _GEN_558 : 12'h0; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorMul_clock = clock;
  assign executorMul_reset = reset;
  assign executorMul_io_in_valid = _T_3 & _GEN_579; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorMul_io_in_bits_a_mantissa = _T_3 ? _GEN_560 : 26'h0; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorMul_io_in_bits_a_exponent = _T_3 ? $signed(_GEN_561) : $signed(8'sh0); // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorMul_io_in_bits_a_sign = _T_3 & _GEN_562; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorMul_io_in_bits_b_mantissa = _T_3 ? _GEN_563 : 26'h0; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorMul_io_in_bits_b_exponent = _T_3 ? $signed(_GEN_564) : $signed(8'sh0); // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorMul_io_in_bits_b_sign = _T_3 & _GEN_565; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorMul_io_in_bits_c_mantissa = _T_3 ? _GEN_566 : 26'h0; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorMul_io_in_bits_c_exponent = _T_3 ? $signed(_GEN_567) : $signed(8'sh0); // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorMul_io_in_bits_c_sign = _T_3 & _GEN_568; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorMul_io_in_bits_op = _T_3 ? _GEN_569 : 5'h16; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorMul_io_in_bits_fcsr_0 = _T_3 & _GEN_570; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorMul_io_in_bits_fcsr_1 = _T_3 & _GEN_571; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorMul_io_in_bits_fcsr_2 = _T_3 & _GEN_572; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorMul_io_in_bits_fcsr_3 = _T_3 & _GEN_573; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorMul_io_in_bits_fcsr_4 = _T_3 & _GEN_574; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorMul_io_in_bits_fcsr_5 = _T_3 & _GEN_575; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorMul_io_in_bits_fcsr_6 = _T_3 & _GEN_576; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorMul_io_in_bits_fcsr_7 = _T_3 & _GEN_577; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorMul_io_in_bits_fflags = _T_3 ? _GEN_578 : 12'h0; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorDiv_clock = clock;
  assign executorDiv_reset = reset;
  assign executorDiv_io_in_valid = _T_3 & _GEN_599; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorDiv_io_in_bits_a_mantissa = _T_3 ? _GEN_580 : 26'h0; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorDiv_io_in_bits_a_exponent = _T_3 ? $signed(_GEN_581) : $signed(8'sh0); // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorDiv_io_in_bits_a_sign = _T_3 & _GEN_582; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorDiv_io_in_bits_b_mantissa = _T_3 ? _GEN_583 : 26'h0; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorDiv_io_in_bits_b_exponent = _T_3 ? $signed(_GEN_584) : $signed(8'sh0); // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorDiv_io_in_bits_b_sign = _T_3 & _GEN_585; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorDiv_io_in_bits_op = _T_3 ? _GEN_589 : 5'h16; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorDiv_io_in_bits_fcsr_0 = _T_3 & _GEN_590; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorDiv_io_in_bits_fcsr_1 = _T_3 & _GEN_591; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorDiv_io_in_bits_fcsr_2 = _T_3 & _GEN_592; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorDiv_io_in_bits_fcsr_3 = _T_3 & _GEN_593; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorDiv_io_in_bits_fcsr_4 = _T_3 & _GEN_594; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorDiv_io_in_bits_fcsr_5 = _T_3 & _GEN_595; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorDiv_io_in_bits_fcsr_6 = _T_3 & _GEN_596; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorDiv_io_in_bits_fcsr_7 = _T_3 & _GEN_597; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorDiv_io_in_bits_fflags = _T_3 ? _GEN_598 : 12'h0; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorSqrt_clock = clock;
  assign executorSqrt_reset = reset;
  assign executorSqrt_io_in_valid = _T_3 & _GEN_639; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorSqrt_io_in_bits_a_mantissa = _T_3 ? _GEN_620 : 26'h0; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorSqrt_io_in_bits_a_exponent = _T_3 ? $signed(_GEN_621) : $signed(8'sh0); // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorSqrt_io_in_bits_a_sign = _T_3 & _GEN_622; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorSqrt_io_in_bits_op = _T_3 ? _GEN_629 : 5'h16; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorSqrt_io_in_bits_fflags = _T_3 ? _GEN_638 : 12'h0; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorComp_io_in_valid = _T_3 & _GEN_619; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorComp_io_in_bits_a_mantissa = _T_3 ? _GEN_600 : 26'h0; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorComp_io_in_bits_a_exponent = _T_3 ? $signed(_GEN_601) : $signed(8'sh0); // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorComp_io_in_bits_a_sign = _T_3 & _GEN_602; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorComp_io_in_bits_b_mantissa = _T_3 ? _GEN_603 : 26'h0; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorComp_io_in_bits_b_exponent = _T_3 ? $signed(_GEN_604) : $signed(8'sh0); // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorComp_io_in_bits_b_sign = _T_3 & _GEN_605; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorComp_io_in_bits_op = _T_3 ? _GEN_609 : 5'h16; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorComp_io_in_bits_fcsr_0 = _T_3 & _GEN_610; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorComp_io_in_bits_fcsr_1 = _T_3 & _GEN_611; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorComp_io_in_bits_fcsr_2 = _T_3 & _GEN_612; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorComp_io_in_bits_fcsr_3 = _T_3 & _GEN_613; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorComp_io_in_bits_fcsr_4 = _T_3 & _GEN_614; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorComp_io_in_bits_fcsr_5 = _T_3 & _GEN_615; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorComp_io_in_bits_fcsr_6 = _T_3 & _GEN_616; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorComp_io_in_bits_fcsr_7 = _T_3 & _GEN_617; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorComp_io_in_bits_fflags = _T_3 ? _GEN_618 : 12'h0; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorSgn_io_in_valid = _T_3 & _GEN_659; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorSgn_io_in_bits_a_mantissa = _T_3 ? _GEN_640 : 26'h0; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorSgn_io_in_bits_a_exponent = _T_3 ? $signed(_GEN_641) : $signed(8'sh0); // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorSgn_io_in_bits_a_sign = _T_3 & _GEN_642; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorSgn_io_in_bits_b_sign = _T_3 & _GEN_645; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorSgn_io_in_bits_op = _T_3 ? _GEN_649 : 5'h16; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorSgn_io_in_bits_fcsr_0 = _T_3 & _GEN_650; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorSgn_io_in_bits_fcsr_1 = _T_3 & _GEN_651; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorSgn_io_in_bits_fcsr_2 = _T_3 & _GEN_652; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorSgn_io_in_bits_fcsr_3 = _T_3 & _GEN_653; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorSgn_io_in_bits_fcsr_4 = _T_3 & _GEN_654; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorSgn_io_in_bits_fcsr_5 = _T_3 & _GEN_655; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorSgn_io_in_bits_fcsr_6 = _T_3 & _GEN_656; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorSgn_io_in_bits_fcsr_7 = _T_3 & _GEN_657; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorClass_io_in_valid = _T_3 & _GEN_679; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorClass_io_in_bits_a_mantissa = _T_3 ? _GEN_660 : 26'h0; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorClass_io_in_bits_a_sign = _T_3 & _GEN_662; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorClass_io_in_bits_op = _T_3 ? _GEN_669 : 5'h16; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorClass_io_in_bits_fcsr_0 = _T_3 & _GEN_670; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorClass_io_in_bits_fcsr_1 = _T_3 & _GEN_671; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorClass_io_in_bits_fcsr_2 = _T_3 & _GEN_672; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorClass_io_in_bits_fcsr_3 = _T_3 & _GEN_673; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorClass_io_in_bits_fcsr_4 = _T_3 & _GEN_674; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorClass_io_in_bits_fcsr_5 = _T_3 & _GEN_675; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorClass_io_in_bits_fcsr_6 = _T_3 & _GEN_676; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorClass_io_in_bits_fcsr_7 = _T_3 & _GEN_677; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign executorClass_io_in_bits_fflags = _T_3 ? _GEN_678 : 12'h0; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign passthrough_io_in_valid = _T_3 & _GEN_698; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign passthrough_io_in_bits_a_mantissa = _T_3 ? _GEN_680 : 26'h0; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign passthrough_io_in_bits_a_exponent = _T_3 ? $signed(_GEN_681) : $signed(8'sh0); // @[FPUExecutionUnit.scala 45:27 82:61]
  assign passthrough_io_in_bits_a_sign = _T_3 & _GEN_682; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign passthrough_io_in_bits_op = _T_3 ? _GEN_689 : 5'h16; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign passthrough_io_in_bits_fcsr_0 = _T_3 & _GEN_690; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign passthrough_io_in_bits_fcsr_1 = _T_3 & _GEN_691; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign passthrough_io_in_bits_fcsr_2 = _T_3 & _GEN_692; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign passthrough_io_in_bits_fcsr_3 = _T_3 & _GEN_693; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign passthrough_io_in_bits_fcsr_4 = _T_3 & _GEN_694; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign passthrough_io_in_bits_fcsr_5 = _T_3 & _GEN_695; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign passthrough_io_in_bits_fcsr_6 = _T_3 & _GEN_696; // @[FPUExecutionUnit.scala 45:27 82:61]
  assign passthrough_io_in_bits_fcsr_7 = _T_3 & _GEN_697; // @[FPUExecutionUnit.scala 45:27 82:61]
endmodule
module FPUPostprocessor(
  output        io_in_ready,
  input         io_in_valid,
  input  [25:0] io_in_bits_result_mantissa,
  input  [7:0]  io_in_bits_result_exponent,
  input         io_in_bits_result_sign,
  input  [4:0]  io_in_bits_op,
  input         io_in_bits_fcsr_0,
  input         io_in_bits_fcsr_1,
  input         io_in_bits_fcsr_2,
  input         io_in_bits_fcsr_3,
  input         io_in_bits_fcsr_4,
  input         io_in_bits_fcsr_5,
  input         io_in_bits_fcsr_6,
  input         io_in_bits_fcsr_7,
  input         io_out_ready,
  output        io_out_valid,
  output [31:0] io_out_bits_rd,
  output        io_out_bits_fcsr_0,
  output        io_out_bits_fcsr_1,
  output        io_out_bits_fcsr_2,
  output        io_out_bits_fcsr_3,
  output        io_out_bits_fcsr_4,
  output        io_out_bits_fcsr_5,
  output        io_out_bits_fcsr_6,
  output        io_out_bits_fcsr_7
);
  wire  rNaN = io_in_bits_result_mantissa > 26'h0; // @[FPUPostprocessor.scala 53:44]
  wire  _type3_T_1 = io_in_bits_op == 5'hb; // @[FPUPostprocessor.scala 88:30]
  wire  _type3_T_2 = io_in_bits_op == 5'hc | _type3_T_1; // @[FPUPostprocessor.scala 87:55]
  wire  _type3_T_3 = io_in_bits_op == 5'hd; // @[FPUPostprocessor.scala 89:30]
  wire  _type3_T_4 = _type3_T_2 | _type3_T_3; // @[FPUPostprocessor.scala 88:55]
  wire  _type3_T_5 = io_in_bits_op == 5'h15; // @[FPUPostprocessor.scala 90:30]
  wire  _type3_T_6 = _type3_T_4 | _type3_T_5; // @[FPUPostprocessor.scala 89:55]
  wire  _type3_T_7 = io_in_bits_op == 5'h4; // @[FPUPostprocessor.scala 91:30]
  wire  type3 = _type3_T_6 | _type3_T_7; // @[FPUPostprocessor.scala 90:55]
  wire [15:0] _GEN_213 = {{8'd0}, io_in_bits_result_mantissa[15:8]}; // @[Bitwise.scala 108:31]
  wire [15:0] _bitLocation_T_5 = _GEN_213 & 16'hff; // @[Bitwise.scala 108:31]
  wire [15:0] _bitLocation_T_7 = {io_in_bits_result_mantissa[7:0], 8'h0}; // @[Bitwise.scala 108:70]
  wire [15:0] _bitLocation_T_9 = _bitLocation_T_7 & 16'hff00; // @[Bitwise.scala 108:80]
  wire [15:0] _bitLocation_T_10 = _bitLocation_T_5 | _bitLocation_T_9; // @[Bitwise.scala 108:39]
  wire [15:0] _GEN_214 = {{4'd0}, _bitLocation_T_10[15:4]}; // @[Bitwise.scala 108:31]
  wire [15:0] _bitLocation_T_15 = _GEN_214 & 16'hf0f; // @[Bitwise.scala 108:31]
  wire [15:0] _bitLocation_T_17 = {_bitLocation_T_10[11:0], 4'h0}; // @[Bitwise.scala 108:70]
  wire [15:0] _bitLocation_T_19 = _bitLocation_T_17 & 16'hf0f0; // @[Bitwise.scala 108:80]
  wire [15:0] _bitLocation_T_20 = _bitLocation_T_15 | _bitLocation_T_19; // @[Bitwise.scala 108:39]
  wire [15:0] _GEN_215 = {{2'd0}, _bitLocation_T_20[15:2]}; // @[Bitwise.scala 108:31]
  wire [15:0] _bitLocation_T_25 = _GEN_215 & 16'h3333; // @[Bitwise.scala 108:31]
  wire [15:0] _bitLocation_T_27 = {_bitLocation_T_20[13:0], 2'h0}; // @[Bitwise.scala 108:70]
  wire [15:0] _bitLocation_T_29 = _bitLocation_T_27 & 16'hcccc; // @[Bitwise.scala 108:80]
  wire [15:0] _bitLocation_T_30 = _bitLocation_T_25 | _bitLocation_T_29; // @[Bitwise.scala 108:39]
  wire [15:0] _GEN_216 = {{1'd0}, _bitLocation_T_30[15:1]}; // @[Bitwise.scala 108:31]
  wire [15:0] _bitLocation_T_35 = _GEN_216 & 16'h5555; // @[Bitwise.scala 108:31]
  wire [15:0] _bitLocation_T_37 = {_bitLocation_T_30[14:0], 1'h0}; // @[Bitwise.scala 108:70]
  wire [15:0] _bitLocation_T_39 = _bitLocation_T_37 & 16'haaaa; // @[Bitwise.scala 108:80]
  wire [15:0] _bitLocation_T_40 = _bitLocation_T_35 | _bitLocation_T_39; // @[Bitwise.scala 108:39]
  wire [7:0] _GEN_217 = {{4'd0}, io_in_bits_result_mantissa[23:20]}; // @[Bitwise.scala 108:31]
  wire [7:0] _bitLocation_T_46 = _GEN_217 & 8'hf; // @[Bitwise.scala 108:31]
  wire [7:0] _bitLocation_T_48 = {io_in_bits_result_mantissa[19:16], 4'h0}; // @[Bitwise.scala 108:70]
  wire [7:0] _bitLocation_T_50 = _bitLocation_T_48 & 8'hf0; // @[Bitwise.scala 108:80]
  wire [7:0] _bitLocation_T_51 = _bitLocation_T_46 | _bitLocation_T_50; // @[Bitwise.scala 108:39]
  wire [7:0] _GEN_218 = {{2'd0}, _bitLocation_T_51[7:2]}; // @[Bitwise.scala 108:31]
  wire [7:0] _bitLocation_T_56 = _GEN_218 & 8'h33; // @[Bitwise.scala 108:31]
  wire [7:0] _bitLocation_T_58 = {_bitLocation_T_51[5:0], 2'h0}; // @[Bitwise.scala 108:70]
  wire [7:0] _bitLocation_T_60 = _bitLocation_T_58 & 8'hcc; // @[Bitwise.scala 108:80]
  wire [7:0] _bitLocation_T_61 = _bitLocation_T_56 | _bitLocation_T_60; // @[Bitwise.scala 108:39]
  wire [7:0] _GEN_219 = {{1'd0}, _bitLocation_T_61[7:1]}; // @[Bitwise.scala 108:31]
  wire [7:0] _bitLocation_T_66 = _GEN_219 & 8'h55; // @[Bitwise.scala 108:31]
  wire [7:0] _bitLocation_T_68 = {_bitLocation_T_61[6:0], 1'h0}; // @[Bitwise.scala 108:70]
  wire [7:0] _bitLocation_T_70 = _bitLocation_T_68 & 8'haa; // @[Bitwise.scala 108:80]
  wire [7:0] _bitLocation_T_71 = _bitLocation_T_66 | _bitLocation_T_70; // @[Bitwise.scala 108:39]
  wire [24:0] _bitLocation_T_74 = {_bitLocation_T_40,_bitLocation_T_71,io_in_bits_result_mantissa[24]}; // @[Cat.scala 33:92]
  wire [4:0] _bitLocation_T_100 = _bitLocation_T_74[23] ? 5'h17 : 5'h18; // @[Mux.scala 47:70]
  wire [4:0] _bitLocation_T_101 = _bitLocation_T_74[22] ? 5'h16 : _bitLocation_T_100; // @[Mux.scala 47:70]
  wire [4:0] _bitLocation_T_102 = _bitLocation_T_74[21] ? 5'h15 : _bitLocation_T_101; // @[Mux.scala 47:70]
  wire [4:0] _bitLocation_T_103 = _bitLocation_T_74[20] ? 5'h14 : _bitLocation_T_102; // @[Mux.scala 47:70]
  wire [4:0] _bitLocation_T_104 = _bitLocation_T_74[19] ? 5'h13 : _bitLocation_T_103; // @[Mux.scala 47:70]
  wire [4:0] _bitLocation_T_105 = _bitLocation_T_74[18] ? 5'h12 : _bitLocation_T_104; // @[Mux.scala 47:70]
  wire [4:0] _bitLocation_T_106 = _bitLocation_T_74[17] ? 5'h11 : _bitLocation_T_105; // @[Mux.scala 47:70]
  wire [4:0] _bitLocation_T_107 = _bitLocation_T_74[16] ? 5'h10 : _bitLocation_T_106; // @[Mux.scala 47:70]
  wire [4:0] _bitLocation_T_108 = _bitLocation_T_74[15] ? 5'hf : _bitLocation_T_107; // @[Mux.scala 47:70]
  wire [4:0] _bitLocation_T_109 = _bitLocation_T_74[14] ? 5'he : _bitLocation_T_108; // @[Mux.scala 47:70]
  wire [4:0] _bitLocation_T_110 = _bitLocation_T_74[13] ? 5'hd : _bitLocation_T_109; // @[Mux.scala 47:70]
  wire [4:0] _bitLocation_T_111 = _bitLocation_T_74[12] ? 5'hc : _bitLocation_T_110; // @[Mux.scala 47:70]
  wire [4:0] _bitLocation_T_112 = _bitLocation_T_74[11] ? 5'hb : _bitLocation_T_111; // @[Mux.scala 47:70]
  wire [4:0] _bitLocation_T_113 = _bitLocation_T_74[10] ? 5'ha : _bitLocation_T_112; // @[Mux.scala 47:70]
  wire [4:0] _bitLocation_T_114 = _bitLocation_T_74[9] ? 5'h9 : _bitLocation_T_113; // @[Mux.scala 47:70]
  wire [4:0] _bitLocation_T_115 = _bitLocation_T_74[8] ? 5'h8 : _bitLocation_T_114; // @[Mux.scala 47:70]
  wire [4:0] _bitLocation_T_116 = _bitLocation_T_74[7] ? 5'h7 : _bitLocation_T_115; // @[Mux.scala 47:70]
  wire [4:0] _bitLocation_T_117 = _bitLocation_T_74[6] ? 5'h6 : _bitLocation_T_116; // @[Mux.scala 47:70]
  wire [4:0] _bitLocation_T_118 = _bitLocation_T_74[5] ? 5'h5 : _bitLocation_T_117; // @[Mux.scala 47:70]
  wire [4:0] _bitLocation_T_119 = _bitLocation_T_74[4] ? 5'h4 : _bitLocation_T_118; // @[Mux.scala 47:70]
  wire [4:0] _bitLocation_T_120 = _bitLocation_T_74[3] ? 5'h3 : _bitLocation_T_119; // @[Mux.scala 47:70]
  wire [4:0] _bitLocation_T_121 = _bitLocation_T_74[2] ? 5'h2 : _bitLocation_T_120; // @[Mux.scala 47:70]
  wire [4:0] _bitLocation_T_122 = _bitLocation_T_74[1] ? 5'h1 : _bitLocation_T_121; // @[Mux.scala 47:70]
  wire [4:0] _bitLocation_T_123 = _bitLocation_T_74[0] ? 5'h0 : _bitLocation_T_122; // @[Mux.scala 47:70]
  wire [7:0] bitLocation = rNaN & ~type3 & ~(io_in_bits_fcsr_0 | io_in_bits_fcsr_1 | io_in_bits_fcsr_2 |
    io_in_bits_fcsr_3 | io_in_bits_fcsr_4 | io_in_bits_fcsr_5 | io_in_bits_fcsr_6 | io_in_bits_fcsr_7) ? {{3'd0},
    _bitLocation_T_123} : 8'h0; // @[FPUPostprocessor.scala 102:75 103:17 42:36]
  wire [7:0] _shiftAmount_T_1 = 8'h1 - bitLocation; // @[FPUPostprocessor.scala 105:26]
  wire [7:0] _shiftAmount_T_3 = bitLocation - 8'h1; // @[FPUPostprocessor.scala 109:34]
  wire [7:0] _GEN_0 = bitLocation > 8'h1 ? _shiftAmount_T_3 : 8'h0; // @[FPUPostprocessor.scala 108:35 109:19 43:36]
  wire [7:0] _GEN_3 = bitLocation < 8'h1 ? _shiftAmount_T_1 : _GEN_0; // @[FPUPostprocessor.scala 104:29 105:19]
  wire [7:0] shiftAmount = rNaN & ~type3 & ~(io_in_bits_fcsr_0 | io_in_bits_fcsr_1 | io_in_bits_fcsr_2 |
    io_in_bits_fcsr_3 | io_in_bits_fcsr_4 | io_in_bits_fcsr_5 | io_in_bits_fcsr_6 | io_in_bits_fcsr_7) ? _GEN_3 : 8'h0; // @[FPUPostprocessor.scala 102:75 43:36]
  wire [25:0] _normalizedMantissa_T = io_in_bits_result_mantissa >> shiftAmount; // @[FPUPostprocessor.scala 106:56]
  wire [280:0] _GEN_6 = {{255'd0}, io_in_bits_result_mantissa}; // @[FPUPostprocessor.scala 110:56]
  wire [280:0] _normalizedMantissa_T_1 = _GEN_6 << shiftAmount; // @[FPUPostprocessor.scala 110:56]
  wire [280:0] _GEN_1 = bitLocation > 8'h1 ? _normalizedMantissa_T_1 : {{255'd0}, io_in_bits_result_mantissa}; // @[FPUPostprocessor.scala 108:35 110:26 45:22]
  wire [280:0] _GEN_4 = bitLocation < 8'h1 ? {{255'd0}, _normalizedMantissa_T} : _GEN_1; // @[FPUPostprocessor.scala 104:29 106:26]
  wire [280:0] _GEN_8 = rNaN & ~type3 & ~(io_in_bits_fcsr_0 | io_in_bits_fcsr_1 | io_in_bits_fcsr_2 | io_in_bits_fcsr_3
     | io_in_bits_fcsr_4 | io_in_bits_fcsr_5 | io_in_bits_fcsr_6 | io_in_bits_fcsr_7) ? _GEN_4 : {{255'd0},
    io_in_bits_result_mantissa}; // @[FPUPostprocessor.scala 102:75 45:22]
  wire [25:0] normalizedMantissa = _GEN_8[25:0]; // @[FPUPostprocessor.scala 40:32]
  wire [8:0] _adjExponent_T_1 = {1'h0,shiftAmount}; // @[FPUPostprocessor.scala 107:78]
  wire [8:0] _GEN_220 = {{1{io_in_bits_result_exponent[7]}},io_in_bits_result_exponent}; // @[FPUPostprocessor.scala 107:49]
  wire [8:0] _adjExponent_T_4 = $signed(_GEN_220) + $signed(_adjExponent_T_1); // @[FPUPostprocessor.scala 107:49]
  wire [8:0] _adjExponent_T_9 = $signed(_GEN_220) - $signed(_adjExponent_T_1); // @[FPUPostprocessor.scala 111:49]
  wire [8:0] _GEN_2 = bitLocation > 8'h1 ? $signed(_adjExponent_T_9) : $signed({{1{io_in_bits_result_exponent[7]}},
    io_in_bits_result_exponent}); // @[FPUPostprocessor.scala 108:35 111:19 46:22]
  wire [8:0] _GEN_5 = bitLocation < 8'h1 ? $signed(_adjExponent_T_4) : $signed(_GEN_2); // @[FPUPostprocessor.scala 104:29 107:19]
  wire [8:0] _GEN_9 = rNaN & ~type3 & ~(io_in_bits_fcsr_0 | io_in_bits_fcsr_1 | io_in_bits_fcsr_2 | io_in_bits_fcsr_3 |
    io_in_bits_fcsr_4 | io_in_bits_fcsr_5 | io_in_bits_fcsr_6 | io_in_bits_fcsr_7) ? $signed(_GEN_5) : $signed({{1{
    io_in_bits_result_exponent[7]}},io_in_bits_result_exponent}); // @[FPUPostprocessor.scala 102:75 46:22]
  wire [7:0] adjExponent = _GEN_9[7:0]; // @[FPUPostprocessor.scala 41:32]
  wire [7:0] _finalExponent_T_2 = $signed(adjExponent) + 8'sh7f; // @[FPUPostprocessor.scala 49:32]
  wire  maxExp = $signed(io_in_bits_result_exponent) == 8'sh7f; // @[FPUPostprocessor.scala 52:44]
  wire  minExp = $signed(io_in_bits_result_exponent) == -8'sh7e; // @[FPUPostprocessor.scala 54:44]
  wire  isPos = ~io_in_bits_result_sign; // @[FPUPostprocessor.scala 55:44]
  wire  _isSub_T = io_in_bits_op == 5'h1; // @[FPUPostprocessor.scala 57:31]
  wire  _isSub_T_1 = io_in_bits_op == 5'h8; // @[FPUPostprocessor.scala 58:31]
  wire  _isSub_T_3 = io_in_bits_op == 5'ha; // @[FPUPostprocessor.scala 59:31]
  wire  _maxmin_T = io_in_bits_op == 5'h6; // @[FPUPostprocessor.scala 62:31]
  wire  _maxmin_T_1 = io_in_bits_op == 5'h5; // @[FPUPostprocessor.scala 63:31]
  wire  maxmin = io_in_bits_op == 5'h6 | _maxmin_T_1; // @[FPUPostprocessor.scala 62:52]
  wire  _type1_T_2 = io_in_bits_op == 5'h0 | _isSub_T; // @[FPUPostprocessor.scala 66:53]
  wire  _type1_T_3 = io_in_bits_op == 5'h2; // @[FPUPostprocessor.scala 68:30]
  wire  _type1_T_4 = _type1_T_2 | _type1_T_3; // @[FPUPostprocessor.scala 67:53]
  wire  _type1_T_5 = io_in_bits_op == 5'h3; // @[FPUPostprocessor.scala 69:30]
  wire  _type1_T_6 = _type1_T_4 | _type1_T_5; // @[FPUPostprocessor.scala 68:53]
  wire  _type1_T_7 = io_in_bits_op == 5'h7; // @[FPUPostprocessor.scala 70:30]
  wire  _type1_T_8 = _type1_T_6 | _type1_T_7; // @[FPUPostprocessor.scala 69:53]
  wire  _type1_T_10 = _type1_T_8 | _isSub_T_1; // @[FPUPostprocessor.scala 70:53]
  wire  _type1_T_11 = io_in_bits_op == 5'h9; // @[FPUPostprocessor.scala 72:30]
  wire  _type1_T_12 = _type1_T_10 | _type1_T_11; // @[FPUPostprocessor.scala 71:53]
  wire  type1 = _type1_T_12 | _isSub_T_3; // @[FPUPostprocessor.scala 72:53]
  wire  _type2_T_2 = _maxmin_T_1 | _maxmin_T; // @[FPUPostprocessor.scala 77:55]
  wire  _type2_T_3 = io_in_bits_op == 5'h12; // @[FPUPostprocessor.scala 79:30]
  wire  _type2_T_4 = _type2_T_2 | _type2_T_3; // @[FPUPostprocessor.scala 78:55]
  wire  _type2_T_5 = io_in_bits_op == 5'h13; // @[FPUPostprocessor.scala 80:30]
  wire  _type2_T_6 = _type2_T_4 | _type2_T_5; // @[FPUPostprocessor.scala 79:55]
  wire  _type2_T_7 = io_in_bits_op == 5'h14; // @[FPUPostprocessor.scala 81:30]
  wire  _type2_T_8 = _type2_T_6 | _type2_T_7; // @[FPUPostprocessor.scala 80:55]
  wire  _type2_T_9 = io_in_bits_op == 5'hf; // @[FPUPostprocessor.scala 82:30]
  wire  _type2_T_10 = _type2_T_8 | _type2_T_9; // @[FPUPostprocessor.scala 81:55]
  wire  _type2_T_11 = io_in_bits_op == 5'h11; // @[FPUPostprocessor.scala 83:30]
  wire  type2 = _type2_T_10 | _type2_T_11; // @[FPUPostprocessor.scala 82:55]
  wire  _type4_T = io_in_bits_op == 5'he; // @[FPUPostprocessor.scala 96:30]
  wire  _type4_T_1 = io_in_bits_op == 5'h10; // @[FPUPostprocessor.scala 97:30]
  wire  type4 = io_in_bits_op == 5'he | _type4_T_1; // @[FPUPostprocessor.scala 96:53]
  wire [7:0] _GEN_10 = type3 ? $signed(io_in_bits_result_exponent) : $signed(_finalExponent_T_2); // @[FPUPostprocessor.scala 116:15 117:21 49:17]
  wire [25:0] _GEN_11 = type3 ? io_in_bits_result_mantissa : {{3'd0}, normalizedMantissa[22:0]}; // @[FPUPostprocessor.scala 116:15 118:21 48:17]
  wire [31:0] _result_T_10 = isPos | io_in_bits_result_sign & maxExp & io_in_bits_result_mantissa[22:0] != 23'h0 ? 32'h7fffffff
     : 32'h80000000; // @[FPUPostprocessor.scala 144:20]
  wire [7:0] _integer_T_2 = io_in_bits_result_exponent - 8'h17; // @[FPUPostprocessor.scala 148:39]
  wire [280:0] _GEN_7 = {{255'd0}, io_in_bits_result_mantissa}; // @[FPUPostprocessor.scala 148:25]
  wire [280:0] _integer_T_3 = _GEN_7 << _integer_T_2; // @[FPUPostprocessor.scala 148:25]
  wire [7:0] _integer_T_6 = 8'h17 - io_in_bits_result_exponent; // @[FPUPostprocessor.scala 150:33]
  wire [25:0] _integer_T_7 = io_in_bits_result_mantissa >> _integer_T_6; // @[FPUPostprocessor.scala 150:25]
  wire [277:0] _GEN_12 = {{255'd0}, io_in_bits_result_mantissa[22:0]}; // @[FPUPostprocessor.scala 155:34]
  wire [277:0] _outbits_T_3 = _GEN_12 << io_in_bits_result_exponent; // @[FPUPostprocessor.scala 155:34]
  wire [277:0] _GEN_37 = $signed(io_in_bits_result_exponent) < 8'sh0 ? {{255'd0}, io_in_bits_result_mantissa[22:0]} :
    _outbits_T_3; // @[FPUPostprocessor.scala 152:25 153:19 155:19]
  wire [31:0] outbits = _GEN_37[31:0]; // @[FPUPostprocessor.scala 151:34]
  wire  _GEN_46 = outbits != 32'h0 | io_in_bits_fcsr_0; // @[FPUPostprocessor.scala 157:31 30:20]
  wire [280:0] _GEN_54 = $signed(io_in_bits_result_exponent) >= 8'sh17 ? _integer_T_3 : {{255'd0}, _integer_T_7}; // @[FPUPostprocessor.scala 147:25 148:17 150:17]
  wire  _GEN_55 = $signed(io_in_bits_result_exponent) >= 8'sh17 ? io_in_bits_fcsr_0 : _GEN_46; // @[FPUPostprocessor.scala 147:25 30:20]
  wire [31:0] integer_ = _GEN_54[31:0]; // @[FPUPostprocessor.scala 146:32]
  wire [30:0] _result_T_13 = ~integer_[30:0]; // @[FPUPostprocessor.scala 162:56]
  wire [31:0] _result_T_14 = {io_in_bits_result_sign,_result_T_13}; // @[Cat.scala 33:92]
  wire [31:0] _result_T_16 = _result_T_14 + 32'h1; // @[FPUPostprocessor.scala 162:73]
  wire [31:0] _result_T_17 = isPos ? integer_ : _result_T_16; // @[FPUPostprocessor.scala 162:22]
  wire [31:0] _GEN_63 = _type4_T ? _result_T_17 : integer_; // @[FPUPostprocessor.scala 161:50 162:16 164:16]
  wire  _GEN_64 = _type4_T & $signed(io_in_bits_result_exponent) > 8'sh1e ? io_in_bits_fcsr_0 : _GEN_55; // @[FPUPostprocessor.scala 142:73]
  wire  _GEN_68 = _type4_T & $signed(io_in_bits_result_exponent) > 8'sh1e | io_in_bits_fcsr_4; // @[FPUPostprocessor.scala 142:73]
  wire [31:0] _GEN_72 = _type4_T & $signed(io_in_bits_result_exponent) > 8'sh1e ? _result_T_10 : _GEN_63; // @[FPUPostprocessor.scala 142:73 144:14]
  wire  _GEN_73 = _type4_T_1 & $signed(io_in_bits_result_exponent) > 8'sh1f ? io_in_bits_fcsr_0 : _GEN_64; // @[FPUPostprocessor.scala 136:74]
  wire  _GEN_77 = _type4_T_1 & $signed(io_in_bits_result_exponent) > 8'sh1f | _GEN_68; // @[FPUPostprocessor.scala 136:74]
  wire [31:0] _GEN_81 = _type4_T_1 & $signed(io_in_bits_result_exponent) > 8'sh1f ? 32'hffffffff : _GEN_72; // @[FPUPostprocessor.scala 136:74 141:14]
  wire  _GEN_82 = _type4_T_1 & ~isPos & ~(maxExp & rNaN) ? io_in_bits_fcsr_0 : _GEN_73; // @[FPUPostprocessor.scala 132:82]
  wire  _GEN_86 = _type4_T_1 & ~isPos & ~(maxExp & rNaN) | _GEN_77; // @[FPUPostprocessor.scala 132:82]
  wire [31:0] _GEN_90 = _type4_T_1 & ~isPos & ~(maxExp & rNaN) ? 32'h0 : _GEN_81; // @[FPUPostprocessor.scala 132:82 135:14]
  wire  _GEN_91 = type4 ? _GEN_82 : io_in_bits_fcsr_0; // @[FPUPostprocessor.scala 122:15 30:20]
  wire  ff_4 = type4 ? _GEN_86 : io_in_bits_fcsr_4; // @[FPUPostprocessor.scala 122:15 30:20]
  wire [31:0] result = type4 ? _GEN_90 : 32'h0; // @[FPUPostprocessor.scala 122:15 38:34]
  wire  _T_48 = $signed(io_in_bits_result_exponent) != 8'sh7f; // @[FPUPostprocessor.scala 191:44]
  wire  _T_49 = io_in_bits_result_mantissa == 26'h0 & _T_48; // @[FPUPostprocessor.scala 190:53]
  wire [7:0] _GEN_100 = ff_4 ? $signed(io_in_bits_result_exponent) : $signed(_finalExponent_T_2); // @[FPUPostprocessor.scala 171:19 174:27 175:23]
  wire [25:0] _GEN_101 = ff_4 ? io_in_bits_result_mantissa : normalizedMantissa; // @[FPUPostprocessor.scala 170:19 174:27 176:23]
  wire [7:0] _GEN_103 = maxmin ? $signed(_GEN_100) : $signed(_finalExponent_T_2); // @[FPUPostprocessor.scala 171:19 173:84]
  wire [25:0] _GEN_104 = maxmin ? _GEN_101 : normalizedMantissa; // @[FPUPostprocessor.scala 170:19 173:84]
  wire [25:0] _GEN_106 = type2 ? _GEN_104 : _GEN_11; // @[FPUPostprocessor.scala 169:15]
  wire [7:0] _GEN_107 = type2 ? $signed(_GEN_103) : $signed(_GEN_10); // @[FPUPostprocessor.scala 169:15]
  wire [25:0] _GEN_141 = $signed(adjExponent) < -8'sh7e ? 26'h0 : normalizedMantissa; // @[FPUPostprocessor.scala 199:46 200:23 206:23]
  wire [7:0] _GEN_142 = $signed(adjExponent) < -8'sh7e ? $signed(8'sh1) : $signed(_finalExponent_T_2); // @[FPUPostprocessor.scala 199:46 201:23 205:23]
  wire  _GEN_143 = $signed(adjExponent) < -8'sh7e | _GEN_91; // @[FPUPostprocessor.scala 199:46]
  wire  _GEN_144 = $signed(adjExponent) < -8'sh7e | io_in_bits_fcsr_1; // @[FPUPostprocessor.scala 199:46]
  wire [7:0] _GEN_161 = _T_49 ? $signed(8'sh0) : $signed(_GEN_142); // @[FPUPostprocessor.scala 191:62 192:21]
  wire [25:0] _GEN_162 = _T_49 ? _GEN_106 : _GEN_141; // @[FPUPostprocessor.scala 191:62]
  wire  _GEN_163 = _T_49 ? _GEN_91 : _GEN_143; // @[FPUPostprocessor.scala 191:62]
  wire  _GEN_164 = _T_49 ? io_in_bits_fcsr_1 : _GEN_144; // @[FPUPostprocessor.scala 191:62]
  wire [25:0] _GEN_171 = io_in_bits_fcsr_3 ? {{3'd0}, io_in_bits_result_mantissa[22:0]} : _GEN_162; // @[FPUPostprocessor.scala 187:44 188:21]
  wire [8:0] _GEN_172 = io_in_bits_fcsr_3 ? $signed(9'shff) : $signed({{1{_GEN_161[7]}},_GEN_161}); // @[FPUPostprocessor.scala 187:44 189:21]
  wire  _GEN_173 = io_in_bits_fcsr_3 ? _GEN_91 : _GEN_163; // @[FPUPostprocessor.scala 187:44]
  wire  _GEN_174 = io_in_bits_fcsr_3 ? io_in_bits_fcsr_1 : _GEN_164; // @[FPUPostprocessor.scala 187:44]
  wire [25:0] _GEN_181 = io_in_bits_fcsr_4 ? io_in_bits_result_mantissa : _GEN_171; // @[FPUPostprocessor.scala 184:38 185:21]
  wire [8:0] _GEN_182 = io_in_bits_fcsr_4 ? $signed({{1{io_in_bits_result_exponent[7]}},io_in_bits_result_exponent}) :
    $signed(_GEN_172); // @[FPUPostprocessor.scala 184:38 186:21]
  wire  _GEN_183 = io_in_bits_fcsr_4 ? _GEN_91 : _GEN_173; // @[FPUPostprocessor.scala 184:38]
  wire  _GEN_184 = io_in_bits_fcsr_4 ? io_in_bits_fcsr_1 : _GEN_174; // @[FPUPostprocessor.scala 184:38]
  wire [25:0] _GEN_191 = type1 ? _GEN_181 : _GEN_106; // @[FPUPostprocessor.scala 183:15]
  wire [8:0] _GEN_192 = type1 ? $signed(_GEN_182) : $signed({{1{_GEN_107[7]}},_GEN_107}); // @[FPUPostprocessor.scala 183:15]
  wire  _T_57 = ~io_in_bits_result_mantissa[23]; // @[FPUPostprocessor.scala 213:40]
  wire  _T_59 = ~io_in_bits_result_mantissa[23] & minExp; // @[FPUPostprocessor.scala 213:49]
  wire  _T_60 = type1 | type2; // @[FPUPostprocessor.scala 215:14]
  wire  _T_61 = _T_59 & _T_60; // @[FPUPostprocessor.scala 214:47]
  wire  _T_65 = io_in_bits_result_exponent == 8'h80; // @[FPUPostprocessor.scala 220:49]
  wire  _T_66 = _T_57 & _T_65; // @[FPUPostprocessor.scala 219:56]
  wire  _T_68 = _T_66 & _T_60; // @[FPUPostprocessor.scala 220:60]
  wire  _T_73 = io_in_bits_result_mantissa[22] & _T_65; // @[FPUPostprocessor.scala 225:56]
  wire  _T_75 = _T_73 & _T_60; // @[FPUPostprocessor.scala 226:60]
  wire [8:0] _GEN_202 = _T_75 ? $signed(9'shff) : $signed(_GEN_192); // @[FPUPostprocessor.scala 227:32 228:21]
  wire [25:0] _GEN_203 = _T_75 ? {{3'd0}, io_in_bits_result_mantissa[22:0]} : _GEN_191; // @[FPUPostprocessor.scala 227:32 229:21]
  wire [8:0] _GEN_205 = _T_68 ? $signed(9'shff) : $signed(_GEN_202); // @[FPUPostprocessor.scala 221:32 222:21]
  wire [25:0] _GEN_206 = _T_68 ? {{3'd0}, io_in_bits_result_mantissa[22:0]} : _GEN_203; // @[FPUPostprocessor.scala 221:32 223:21]
  wire [8:0] _GEN_208 = _T_61 ? $signed(9'sh0) : $signed(_GEN_205); // @[FPUPostprocessor.scala 215:25 216:21]
  wire [25:0] _GEN_209 = _T_61 ? {{3'd0}, io_in_bits_result_mantissa[22:0]} : _GEN_206; // @[FPUPostprocessor.scala 215:25 217:21]
  wire [7:0] _io_out_bits_rd_T = _GEN_208[7:0]; // @[FPUPostprocessor.scala 240:52]
  wire [22:0] finalMantissa = _GEN_209[22:0]; // @[FPUPostprocessor.scala 35:27]
  wire [31:0] _io_out_bits_rd_T_1 = {io_in_bits_result_sign,_io_out_bits_rd_T,finalMantissa}; // @[Cat.scala 33:92]
  wire [31:0] _GEN_211 = io_in_bits_op == 5'h16 ? 32'h0 : _io_out_bits_rd_T_1; // @[FPUPostprocessor.scala 237:49 238:20 240:20]
  assign io_in_ready = io_out_ready; // @[FPUPostprocessor.scala 244:15]
  assign io_out_valid = io_in_valid; // @[FPUPostprocessor.scala 245:16]
  assign io_out_bits_rd = type4 ? result : _GEN_211; // @[FPUPostprocessor.scala 235:15 236:20]
  assign io_out_bits_fcsr_0 = type1 ? _GEN_183 : _GEN_91; // @[FPUPostprocessor.scala 183:15]
  assign io_out_bits_fcsr_1 = type1 ? _GEN_184 : io_in_bits_fcsr_1; // @[FPUPostprocessor.scala 183:15]
  assign io_out_bits_fcsr_2 = io_in_bits_fcsr_2; // @[FPUPostprocessor.scala 183:15]
  assign io_out_bits_fcsr_3 = io_in_bits_fcsr_3; // @[FPUPostprocessor.scala 183:15]
  assign io_out_bits_fcsr_4 = type4 ? _GEN_86 : io_in_bits_fcsr_4; // @[FPUPostprocessor.scala 122:15 30:20]
  assign io_out_bits_fcsr_5 = io_in_bits_fcsr_5; // @[FPUPostprocessor.scala 183:15]
  assign io_out_bits_fcsr_6 = io_in_bits_fcsr_6; // @[FPUPostprocessor.scala 183:15]
  assign io_out_bits_fcsr_7 = io_in_bits_fcsr_7; // @[FPUPostprocessor.scala 183:15]
endmodule
module fpu(
  input         clock,
  input         reset,
  output        io_in_ready,
  input         io_in_valid,
  input  [31:0] io_in_bits_rs1,
  input  [31:0] io_in_bits_rs2,
  input  [31:0] io_in_bits_rs3,
  input  [4:0]  io_in_bits_op,
  input  [7:0]  io_in_bits_fcsr,
  input         io_out_ready,
  output        io_out_valid,
  output [31:0] io_out_bits_rd,
  output [7:0]  io_out_bits_fcsr
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [31:0] _RAND_39;
  reg [31:0] _RAND_40;
  reg [31:0] _RAND_41;
  reg [31:0] _RAND_42;
  reg [31:0] _RAND_43;
`endif // RANDOMIZE_REG_INIT
  wire  preprocessor_io_in_ready; // @[FPU.scala 56:30]
  wire  preprocessor_io_in_valid; // @[FPU.scala 56:30]
  wire [31:0] preprocessor_io_in_bits_rs1; // @[FPU.scala 56:30]
  wire [31:0] preprocessor_io_in_bits_rs2; // @[FPU.scala 56:30]
  wire [31:0] preprocessor_io_in_bits_rs3; // @[FPU.scala 56:30]
  wire [4:0] preprocessor_io_in_bits_op; // @[FPU.scala 56:30]
  wire  preprocessor_io_in_bits_fcsr_0; // @[FPU.scala 56:30]
  wire  preprocessor_io_in_bits_fcsr_1; // @[FPU.scala 56:30]
  wire  preprocessor_io_in_bits_fcsr_2; // @[FPU.scala 56:30]
  wire  preprocessor_io_in_bits_fcsr_3; // @[FPU.scala 56:30]
  wire  preprocessor_io_in_bits_fcsr_4; // @[FPU.scala 56:30]
  wire  preprocessor_io_in_bits_fcsr_5; // @[FPU.scala 56:30]
  wire  preprocessor_io_in_bits_fcsr_6; // @[FPU.scala 56:30]
  wire  preprocessor_io_in_bits_fcsr_7; // @[FPU.scala 56:30]
  wire  preprocessor_io_out_ready; // @[FPU.scala 56:30]
  wire  preprocessor_io_out_valid; // @[FPU.scala 56:30]
  wire [25:0] preprocessor_io_out_bits_a_mantissa; // @[FPU.scala 56:30]
  wire [7:0] preprocessor_io_out_bits_a_exponent; // @[FPU.scala 56:30]
  wire  preprocessor_io_out_bits_a_sign; // @[FPU.scala 56:30]
  wire [25:0] preprocessor_io_out_bits_b_mantissa; // @[FPU.scala 56:30]
  wire [7:0] preprocessor_io_out_bits_b_exponent; // @[FPU.scala 56:30]
  wire  preprocessor_io_out_bits_b_sign; // @[FPU.scala 56:30]
  wire [25:0] preprocessor_io_out_bits_c_mantissa; // @[FPU.scala 56:30]
  wire [7:0] preprocessor_io_out_bits_c_exponent; // @[FPU.scala 56:30]
  wire  preprocessor_io_out_bits_c_sign; // @[FPU.scala 56:30]
  wire [4:0] preprocessor_io_out_bits_op; // @[FPU.scala 56:30]
  wire  preprocessor_io_out_bits_fcsr_0; // @[FPU.scala 56:30]
  wire  preprocessor_io_out_bits_fcsr_1; // @[FPU.scala 56:30]
  wire  preprocessor_io_out_bits_fcsr_2; // @[FPU.scala 56:30]
  wire  preprocessor_io_out_bits_fcsr_3; // @[FPU.scala 56:30]
  wire  preprocessor_io_out_bits_fcsr_4; // @[FPU.scala 56:30]
  wire  preprocessor_io_out_bits_fcsr_5; // @[FPU.scala 56:30]
  wire  preprocessor_io_out_bits_fcsr_6; // @[FPU.scala 56:30]
  wire  preprocessor_io_out_bits_fcsr_7; // @[FPU.scala 56:30]
  wire [11:0] preprocessor_io_out_bits_fflags; // @[FPU.scala 56:30]
  wire  preprocessor_io_out_bits_control_adder; // @[FPU.scala 56:30]
  wire  preprocessor_io_out_bits_control_multiplier; // @[FPU.scala 56:30]
  wire  preprocessor_io_out_bits_control_fused; // @[FPU.scala 56:30]
  wire  preprocessor_io_out_bits_control_divider; // @[FPU.scala 56:30]
  wire  preprocessor_io_out_bits_control_comparator; // @[FPU.scala 56:30]
  wire  preprocessor_io_out_bits_control_squareRoot; // @[FPU.scala 56:30]
  wire  preprocessor_io_out_bits_control_bitInject; // @[FPU.scala 56:30]
  wire  preprocessor_io_out_bits_control_classify; // @[FPU.scala 56:30]
  wire  preprocessor_io_out_bits_stall_stall; // @[FPU.scala 56:30]
  wire  executionUnit_clock; // @[FPU.scala 57:30]
  wire  executionUnit_reset; // @[FPU.scala 57:30]
  wire  executionUnit_io_in_ready; // @[FPU.scala 57:30]
  wire  executionUnit_io_in_valid; // @[FPU.scala 57:30]
  wire [25:0] executionUnit_io_in_bits_a_mantissa; // @[FPU.scala 57:30]
  wire [7:0] executionUnit_io_in_bits_a_exponent; // @[FPU.scala 57:30]
  wire  executionUnit_io_in_bits_a_sign; // @[FPU.scala 57:30]
  wire [25:0] executionUnit_io_in_bits_b_mantissa; // @[FPU.scala 57:30]
  wire [7:0] executionUnit_io_in_bits_b_exponent; // @[FPU.scala 57:30]
  wire  executionUnit_io_in_bits_b_sign; // @[FPU.scala 57:30]
  wire [25:0] executionUnit_io_in_bits_c_mantissa; // @[FPU.scala 57:30]
  wire [7:0] executionUnit_io_in_bits_c_exponent; // @[FPU.scala 57:30]
  wire  executionUnit_io_in_bits_c_sign; // @[FPU.scala 57:30]
  wire [4:0] executionUnit_io_in_bits_op; // @[FPU.scala 57:30]
  wire  executionUnit_io_in_bits_fcsr_0; // @[FPU.scala 57:30]
  wire  executionUnit_io_in_bits_fcsr_1; // @[FPU.scala 57:30]
  wire  executionUnit_io_in_bits_fcsr_2; // @[FPU.scala 57:30]
  wire  executionUnit_io_in_bits_fcsr_3; // @[FPU.scala 57:30]
  wire  executionUnit_io_in_bits_fcsr_4; // @[FPU.scala 57:30]
  wire  executionUnit_io_in_bits_fcsr_5; // @[FPU.scala 57:30]
  wire  executionUnit_io_in_bits_fcsr_6; // @[FPU.scala 57:30]
  wire  executionUnit_io_in_bits_fcsr_7; // @[FPU.scala 57:30]
  wire [11:0] executionUnit_io_in_bits_fflags; // @[FPU.scala 57:30]
  wire  executionUnit_io_in_bits_control_adder; // @[FPU.scala 57:30]
  wire  executionUnit_io_in_bits_control_multiplier; // @[FPU.scala 57:30]
  wire  executionUnit_io_in_bits_control_fused; // @[FPU.scala 57:30]
  wire  executionUnit_io_in_bits_control_divider; // @[FPU.scala 57:30]
  wire  executionUnit_io_in_bits_control_comparator; // @[FPU.scala 57:30]
  wire  executionUnit_io_in_bits_control_squareRoot; // @[FPU.scala 57:30]
  wire  executionUnit_io_in_bits_control_bitInject; // @[FPU.scala 57:30]
  wire  executionUnit_io_in_bits_control_classify; // @[FPU.scala 57:30]
  wire  executionUnit_io_in_bits_stall_stall; // @[FPU.scala 57:30]
  wire [4:0] executionUnit_io_in_bits_stall_op; // @[FPU.scala 57:30]
  wire  executionUnit_io_in_bits_stall_pass; // @[FPU.scala 57:30]
  wire  executionUnit_io_out_valid; // @[FPU.scala 57:30]
  wire [25:0] executionUnit_io_out_bits_result_mantissa; // @[FPU.scala 57:30]
  wire [7:0] executionUnit_io_out_bits_result_exponent; // @[FPU.scala 57:30]
  wire  executionUnit_io_out_bits_result_sign; // @[FPU.scala 57:30]
  wire  executionUnit_io_out_bits_fcsr_0; // @[FPU.scala 57:30]
  wire  executionUnit_io_out_bits_fcsr_1; // @[FPU.scala 57:30]
  wire  executionUnit_io_out_bits_fcsr_2; // @[FPU.scala 57:30]
  wire  executionUnit_io_out_bits_fcsr_3; // @[FPU.scala 57:30]
  wire  executionUnit_io_out_bits_fcsr_4; // @[FPU.scala 57:30]
  wire  executionUnit_io_out_bits_fcsr_5; // @[FPU.scala 57:30]
  wire  executionUnit_io_out_bits_fcsr_6; // @[FPU.scala 57:30]
  wire  executionUnit_io_out_bits_fcsr_7; // @[FPU.scala 57:30]
  wire [4:0] executionUnit_io_out_bits_op; // @[FPU.scala 57:30]
  wire [4:0] executionUnit_io_out_bits_stall_op; // @[FPU.scala 57:30]
  wire  executionUnit_io_out_bits_stall_pass; // @[FPU.scala 57:30]
  wire  executionUnit_io_out_bits_stall_inProc; // @[FPU.scala 57:30]
  wire  postprocessor_io_in_ready; // @[FPU.scala 58:30]
  wire  postprocessor_io_in_valid; // @[FPU.scala 58:30]
  wire [25:0] postprocessor_io_in_bits_result_mantissa; // @[FPU.scala 58:30]
  wire [7:0] postprocessor_io_in_bits_result_exponent; // @[FPU.scala 58:30]
  wire  postprocessor_io_in_bits_result_sign; // @[FPU.scala 58:30]
  wire [4:0] postprocessor_io_in_bits_op; // @[FPU.scala 58:30]
  wire  postprocessor_io_in_bits_fcsr_0; // @[FPU.scala 58:30]
  wire  postprocessor_io_in_bits_fcsr_1; // @[FPU.scala 58:30]
  wire  postprocessor_io_in_bits_fcsr_2; // @[FPU.scala 58:30]
  wire  postprocessor_io_in_bits_fcsr_3; // @[FPU.scala 58:30]
  wire  postprocessor_io_in_bits_fcsr_4; // @[FPU.scala 58:30]
  wire  postprocessor_io_in_bits_fcsr_5; // @[FPU.scala 58:30]
  wire  postprocessor_io_in_bits_fcsr_6; // @[FPU.scala 58:30]
  wire  postprocessor_io_in_bits_fcsr_7; // @[FPU.scala 58:30]
  wire  postprocessor_io_out_ready; // @[FPU.scala 58:30]
  wire  postprocessor_io_out_valid; // @[FPU.scala 58:30]
  wire [31:0] postprocessor_io_out_bits_rd; // @[FPU.scala 58:30]
  wire  postprocessor_io_out_bits_fcsr_0; // @[FPU.scala 58:30]
  wire  postprocessor_io_out_bits_fcsr_1; // @[FPU.scala 58:30]
  wire  postprocessor_io_out_bits_fcsr_2; // @[FPU.scala 58:30]
  wire  postprocessor_io_out_bits_fcsr_3; // @[FPU.scala 58:30]
  wire  postprocessor_io_out_bits_fcsr_4; // @[FPU.scala 58:30]
  wire  postprocessor_io_out_bits_fcsr_5; // @[FPU.scala 58:30]
  wire  postprocessor_io_out_bits_fcsr_6; // @[FPU.scala 58:30]
  wire  postprocessor_io_out_bits_fcsr_7; // @[FPU.scala 58:30]
  reg [25:0] preprocessorReg_a_mantissa; // @[Reg.scala 19:16]
  reg [7:0] preprocessorReg_a_exponent; // @[Reg.scala 19:16]
  reg  preprocessorReg_a_sign; // @[Reg.scala 19:16]
  reg [25:0] preprocessorReg_b_mantissa; // @[Reg.scala 19:16]
  reg [7:0] preprocessorReg_b_exponent; // @[Reg.scala 19:16]
  reg  preprocessorReg_b_sign; // @[Reg.scala 19:16]
  reg [25:0] preprocessorReg_c_mantissa; // @[Reg.scala 19:16]
  reg [7:0] preprocessorReg_c_exponent; // @[Reg.scala 19:16]
  reg  preprocessorReg_c_sign; // @[Reg.scala 19:16]
  reg [4:0] preprocessorReg_op; // @[Reg.scala 19:16]
  reg  preprocessorReg_fcsr_0; // @[Reg.scala 19:16]
  reg  preprocessorReg_fcsr_1; // @[Reg.scala 19:16]
  reg  preprocessorReg_fcsr_2; // @[Reg.scala 19:16]
  reg  preprocessorReg_fcsr_3; // @[Reg.scala 19:16]
  reg  preprocessorReg_fcsr_4; // @[Reg.scala 19:16]
  reg  preprocessorReg_fcsr_5; // @[Reg.scala 19:16]
  reg  preprocessorReg_fcsr_6; // @[Reg.scala 19:16]
  reg  preprocessorReg_fcsr_7; // @[Reg.scala 19:16]
  reg [11:0] preprocessorReg_fflags; // @[Reg.scala 19:16]
  reg  preprocessorReg_control_adder; // @[Reg.scala 19:16]
  reg  preprocessorReg_control_multiplier; // @[Reg.scala 19:16]
  reg  preprocessorReg_control_fused; // @[Reg.scala 19:16]
  reg  preprocessorReg_control_divider; // @[Reg.scala 19:16]
  reg  preprocessorReg_control_comparator; // @[Reg.scala 19:16]
  reg  preprocessorReg_control_squareRoot; // @[Reg.scala 19:16]
  reg  preprocessorReg_control_bitInject; // @[Reg.scala 19:16]
  reg  preprocessorReg_control_classify; // @[Reg.scala 19:16]
  reg  preprocessorReg_stall_stall; // @[Reg.scala 19:16]
  reg [4:0] preprocessorReg_stall_op; // @[Reg.scala 19:16]
  reg  preprocessorReg_stall_pass; // @[Reg.scala 19:16]
  wire  _GEN_27 = preprocessor_io_out_valid ? preprocessor_io_out_bits_stall_stall : preprocessorReg_stall_stall; // @[Reg.scala 19:16 20:{18,22}]
  reg  preprocessorValidReg; // @[FPU.scala 125:41]
  wire  _T = ~preprocessorReg_stall_stall; // @[FPU.scala 132:48]
  wire  _T_2 = ~preprocessor_io_out_bits_stall_stall; // @[FPU.scala 136:15]
  wire  _GEN_31 = ~preprocessor_io_out_bits_stall_stall & preprocessorReg_stall_stall | _GEN_27; // @[FPU.scala 136:85 137:34]
  wire  _GEN_32 = ~preprocessor_io_out_bits_stall_stall & preprocessorReg_stall_stall ? 1'h0 :
    executionUnit_io_out_bits_stall_pass; // @[FPU.scala 127:32 136:85 138:34]
  wire  _GEN_33 = preprocessor_io_out_bits_stall_stall & ~preprocessorReg_stall_stall | _GEN_31; // @[FPU.scala 132:78 133:34]
  wire  _GEN_34 = preprocessor_io_out_bits_stall_stall & ~preprocessorReg_stall_stall | _GEN_32; // @[FPU.scala 132:78 134:34]
  wire [4:0] _GEN_35 = preprocessor_io_out_bits_stall_stall & ~preprocessorReg_stall_stall ? preprocessor_io_out_bits_op
     : executionUnit_io_out_bits_stall_op; // @[FPU.scala 128:32 132:78 135:34]
  wire  _GEN_36 = ~io_in_valid ? 1'h0 : _GEN_33; // @[FPU.scala 142:22 143:34]
  wire  _GEN_37 = ~io_in_valid | _GEN_34; // @[FPU.scala 142:22 144:34]
  wire [4:0] _GEN_39 = ~io_in_valid ? 5'h16 : _GEN_35; // @[FPU.scala 142:22 146:34]
  wire  _executorReg_T = executionUnit_io_out_valid & postprocessor_io_in_ready; // @[FPU.scala 156:91]
  reg [25:0] executorReg_result_mantissa; // @[Reg.scala 19:16]
  reg [7:0] executorReg_result_exponent; // @[Reg.scala 19:16]
  reg  executorReg_result_sign; // @[Reg.scala 19:16]
  reg  executorReg_fcsr_0; // @[Reg.scala 19:16]
  reg  executorReg_fcsr_1; // @[Reg.scala 19:16]
  reg  executorReg_fcsr_2; // @[Reg.scala 19:16]
  reg  executorReg_fcsr_3; // @[Reg.scala 19:16]
  reg  executorReg_fcsr_4; // @[Reg.scala 19:16]
  reg  executorReg_fcsr_5; // @[Reg.scala 19:16]
  reg  executorReg_fcsr_6; // @[Reg.scala 19:16]
  reg  executorReg_fcsr_7; // @[Reg.scala 19:16]
  reg [4:0] executorReg_op; // @[Reg.scala 19:16]
  reg  executorValidReg; // @[FPU.scala 157:34]
  wire  endStall = executorValidReg & ~preprocessorReg_stall_pass & preprocessorReg_stall_stall; // @[FPU.scala 161:66]
  wire  _T_7 = ~executionUnit_io_out_bits_stall_inProc; // @[FPU.scala 168:27]
  wire  _GEN_57 = endStall & ~executionUnit_io_out_bits_stall_inProc | _GEN_36; // @[FPU.scala 168:68 169:34]
  wire  _GEN_58 = endStall & ~executionUnit_io_out_bits_stall_inProc | _GEN_37; // @[FPU.scala 168:68 170:34]
  wire  _GEN_60 = endStall & ~executionUnit_io_out_bits_stall_inProc | executionUnit_io_in_ready; // @[FPU.scala 120:29 168:68 172:34]
  wire  _GEN_62 = endStall & _T_2 ? 1'h0 : _GEN_58; // @[FPU.scala 165:59 167:34]
  wire  _GEN_64 = endStall & _T_2 ? executionUnit_io_in_ready : _GEN_60; // @[FPU.scala 120:29 165:59]
  wire  _fused_T_1 = preprocessorReg_stall_op == 5'h9; // @[FPU.scala 176:41]
  wire  _fused_T_2 = preprocessorReg_stall_op == 5'h7 | _fused_T_1; // @[FPU.scala 175:64]
  wire  _fused_T_3 = preprocessorReg_stall_op == 5'h8; // @[FPU.scala 177:41]
  wire  _fused_T_4 = _fused_T_2 | _fused_T_3; // @[FPU.scala 176:65]
  wire  _fused_T_5 = preprocessorReg_stall_op == 5'ha; // @[FPU.scala 178:41]
  wire  fused = _fused_T_4 | _fused_T_5; // @[FPU.scala 177:64]
  wire  _T_11 = fused & preprocessorReg_control_fused; // @[FPU.scala 184:22]
  wire  _T_12 = preprocessorReg_stall_op == preprocessorReg_op | _T_11; // @[FPU.scala 183:66]
  wire  _T_14 = preprocessor_io_out_bits_control_fused & preprocessorReg_control_fused; // @[FPU.scala 187:50]
  wire  _T_15 = preprocessor_io_out_bits_op == preprocessorReg_op | _T_14; // @[FPU.scala 186:63]
  wire  _GEN_65 = _T_15 | _GEN_64; // @[FPU.scala 187:85 188:33]
  wire  _GEN_66 = _T_12 | preprocessorReg_stall_pass; // @[FPU.scala 151:30 184:58 185:41]
  wire  _GEN_67 = _T_12 ? _GEN_65 : _GEN_64; // @[FPU.scala 184:58]
  wire  _GEN_70 = _T ? _GEN_64 : _GEN_67; // @[FPU.scala 181:38]
  wire  _T_18 = preprocessor_io_out_bits_op == preprocessorReg_stall_op; // @[FPU.scala 194:37]
  wire  _T_19 = preprocessorReg_stall_stall & _T_7 & _T_18; // @[FPU.scala 193:79]
  wire [3:0] io_out_bits_fcsr_lo = {postprocessor_io_out_bits_fcsr_3,postprocessor_io_out_bits_fcsr_2,
    postprocessor_io_out_bits_fcsr_1,postprocessor_io_out_bits_fcsr_0}; // @[FPU.scala 207:60]
  wire [3:0] io_out_bits_fcsr_hi = {postprocessor_io_out_bits_fcsr_7,postprocessor_io_out_bits_fcsr_6,
    postprocessor_io_out_bits_fcsr_5,postprocessor_io_out_bits_fcsr_4}; // @[FPU.scala 207:60]
  FPUPreprocessor preprocessor ( // @[FPU.scala 56:30]
    .io_in_ready(preprocessor_io_in_ready),
    .io_in_valid(preprocessor_io_in_valid),
    .io_in_bits_rs1(preprocessor_io_in_bits_rs1),
    .io_in_bits_rs2(preprocessor_io_in_bits_rs2),
    .io_in_bits_rs3(preprocessor_io_in_bits_rs3),
    .io_in_bits_op(preprocessor_io_in_bits_op),
    .io_in_bits_fcsr_0(preprocessor_io_in_bits_fcsr_0),
    .io_in_bits_fcsr_1(preprocessor_io_in_bits_fcsr_1),
    .io_in_bits_fcsr_2(preprocessor_io_in_bits_fcsr_2),
    .io_in_bits_fcsr_3(preprocessor_io_in_bits_fcsr_3),
    .io_in_bits_fcsr_4(preprocessor_io_in_bits_fcsr_4),
    .io_in_bits_fcsr_5(preprocessor_io_in_bits_fcsr_5),
    .io_in_bits_fcsr_6(preprocessor_io_in_bits_fcsr_6),
    .io_in_bits_fcsr_7(preprocessor_io_in_bits_fcsr_7),
    .io_out_ready(preprocessor_io_out_ready),
    .io_out_valid(preprocessor_io_out_valid),
    .io_out_bits_a_mantissa(preprocessor_io_out_bits_a_mantissa),
    .io_out_bits_a_exponent(preprocessor_io_out_bits_a_exponent),
    .io_out_bits_a_sign(preprocessor_io_out_bits_a_sign),
    .io_out_bits_b_mantissa(preprocessor_io_out_bits_b_mantissa),
    .io_out_bits_b_exponent(preprocessor_io_out_bits_b_exponent),
    .io_out_bits_b_sign(preprocessor_io_out_bits_b_sign),
    .io_out_bits_c_mantissa(preprocessor_io_out_bits_c_mantissa),
    .io_out_bits_c_exponent(preprocessor_io_out_bits_c_exponent),
    .io_out_bits_c_sign(preprocessor_io_out_bits_c_sign),
    .io_out_bits_op(preprocessor_io_out_bits_op),
    .io_out_bits_fcsr_0(preprocessor_io_out_bits_fcsr_0),
    .io_out_bits_fcsr_1(preprocessor_io_out_bits_fcsr_1),
    .io_out_bits_fcsr_2(preprocessor_io_out_bits_fcsr_2),
    .io_out_bits_fcsr_3(preprocessor_io_out_bits_fcsr_3),
    .io_out_bits_fcsr_4(preprocessor_io_out_bits_fcsr_4),
    .io_out_bits_fcsr_5(preprocessor_io_out_bits_fcsr_5),
    .io_out_bits_fcsr_6(preprocessor_io_out_bits_fcsr_6),
    .io_out_bits_fcsr_7(preprocessor_io_out_bits_fcsr_7),
    .io_out_bits_fflags(preprocessor_io_out_bits_fflags),
    .io_out_bits_control_adder(preprocessor_io_out_bits_control_adder),
    .io_out_bits_control_multiplier(preprocessor_io_out_bits_control_multiplier),
    .io_out_bits_control_fused(preprocessor_io_out_bits_control_fused),
    .io_out_bits_control_divider(preprocessor_io_out_bits_control_divider),
    .io_out_bits_control_comparator(preprocessor_io_out_bits_control_comparator),
    .io_out_bits_control_squareRoot(preprocessor_io_out_bits_control_squareRoot),
    .io_out_bits_control_bitInject(preprocessor_io_out_bits_control_bitInject),
    .io_out_bits_control_classify(preprocessor_io_out_bits_control_classify),
    .io_out_bits_stall_stall(preprocessor_io_out_bits_stall_stall)
  );
  FPUExecutionUnit executionUnit ( // @[FPU.scala 57:30]
    .clock(executionUnit_clock),
    .reset(executionUnit_reset),
    .io_in_ready(executionUnit_io_in_ready),
    .io_in_valid(executionUnit_io_in_valid),
    .io_in_bits_a_mantissa(executionUnit_io_in_bits_a_mantissa),
    .io_in_bits_a_exponent(executionUnit_io_in_bits_a_exponent),
    .io_in_bits_a_sign(executionUnit_io_in_bits_a_sign),
    .io_in_bits_b_mantissa(executionUnit_io_in_bits_b_mantissa),
    .io_in_bits_b_exponent(executionUnit_io_in_bits_b_exponent),
    .io_in_bits_b_sign(executionUnit_io_in_bits_b_sign),
    .io_in_bits_c_mantissa(executionUnit_io_in_bits_c_mantissa),
    .io_in_bits_c_exponent(executionUnit_io_in_bits_c_exponent),
    .io_in_bits_c_sign(executionUnit_io_in_bits_c_sign),
    .io_in_bits_op(executionUnit_io_in_bits_op),
    .io_in_bits_fcsr_0(executionUnit_io_in_bits_fcsr_0),
    .io_in_bits_fcsr_1(executionUnit_io_in_bits_fcsr_1),
    .io_in_bits_fcsr_2(executionUnit_io_in_bits_fcsr_2),
    .io_in_bits_fcsr_3(executionUnit_io_in_bits_fcsr_3),
    .io_in_bits_fcsr_4(executionUnit_io_in_bits_fcsr_4),
    .io_in_bits_fcsr_5(executionUnit_io_in_bits_fcsr_5),
    .io_in_bits_fcsr_6(executionUnit_io_in_bits_fcsr_6),
    .io_in_bits_fcsr_7(executionUnit_io_in_bits_fcsr_7),
    .io_in_bits_fflags(executionUnit_io_in_bits_fflags),
    .io_in_bits_control_adder(executionUnit_io_in_bits_control_adder),
    .io_in_bits_control_multiplier(executionUnit_io_in_bits_control_multiplier),
    .io_in_bits_control_fused(executionUnit_io_in_bits_control_fused),
    .io_in_bits_control_divider(executionUnit_io_in_bits_control_divider),
    .io_in_bits_control_comparator(executionUnit_io_in_bits_control_comparator),
    .io_in_bits_control_squareRoot(executionUnit_io_in_bits_control_squareRoot),
    .io_in_bits_control_bitInject(executionUnit_io_in_bits_control_bitInject),
    .io_in_bits_control_classify(executionUnit_io_in_bits_control_classify),
    .io_in_bits_stall_stall(executionUnit_io_in_bits_stall_stall),
    .io_in_bits_stall_op(executionUnit_io_in_bits_stall_op),
    .io_in_bits_stall_pass(executionUnit_io_in_bits_stall_pass),
    .io_out_valid(executionUnit_io_out_valid),
    .io_out_bits_result_mantissa(executionUnit_io_out_bits_result_mantissa),
    .io_out_bits_result_exponent(executionUnit_io_out_bits_result_exponent),
    .io_out_bits_result_sign(executionUnit_io_out_bits_result_sign),
    .io_out_bits_fcsr_0(executionUnit_io_out_bits_fcsr_0),
    .io_out_bits_fcsr_1(executionUnit_io_out_bits_fcsr_1),
    .io_out_bits_fcsr_2(executionUnit_io_out_bits_fcsr_2),
    .io_out_bits_fcsr_3(executionUnit_io_out_bits_fcsr_3),
    .io_out_bits_fcsr_4(executionUnit_io_out_bits_fcsr_4),
    .io_out_bits_fcsr_5(executionUnit_io_out_bits_fcsr_5),
    .io_out_bits_fcsr_6(executionUnit_io_out_bits_fcsr_6),
    .io_out_bits_fcsr_7(executionUnit_io_out_bits_fcsr_7),
    .io_out_bits_op(executionUnit_io_out_bits_op),
    .io_out_bits_stall_op(executionUnit_io_out_bits_stall_op),
    .io_out_bits_stall_pass(executionUnit_io_out_bits_stall_pass),
    .io_out_bits_stall_inProc(executionUnit_io_out_bits_stall_inProc)
  );
  FPUPostprocessor postprocessor ( // @[FPU.scala 58:30]
    .io_in_ready(postprocessor_io_in_ready),
    .io_in_valid(postprocessor_io_in_valid),
    .io_in_bits_result_mantissa(postprocessor_io_in_bits_result_mantissa),
    .io_in_bits_result_exponent(postprocessor_io_in_bits_result_exponent),
    .io_in_bits_result_sign(postprocessor_io_in_bits_result_sign),
    .io_in_bits_op(postprocessor_io_in_bits_op),
    .io_in_bits_fcsr_0(postprocessor_io_in_bits_fcsr_0),
    .io_in_bits_fcsr_1(postprocessor_io_in_bits_fcsr_1),
    .io_in_bits_fcsr_2(postprocessor_io_in_bits_fcsr_2),
    .io_in_bits_fcsr_3(postprocessor_io_in_bits_fcsr_3),
    .io_in_bits_fcsr_4(postprocessor_io_in_bits_fcsr_4),
    .io_in_bits_fcsr_5(postprocessor_io_in_bits_fcsr_5),
    .io_in_bits_fcsr_6(postprocessor_io_in_bits_fcsr_6),
    .io_in_bits_fcsr_7(postprocessor_io_in_bits_fcsr_7),
    .io_out_ready(postprocessor_io_out_ready),
    .io_out_valid(postprocessor_io_out_valid),
    .io_out_bits_rd(postprocessor_io_out_bits_rd),
    .io_out_bits_fcsr_0(postprocessor_io_out_bits_fcsr_0),
    .io_out_bits_fcsr_1(postprocessor_io_out_bits_fcsr_1),
    .io_out_bits_fcsr_2(postprocessor_io_out_bits_fcsr_2),
    .io_out_bits_fcsr_3(postprocessor_io_out_bits_fcsr_3),
    .io_out_bits_fcsr_4(postprocessor_io_out_bits_fcsr_4),
    .io_out_bits_fcsr_5(postprocessor_io_out_bits_fcsr_5),
    .io_out_bits_fcsr_6(postprocessor_io_out_bits_fcsr_6),
    .io_out_bits_fcsr_7(postprocessor_io_out_bits_fcsr_7)
  );
  assign io_in_ready = preprocessor_io_in_ready; // @[FPU.scala 163:15]
  assign io_out_valid = postprocessor_io_out_valid; // @[FPU.scala 208:20]
  assign io_out_bits_rd = postprocessor_io_out_bits_rd; // @[FPU.scala 206:20]
  assign io_out_bits_fcsr = {io_out_bits_fcsr_hi,io_out_bits_fcsr_lo}; // @[FPU.scala 207:60]
  assign preprocessor_io_in_valid = io_in_valid; // @[FPU.scala 112:31]
  assign preprocessor_io_in_bits_rs1 = io_in_bits_rs1; // @[FPU.scala 114:31]
  assign preprocessor_io_in_bits_rs2 = io_in_bits_rs2; // @[FPU.scala 115:31]
  assign preprocessor_io_in_bits_rs3 = io_in_bits_rs3; // @[FPU.scala 116:31]
  assign preprocessor_io_in_bits_op = io_in_bits_op; // @[FPU.scala 113:31]
  assign preprocessor_io_in_bits_fcsr_0 = io_in_bits_fcsr[0]; // @[FPU.scala 118:55]
  assign preprocessor_io_in_bits_fcsr_1 = io_in_bits_fcsr[1]; // @[FPU.scala 118:55]
  assign preprocessor_io_in_bits_fcsr_2 = io_in_bits_fcsr[2]; // @[FPU.scala 118:55]
  assign preprocessor_io_in_bits_fcsr_3 = io_in_bits_fcsr[3]; // @[FPU.scala 118:55]
  assign preprocessor_io_in_bits_fcsr_4 = io_in_bits_fcsr[4]; // @[FPU.scala 118:55]
  assign preprocessor_io_in_bits_fcsr_5 = io_in_bits_fcsr[5]; // @[FPU.scala 118:55]
  assign preprocessor_io_in_bits_fcsr_6 = io_in_bits_fcsr[6]; // @[FPU.scala 118:55]
  assign preprocessor_io_in_bits_fcsr_7 = io_in_bits_fcsr[7]; // @[FPU.scala 118:55]
  assign preprocessor_io_out_ready = _T_19 ? _T_7 : _GEN_70; // @[FPU.scala 194:68 195:31]
  assign executionUnit_clock = clock;
  assign executionUnit_reset = reset;
  assign executionUnit_io_in_valid = preprocessorValidReg; // @[FPU.scala 152:30]
  assign executionUnit_io_in_bits_a_mantissa = preprocessorReg_a_mantissa; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_a_exponent = preprocessorReg_a_exponent; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_a_sign = preprocessorReg_a_sign; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_b_mantissa = preprocessorReg_b_mantissa; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_b_exponent = preprocessorReg_b_exponent; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_b_sign = preprocessorReg_b_sign; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_c_mantissa = preprocessorReg_c_mantissa; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_c_exponent = preprocessorReg_c_exponent; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_c_sign = preprocessorReg_c_sign; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_op = preprocessorReg_op; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_fcsr_0 = preprocessorReg_fcsr_0; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_fcsr_1 = preprocessorReg_fcsr_1; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_fcsr_2 = preprocessorReg_fcsr_2; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_fcsr_3 = preprocessorReg_fcsr_3; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_fcsr_4 = preprocessorReg_fcsr_4; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_fcsr_5 = preprocessorReg_fcsr_5; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_fcsr_6 = preprocessorReg_fcsr_6; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_fcsr_7 = preprocessorReg_fcsr_7; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_fflags = preprocessorReg_fflags; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_control_adder = preprocessorReg_control_adder; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_control_multiplier = preprocessorReg_control_multiplier; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_control_fused = preprocessorReg_control_fused; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_control_divider = preprocessorReg_control_divider; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_control_comparator = preprocessorReg_control_comparator; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_control_squareRoot = preprocessorReg_control_squareRoot; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_control_bitInject = preprocessorReg_control_bitInject; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_control_classify = preprocessorReg_control_classify; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_stall_stall = preprocessorReg_stall_stall; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_stall_op = preprocessorReg_stall_op; // @[FPU.scala 151:30]
  assign executionUnit_io_in_bits_stall_pass = _T ? preprocessorReg_stall_pass : _GEN_66; // @[FPU.scala 151:30 181:38]
  assign postprocessor_io_in_valid = executorValidReg; // @[FPU.scala 200:30]
  assign postprocessor_io_in_bits_result_mantissa = executorReg_result_mantissa; // @[FPU.scala 199:30]
  assign postprocessor_io_in_bits_result_exponent = executorReg_result_exponent; // @[FPU.scala 199:30]
  assign postprocessor_io_in_bits_result_sign = executorReg_result_sign; // @[FPU.scala 199:30]
  assign postprocessor_io_in_bits_op = executorReg_op; // @[FPU.scala 199:30]
  assign postprocessor_io_in_bits_fcsr_0 = executorReg_fcsr_0; // @[FPU.scala 199:30]
  assign postprocessor_io_in_bits_fcsr_1 = executorReg_fcsr_1; // @[FPU.scala 199:30]
  assign postprocessor_io_in_bits_fcsr_2 = executorReg_fcsr_2; // @[FPU.scala 199:30]
  assign postprocessor_io_in_bits_fcsr_3 = executorReg_fcsr_3; // @[FPU.scala 199:30]
  assign postprocessor_io_in_bits_fcsr_4 = executorReg_fcsr_4; // @[FPU.scala 199:30]
  assign postprocessor_io_in_bits_fcsr_5 = executorReg_fcsr_5; // @[FPU.scala 199:30]
  assign postprocessor_io_in_bits_fcsr_6 = executorReg_fcsr_6; // @[FPU.scala 199:30]
  assign postprocessor_io_in_bits_fcsr_7 = executorReg_fcsr_7; // @[FPU.scala 199:30]
  assign postprocessor_io_out_ready = io_out_ready; // @[FPU.scala 201:30]
  always @(posedge clock) begin
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_a_mantissa <= preprocessor_io_out_bits_a_mantissa; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_a_exponent <= preprocessor_io_out_bits_a_exponent; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_a_sign <= preprocessor_io_out_bits_a_sign; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_b_mantissa <= preprocessor_io_out_bits_b_mantissa; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_b_exponent <= preprocessor_io_out_bits_b_exponent; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_b_sign <= preprocessor_io_out_bits_b_sign; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_c_mantissa <= preprocessor_io_out_bits_c_mantissa; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_c_exponent <= preprocessor_io_out_bits_c_exponent; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_c_sign <= preprocessor_io_out_bits_c_sign; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_op <= preprocessor_io_out_bits_op; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_fcsr_0 <= preprocessor_io_out_bits_fcsr_0; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_fcsr_1 <= preprocessor_io_out_bits_fcsr_1; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_fcsr_2 <= preprocessor_io_out_bits_fcsr_2; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_fcsr_3 <= preprocessor_io_out_bits_fcsr_3; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_fcsr_4 <= preprocessor_io_out_bits_fcsr_4; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_fcsr_5 <= preprocessor_io_out_bits_fcsr_5; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_fcsr_6 <= preprocessor_io_out_bits_fcsr_6; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_fcsr_7 <= preprocessor_io_out_bits_fcsr_7; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_fflags <= preprocessor_io_out_bits_fflags; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_control_adder <= preprocessor_io_out_bits_control_adder; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_control_multiplier <= preprocessor_io_out_bits_control_multiplier; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_control_fused <= preprocessor_io_out_bits_control_fused; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_control_divider <= preprocessor_io_out_bits_control_divider; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_control_comparator <= preprocessor_io_out_bits_control_comparator; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_control_squareRoot <= preprocessor_io_out_bits_control_squareRoot; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_control_bitInject <= preprocessor_io_out_bits_control_bitInject; // @[Reg.scala 20:22]
    end
    if (preprocessor_io_out_valid) begin // @[Reg.scala 20:18]
      preprocessorReg_control_classify <= preprocessor_io_out_bits_control_classify; // @[Reg.scala 20:22]
    end
    if (endStall & _T_2) begin // @[FPU.scala 165:59]
      preprocessorReg_stall_stall <= 1'h0; // @[FPU.scala 166:34]
    end else begin
      preprocessorReg_stall_stall <= _GEN_57;
    end
    if (endStall & _T_2) begin // @[FPU.scala 165:59]
      preprocessorReg_stall_op <= _GEN_39;
    end else if (endStall & ~executionUnit_io_out_bits_stall_inProc) begin // @[FPU.scala 168:68]
      preprocessorReg_stall_op <= preprocessorReg_op; // @[FPU.scala 171:34]
    end else begin
      preprocessorReg_stall_op <= _GEN_39;
    end
    preprocessorReg_stall_pass <= _T | _GEN_62; // @[FPU.scala 181:38 182:32]
    if (reset) begin // @[FPU.scala 125:41]
      preprocessorValidReg <= 1'h0; // @[FPU.scala 125:41]
    end else begin
      preprocessorValidReg <= preprocessor_io_out_valid; // @[FPU.scala 126:32]
    end
    if (_executorReg_T) begin // @[Reg.scala 20:18]
      executorReg_result_mantissa <= executionUnit_io_out_bits_result_mantissa; // @[Reg.scala 20:22]
    end
    if (_executorReg_T) begin // @[Reg.scala 20:18]
      executorReg_result_exponent <= executionUnit_io_out_bits_result_exponent; // @[Reg.scala 20:22]
    end
    if (_executorReg_T) begin // @[Reg.scala 20:18]
      executorReg_result_sign <= executionUnit_io_out_bits_result_sign; // @[Reg.scala 20:22]
    end
    if (_executorReg_T) begin // @[Reg.scala 20:18]
      executorReg_fcsr_0 <= executionUnit_io_out_bits_fcsr_0; // @[Reg.scala 20:22]
    end
    if (_executorReg_T) begin // @[Reg.scala 20:18]
      executorReg_fcsr_1 <= executionUnit_io_out_bits_fcsr_1; // @[Reg.scala 20:22]
    end
    if (_executorReg_T) begin // @[Reg.scala 20:18]
      executorReg_fcsr_2 <= executionUnit_io_out_bits_fcsr_2; // @[Reg.scala 20:22]
    end
    if (_executorReg_T) begin // @[Reg.scala 20:18]
      executorReg_fcsr_3 <= executionUnit_io_out_bits_fcsr_3; // @[Reg.scala 20:22]
    end
    if (_executorReg_T) begin // @[Reg.scala 20:18]
      executorReg_fcsr_4 <= executionUnit_io_out_bits_fcsr_4; // @[Reg.scala 20:22]
    end
    if (_executorReg_T) begin // @[Reg.scala 20:18]
      executorReg_fcsr_5 <= executionUnit_io_out_bits_fcsr_5; // @[Reg.scala 20:22]
    end
    if (_executorReg_T) begin // @[Reg.scala 20:18]
      executorReg_fcsr_6 <= executionUnit_io_out_bits_fcsr_6; // @[Reg.scala 20:22]
    end
    if (_executorReg_T) begin // @[Reg.scala 20:18]
      executorReg_fcsr_7 <= executionUnit_io_out_bits_fcsr_7; // @[Reg.scala 20:22]
    end
    if (_executorReg_T) begin // @[Reg.scala 20:18]
      executorReg_op <= executionUnit_io_out_bits_op; // @[Reg.scala 20:22]
    end
    if (reset) begin // @[FPU.scala 157:34]
      executorValidReg <= 1'h0; // @[FPU.scala 157:34]
    end else begin
      executorValidReg <= executionUnit_io_out_valid; // @[FPU.scala 158:24]
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  preprocessorReg_a_mantissa = _RAND_0[25:0];
  _RAND_1 = {1{`RANDOM}};
  preprocessorReg_a_exponent = _RAND_1[7:0];
  _RAND_2 = {1{`RANDOM}};
  preprocessorReg_a_sign = _RAND_2[0:0];
  _RAND_3 = {1{`RANDOM}};
  preprocessorReg_b_mantissa = _RAND_3[25:0];
  _RAND_4 = {1{`RANDOM}};
  preprocessorReg_b_exponent = _RAND_4[7:0];
  _RAND_5 = {1{`RANDOM}};
  preprocessorReg_b_sign = _RAND_5[0:0];
  _RAND_6 = {1{`RANDOM}};
  preprocessorReg_c_mantissa = _RAND_6[25:0];
  _RAND_7 = {1{`RANDOM}};
  preprocessorReg_c_exponent = _RAND_7[7:0];
  _RAND_8 = {1{`RANDOM}};
  preprocessorReg_c_sign = _RAND_8[0:0];
  _RAND_9 = {1{`RANDOM}};
  preprocessorReg_op = _RAND_9[4:0];
  _RAND_10 = {1{`RANDOM}};
  preprocessorReg_fcsr_0 = _RAND_10[0:0];
  _RAND_11 = {1{`RANDOM}};
  preprocessorReg_fcsr_1 = _RAND_11[0:0];
  _RAND_12 = {1{`RANDOM}};
  preprocessorReg_fcsr_2 = _RAND_12[0:0];
  _RAND_13 = {1{`RANDOM}};
  preprocessorReg_fcsr_3 = _RAND_13[0:0];
  _RAND_14 = {1{`RANDOM}};
  preprocessorReg_fcsr_4 = _RAND_14[0:0];
  _RAND_15 = {1{`RANDOM}};
  preprocessorReg_fcsr_5 = _RAND_15[0:0];
  _RAND_16 = {1{`RANDOM}};
  preprocessorReg_fcsr_6 = _RAND_16[0:0];
  _RAND_17 = {1{`RANDOM}};
  preprocessorReg_fcsr_7 = _RAND_17[0:0];
  _RAND_18 = {1{`RANDOM}};
  preprocessorReg_fflags = _RAND_18[11:0];
  _RAND_19 = {1{`RANDOM}};
  preprocessorReg_control_adder = _RAND_19[0:0];
  _RAND_20 = {1{`RANDOM}};
  preprocessorReg_control_multiplier = _RAND_20[0:0];
  _RAND_21 = {1{`RANDOM}};
  preprocessorReg_control_fused = _RAND_21[0:0];
  _RAND_22 = {1{`RANDOM}};
  preprocessorReg_control_divider = _RAND_22[0:0];
  _RAND_23 = {1{`RANDOM}};
  preprocessorReg_control_comparator = _RAND_23[0:0];
  _RAND_24 = {1{`RANDOM}};
  preprocessorReg_control_squareRoot = _RAND_24[0:0];
  _RAND_25 = {1{`RANDOM}};
  preprocessorReg_control_bitInject = _RAND_25[0:0];
  _RAND_26 = {1{`RANDOM}};
  preprocessorReg_control_classify = _RAND_26[0:0];
  _RAND_27 = {1{`RANDOM}};
  preprocessorReg_stall_stall = _RAND_27[0:0];
  _RAND_28 = {1{`RANDOM}};
  preprocessorReg_stall_op = _RAND_28[4:0];
  _RAND_29 = {1{`RANDOM}};
  preprocessorReg_stall_pass = _RAND_29[0:0];
  _RAND_30 = {1{`RANDOM}};
  preprocessorValidReg = _RAND_30[0:0];
  _RAND_31 = {1{`RANDOM}};
  executorReg_result_mantissa = _RAND_31[25:0];
  _RAND_32 = {1{`RANDOM}};
  executorReg_result_exponent = _RAND_32[7:0];
  _RAND_33 = {1{`RANDOM}};
  executorReg_result_sign = _RAND_33[0:0];
  _RAND_34 = {1{`RANDOM}};
  executorReg_fcsr_0 = _RAND_34[0:0];
  _RAND_35 = {1{`RANDOM}};
  executorReg_fcsr_1 = _RAND_35[0:0];
  _RAND_36 = {1{`RANDOM}};
  executorReg_fcsr_2 = _RAND_36[0:0];
  _RAND_37 = {1{`RANDOM}};
  executorReg_fcsr_3 = _RAND_37[0:0];
  _RAND_38 = {1{`RANDOM}};
  executorReg_fcsr_4 = _RAND_38[0:0];
  _RAND_39 = {1{`RANDOM}};
  executorReg_fcsr_5 = _RAND_39[0:0];
  _RAND_40 = {1{`RANDOM}};
  executorReg_fcsr_6 = _RAND_40[0:0];
  _RAND_41 = {1{`RANDOM}};
  executorReg_fcsr_7 = _RAND_41[0:0];
  _RAND_42 = {1{`RANDOM}};
  executorReg_op = _RAND_42[4:0];
  _RAND_43 = {1{`RANDOM}};
  executorValidReg = _RAND_43[0:0];
`endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
