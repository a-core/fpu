# Show only last hierarchy name
configure wave -signalnamewidth 1

# Save all signals
log -r *

set clock_reset [list \
  clock \
  reset \
]

set input_signals [list \
  io_in_bits_rs1 \
  io_in_bits_rs2 \
  io_in_bits_rs3 \
]

set input_control_signals [list \
  io_in_valid \
  io_in_ready \
]

set output_signals [list \
  io_out_bits_rd \
]

set output_control_signals [list \
  io_out_ready \
  io_out_valid \
  io_out_bits_rd \
  io_out_bits_fcsr \
]

radix define FPUOp {
  5'd0 "FADD_S"
  5'd1 "FSUB_S"
  5'd2 "FMUL_S"
  5'd3 "FDIV_S"
  5'd4 "FSQRT_S"
  5'd5 "FMIN_S"
  5'd6 "FMAX_S"
  5'd7 "FMADD_S"
  5'd8 "FMSUB_S"
  5'd9 "FNMADD_S"
  5'd10 "FNMSUB_S"
  5'd11 "FCMP_FEQ_S"
  5'd12 "FCMP_FLT_S"
  5'd13 "FCMP_FLE_S"
  5'd14 "FCVT_W_S"
  5'd15 "FCVT_S_W"
  5'd16 "FCVT_WU_S"
  5'd17 "FCVT_S_WU"
  5'd18 "FSGNJ_S"
  5'd19 "FSGNJN_S"
  5'd20 "FSGNJX_S"
  5'd21 "FCLASS_S"
  5'd22 "NULL"
  
}

add wave -divider "Clock and reset"

foreach wave $clock_reset {
  add wave $wave
}

add wave -divider "Inputs"

foreach wave $input_signals {
  add wave -radix float32 $wave
}
  
add wave -radix FPUOp io_in_bits_op

foreach wave $input_control_signals {
  add wave -radix float32 $wave
}

add wave -divider "Outputs"

foreach wave $output_signals {
  add wave -radix float32 $wave
}

foreach wave $output_control_signals {
  add wave $wave
}

run 1.2ms -all
wave zoom full

